module pulsar.db.odbc.command;

version(odbc):

import std.conv;
import pulsar.atoms;
import pulsar.db.sql;
import pulsar.db.odbc.exports;
import pulsar.db.odbc.tools;
import pulsar.db.odbc.connection;
import pulsar.db.odbc.reader;

/// Класс формирования и выполнения команды через ODBC
class OdbcCommand : IDbCommand
{
private:
	package OdbcConnection _con = null;
	string _text;
	ushort _timeout = 30;
	bool wasModify = true;

	package SQLHSTMT _hStat = null;

public:
	/// Соединение с сервером баз данных, на котором исполняется команда
	override @property	OdbcConnection connection() { return _con; }
	/// Текст команды
	override @property
	{
		string text() const { return _text; }
		void text(string value) { _text = value; }
	}
	/// Тайм-аут выполнения команды, сек
	override @property
	{
		ushort timeout() const { return _timeout; }
		void timeout(ushort value) { _timeout = value; wasModify = true;  }
	}

public:
	this() { }
	this(OdbcConnection con, string text = "")
	{
		_con = con;
		_text = text;
	}
	~this()
	{
		close();
	}

public:
	/// Выполняет комманду и возвращает количество строк, затронутых коммнадой
	override int exec()
	{
		if(_text is null || _text.length == 0)
			throw new Exception("Не указан текст команды!");

		prepareStatement();

		//SQLWCHAR* str = toSQLWCharStr(_text);
		//SQLRETURN r = SQLExecDirectW(_hStat,str, SQL_NTS);

		SQLCHAR* str = toSQLCharStr(_text);
		SQLRETURN r = SQLExecDirect(_hStat, str, SQL_NTS);
		if (r != ReturnCode.SQL_SUCCESS)
			_con.fetchMessage(HandleType.SQL_HANDLE_STMT, _hStat, r);

		// Determining the Number of Affected Rows
		int rowCount = -1;
		do
		{
			SQLLEN rc = 0;
			r = SQLRowCount(_hStat, &rc);
			if (r != ReturnCode.SQL_SUCCESS)
				_con.fetchMessage(HandleType.SQL_HANDLE_STMT, _hStat, r);
			if(rc >= 0)
				rowCount = (rowCount < 0 ? 0 : rowCount) + rc;
		} while(SQLMoreResults(_hStat) == ReturnCode.SQL_SUCCESS);

		return rowCount;
	}
	/// Выполняет комманду и возвращает количество строк, затронутых коммнадой
	override int exec(string commandText)
	{
		CheckNullOrEmpty!commandText;
		text = commandText;
		return exec();
	}
	/// Выполняет команду и возвращает reader для чтения результирующего набора.
	override OdbcDataReader execReader()
	{
		if(_text is null || _text.length == 0)
			throw new Exception("Не указан текст команды!");

		prepareStatement();

		//SQLWCHAR* str = toSQLWCharStr(_text);
		//SQLRETURN r = SQLExecDirectW(_hStat, str, SQL_NTS);

		SQLCHAR* str = toSQLCharStr(_text);
		SQLRETURN r = SQLExecDirect(_hStat, str, SQL_NTS);
		if (r != ReturnCode.SQL_SUCCESS)
			_con.fetchMessage(HandleType.SQL_HANDLE_STMT, _hStat, r);

		return new OdbcDataReader(this);
	}
	/// Выполняет команду и возвращает reader для чтения результирующего набора.
	override OdbcDataReader execReader(string commandText)
	{
		CheckNullOrEmpty!commandText;
		_text = commandText;
		return execReader();
	}
	/// Закрывает команду, освобождая все ресурсы, связанные с запросом.
	override void close()
	{
		if(_hStat != null)
		{
			SQLCancel(_hStat);
			SQLFreeHandle(HandleType.SQL_HANDLE_STMT, _hStat);
			_hStat = null;
			wasModify = true;
		}
	}
	//-------------------------------------------------------------------------------------------
	private:
	void prepareStatement()
	{
		scope(failure) wasModify = true;
		if(wasModify == false && _hStat !is null)
			return;

		close();

		if(_con is null || _con.state != DbConnectionState.Opened)
			throw new Exception("Соединение не указано или не готово!");

		// Need to allocate a statement handle
		SQLRETURN r = SQLAllocHandle(HandleType.SQL_HANDLE_STMT, _con._hCon, &_hStat);
		if (r != ReturnCode.SQL_SUCCESS)
			_con.fetchMessage(HandleType.SQL_HANDLE_STMT, _hStat, r);

		wasModify = false;
	}
	package	bool testConnectivity()
	{
		prepareStatement();

		SQLCHAR* str = toSQLCharStr("SET ANSI_NULLS ON;");
		SQLRETURN r = SQLExecDirect(_hStat, str, SQL_NTS);
//		if (r != ReturnCode.SQL_SUCCESS)
//			_con.fetchMessage(HandleType.SQL_HANDLE_STMT, _hStat, r);
		return r >= 0;
	}
}

