module pulsar.db.odbc.tools;

version(odbc):

import pulsar.db.odbc.exports;
import std.conv;
import std.string;

package string fromSQLCharStr(SQLCHAR* txt, size_t len)
{
	if(len==0)
		return "";
	char* cstr = cast(char*)txt;
	return to!string(cstr[0..len]);
}
package string fromSQLCharStrVoid(const(void*) txt, const(size_t) len)
{
	if(len==0)
		return "";
	char* cstr = cast(char*)txt;
	return to!string(cstr[0..len]);
}
package string fromSQLCharStrW(void* txt, size_t len)
{
	if(len==0)
		return "";
	wchar* cstr = cast(wchar*)txt;
	return to!string(cstr[0..len]);
}
package SQLCHAR* toSQLCharStr(string txt)
{
	if(txt is null)
		return null;
	immutable(char)* cstr =  txt.toStringz();
	return cast(SQLCHAR*)cstr;
}
public SQLCHAR* toSQLCharStrW(wstring txt)
{
	if(txt is null)
		return null;
	string stxt = text(txt);
	immutable(char)* cstr =  stxt.toStringz();
	return cast(SQLCHAR*)cstr;
}
public SQLWCHAR* toSQLWCharStr(string txt)
{
	import std.utf;

	if(txt is null)
		return null;

	auto arr = toUTF16z(txt);

	return cast(SQLWCHAR*)arr;
}
public wstring ToString(SQLCHAR* txt, int len) {
	if(len==0) return "";
	char* cstr = cast(char*)txt;
	char[] str = cstr[0..len];
	return to!wstring(str);
}
//
//immutable(wchar)* toWStringz(const(wchar)[] s) pure nothrow
//{
//	// Need to make a copy
//	auto copy = new wchar[s.length + 1];
//	copy[0..s.length] = s[];
//	copy[s.length] = 0;
//
//	return assumeUnique(copy).ptr;
//}
