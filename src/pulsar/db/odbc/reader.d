module pulsar.db.odbc.reader;

version(odbc):

import std.conv;
import std.datetime;
import std.uuid;
import core.stdc.stdlib;

import pulsar.atoms;
import pulsar.db.sql;
import pulsar.db.odbc.exports;
import pulsar.db.odbc.tools;

import pulsar.db.odbc.command;

class OdbcDataReader : IDbDataReader
{
private:
	OdbcCommand _cmd = null;
	OdbcRowField[] _fields = null;
	OdbcRowField[string] _fieldsIndex;
	//-------------------------------------------------------------------------------------------
	package this(OdbcCommand cmd)
	{
		_cmd = cmd;

		// first calls SQLNumResultCols to determine the number of columns in the result set
		SQLSMALLINT colCount = 0;
		SQLRETURN	r = SQLNumResultCols(_cmd._hStat, &colCount);
		if (r != ReturnCode.SQL_SUCCESS)
			_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);

		// Unbind exisits column bindings
		r = SQLFreeStmt(_cmd._hStat, SQLFreeStmtOptions.SQL_UNBIND);
		if (r != ReturnCode.SQL_SUCCESS)
			_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);

		_fields = new OdbcRowField[colCount];

		SQLCHAR* columnName = cast(SQLCHAR*)(malloc(255));
		scope(exit) free(columnName);
		for(SQLUSMALLINT i = 1; i <= colCount; i++)
		{
			SQLSMALLINT NameLengthPtr;
			SQLSMALLINT DataTypePtr;
			SQLULEN ColumnSizePtr;
			SQLSMALLINT DecimalDigitsPtr;
			SQLSMALLINT NullablePtr;

			r = SQLDescribeCol(_cmd._hStat, i, columnName, 255, &NameLengthPtr,
																							&DataTypePtr, &ColumnSizePtr, &DecimalDigitsPtr, &NullablePtr);
			if (r != ReturnCode.SQL_SUCCESS)
				_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);

			OdbcRowField f = new OdbcRowField();
			f._name = fromSQLCharStr(columnName, NameLengthPtr);
			f._type = to!OdbcSqlType(DataTypePtr);
			f._isSomeString = f._type == OdbcSqlType.SQL_VARCHAR ||
																					f._type == OdbcSqlType.SQL_WVARCHAR ||
																					f._type == OdbcSqlType.SQL_LONGVARCHAR ||
																					f._type == OdbcSqlType.SQL_WLONGVARCHAR;
			f._colSize = ColumnSizePtr;
			f._decDigits = DecimalDigitsPtr;

			SQLLEN val;
			r = SQLColAttribute(_cmd._hStat,i, ColumnAttribute.SQL_DESC_NULLABLE,null,0,null,&val);
			if (r != ReturnCode.SQL_SUCCESS)
				_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);
			f._isNullable = cast(Nullable)(val) == Nullable.SQL_NULLABLE;

			r = SQLColAttribute(_cmd._hStat,i, ColumnAttribute.SQL_DESC_AUTO_UNIQUE_VALUE ,null,0,null,&val);
			if (r != ReturnCode.SQL_SUCCESS)
				_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);
			f._isAutoIncr = cast(bool)(val);

			r = SQLColAttribute(_cmd._hStat,i, ColumnAttribute.SQL_DESC_OCTET_LENGTH,null,0,null,&val);
			if (r != ReturnCode.SQL_SUCCESS)
				_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);
			f.maxlen = val;

			if(f._type == OdbcSqlType.SQL_GUID)
				f.maxlen = ColumnSizePtr + 1;

			_fields[i-1] = f;
			_fieldsIndex[f._name] = f;
		}
	}
	~this()
	{
		close();
	}
	//-------------------------------------------------------------------------------------------
public:
	/// Массив описаний полей строки результирующего надора данных ODBC.
	override @property IDbRowField[] fields() const { return cast(IDbRowField[])(_fields); }

/// Переход к следующей строке набора.
	override bool next()
	{
		enforce(_fields && _fields.length, "OdbcDataReader закрыт!");

		clearFieldsData();
		SQLRETURN r = SQLFetch(_cmd._hStat);
		if (r != ReturnCode.SQL_SUCCESS)
		{
			_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, ReturnCode.SQL_SUCCESS);
			return false;
		}

		for(ushort a = 1; a <= _fields.length; a++)
		{
			OdbcRowField f = _fields[a-1];

			enum SQLLEN READLEN = 10000;
			SQLLEN bufSize = f.maxlen == 0 ? READLEN : f.maxlen;
			f.data = malloc(bufSize);
			SQLLEN StrLen_or_IndPtr = 0;

			r = SQLGetData(_cmd._hStat, a, 99, f.data, bufSize, &StrLen_or_IndPtr);
			if (r == ReturnCode.SQL_SUCCESS)
				f.datalen = StrLen_or_IndPtr < 0 ? 0 : StrLen_or_IndPtr;
			else if(r == ReturnCode.SQL_SUCCESS_WITH_INFO) // && StrLen_or_IndPtr == -4) // SQL_NO_TOTAL
			{
				if(StrLen_or_IndPtr > 0)
				{
					assert(0,"Надо проверить"); // Надо проверить
					f.datalen = StrLen_or_IndPtr;//Похоже на ошибку надо bufSize + StrLen_or_IndPtr ?
					f.data = realloc(f.data, f.datalen);
					void* ptr = f.data; // + bufSize;

					r = SQLGetData(_cmd._hStat, a, 99, ptr, StrLen_or_IndPtr, &StrLen_or_IndPtr);
					if(r != ReturnCode.SQL_SUCCESS && r != ReturnCode.SQL_NO_DATA)
						_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);
				}
				else if(StrLen_or_IndPtr == SQL_NO_TOTAL)
				{
					void* ptr;
					f.datalen = bufSize; // -1; ????
					do
					{
						// У строк последний байт 0. Но если предыдущий символ больше 1 байта,
						// то 0 может быть в позиции от $-3
						if(f._isSomeString && f.datalen > 4)
						{
							f.datalen -= 3;
							foreach(i; 0..3)
								if(*(cast(ubyte*)(f.data + f.datalen)) == 0)
									break;
								else
									f.datalen += 1;
						}

						f.data = realloc(f.data, f.datalen + READLEN);
						ptr = f.data + f.datalen;

						r = SQLGetData(_cmd._hStat, a, 99, ptr, READLEN, &StrLen_or_IndPtr);
						if(r == ReturnCode.SQL_NO_DATA && StrLen_or_IndPtr == SQL_NO_TOTAL)
							throw new Exception("ODBC не вернул часть данных для поля " ~ f._name);
						f.datalen += StrLen_or_IndPtr < 0 ? READLEN-1 : StrLen_or_IndPtr;
					}
					while(r == ReturnCode.SQL_SUCCESS_WITH_INFO);

					if(r != ReturnCode.SQL_SUCCESS && r != ReturnCode.SQL_NO_DATA)
						_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);
				}
				else
					throw new Exception("TODO: Не хватило основного буфера!");
			}
			else
				_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, r);
		}
		return true;
	}
	/// Переход к следующему результирующему набору.
	override bool nextResultSet()
	{
		enforce(_fields && _fields.length, "OdbcDataReader закрыт!");

		SQLRETURN r = SQLMoreResults(_cmd._hStat);
		if (r != ReturnCode.SQL_SUCCESS)
		{
			_cmd._con.fetchMessage(HandleType.SQL_HANDLE_STMT, _cmd._hStat, ReturnCode.SQL_SUCCESS);
			return false;
		}
		return true;
	}

	/// Метод проверки значения поля на null.
	override bool isNull(uint fieldIndex) const
	{
		if(fieldIndex >= _fields.length)
			throw new OutOfRangeException("fieldIndex");
		return _fields[fieldIndex].datalen == 0;
	}
	/// Метод проверки значения поля на null.
	override bool isNull(string fieldName) const
	{
		const(OdbcRowField) f = _fieldsIndex.get(fieldName, null);
		if(f is null)
			throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");
		return f.datalen == 0;
	}

	/// Возвращает описание поля по его индексу.
	override IDbRowField opIndex(uint fieldIndex)
	{
		if(fieldIndex >= _fields.length)
			throw new OutOfRangeException("fieldIndex");
		return _fields[fieldIndex];
	}
	/// Возвращает описание поля по его имени.
	override IDbRowField opIndex(string fieldName)
	{
		OdbcRowField f = _fieldsIndex.get(fieldName, null);
		if(f is null)
			throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");
		return f;
	}

	/// Определяет, есть ли поле с указанным имененм.
	override bool hasField(string name) const { return (name in _fieldsIndex) !is null; }

	override void close()
	{
		clearFieldsData();
		_fieldsIndex = null;
		_fields.length = 0;
		_fields = null;
	}

private:
	void clearFieldsData()
	{
		foreach(f;_fields)
			if(f.data !is null)
			{
				free(f.data);
				f.data = null;
				f.datalen = 0;
				f.maxlen = 0;
			}
	}
}

//-------------------------------------------------------------------------------------------
/// Класс поля строки результирующего надора данных ODBC.
class OdbcRowField : IDbRowField
{
private:
	string _name;
	OdbcSqlType _type;
	uint _colSize;
	short _decDigits;
	bool _isSomeString;
	bool _isNullable;
	bool _isAutoIncr;

	void*  data = null;
	size_t datalen = 0;
	SQLLEN maxlen = 0;

public:
	override @property string name() const { return _name; }
	override @property int dbTypeCode() const { return cast(int)_type; }
	override @property string dbTypeName() const { return to!string(_type); }
	@property OdbcSqlType dbType() const { return _type; }
	override @property int columnSize() const { return _colSize; }
	override @property int decimalDigits() const { return _decDigits; }
	override @property bool isNullable() const { return _isNullable; }
	override @property bool isAutoincrement() const { return _isAutoIncr; }

	T value(T)() const
	{
		if(datalen == 0 || data is null)
			throw new Exception("Значение поля [" ~ _name ~ "] равно null!");

		static if(is(T == byte[]))
		{
			byte* p = cast(byte*)(data);
			return p[0..datalen];
		}
		else static if(is(T == ubyte[]))
		{
			ubyte* p = cast(ubyte*)(data);
			return p[0..datalen];
			//return to!byte[](p[0..len]);
		}
		else switch(_type)
		{
			case OdbcSqlType.SQL_BIGINT  : {
				static if(is(T == long) || __traits(compiles, to!T(long.init)))
						if(datalen >= 8)
						return to!(T)(*(cast(long*)(data)));
			}	break;
			case OdbcSqlType.SQL_INTEGER : {
				static if(is(T == int) || __traits(compiles, to!T(int.init)))
						if(datalen >= 4)
						return to!(T)(*(cast(int*)(data)));
			}	break;
			case OdbcSqlType.SQL_SMALLINT: {
				static if(is(T == short) || __traits(compiles, to!T(short.init)))
						if(datalen >= 2)
						return to!(T)(*(cast(short*)(data)));
			}	break;
			case OdbcSqlType.SQL_TINYINT : {
				static if(is(T == ubyte) || __traits(compiles, to!T(ubyte.init)))
						if(datalen >= 1)
						return to!(T)(*(cast(ubyte*)(data)));
			} break;
			case OdbcSqlType.SQL_BIT     : {
				static if(is(T == bool) || __traits(compiles, to!T(bool.init)))
						if(datalen >= 1)
						return to!(T)(*(cast(ubyte*)(data)) > 0);
			} break;

			case OdbcSqlType.SQL_DOUBLE  : goto case OdbcSqlType.SQL_FLOAT;
			case OdbcSqlType.SQL_REAL    : goto case OdbcSqlType.SQL_FLOAT;
			case OdbcSqlType.SQL_FLOAT   : {
				static if(is(T == float) || is(T == double) || (__traits(compiles, to!T(float.init)) && __traits(compiles, to!T(double.init))))
						if(datalen >= 4)
				{
					if(datalen == 4)
						return to!(T)(*(cast(float*)(data)));
					else if(datalen >= 8)
						return to!(T)(*(cast(double*)(data)));
				}
			}	break;

				// TODO : заменить real на decimal
			case OdbcSqlType.SQL_NUMERIC : goto case OdbcSqlType.SQL_DECIMAL;
			case OdbcSqlType.SQL_DECIMAL : {
				static if(is(T == real) || __traits(compiles, to!T(real.init)))
						if(datalen >= 1)
				{
					char* cstr = cast(char*)data;
					string s = to!string(cstr[0..datalen]);
					real r = parse!real(s);
					return to!T(r);
				}
			} break;

			case OdbcSqlType.SQL_TYPE_TIMESTAMP : {
				static if(is(T == DateTime) ||  __traits(compiles, to!T(DateTime.init)))
				{
					if(datalen >= 16)
					{
						DateTime dt;
						SQL_TIMESTAMP* ts = cast(SQL_TIMESTAMP*)data;
						dt.year = (*ts).year;
						dt.month= cast(Month)((*ts).month);
						dt.day = (*ts).day;
						dt.hour = (*ts).hour;
						dt.minute = (*ts).minute;
						dt.second = (*ts).second;
						return to!T(dt);
					}
				}
				else
				static if(is(T == Date) ||  __traits(compiles, to!T(Date.init)))
				{
					if(datalen >= 16)
					{
						Date dt;
						SQL_TIMESTAMP* ts = cast(SQL_TIMESTAMP*)data;
						dt.year = (*ts).year;
						dt.month= cast(Month)((*ts).month);
						dt.day = (*ts).day;
						return to!T(dt);
					}
				}
			} break;

			case OdbcSqlType.SQL_CHAR :
			{
				static if(is(T == string) || __traits(compiles, to!T(string.init)))
					if(datalen >= 1)
					{
						char* cstr = cast(char*)data;
						string s = to!string(cstr[0..datalen]);
						return to!T(s);
					}
			} break;
			case OdbcSqlType.SQL_VARCHAR      : goto case OdbcSqlType.SQL_CHAR;
			case OdbcSqlType.SQL_LONGVARCHAR  : goto case OdbcSqlType.SQL_CHAR;
			case OdbcSqlType.SQL_WCHAR        : goto case OdbcSqlType.SQL_CHAR;
			case OdbcSqlType.SQL_WVARCHAR     : goto case OdbcSqlType.SQL_CHAR;
			case OdbcSqlType.SQL_WLONGVARCHAR : goto case OdbcSqlType.SQL_CHAR;

			case OdbcSqlType.SQL_BINARY        : {
				static if(__traits(compiles, to!T((ubyte[]).init)))
						return to!T(cast(ubyte*)(data)[0..datalen]);
			} break;
			case OdbcSqlType.SQL_VARBINARY     : goto case OdbcSqlType.SQL_BINARY;
			case OdbcSqlType.SQL_LONGVARBINARY : goto case OdbcSqlType.SQL_BINARY;
			case OdbcSqlType.SQL_VARIANT       : goto case OdbcSqlType.SQL_BINARY;

			case OdbcSqlType.SQL_GUID :
				static if(is(T : UUID) || is(T : string) || __traits(compiles, to!T(UUID)))
				{
					if(datalen != 16)
						throw new Exception("UUID data error!");
					ubyte[] u = (cast(ubyte*)data)[0..16];
					ubyte[16] upd = [ u[3],u[2],u[1],u[0],u[5],u[4],u[7],u[6],
																							u[8],u[9],u[10],u[11],u[12],u[13],u[14],u[15]
																					];
					auto uuid = UUID(upd);
					//auto uuid = UUID(fromSQLCharStrVoid(data, datalen));
					return to!T(uuid);
				} break;

				// нужна проверка
				//	SQL_DATETIME      =    9,
				//	SQL_DATE          =    9,
				//	SQL_TIME          =   10,
				//	SQL_INTERVAL		    =			10,
				//	SQL_TIMESTAMP     =   11,
				//	SQL_TYPE_DATE     =   91,
				//	SQL_TYPE_TIME     =   92,
				//				SQL_GRAPHIC           =  -95,
				//				SQL_VARGRAPHIC        =  -96,
				//				SQL_LONGVARGRAPHIC    =  -97,
				//				SQL_BLOB              =  -98,
				//				SQL_CLOB              =  -99,
				//				SQL_DBCLOB            =  -350,
				//				SQL_DATALINK          =  -400,
				//				SQL_USER_DEFINED_TYPE =  -450
			default: throw new Exception("Извлечение значения поля типа " ~ to!string(_type) ~ " нереализовано!");
		}
		throw new Exception("Не удалось извлечь значение поля типа " ~ to!string(_type) ~ "!");
	}

}
//-------------------------------------------------------------------------------------------

/// Метод извлечения данных из поля по его индексу.
T getValue(T)(IDbDataReader r, uint fieldIndex)
{
	CheckNull!r;
	if(fieldIndex >= r.fields.length)
		throw new OutOfRangeException("fieldIndex");
	OdbcRowField f = cast(OdbcRowField)(r.fields[fieldIndex]);

	if(f is null)
		throw new Exception("Поле строки равно null или не является типом OdbcRowField!");

	return f.value!T();
}
/// Метод извлечения данных из поля по его индексу.
/// Если значение поля NULL, возвращает ifNullValue.
T getValue(T)(IDbDataReader r, uint fieldIndex, T ifNullValue)
{
	CheckNull!r;
	if(fieldIndex >= r.fields.length)
		throw new OutOfRangeException("fieldIndex");
	OdbcRowField f = cast(OdbcRowField)(r.fields[fieldIndex]);

	if(f is null)
		throw new Exception("Поле строки равно null или не является типом OdbcRowField!");

	return r.isNull(fieldIndex) ? ifNullValue : f.value!T();
}

/// Метод извлечения данных из поля по его имени.
T getValue(T)(IDbDataReader reader, string fieldName)
{
	CheckNull!reader;
	CheckNullOrEmpty!fieldName;

	OdbcDataReader r = cast(OdbcDataReader)reader;
	if(r is null)
		throw new Exception("reader не является  объектом типа OdbcDataReader!");

	OdbcRowField f = r._fieldsIndex.get(fieldName, null);
	if(f is null)
		throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");

	return f.value!T();
}
/// Метод извлечения данных из поля по его имени.
/// Если значение поля NULL, возвращает ifNullValue.
T getValue(T)(IDbDataReader reader, string fieldName, T ifNullValue)
{
	CheckNull!reader;
	CheckNullOrEmpty!fieldName;

	OdbcDataReader r = cast(OdbcDataReader)reader;
	if(r is null)
		throw new Exception("reader не является  объектом типа OdbcDataReader!");

	OdbcRowField f = r._fieldsIndex.get(fieldName, null);
	if(f is null)
		throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");

	return r.isNull(fieldName) ? ifNullValue : f.value!T();
}

//void trace(T...)(T args)
//	{
//		import std.stdio;
//		writeln!T(args);
//		stdout.flush();
//	}
