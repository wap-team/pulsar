module pulsar.db.odbc.exports;

version(odbc):

extern(C)
{

	/// Перечисление типов данных SQL ODBC
	enum OdbcSqlType : short
	{
		SQL_UNKNOWN_TYPE  =    0,
		SQL_CHAR          =    1,
		SQL_NUMERIC       =    2,
		SQL_DECIMAL       =    3,
		SQL_INTEGER       =    4,
		SQL_SMALLINT      =    5,
		SQL_FLOAT         =    6,
		SQL_REAL          =    7,
		SQL_DOUBLE        =    8,
		SQL_DATETIME      =    9,
		SQL_DATE          =    9,
		SQL_TIME          =   10,
		SQL_INTERVAL		    =			10,
		SQL_TIMESTAMP     =   11,
		SQL_VARCHAR       =   12,
		SQL_TYPE_DATE     =   91,
		SQL_TYPE_TIME     =   92,
		SQL_TYPE_TIMESTAMP  = 93,
		SQL_LONGVARCHAR   = (-1),
		SQL_BINARY        = (-2),
		SQL_VARBINARY     = (-3),
		SQL_LONGVARBINARY = (-4),
		SQL_BIGINT        = (-5),
		SQL_TINYINT       = (-6),
		SQL_BIT           = (-7),
		SQL_WCHAR		       =	(-8),
		SQL_WVARCHAR	     =	(-9),
		SQL_WLONGVARCHAR  =	(-10),
		SQL_GUID		        =	(-11),
		SQL_GRAPHIC           =  -95,
		SQL_VARGRAPHIC        =  -96,
		SQL_LONGVARGRAPHIC    =  -97,
		SQL_BLOB              =  -98,
		SQL_CLOB              =  -99,
		SQL_VARIANT           =  -150,
		SQL_DBCLOB            =  -350,
		SQL_DATALINK          =  -400,
		SQL_USER_DEFINED_TYPE =  -450
	}


	// odbc types
	alias void*        SQLHANDLE;
	alias SQLHANDLE    SQLHENV;
	alias SQLHANDLE    SQLHDBC;
	alias SQLHANDLE    SQLHSTMT;
	alias SQLHANDLE    SQLHDESC;

	alias void *       SQLPOINTER;
	alias void*        SQLHWND;
	alias short        SQLRETURN;
	alias int          SQLLEN;
	alias uint         SQLULEN;
	alias SQLLEN       DATASTATUS_TYPE;
	alias SQLUSMALLINT SQLSETPOSIROW;

	alias short        SQLSMALLINT;
	alias ushort       SQLUSMALLINT;
	alias float        SQLREAL;
	alias ubyte        SQLTIME;
	alias ubyte        SQLTIMESTAMP;
	alias ubyte        SQLVARCHAR;
	alias wchar        SQLWCHAR;
	alias char         SQLCHAR;
	alias int          SQLINTEGER;
	alias uint         SQLUINTEGER;

	const short SQL_MAX_MESSAGE_LENGTH = 512;
	const short SQL_SQLSTATE_SIZE = 5;
	const short SQL_NTS	= -3;
	const short SQL_NO_TOTAL	= -4;

	/// Return codes
	package enum ReturnCode : short
	{
		SQL_SUCCESS           = 0,
		SQL_SUCCESS_WITH_INFO = 1,
		SQL_NEED_DATA         = 99,
		SQL_NO_DATA           = 100,
		SQL_ERROR             = -1,
		SQL_INVALID_HANDLE    = -2
	}
	/// Types of handles
	package enum HandleType : short
	{
		SQL_HANDLE_ENV  = 1,
		SQL_HANDLE_DBC  = 2,
		SQL_HANDLE_STMT = 3,
		SQL_HANDLE_DESC = 4
	}

	package enum OdbcAttribute : short
	{
		SQL_ATTR_READONLY           =  0,
		SQL_ATTR_WRITE              =  1,
		SQL_ATTR_READWRITE_UNKNOWN  =  2,
		SQL_ATTR_ASYNC_ENABLE	     	=		4,
		SQL_ATTR_CONCURRENCY		      =		StatementAttribute.SQL_CONCURRENCY,
		SQL_ATTR_CURSOR_TYPE		      =		StatementAttribute.SQL_CURSOR_TYPE,
		SQL_ATTR_ENABLE_AUTO_IPD	   =		15,
		SQL_ATTR_FETCH_BOOKMARK_PTR	=		16,
		SQL_ATTR_KEYSET_SIZE		      =		StatementAttribute.SQL_KEYSET_SIZE,
		SQL_ATTR_MAX_LENGTH			      =		StatementAttribute.SQL_MAX_LENGTH,
		SQL_ATTR_MAX_ROWS			        =		StatementAttribute.SQL_MAX_ROWS,
		SQL_ATTR_NOSCAN				         =		StatementAttribute.SQL_NOSCAN,
		SQL_ATTR_PARAM_BIND_OFFSET_PTR	=	17,
		SQL_ATTR_PARAM_BIND_TYPE		  =	18,
		SQL_ATTR_PARAM_OPERATION_PTR	=	19,
		SQL_ATTR_PARAM_STATUS_PTR		 =	20,
		SQL_ATTR_PARAMS_PROCESSED_PTR	=	21,
		SQL_ATTR_PARAMSET_SIZE			   =	22,
		SQL_ATTR_QUERY_TIMEOUT			   =	StatementAttribute.SQL_QUERY_TIMEOUT,
		SQL_ATTR_RETRIEVE_DATA			   =	StatementAttribute.SQL_RETRIEVE_DATA,
		SQL_ATTR_ROW_BIND_OFFSET_PTR	=	23,
		SQL_ATTR_ROW_BIND_TYPE			   =	StatementAttribute.SQL_BIND_TYPE,
		SQL_ATTR_ROW_NUMBER				     =	StatementAttribute.SQL_ROW_NUMBER,	  	/*GetStmtAttr*/
		SQL_ATTR_ROW_OPERATION_PTR		=	24,
		SQL_ATTR_ROW_STATUS_PTR			  =	25,
		SQL_ATTR_ROWS_FETCHED_PTR		 =	26,
		SQL_ATTR_ROW_ARRAY_SIZE			  =	27,
		SQL_ATTR_SIMULATE_CURSOR		  =	StatementAttribute.SQL_SIMULATE_CURSOR,
		SQL_ATTR_USE_BOOKMARKS			   =	StatementAttribute.SQL_USE_BOOKMARKS,
		SQL_ATTR_ACCESS_MODE        = 101,
		SQL_ATTR_AUTOCOMMIT         = 102,
		SQL_ATTR_LOGIN_TIMEOUT      = 103,
		SQL_ATTR_TRACE              = 104,
		SQL_ATTR_TRACEFILE          = 105,
		SQL_ATTR_TXN_ISOLATION      = 108,
		SQL_ATTR_CURRENT_CATALOG    = 109,
		SQL_ATTR_ODBC_VERSION				   = 200
	}

	package enum StatementAttribute : short
	{
		SQL_QUERY_TIMEOUT 	 =	0,
		SQL_MAX_ROWS		      =	1,
		SQL_NOSCAN			       =	2,
		SQL_MAX_LENGTH	    	=	3,
		SQL_ASYNC_ENABLE   	=	4,	/* same as SQL_ATTR_ASYNC_ENABLE */
		SQL_BIND_TYPE		     =	5,
		SQL_CURSOR_TYPE		   =	6,
		SQL_CONCURRENCY		   =	7,
		SQL_KEYSET_SIZE		   =	8,
		SQL_ROWSET_SIZE		   =	9,
		SQL_SIMULATE_CURSOR	=	10,
		SQL_RETRIEVE_DATA	  =	11,
		SQL_USE_BOOKMARKS   =	12,
		SQL_GET_BOOKMARK	   =	13,     /*      GetStmtOption Only */
		SQL_ROW_NUMBER		    =	14,      /*      GetStmtOption Only */
		SQL_COPT_SS_BASE_EX = 1240,
		/* dbdead SQLGetConnectOption only. It will try to ping the server. Expensive connection check */
		SQL_COPT_SS_CONNECTION_DEAD = SQL_COPT_SS_BASE_EX+4
	}

	/// SQLColAttributes defines
	package enum ColumnAttribute : short
	{
		SQL_COLUMN_COUNT           =     0,
		SQL_COLUMN_NAME            =     1,
		SQL_COLUMN_TYPE            =     2,
		SQL_COLUMN_LENGTH          =     3,
		SQL_COLUMN_PRECISION       =     4,
		SQL_COLUMN_SCALE           =     5,
		SQL_COLUMN_DISPLAY_SIZE    =     6,
		SQL_COLUMN_NULLABLE        =     7,
		SQL_COLUMN_UNSIGNED        =     8,
		SQL_COLUMN_MONEY           =     9,
		SQL_COLUMN_UPDATABLE       =     10,
		SQL_COLUMN_AUTO_INCREMENT  =     11,
		SQL_COLUMN_CASE_SENSITIVE  =     12,
		SQL_COLUMN_SEARCHABLE      =     13,
		SQL_COLUMN_TYPE_NAME       =     14,
		SQL_COLUMN_TABLE_NAME      =     15,
		SQL_COLUMN_OWNER_NAME      =     16,
		SQL_COLUMN_QUALIFIER_NAME  =     17,
		SQL_COLUMN_LABEL           =     18,
		SQL_COLATT_OPT_MAX         =     SQL_COLUMN_LABEL,
		SQL_DESC_AUTO_UNIQUE_VALUE =     SQL_COLUMN_AUTO_INCREMENT,
		SQL_DESC_COUNT                =  1001,
		SQL_DESC_TYPE                 =  1002,
		SQL_DESC_LENGTH               =  1003,
		SQL_DESC_OCTET_LENGTH_PTR     =  1004,
		SQL_DESC_PRECISION            =  1005,
		SQL_DESC_SCALE                =  1006,
		SQL_DESC_DATETIME_INTERVAL_CODE = 1007,
		SQL_DESC_NULLABLE             =  1008,
		SQL_DESC_INDICATOR_PTR        =  1009,
		SQL_DESC_DATA_PTR             =  1010,
		SQL_DESC_NAME                 =  1011,
		SQL_DESC_UNNAMED              =  1012,
		SQL_DESC_OCTET_LENGTH         =  1013,
		SQL_DESC_ALLOC_TYPE           =  1099
	}
	/// SQLFreeStmt() options
	package enum SQLFreeStmtOptions : short
	{
		SQL_CLOSE         = 0,
		SQL_DROP          = 1,
		SQL_UNBIND        = 2,
		SQL_RESET_PARAMS  = 3
	}

	// values of NULLABLE field in descriptor
	package enum Nullable : short
	{
		SQL_NO_NULLS         = 0,
		SQL_NULLABLE         = 1,
		SQL_NULLABLE_UNKNOWN = 2
	}
	//-------------------------------------------------------------------------------------------
	struct SQL_DATE_STRUCT
	{
		SQLSMALLINT    year;
		SQLUSMALLINT   month;
		SQLUSMALLINT   day;
	};
	struct SQL_TIME_STRUCT
	{
		SQLUSMALLINT   hour;
		SQLUSMALLINT   minute;
		SQLUSMALLINT   second;
	};
	struct SQL_TIMESTAMP
	{
		SQLSMALLINT    year;
		SQLUSMALLINT   month;
		SQLUSMALLINT   day;
		SQLUSMALLINT   hour;
		SQLUSMALLINT   minute;
		SQLUSMALLINT   second;
		SQLUINTEGER    fraction;
	};
	//-------------------------------------------------------------------------------------------
	SQLRETURN SQLAllocHandle
	(
		SQLSMALLINT HandleType,
		SQLHANDLE InputHandle,
		SQLHANDLE *OutputHandle
	);
	SQLRETURN SQLSetEnvAttr
	(
		SQLHENV EnvironmentHandle,
		SQLINTEGER Attribute,
		SQLPOINTER Value,
		SQLINTEGER StringLength
	);
	SQLRETURN SQLGetDiagRec
	(
		SQLSMALLINT HandleType,
		SQLHANDLE Handle,
		SQLSMALLINT RecNumber,
		SQLCHAR *Sqlstate,
		SQLINTEGER *NativeError,
		SQLCHAR *MessageText,
		SQLSMALLINT BufferLength,
		SQLSMALLINT *TextLength
	);
	SQLRETURN SQLGetDiagRecW
	(
		SQLSMALLINT handle_type,
		SQLHANDLE   handle,
		SQLSMALLINT rec_number,
		SQLWCHAR     *sqlstate,
		SQLINTEGER  *native,
		SQLWCHAR     *message_text,
		SQLSMALLINT buffer_length,
		SQLSMALLINT *text_length_ptr
	);
	SQLRETURN SQLGetDiagField
	(
		SQLSMALLINT     HandleType,
		SQLHANDLE       Handle,
		SQLSMALLINT     RecNumber,
		SQLSMALLINT     DiagIdentifier,
		SQLPOINTER      DiagInfoPtr,
		SQLSMALLINT     BufferLength,
		SQLSMALLINT *   StringLengthPtr
	);
	SQLRETURN SQLSetConnectAttr
	(
		SQLHDBC ConnectionHandle,
		SQLINTEGER Attribute,
		SQLPOINTER Value,
		SQLINTEGER StringLength
	);
	SQLRETURN SQLGetConnectAttr
	(
		SQLHDBC        ConnectionHandle,
		SQLINTEGER     Attribute,
		SQLPOINTER     ValuePtr,
		SQLINTEGER     BufferLength,
		SQLINTEGER *   StringLengthPtr
	);
	SQLRETURN SQLConnect
	(
		SQLHDBC ConnectionHandle,
		SQLCHAR* ServerName,
		SQLSMALLINT ServerNameLength,
		SQLCHAR* UserName,
		SQLSMALLINT UserNameLength,
		SQLCHAR* Password,
		SQLSMALLINT PasswordLength
	);
	SQLRETURN SQLDriverConnect
	(
		SQLHDBC         ConnectionHandle,
		SQLHWND         WindowHandle,
		SQLCHAR *       InConnectionString,
		SQLSMALLINT     StringLength1,
		SQLCHAR *       OutConnectionString,
		SQLSMALLINT     BufferLength,
		SQLSMALLINT *   StringLength2Ptr,
		SQLUSMALLINT    DriverCompletion
	);
	SQLRETURN SQLExecDirect
	(
		SQLHSTMT StatementHandle,
		SQLCHAR *StatementText,
		SQLINTEGER TextLength
	);
	SQLRETURN SQLExecDirectW
	(
		SQLHSTMT StatementHandle,
		SQLWCHAR *StatementText,
		SQLINTEGER TextLength
	);

	SQLRETURN SQLRowCount(SQLHSTMT StatementHandle, SQLLEN *RowCount);
	SQLRETURN SQLMoreResults(SQLHSTMT StatementHandle);
	SQLRETURN SQLCancel(SQLHSTMT StatementHandle);
	SQLRETURN SQLNumResultCols(SQLHSTMT StatementHandle, SQLSMALLINT *ColumnCount);
	SQLRETURN SQLDescribeCol
	(
		SQLHSTMT       StatementHandle,
		SQLUSMALLINT   ColumnNumber,
		SQLCHAR *      ColumnName,
		SQLSMALLINT    BufferLength,
		SQLSMALLINT *  NameLengthPtr,
		SQLSMALLINT *  DataTypePtr,
		SQLULEN *      ColumnSizePtr,
		SQLSMALLINT *  DecimalDigitsPtr,
		SQLSMALLINT *  NullablePtr
	);
	SQLRETURN SQLColAttribute
	(
		SQLHSTMT StatementHandle,
		SQLUSMALLINT ColumnNumber,
		SQLUSMALLINT FieldIdentifier,
		SQLPOINTER CharacterAttribute,
		SQLSMALLINT BufferLength,
		SQLSMALLINT *StringLength,
		SQLLEN *NumericAttribute
	);
	SQLRETURN SQLFetch(SQLHSTMT StatementHandle);
	SQLRETURN SQLGetData(
		SQLHSTMT       StatementHandle,
		SQLUSMALLINT   Col_or_Param_Num,
		SQLSMALLINT    TargetType,
		SQLPOINTER     TargetValuePtr,
		SQLLEN         BufferLength,
		SQLLEN *       StrLen_or_IndPtr);
	SQLRETURN SQLFreeStmt(SQLHSTMT StatementHandle, SQLUSMALLINT Option);
	SQLRETURN SQLDisconnect(SQLHDBC ConnectionHandle);
	SQLRETURN SQLFreeHandle(SQLSMALLINT HandleType, SQLHANDLE Handle);
}

