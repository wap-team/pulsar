module pulsar.db.odbc.connection;

version(odbc):

import std.conv;
import std.typecons : Unique;

import pulsar.atoms;
import pulsar.atoms.events;
import pulsar.db.sql;
import pulsar.db.odbc.command;
import pulsar.db.odbc.reader;
import pulsar.db.odbc.exports;
import pulsar.db.odbc.tools;

/// Класс соединения с базой данных через ODBC
@noproxy class OdbcConnection : IDbConnection
{
private:
	string _conStr = null;
	ushort _timeout = 30;
	DbConnectionState _state = DbConnectionState.Closed;

	package static __gshared SQLHENV _hEenv = null;
	package SQLHDBC _hCon = null;

public:
	this()
	{
		SQLRETURN r;
		if(_hEenv == null)
		{
			// The first step in connecting to the data source is to load the Driver Manager and allocate the environment handle
			r = SQLAllocHandle(HandleType.SQL_HANDLE_ENV, null, &_hEenv);
			if (r != ReturnCode.SQL_SUCCESS && r != ReturnCode.SQL_SUCCESS_WITH_INFO)
			{
				_state = DbConnectionState.Broken;
				throw new SqlException("Failed to allocate environment handle!");
			}
			// The application then registers the version of ODBC to which it conforms by calling SQLSetEnvAttr with the SQL_ATTR_APP_ODBC_VER environment attribute
			r = SQLSetEnvAttr(_hEenv, OdbcAttribute.SQL_ATTR_ODBC_VERSION, cast(SQLPOINTER)3, int.sizeof);
			if (r != ReturnCode.SQL_SUCCESS)
				fetchMessage(HandleType.SQL_HANDLE_ENV, _hEenv, r);
		}
		//Next, the application allocates a connection handle with SQLAllocHandle
		r = SQLAllocHandle(HandleType.SQL_HANDLE_DBC, _hEenv, &_hCon);
		if (r != ReturnCode.SQL_SUCCESS)
			fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);
	}
	~this()
	{
		close();
		if(_hCon != null)
		{
			SQLRETURN	r = SQLFreeHandle(HandleType.SQL_HANDLE_DBC, _hCon);
			if (r != ReturnCode.SQL_SUCCESS)
				fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);
			_hCon = null;
			_state = DbConnectionState.Disposed;
		}
	}

public:
	/// Строка соединения, возврашаемая после установки соединения.
	override @property	string connectionString() const { return _conStr; }
	/// Состояние соединения
	override @property DbConnectionState state() const { return _state; }
	/// Тайм-аут соединения, сек.
	override @property
	{
		ushort timeout() const { return _timeout; }
		void timeout(ushort value) {	_timeout = value;}
	}

	/// Событие, вызываемое при получении сообщения от сервера
	Event!(string) OnServerMessage;

public:
	/// Открывает соединение через SQLConnect (не используя строку подключения)
	void open(string dsn, string user, string password, string dataBase = null)
	{
		if(_state == DbConnectionState.Opened)
			return;

		CheckNullOrEmpty!(dsn,user,password);

		scope(failure) _state = DbConnectionState.Broken;
		scope(success) _state = DbConnectionState.Opened;

		SQLRETURN	r = SQLSetConnectAttr(_hCon, OdbcAttribute.SQL_ATTR_LOGIN_TIMEOUT, cast(void*)(_timeout), _timeout.sizeof);
		if (r != ReturnCode.SQL_SUCCESS)
			fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);

		if(dataBase !is null)
		{
			r = SQLSetConnectAttr(_hCon, OdbcAttribute.SQL_ATTR_CURRENT_CATALOG, cast(void*)(toSQLCharStr(dataBase)), cast(int)dataBase.length);
			if (r != ReturnCode.SQL_SUCCESS)
				fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);
		}

		//connects to the data source with SQLConnect
		r = SQLConnect(_hCon, toSQLCharStr(dsn), cast(short)dsn.length,
																	toSQLCharStr(user), cast(short)user.length,
																	toSQLCharStr(password), cast(short)password.length);
		if (r != ReturnCode.SQL_SUCCESS)
			fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);

		// TODO : At this point, it is common to use SQLGetInfo to discover the capabilities of the driver.
	}

	/// Открывает соединение
	override void open(string connectionString)
	{
		//Logger.log(0,"open(string connectionString)");

		if(_state == DbConnectionState.Opened)
			return;
		CheckNullOrEmpty!connectionString;

		scope(failure) _state = DbConnectionState.Broken;
		scope(success) _state = DbConnectionState.Opened;

		SQLRETURN	r = SQLSetConnectAttr(_hCon, OdbcAttribute.SQL_ATTR_LOGIN_TIMEOUT, cast(void*)(_timeout), _timeout.sizeof);
		if (r != ReturnCode.SQL_SUCCESS)
			fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);

		SQLCHAR[] rcs = new SQLCHAR[connectionString.length + 1000];
		SQLSMALLINT rcsLen;

		SQLCHAR* cs = toSQLCharStr(connectionString);

		r = SQLDriverConnect(_hCon, cast(void*)0,
																							cs, SQL_NTS,
																							rcs.ptr, cast(short)rcs.length,
																							&rcsLen, 0);

		if (r != ReturnCode.SQL_SUCCESS)
			fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);

		_conStr = to!string(rcs[0..rcsLen]);

		// TODO : At this point, it is common to use SQLGetInfo to discover the capabilities of the driver.

	}
	/// Закрывает соединение
	override void close()
	{
		scope(exit) _state = DbConnectionState.Closed;
		if(_state == DbConnectionState.Opened && _hCon != null)
		{
			SQLRETURN	r = SQLDisconnect(_hCon);
			if (r != ReturnCode.SQL_SUCCESS)
				fetchMessage(HandleType.SQL_HANDLE_DBC, _hCon, r);
		}

		//			r = SQLFreeHandle(SQL_HANDLE_ENV, mEnvironmentHandle);
		//			if (r != SQL_SUCCESS)
		//				fetchMessage(SQL_HANDLE_ENV, mEnvironmentHandle, r);
	}

	/// Проверяет, живо ли еще соединение
	override bool isAlive()
	{
		if(_state != DbConnectionState.Opened)
			return false;

		OdbcCommand cmd = new OdbcCommand(this);
		scope(exit) cmd.close();
		return cmd.testConnectivity();
	}

	/// Создает объект комманды.
	override OdbcCommand getCommand(string commandText = "")
	{
		return new OdbcCommand(this, commandText);
	}

	/// Выполняет команду и возвращает количество строк, затронутых коммнадой
	override int exec(string commandText)
	{
		assert(commandText);
		OdbcCommand cmd = new OdbcCommand(this);
		scope(exit) cmd.close();
		int res = cmd.exec(commandText);
		return res;
	}
	/** Выполняет команду и возвращает reader для чтения результирующего набора.
					Усли указан benchmarkName, производится замер времени выполнения запроса
						и его логирование с этим именем.
	*/
		override ScopedDataReader execReader(string commandText, string benchmarkName = null)
	{
		CheckNullOrEmpty!commandText;

		StopWatch sw = StopWatch(AutoStart.no);
		if(benchmarkName.length > 0)
			sw.start();

		IDbCommand cmd = new OdbcCommand(this);
		IDbDataReader res = cmd.execReader(commandText);

		if(benchmarkName.length > 0)
		{
			sw.stop();
			Logger.log("[BENCHMARK][OdbcConnection][execReader] %s => %s msecs", benchmarkName, sw.peek.total!"msecs");
		}

		return ScopedDataReader(cmd,res);
	}

package:
	void fetchMessage(short handleType, SQLHANDLE h, const int result)
	{
		import std.utf: toUTF8;
		import std.encoding: isValid;

		string state;
		string description;
		SQLINTEGER nativeErr, firstNativeErr;
		short idx = 1;
		string err;

		int r;
		do
		{
			r	= OdbcSQLGetDiagRec(handleType, h, idx++, state, nativeErr, description,
														SQL_MAX_MESSAGE_LENGTH-1);
			if(r == ReturnCode.SQL_SUCCESS)
			{
				if(state == "01004")
					continue;
				if(result < 0)
				{
					if(isValid(description) == false)
					{
						if(isValid(cast(wstring)description))
							description = toUTF8(cast(wstring)description);
						else if(isValid(cast(dstring)description))
							description = toUTF8(cast(dstring)description);
					}

					err ~= (err == null ? "" : "\n") ~ description;
					firstNativeErr = nativeErr;
				}
				else
					OnServerMessage(description);
			}
			else if(r != ReturnCode.SQL_NO_DATA)
				throw new Exception("fetchMessage error!");
		} while(r == ReturnCode.SQL_SUCCESS);
		if(result < 0)
			throw new SqlException(err is null ? " Неизвестная ошибка" : err,
															firstNativeErr, state);

	}
	SQLRETURN OdbcSQLGetDiagRec(short handleType, SQLHANDLE handle, short recNumber,
															out string sqlstate, out SQLINTEGER nativeErr,
															out string messageText, short bufferLength)
	{
		import core.stdc.stdlib;

		SQLWCHAR* state;
		SQLWCHAR* msg;
		SQLRETURN ret;
		try
		{
			state = cast(SQLWCHAR*)malloc(SQLWCHAR.sizeof * (SQL_SQLSTATE_SIZE+1));
			msg = cast(SQLWCHAR*)malloc(SQLWCHAR.sizeof * bufferLength);
			SQLSMALLINT textLen;
			ret = SQLGetDiagRecW(handleType, handle, recNumber, state,
														&nativeErr, msg, bufferLength, &textLen);
			sqlstate = fromSQLCharStrW(state, 5);
			//messageText = fromSQLCharStr(msg, textLen);
			if(textLen == 0)
				messageText = "";
			else
//			{
//				// UTF8 fix
//				int a = 0;
//				for(;a < bufferLength; a++)
//					if(msg[a] == '\0')
//						break;
//				messageText = fromSQLCharStr(msg, a );
//			}
			messageText = fromSQLCharStrW(msg, textLen );
		}
		finally
		{
			if(msg !is null)
				free(msg);
			if(state !is null)
				free(state);
		}
		return ret;
	}
}

