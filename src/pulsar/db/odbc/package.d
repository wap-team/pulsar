module pulsar.db.odbc;

version(odbc):

import std.uuid;
import std.datetime;
import pulsar.db.sql;

public import pulsar.db.odbc.tools;
public import pulsar.db.odbc.connection;
public import pulsar.db.odbc.command;
public import pulsar.db.odbc.reader;

version(unittest)
{
	import pulsar.atoms;
	import std.functional;

	import pulsar.log;

	void main(string[] args)
	{
		Logger.log(0, "unittest: pulsar.sql.odbc");

		try
		{
			trace("******* Step 1: Connect to the Data Source ******");
			OdbcConnection con = OdbcConnection.factory(); //"finist2", "test", "testuser", "123");
			scope(exit) destroy(con);

			SqlLog log = new SqlLog();
			//con.OnServerMessage += &(log.log);
			//con.open("finist2","testuser","123");
			con.open("DRIVER=FreeTDS;SERVERNAME=finist2;DATABASE=test;UID=testuser;PWD=123;"); // /opt/microsoft/msodbcsql/lib64/libmsodbcsql-11.0.so.2270.0

			trace("******* Step 2: Build and Execute an SQL Statement ******");
			OdbcCommand cmd = new OdbcCommand(con);
			scope(exit) destroy(cmd);
			trace("PRINT 'Привет'"," => ",cmd.exec("PRINT 'Привет'"));
			trace("DELETE FROM XXX;"," => ",cmd.exec("DELETE FROM XXX;"));
			cmd.text = "insert into XXX(Name,Amount,Del) values ('111', 1.111, 0);" ~
				"insert into XXX(Name,Amount,Del) values ('222', 22.22, 0);" ~
					"insert into XXX(Name,Amount,Del) values (N'йцук222', 333, 1);";
			trace("inserts ..."," => ",cmd.exec());

			trace("******* Step 3: Fetch the Result Set ******");
			ExecReader!int(cmd, "select cast(1 as int) as Value");
			ExecReader!int(cmd, "select cast(125568 as int) as Value");
			ExecReader!long(cmd, "select cast(125568001 as bigint) as Value");
			ExecReader!short(cmd, "select cast(30205 as smallint) as Value");
			ExecReader!byte(cmd, "select cast(120 as tinyint) as Value");

			ExecReader!float(cmd, "select cast(120.012012 as float) as Value");
			ExecReader!double(cmd, "select cast(120.012012 as decimal(18,9)) as Value");

			ExecReader!DateTime(cmd, "select cast('01/01/98 23:59:59.993' as datetime) as Value");
			ExecReader!DateTime(cmd, "select cast('2000-05-08 12:35:29' as datetime) as Value");
			ExecReader!DateTime(cmd, "select cast('2000-05-08 12:35:29' as smalldatetime) as Value");

			ExecReader!string(cmd, "select N'xПривет!y' as a1, 10 as A2");
			ExecReader!string(cmd, "select cast('xПривет!y' as char(15)) as Value");
			ExecReader!string(cmd, "select cast('xПривет!y' as varchar(13)) as Value");
			ExecReader!string(cmd, "select cast('xПривет!y' as varchar(max)) as Value");

			ExecReader!string(cmd, "select cast('xПривет!y' as nchar(15)) as Value");
			ExecReader!dstring(cmd, "select cast(N'xПривет!y' as nvarchar(15)) as Value");
			ExecReader!wstring(cmd, "select cast('xПривет!y' as nvarchar(max)) as Value");

			ExecReader!(byte[])(cmd, "select cast('xПривет!y' as binary(10)) as Value");
			ExecReader!(ubyte[])(cmd, "select cast('xПривет!y' as varbinary) as Value");

			ExecReader!UUID(cmd, "select cast('f81a944e-1a82-4f55-94e3-ba04db9987a5' as uniqueidentifier) as Value");

			ExecReader(cmd, "Select * from XXX;");


		}
		catch(SqlException exc)
		{
			trace(exc);
		}
		catch(Throwable tr)
		{
			trace(tr);
		}

	}
	void ExecReader(T = string)(OdbcCommand cmd, string text)
	{
		cmd.text = text;
		trace(text);
		trace("----------------------------------------------------------------------");
		OdbcDataReader r = cmd.execReader();
		scope(exit) destroy(r);

		trace("Name  \tDbType  \tColumnSize\tDecimalDigits\tIsNullable\tIsAutoincrement");
		foreach(c;r.fields)
			trace(c.name, "\t", c.dbType, "\t\t", c.columnSize, "\t\t", c.decimalDigits, "\t\t", c.isNullable, "\t\t", c.isAutoincrement);
		do
		{
			while(r.next())
			{
				string res;
				traceChars(".");
				for(int f = 0; f < r.fields.length; f++)
				{
					traceChars("[");
					auto id = r.getValue!(T)(f);
					traceChars(id, "]");
				}
				trace();
			}
		}while(r.nextResultSet());
		trace("----------------------------------------------------------------------");
	}
	class SqlLog
	{
		void log(OdbcConnection con, string message)
		{
			Logger.log(1,message);
		}
	}
}
