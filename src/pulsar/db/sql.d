module pulsar.db.sql;

import std.variant;
public import std.typecons : Unique;

import pulsar.atoms;

/// Класс исключения при работе с SQL
class SqlException : Exception
{
private:
	int _number;
	string _state;

public:
	/// Номер ошибки
	@property uint number() { return _number; }
	///
	@property string state() { return _state; }

	@safe pure nothrow this(string msg, string file = __FILE__,
													size_t line = __LINE__, Throwable next = null)
	{
		super(msg, file, line, next);
	}
	@safe pure nothrow this(string msg, int number, string state, Throwable next = null,
													string file = __FILE__, size_t line = __LINE__)
	{
		super(msg, file, line, next);
		_number = number;
		_state = state;
	}

	override string toString()
	{
		return "State: " ~ _state ~ "; Number: " ~ to!string(_number) ~ "; Msg: " ~ msg;
	}
}

/// Перечисление состояний соединения с базой данных
enum DbConnectionState : ubyte
{
	/// Закрыто
	Closed = 0,
	/// Открыто
	Opened = 1,
	/// Неработоспособно
	Broken = 100,
	/// Уничтожено
	Disposed = 200
}

/// Базовый интерфейс соединения с базой данных.
abstract class IDbConnection
{
	/// Строка соединения, возврашаемая после установки соединения.
	@property string connectionString() const;
	/// Тайм-аут соединения, сек
	@property
	{
		ushort timeout() const;
		void timeout(ushort value);
	}
	/// Состояние соединения
	@property	DbConnectionState state() const;
	/// Открывает соединение
	void open(string connectionString);
	/// Закрывает соединение
	void close();

	/// Проверяет, живо ли еще соединение
	bool isAlive();

	/// Создает объект комманды.
	IDbCommand getCommand(string commandText = "");

	/// Выполняет команду и возвращает количество строк, затронутых коммнадой
	int exec(string commandText);
	/** Выполняет команду и возвращает reader для чтения результирующего набора.
					Усли указан benchmarkName, производится замер времени выполнения запроса
						и его логирование с этим именем.
	*/
	ScopedDataReader execReader(string commandText, string benchmarkName = null);
}

/// Базовый интерфейс команды, выполняемой сервером баз данных.
abstract class IDbCommand
{
	/// Соединение с сервером баз данных, на котором исполняется команда
	@property IDbConnection connection();
	/// Текст команды
	@property // text
	{
		string text() const;
		void text(string value);
	}
	/// Тайм-аут выполнения команды, сек
	@property // timeout
	{
		ushort timeout() const;
		void timeout(ushort value);
	}

	/// Выполняет комманду и возвращает количество строк, затронутых коммнадой.
	int exec();
	/// Выполняет комманду и возвращает количество строк, затронутых коммнадой.
	int exec(string commandText);
	/// Выполняет команду и возвращает reader для чтения результирующего набора.
	IDbDataReader execReader();
	/// Выполняет команду и возвращает reader для чтения результирующего набора.
	IDbDataReader execReader(string commandText);

	void close();
}

/// Интерфейс объектов-читателей результирующих наборов данных.
abstract class IDbDataReader
{
	/// Массив описаний полей строки результирующего надора данных.
	@property IDbRowField[] fields();
	/// Определяет, есть ли поле с указанным именем.
	bool hasField(string name) const;

	/// Возвращает описание поля по его индексу.
	IDbRowField opIndex(uint fieldIndex);
	/// Возвращает описание поля по его имени.
	IDbRowField opIndex(string fieldName);

	/// Переход к следующей строке набора.
	bool next();
	/// Переход к следующему результирующему набору.
	bool nextResultSet();

	/// Метод проверки значения поля на null.
	bool isNull(uint fieldIndex) const;
	/// Метод проверки значения поля на null.
	bool isNull(string fieldName) const;

	/// Закрывает DataReader
	void close();
}

/// Интерфейс описания поля строки результирующего надора данных.
abstract class IDbRowField
{
public:
	/// Имя поля
	@property string name() const;
	/// Тип поля
	@property int dbTypeCode() const;
	@property string dbTypeName() const;
	@property int columnSize() const;
	@property int decimalDigits() const;
	@property bool isNullable() const;
	@property bool isAutoincrement() const;
	// Возвращает значение поля в виде Variant
	//@property Variant value();
}

/// Сруктура, закрывающиая IDbCommand и IDbDataReader при своем уничтожении
struct ScopedDataReader
{
private:
	IDbCommand _command;
	IDbDataReader _reader;

public:
	@property	IDbCommand command() { return _command; }
	@property IDbDataReader reader() { return _reader; }
	alias reader this;

public:
	this(IDbCommand command, IDbDataReader reader)
	{
		CheckNull!(command,reader);
		_command = command;
		_reader = reader;
	}
	~this()
	{
		_reader.close();
		_command.close();
	}
}
