module pulsar.db.ifs;

public import std.file : SpanMode; //DirEntry,
import std.file;
import std.path;
import std.uuid;
import std.datetime;

import pulsar.atoms;

/**	Термины:
			file - имя(если далее name) или путь файла
			dir  - имя(если далее name) или путь каталога
			path(путь) - имя файла или каталога c путем
			name(имя) - имя файла или каталога без пути

			без приставок -
			full - приставка полного пути
			rel  - приставка относительного пути
			ifs  - приставка относительного пути относительно корневого каталога IFS
			ix - приставка пути в индексе
*/

/// Статический класс для работы с расширенными атрибутами файлов.
static class FileXAttr
{
	import core.sys.linux.sys.xattr;
	import std.exception;
static:

	bool has(string fileName, string attr)
	{
		import core.sys.posix.sys.types;
		import core.stdc.errno;

		ubyte[1] value;
		CheckNullOrEmpty!(fileName,attr);
		ssize_t r = lgetxattr(toStringz(fileName),
																								toStringz("user." ~ attr),
																								value.ptr,
																								value.length);
		return r > -1 || errno == ERANGE;
	}
	void set(string fileName, string attr, void[] value, bool errorIfExists = false)
	{
		CheckNullOrEmpty!(fileName,attr);
		int r = lsetxattr(toStringz(fileName),
																				toStringz("user." ~ attr),
																				value.ptr,
																				value.length,
																				errorIfExists ? XATTR_CREATE : 0);
		errnoEnforce(r == 0);
	}
	void[] get(string fileName, string attr)
	{
		import core.sys.posix.sys.types;
		import core.stdc.errno;

		ubyte[256] value;
		CheckNullOrEmpty!(fileName,attr);
		ssize_t r = lgetxattr(toStringz(fileName),
																								toStringz("user." ~ attr),
																								value.ptr,
																								value.length);
		//trace(errno);
		if(r == -1 && ( errno == ENODATA || errno == ENOENT))
			return null;
		errnoEnforce(r != -1);
		return (value[0..r]).dup;
	}
	void remove(string fileName, string attr)
	{
		CheckNullOrEmpty!(fileName,attr);
		int r = lremovexattr(toStringz(fileName),
																							toStringz("user." ~ attr));
		errnoEnforce(r == 0);
	}
}

/// Перечисление опций инициализации IFS
enum IFSOptions : int
{
	None = 0,
	/// Создает корневой каталог, если он не существует.
	CreateRoot = 1,
	/// Создает IFS, если IFS нет в корневом каталоге.
	CreateIFS = 2,
	/// Если IFS есть в корневом каталоге, но нет метки построения (например, было
	/// копирование каталога с IFS на другой диск), переиндексирует содержимое.
	Reindex = 4,
	/// Форсированная переиндексация (всегда выполняется)
	ForceReindex = 8,

	AutoInit = CreateRoot | CreateIFS | Reindex
}

/// Структура информации о файле или каталоге IFS
struct IFSEntry
{
private:
	DirEntry _entry;
	UUID _fid;

public:
	@property DirEntry entry() const { return _entry; }
	alias entry this;
	@property UUID fid() const { return _fid; }

		/// Имя файла или каталога
	@property string name() const { return baseName(entry.name); }
	/// Полный путь файла или каталога
	@property string path() const { return entry.name; }
	/// Родительский каталог файла или каталога
	@property string dir() const { return dirName(entry.name); }

public:
	/// Выбрасывает исключение, если файл(каталог) не существует //или не индексирован
	private this(string path, UUID fid = UUID.init)
	{
		_entry = DirEntry(path);
		if(fid == UUID.init)
			IndexFileSystem.getfid(path, fid);
			//enforce(IndexFileSystem.getfid(path, fid), "Файл " ~ path ~ " не индексирован!");
		_fid = fid;
	}

	string toString() const { return name; }
}

/// Класс индексированной файловой системы (IFS)
class IndexFileSystem
{
private:
//	bool function(string) _needIndexPath;

public:
	/// Имя каталога индекса IFS
	enum indexDirName = ".ifs";
	/// Полный путь к корневому каталогу хранилища файлов
	const	string rootDir;
	/// Полный путь к каталогу индекса IFS
	const	string indexFullDir;

//	/// Функция для определения необходимости индексирования пути
//	@property // needIndexPath
//	{
//		bool function(string) needIndexPath() const
//		{
//			if(_needIndexPath)
//				return _needIndexPath;
//			return function(string) { return true; };
//		}
//		void needIndexPath(bool function(string) value) { _needIndexPath = value; }
//	}

public:
	this(string rootDir, IFSOptions opts = IFSOptions.None)
	{
		if(isAbsolute(rootDir) == false)
			rootDir = absolutePath(rootDir);
		if(exists(rootDir) == false)
			if(opts.isSet(IFSOptions.CreateRoot))
				mkdirRecurse(rootDir);
			else
				throw new Exception("Каталог [" ~ rootDir ~ "] не существует!");
		if(isDir(rootDir) == false)
			throw new Exception("Каталог [" ~ rootDir ~ "] не является каталогом!");

		this.rootDir = rootDir.dup;
		this.indexFullDir = buildPath(rootDir,indexDirName);

		if(exists(indexFullDir) == false)
			if(opts.isSet(IFSOptions.CreateIFS))
			{
				mkdir(indexFullDir);
				//FileXAttr.set(indexFullDir,"indexDate", cast(void[])Clock.currTime.toISOString());
				reindex();
			}
			else
				throw new Exception("Каталог [" ~ rootDir ~ "] не является IFS каталогом!");

		if(FileXAttr.has(indexFullDir, "indexDate") == false || opts.isSet(IFSOptions.ForceReindex))
			if(opts.isSet(IFSOptions.Reindex) || opts.isSet(IFSOptions.ForceReindex))
				reindex();
			else
				throw new Exception("IFS каталогу [" ~ rootDir ~ "] необходима переиндексация!");
	}

public:
	//--- Paths ---
	/// Возвращает путь, относительный корню IFS
	string ifsPath(string fullPath) const
	{
		LogicError.enforce(isValidPath(fullPath), "Недопустимое имя каталога!");
		return relativePath(fullPath, rootDir);
	}
	/// Определяет, принадлежит ли путь к IFS
	bool isFullPath(string fullPath) const
	{
		LogicError.enforce(isValidPath(fullPath), "Недопустимое имя каталога!");
		if(isRooted(fullPath) == false)
			return false;
		fullPath = buildNormalizedPath(fullPath);
		return fullPath.startsWith(rootDir);
	}
	/// Возвращает полный путь файла или каталога в IFS
	string fullPath(string ifsPath) const
	{
		CheckNullOrEmpty!ifsPath;
		LogicError.enforce(isValidPath(ifsPath), "Недопустимое имя каталога!");
		string path = buildNormalizedPath(rootDir, ifsPath);
		enforce(path.startsWith(rootDir), "Путь [" ~ ifsPath ~ "] не принадлежит IFS!");
		return path;
	}
	/// Возвращает полный путь каталога файла в индексе (без имени файла).
	/// Если checkExists == true, вернет null при отсутствии файла в IFS.
	string ixDir(UUID fid, bool checkExists = false) const
	{
		return ixDir(fid.toString(), checkExists);
	}
	string ixDir(string fid, bool checkExists = false) const
	{
		enum uuidLen = UUID.init.toString().length;
		assert(fid.length ==  uuidLen);
		string ixname = buildPath(indexFullDir,fid[0..2],fid[2..4]);
		if(checkExists &&	exists(ixname) == false)
			return null;
		return ixname;
	}
	/// Возвращает полный путь файла в индексе.
	string ixFile(UUID fid, bool checkExists = false) const
	{
		return ixFile(fid.toString(), checkExists);
	}
	string ixFile(string fid, bool checkExists = false) const
	{
		string ixname = buildPath(ixDir(fid),fid);
		if(checkExists &&	exists(ixname) == false)
			return null;
		return ixname;
	}

	bool hasIx(UUID fid) const { return exists(ixFile(fid)); }
	bool hasPath(string ifsPath) const { return exists(fullPath(ifsPath)); }
	/// Возвращает fid по полному или относительному IFS пути.
	UUID getFid(string path) const
	{
		if(isFullPath(path) == false)
			path = fullPath(path);
		UUID fid;
		if(getfid(path, fid) == false)
			return UUID.init;
		return fid;
	}
	/// Возвращает полный путь к файлу IFS
	string getPath(UUID fid) const
	{
		enforce(fid != UUID.init);
		string ixname = ixFile(fid);
		if(exists(ixname) == false)
			throw new Exception("Файл индекса " ~ fid.toString() ~ " не существует!");
		if(isSymlink(ixname) == false)
			return ixname;
		ixname = readLink(ixname);
		return buildNormalizedPath(ixDir(fid), ixname);
	}

	//--- Directories ---
	/// Добавляет каталог в IFS.
	/// ifsPath - путь каталога относительно корня IFS
	IFSEntry addDir(string ifsPath, bool throwIfExists = true) const
	{
		string path = fullPath(ifsPath);
		LogicError.enforce(path != indexFullDir, "Нельзя добавить скрытый каталог!");
		if(exists(path))
			LogicError.enforce(throwIfExists == false, "Каталог с указанным именем уже существует!");
		else
		{
			string[] ps = split(ifsPath, dirSeparator);
			path = rootDir;
			foreach(s; ps)
			{
				path = buildPath(path, s);
				if(exists(path) == false)
				{
					mkdir(path);
					indexAdd(path);
				}
			}
		}
		return IFSEntry(path);
	}
	/// Удаляет каталог в IFS. Каталог должен быть пустым
	/// ifsPath - путь каталога относительно корня IFS
	void delDir(string ifsPath, bool throwIfNotExists = true) const
	{
		string path = fullPath(ifsPath);
		LogicError.enforce(baseName(path) != indexDirName, "Нельзя удалить каталог индекса!");

		if(isDir(path) == false)
			throw new Exception("Путь [" ~ ifsPath ~ "] не является каталогом!");

		if(dirEntries(path, SpanMode.shallow, false).empty == false)
			throw new LogicError("Удаляемый каталог не пустой!");

		if(exists(path) == false)
			LogicError.enforce(throwIfNotExists == false, "Каталог не существует!");
		else
		{
			indexDel(path);
			rmdir(path);
		}
	}
	/** Переименовывает каталог
			* Params:
			*  ifsPath - путь каталога относительно корня IFS
			*  newName - новое имя (не путь!) каталога
			*/
	void renameDir(string ifsPath, string newName) const
	{
		CheckNullOrEmpty!newName;
		LogicError.enforce(isValidFilename(newName), "Недопустимое новое имя каталога!");

		string fpath = fullPath(ifsPath);
		LogicError.enforce(baseName(fpath) != indexDirName, "Нельзя переименовать каталог индекса!");

		if(exists(fpath) == false)
			throw new LogicError("Каталог " ~ ifsPath ~ " не существует!");
		newName = buildPath(std.path.dirName(fpath), newName);
		if(exists(newName))
			throw new LogicError("Каталог " ~ baseName(newName) ~ " уже существует!");

		rename(fpath, newName);
		indexAdd(newName, true);

		foreach(DirEntry f; dirEntries(newName, SpanMode.breadth, false))
			indexAdd(f.name, true);
	}

	// --- Files ---
	/** Создает пустой файл в IFS
			* Params:
			*  ifsDirName - путь каталога назначения в IFS.
			*  fileName - имя файла (без пути) в IFS.
	*/
	IFSEntry createFile(string ifsDirName, string fileName)
	{
		return createFile(ifsDirName, fileName, false, UUID.init);
	}
	IFSEntry createFile(string ifsDirName, string fileName, bool replace, UUID fid)
	{
		CheckNullOrEmpty!(ifsDirName, fileName);
		if(fid == UUID.init)
			fid = randomUUID();
		string fpath;
		fpath = fullPath(ifsDirName);
		mkdirRecurse(fpath);

		fpath = buildPath(fpath, fileName);

		if(exists(fpath))
			if(replace)
				remove(fpath);
			else
				throw new Exception("В IFS уже присутствует файл с именем " ~ fpath ~ "!");
		import std.stdio;
		File f = File(fpath, "w");
		f.close();

		indexAdd(fpath, replace, fid);

		return IFSEntry(fpath, fid);
	}
	/** Добавляет файл.
			* Params:
			*  srcFilePath - полный путь добавляемого файла.
			*  ifsDirName - путь каталога назначения в IFS. Если null, файл размещается в индексе!.
			*  fileName - имя файла (без пути) в IFS.
			*/
	IFSEntry addFile(string srcFilePath, string ifsDirName) const
	{
		return addFile(srcFilePath,ifsDirName, null, false, UUID.init);
	}
	IFSEntry addFile(string srcFilePath, string ifsDirName, UUID fid) const
	{
		return addFile(srcFilePath,ifsDirName, null, false, fid);
	}
	IFSEntry addFile(string srcFilePath, string ifsDirName, bool replace) const
	{
		return addFile(srcFilePath,ifsDirName,null,replace, UUID.init);
	}
	IFSEntry addFile(string srcFilePath, string ifsDirName, bool replace, UUID fid) const
	{
		return addFile(srcFilePath,ifsDirName,null,replace, fid);
	}
	IFSEntry addFile(string srcFilePath, string ifsDirName, string fileName, bool replace) const
	{
		return addFile(srcFilePath,ifsDirName,fileName,replace, UUID.init);
	}
	IFSEntry addFile(string srcFilePath, string ifsDirName, string fileName,	bool replace, UUID fid) const
	{
		CheckNullOrEmpty!srcFilePath;

		if(fid == UUID.init)
			fid = randomUUID();

		string fpath;
		fpath = ifsDirName.length ? fullPath(ifsDirName) : ixDir(fid);
		mkdirRecurse(fpath);

		if(ifsDirName.length)
			fpath = buildPath(fpath, fileName.length ? fileName : baseName(srcFilePath));
		else
			fpath = buildPath(fpath, fid.toString());

		if(exists(fpath))
			if(replace)
				remove(fpath);
			else
				throw new Exception("В IFS уже присутствует файл с именем " ~ fpath ~ "!");
		copy(srcFilePath, fpath);

		if(ifsDirName.length)
			indexAdd(fpath, replace, fid);

		return IFSEntry(fpath, fid);
	}
	/// Удаляет файл
	bool delFile(string ifsFilePath) const { return delFile(getFid(ifsFilePath)); }
	bool delFile(UUID fid) const
	{
		enforce(fid != UUID.init);
		string ixpath = ixDir(fid);
		string ixname = ixFile(fid);

		if(exists(ixname) == false)
			return false;
		string fname;
		if(isSymlink(ixname))
			fname = buildPath(ixpath,readLink(ixname));
		if(fname.length && exists(fname))
			remove(fname);
		remove(ixname);

		if(dirEntries(ixpath, SpanMode.shallow, false).empty)
		{
			rmdir(ixpath);
			ixpath = dirName(ixpath);
			if(dirEntries(ixpath, SpanMode.shallow, false).empty)
				rmdir(ixpath);
		}
		return true;
	}
	/// Переименовывает файл
	void renameFile(UUID fid, string newName) const
	{
		string ixname = ixFile(fid);
		if(exists(ixname) == false)
			throw new Exception("Файл " ~ fid.toString() ~ " не находится в IFS!");
		if(isSymlink(ixname) == false)
			throw new Exception("Нельзя переименовать файл, хранящийся в индексе!");
		renameFile(getPath(fid), newName);
	}
	/// Переименовывает файл
	/// ifsFilePath - полный или относительный путь файла в IFS
	void renameFile(string ifsFilePath, string newName) const
	{
		CheckNullOrEmpty!newName;
		LogicError.enforce(isValidFilename(newName), "Недопустимое новое имя файла!");
		string fname = ifsFilePath;
		if(isFullPath(fname) == false)
			fname = fullPath(fname);

		string fnameNew = buildPath(dirName(fname),newName);
		if(exists(fnameNew))
			throw new LogicError("Файл с именем " ~ newName ~ " уже существует!");
		rename(fname, fnameNew);
		indexAdd(fnameNew, true);
	}

	//--- Browsing ---
	IFSEntry getEntry(UUID fid) const { return IFSEntry(getPath(fid)); }
	IFSEntry getEntry(string ifsPath) const { return IFSEntry(fullPath(ifsPath)); }

	/// Возвращает итератор перечисления файлов и каталогов.
	/// Служебные каталоги IFS не включены.
	auto browse(string ifsDirPath, bool function(IFSEntry) filter = null,
													SpanMode mode = SpanMode.shallow) const
	{
		//CheckNullOrEmpty!ifsDirPath;
		string fpath;
		if(ifsDirPath.length)
			fpath = fullPath(ifsDirPath);
		else
			fpath = rootDir;

		bool noifs(IFSEntry d)
		{
			return d.name.startsWith(indexDirName) == false && (filter ? filter(d) : true);
										//baseName(d.name).startsWith(".") == false &&
		}

		struct Iter
		{
		private:
			TailConst!IndexFileSystem ifs;

		public:
			this(const(IndexFileSystem) ifs) { this.ifs = ifs; }
			int opApply(int delegate(IFSEntry) dg) const
			{
				import std.algorithm.iteration : filter;
				int result = 0;
				if(exists(fpath))
					foreach(DirEntry de; dirEntries(fpath, mode, false))
					{
						auto ide = IFSEntry(de);
						if(noifs(ide))
							result = dg(ide);
						if(result)
							break;
					}
				return result;
			}

		}

		return Iter(this);
	}
	/// Возвращает итератор перечисления путей файлов и каталогов.
	auto browsePaths(string ifsDirPath, bool function(string) filter = null,
													SpanMode mode = SpanMode.shallow) const
	{
		//CheckNullOrEmpty!ifsDirPath;
		string fpath;
		if(ifsDirPath.length)
			fpath = fullPath(ifsDirPath);
		else
			fpath = rootDir;

		bool noifs(string d)
		{
			return d.startsWith(indexDirName) == false && (filter ? filter(d) : true);
										//baseName(d.name).startsWith(".") == false &&
		}

		struct Iter
		{
		private:
			TailConst!IndexFileSystem ifs;

		public:
			this(const(IndexFileSystem) ifs) { this.ifs = ifs; }
			int opApply(int delegate(string) dg) const
			{
				import std.algorithm.iteration : filter;
				int result = 0;
				if(exists(fpath))
					foreach(string p; dirEntries(fpath, mode, false))
					{
						if(noifs(p))
							result = dg(p);
						if(result)
							break;
					}
				return result;
			}

		}

		return Iter(this);
	}

	//--- Indexing ---
	/** Индексирует файл или каталог.
			path - полный или относительный IFS путь к файлу/каталогу.
			fid - UUID файла. Если равен init, пытается загрузить fid из атрибута файла/каталога.
			replace - заменять файл индекса
	*/
	void indexAdd(string path, bool replace = false, UUID fid = UUID.init) const
	{
		if(isRooted(path))
			LogicError.enforce(isFullPath(path), "Путь [" ~ path ~ "] не принадлежит IFS!");
		else
			path = fullPath(path);
//		if(isFile(filePath) == false)
//			return;
		if(fid == UUID.init)
		{
			if(getfid(path, fid) == false)
				fid = randomUUID();
		}
		setfid(path, fid);

		string ixpath = ixDir(fid);
		mkdirRecurse(ixpath);
		string ixname = buildPath(ixpath, fid.toString());
		if(exists(ixname))
			if(replace)
				remove(ixname);
			else
				throw new Exception("Файл индекса " ~ fid.toString() ~ " уже существует!");
		ixpath = relativePath(path, ixpath);
		symlink(ixpath, ixname);
	}
	/** Удаляет индекс файла или каталога. Сам файл не удаляет!
			Params:
			path - полный или относительный IFS путь к файлу/каталогу.
			fid - UUID файла.
	*/
	void indexDel(string path) const
	{
		if(isRooted(path))
			LogicError.enforce(isFullPath(path), "Путь [" ~ path ~ "] не принадлежит IFS!");
		else
			path = fullPath(path);
		UUID fid;
		if(getfid(path, fid) == false)
			return;
		indexDel(fid);
	}
	void indexDel(UUID fid) const
	{
		enforce(fid != UUID.init);
		string ixpath = ixDir(fid);
		string ixname = ixFile(fid);

		if(exists(ixname) == false)
			return;
		remove(ixname);

		if(dirEntries(ixpath, SpanMode.shallow, false).empty)
		{
			rmdir(ixpath);
			ixpath = dirName(ixpath);
			if(dirEntries(ixpath, SpanMode.shallow, false).empty)
				rmdir(ixpath);
		}
		return;
	}
	/// Переиндексирует IFS. Проверяет индекс на существование файлов и
	/// файлы на присутствие в индексе.
	void reindex() const
	{
		Logger.log(1,"Старт переиндексации IFS %s ...", rootDir);
		Logger.logChars(2,"Проверка индекса ... ");
		if(exists(indexFullDir))
		{
			import std.container.array;
			scope DirEntry[] toDel;
			foreach(DirEntry de; dirEntries(indexFullDir, SpanMode.breadth, false))
			{
				if(/*de.isDir || */de.isSymlink == false)
					continue;
				string ixname = de.name;
				string ixpath = dirName(ixname);
				UUID fid = UUID(baseName(ixname));
				string fname = readLink(ixname);
				fname = buildPath(ixpath,fname);
				if(exists(fname))
					setfid(fname, fid);
				else
					toDel ~= de;
			}
			foreach(x; toDel)
			{
				remove(x);
				string ixpath = dirName(x);
				if(dirEntries(ixpath, SpanMode.shallow, false).empty)
				{
					rmdir(ixpath);
					ixpath = dirName(ixpath);
					if(dirEntries(ixpath, SpanMode.shallow, false).empty)
						rmdir(ixpath);
				}
			}
			Logger.log(2,"OK");
		}
		else
			Logger.log(2,"нет индекса");

		Logger.logChars(2,"Проверка файлов ... ");
		foreach(DirEntry f; dirEntries(rootDir, SpanMode.breadth, false))
		{
			if(f.name.startsWith(indexFullDir))
				continue;
			UUID fid;
			if(getfid(f.name, fid) == false)
			{
				fid = randomUUID();
				setfid(f.name,fid);
			}
			string ixname = ixFile(fid);
			if(exists(ixname) == false)
				indexAdd(f.name, false, fid);
		}
		Logger.log(2,"OK");
		FileXAttr.set(indexFullDir,"indexDate", cast(void[])Clock.currTime.toISOString());
		Logger.log(1,"Переиндексация IFS %s завершена.", rootDir);
	}

private:
	static void setfid(string fileName, UUID value)
	{
		FileXAttr.set(fileName,"fid",cast(void[])value.toString());
	}
	static bool getfid(string fileName, ref UUID fid)
	{
		string s = cast(string)FileXAttr.get(fileName,"fid");
		if(s.length)
			fid = UUID(s);
		return s.length > 0;
	}
}

unittest
{
	void ifsTest()
	{
		if(exists("/tmp/i"))
			rmdirRecurse("/tmp/i");

		IndexFileSystem ifs = new IndexFileSystem("/tmp/i/", IFSOptions.AutoInit);

		assert(ifs.hasDir("111") == false);
		assertThrown(ifs.hasDir("../../../111"));
		ifs.addDir("1/2/3");
		assert(ifs.hasDir("1/2/3"));
		ifs.addDir("111");
		assert(ifs.hasDir("111"));
		assertThrown(ifs.addDir("../../../111"));
		ifs.addDir("111/111_1");
		assert(ifs.hasDir("111/111_1"));
		assertThrown(ifs.addDir("111/111_1"));
		assertThrown(ifs.addDir("111/111_1"));
		assertThrown(ifs.hasDir("/111/111_1"));
		assertThrown(ifs.delDir("111"));
		assertNotThrown(ifs.delDir("111/111_1"));
		assertNotThrown(ifs.addDir("111/111_1111"));
		assertNotThrown(ifs.addDir("111/111_2"));
		assertNotThrown(ifs.addDir("111/111_3"));

		write("/tmp/test.txt", "Test test");
		auto ie = ifs.addFile("/tmp/test.txt", "111/111_1111");
		assert(ifs.hasPath("111/111_1111/test.txt"));
		assert(ifs.getFid("111/111_1111/test.txt") == ie.fid);
		assert(ifs.getPath(ie.fid) == "/tmp/i/111/111_1111/test.txt");
		auto ie1 = ifs.addFile("/tmp/test.txt", null);
		assert(ifs.hasPath(ie1) == true);
		//trace(ie1);
		auto fid2 = ie1.fid;
		fid2.data[15] = cast(ubyte)(fid2.data[15]+1);
		//trace(fid2);
		ifs.addFile("/tmp/test.txt", null, false, fid2);
		assert(ifs.hasIx(fid2));
		auto fidd = ifs.getFid("111/111_1111");
		assertNotThrown(ifs.renameDir("111/111_1111", "111_1"));
		assert(ifs.hasPath("111/111_1"));
		assert(ifs.getFid("111/111_1") == fidd);
		ifs.delFile(ie1.fid);
		assert(ifs.hasIx(ie1.fid) == false);
		ifs.delFile(fid2);
		assertNotThrown(ifs.renameFile(ie.fid, "xxx.txt"));
		assert(ifs.hasPath("111/111_1/xxx.txt") == true);

		foreach(d; ifs.browse(null))
			trace(d.path);
	}
}
