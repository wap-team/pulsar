module pulsar.db.postgres;

version(postgres):

public import pulsar.db.sql;
public import pulsar.db.postgres.exports : ExecStatusType, PgSqlType;
public import pulsar.db.postgres.connection;
public import pulsar.db.postgres.command;
public import pulsar.db.postgres.reader;

/*void postgres_main()
{
	try
	{
		trace("*** Postgres SQL tests ***");


		Logger.log("******* Step 1: Connect to the Data Source ******");
		PqConnection con = new PqConnection();
		con.open("localhost", "test", "gold", "zzz");
		scope(exit)
				con.close();
		Logger.log("Соединение установлено: " ~ con.connectionString);
		Logger.log("isAlive: %s", con.isAlive);

		enum pars = ["server_version", "server_encoding", "client_encoding", "application_name",
				"is_superuser", "session_authorization", "DateStyle", "IntervalStyle", "TimeZone",
				"integer_datetimes", "standard_conforming_strings"];
		Logger.log("Параметры сервера:");
		foreach(p; pars)
			trace("\t", p, ": ", con.getServerParam(p));

		trace("******* Step 2: Build and Execute an SQL Statement ******");
		PqCommand cmd = new PqCommand(con);
		scope(exit)
			cmd.close;
		//trace("PRINT 'Привет'"," => ",cmd.exec("PRINT 'Привет'"));
		trace("DELETE FROM XXX;"," => ",cmd.exec("DELETE FROM XXX;"));
		cmd.text = "insert into xxx(id,name,amount,del) values (1,'111', 1.111, true);" ~
			"insert into XXX(id,name,amount,del) values (2,'222', 22.22, false);" ~
			"insert into XXX(id,name,amount,del) values (3,'йцук222', 333333333333.333333333, true);";
		trace("inserts ..."," => ",cmd.exec());

		trace("******* Step 3: Fetch the Result Set ******");
		ExecReader!short(cmd, "select cast(30205 as smallint) as Value;"); // INT2
		ExecReader!int(cmd, "select cast(125568 as int) as Value;"); // INT4
		ExecReader!long(cmd, "select cast(125568001 as bigint) as Value;"); // INT8
		// smallserial, serial и bigserial идут как smallint, int, bigint

		ExecReader!float(cmd, "select cast(120.012912 as float) as Value;"); // FLOAT8
		ExecReader!double(cmd, "select cast(120.012312 as decimal(18,4)) as Value;"); // NUMERIC
		ExecReader!string(cmd, "select cast(1120.017 as money) as Value;"); // CASH

		ExecReader!string(cmd, "select 'xПривет!y' as a1, N'xПривет!y' as a2, cast('xПривет!y' as text) as Text");
		ExecReader!dstring(cmd, "select 'xПривет!y'::char(15), 'xПривет!y'::char(5) ");
		ExecReader!wstring(cmd, "select cast('xПривет!y' as varchar(5)) as Value, 'xПривет!y'::varchar(15)");

		ExecReader!(byte[])(cmd, "select 'abc'::bytea");

		ExecReader!SysTime(cmd, "select cast('2017-05-29 17:30:33.940101' as timestamp) as Value;");
		ExecReader!DateTime(cmd, "select cast('2017-05-29 17:30:33.940101' as timestamp) as Value1, " ~
																											"cast('01/02/98 23:59:59' as timestamp) as Value2;");
		ExecReader!Date(cmd, "select cast('20010508 12:35:29' as date) as Value;");
		ExecReader!TimeOfDay(cmd, "select cast('12:35:29' as time) as Value;");

		ExecReader!bool(cmd, "select 1::bool");

		enum Sex { male, female, american };
		ExecReader!Sex(cmd, //"DROP TYPE sex;" ~
																	"DO $$ begin " ~
																	" IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'sex') THEN" ~
																	"	 CREATE TYPE sex AS ENUM ('male', 'female', 'american');" ~
																	" END IF;	end$$;" ~
																	" select 'female'::sex;");

		ExecReader!UUID(cmd, "select cast('f81a944e-1a82-4f55-94e3-ba04db9987a5' as UUID) as Value");

		ExecReader!string(cmd, "SELECT '{\"bar\": \"baz\", \"balance\": 7.77, \"active\": false}'::json;");

		ExecReader(cmd, "Select * from XXX;");

	}
	catch(SqlException exc)
	{
		trace(exc);
	}
	catch(Throwable tr)
	{
		trace(tr);
	}
}
*/

/*void postgres_main()
{
	try
	{
		trace("*** Postgres SQL async tests ***");

		void onIdle()
		{
			traceChars(".");
			import core.thread;
			Thread.sleep(30.msecs);
		}

		import pulsar.db.postgres.exports;

		traceChars("open connection ... ");
		string connectionString = "host=localhost dbname=test user=gold password=zzz";
		PGconn* _con = PQconnectdb(toStringz(connectionString));
		if(_con is null)
			throw new Exception("Unable to allocate PG connection object");
		scope(exit)
		{
			trace("close connection.");
			PQfinish(_con);
		}
		ConnStatus status = PQstatus(_con);
		if(status != ConnStatus.CONNECTION_OK)
		{
			PQfinish(_con);
			string errMsg = cast(string)fromStringz(PQerrorMessage(_con));
			throw new SqlException(errMsg, cast(int)status, "Соединение с сервером");
		}
		trace("ok");

		trace("send query ... ");
		string query =  "select * from xxx;"
																		~ "select *, pg_sleep(0.5) from pg_roles;"//
																		~ "select * from xxx fetch first 0 rows only;"
																		~ "select * from pg_database;"
																;

		StopWatch sw = StopWatch(AutoStart.yes);

		PGresult* res;
		//res = PQexec(_con, toStringz(query));
		//PQclear(res);
		int hr = PQsendQuery(_con, toStringz(query));
		if(hr == 0)
			throw new SqlException(cast(string)fromStringz(PQerrorMessage(_con)));

		enforce(PQsetSingleRowMode(_con));

		do
		{
			hr = PQconsumeInput(_con);
			enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(_con)));


			int c = 0;
			while (PQisBusy(_con))
			{
				onIdle();
//				if(++c == 3)
//				{
//					PGcancel*  pc = PQgetCancel(_con);
//					scope(exit)
//						PQfreeCancel(pc);
//					char[256] errMsg;
//					if(PQcancel(pc, errMsg.ptr, cast(int)errMsg.length) == 0)
//						trace(cast(string)fromStringz(errMsg.ptr));
//				}

				hr = PQconsumeInput(_con);
				enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(_con)));
			}

			while((res = PQgetResult(_con)) !is null)
			{
				switch (hr = to!ExecStatusType(PQresultStatus(res)))
				{
					case ExecStatusType.PGRES_COMMAND_OK: trace(" command ok (", sw.peek.msecs, ")"); break;
					case ExecStatusType.PGRES_TUPLES_OK:  // No more rows from current query.
					{
	//					// We want the next statement's results row-by-row also.
	//					if (!PQsetSingleRowMode(conn))
	//							{
	//									PQclear(res);
	//									setting_failed(conn);
	//							}
	//					first = 1;
						trace(" full result: ", PQntuples(res), " (", sw.peek.msecs, ")");

					} break;
					case ExecStatusType.PGRES_SINGLE_TUPLE:
					{
	//						if (first)
	//								{
	//										// Produce a "nice" header"
	//										printf("\n%s\nResults of statement number %d:\n\n",
	//																	"-----------------------------"
	//																	"-----------------------------",
	//																	stmtcnt++);

	//										// print out the attribute names
	//										nFields = PQnfields(res);
	//										for (i = 0; i < nFields; i++)
	//												printf("%-15s", PQfname(res, i));
	//										printf("\n\n");

	//										first = 0;
	//								}

	//						// print out the row
	//						for (j = 0; j < nFields; j++)
	//								printf("%-15s", PQgetvalue(res, 0, j));
	//						printf("\n");
							trace(" row result: ", PQntuples(res), " (", sw.peek.msecs, ")");

					} break;
					default: // Always call PQgetResult until it returns null, even on error.
					{
						trace("[PG][ERROR]  ", to!ExecStatusType(hr) ," : ", cast(string)fromStringz(PQerrorMessage(_con)));
					} break;
				}
				PQclear(res);
			}
		} while(res !is null);

		sw.stop();
		trace("query done (", sw.peek.msecs, ")");

//**********************************************************

		trace(" ***** PqConnection *****");
		PqConnection con = new PqConnection();
		con.open("localhost", "test", "gold", "zzz");
		scope(exit)
				con.close();
		con.onIdle = &onIdle;
		Logger.log("Соединение установлено: " ~ con.connectionString);
		PqCommand cmd = new PqCommand(con);
		scope(exit)
			cmd.close;

//		//trace("PRINT 'Привет'"," => ",cmd.exec("PRINT 'Привет'"));
//		trace("DELETE FROM XXX;"," => ",cmd.exec("DELETE FROM XXX;"));
//		cmd.text = "insert into xxx(id,name,amount,del) values (1,'111', 1.111, true);" ~
//			//"select * from xxxx;" ~
//			"insert into XXX(id,name,amount,del) values (2,'222', 22.22, false);" ~
//			"insert into XXX(id,name,amount,del) values (3,'йцук222', 333333333333.333333333, true);";
//		trace("inserts ..."," => ",cmd.exec(&onIdle));


			ExecReader(cmd, query); // INT2

	}
	catch(SqlException exc)
	{
		trace(exc);
	}
	catch(Throwable tr)
	{
		trace(tr);
	}
}
*/

/*void ExecReader(T = string)(PqCommand cmd, string text)
{
	cmd.text = text;
	//trace("----------------------------------------------------------------------");
	trace(text);
	PqDataReader r = cmd.execReader();
	scope(exit) destroy(r);

	do
	{
		trace("\nName  \tDbType\tColumnSize\tDecimalDigits\tIsNullable\tIsAutoincrement");
		foreach(c;r.fields)
			trace(c.name, "\t", c.dbTypeName, "\t", c.columnSize, "\t", c.decimalDigits, "\t\t", c.isNullable, "\t", c.isAutoincrement);

		while(r.next())
		{
			string res;
			traceChars(".");
			for(int f = 0; f < r.fields.length; f++)
			{
				traceChars("[");
				if(r.isNull(f))
					traceChars("NULL]");
				else
				{
					auto id = r.getValue!(T)(f);
					traceChars(id, "]");
				}
			}
			trace();
		}
	} while(r.nextResultSet());
	trace("----------------------------------------------------------------------");
}

*/
