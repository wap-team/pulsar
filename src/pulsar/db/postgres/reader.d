module pulsar.db.postgres.reader;

version(postgres):

import pulsar.atoms;
import pulsar.db.sql;
import pulsar.db.postgres.exports;
import pulsar.db.postgres.connection;
import pulsar.db.postgres.command;

class PqDataReader : IDbDataReader
{
private:
	PqCommand _cmd = null;
	PGresult* _res;
	OnIdleHandler _onIdle;

	PqRowField[] _fields = null;

	bool _isFirstRowInSet = false;

public:
	package this(PqCommand cmd, OnIdleHandler onIdle)
	{
		CheckNull!(cmd);
		_cmd = cmd;
		_onIdle = onIdle;
	}
	~this()
	{
		close();
	}

public:
	/// Массив описаний полей строки результирующего надора данных ODBC.
	override @property IDbRowField[] fields() const { return cast(IDbRowField[])(_fields); }
	/// Определяет, есть ли поле с указанным именем.
	override bool hasField(string name) const { return PQfnumber(_res, toStringz(name)) > -1; }

	/// Возвращает описание поля по его индексу.
	override PqRowField opIndex(uint fieldIndex)
	{
		if(fieldIndex >= _fields.length)
			throw new OutOfRangeException("fieldIndex");
		return _fields[fieldIndex];
	}
	/// Возвращает описание поля по его имени.
	override PqRowField opIndex(string fieldName)
	{
		int col = PQfnumber(_res, toStringz(fieldName));
		if(col == -1)
			throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");
		return _fields[col];
	}

	/// Переход к следующей строке набора.
	override bool next()
	{
		enforce(_cmd, "PqDataReader закрыт!");
		enforce(_cmd.inWork, "Команда не выполняется!");
		enforce(_cmd.connection);
		enforce(_cmd.connection.state == DbConnectionState.Opened, "Соединение не открыто!");

		auto conn = _cmd.connection._con;
		assert(conn);
		int hr;

		scope(failure)
		{
			if(_res)
				PQclear(_res);
			if(_cmd)
				_cmd.close();
		}

		if(_isFirstRowInSet)
		{
			assert(_res);
			assert(_fields.length);
			_isFirstRowInSet = false;
			return PQntuples(_res) > 0;
		}

		if(_res !is null)
		{
			PQclear(_res);
			_res = null;
		}

		if(_onIdle !is null)
		{
			hr = PQconsumeInput(conn);
			enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
			while(PQisBusy(conn))
			{
				try
					_onIdle();
				catch(Throwable) {}
				hr = PQconsumeInput(conn);
				enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
			}
		}

		while((_res = PQgetResult(conn)) !is null)
		{
			switch (hr = to!ExecStatusType(PQresultStatus(_res)))
			{
				case ExecStatusType.PGRES_COMMAND_OK: PQclear(_res); break;
				case ExecStatusType.PGRES_TUPLES_OK:
				{
					_fields.length = 0;
					return false;
				}
				case ExecStatusType.PGRES_SINGLE_TUPLE:
				{
					assert(PQntuples(_res) == 1);
					assert(_fields.length);
					return true;
				}
				break;
				default:
				{
					string err = cast(string)fromStringz(PQerrorMessage(conn)).dup;
					throw new SqlException(err, hr, to!string(to!ExecStatusType(hr)));
				} break;
			}
		}
		this.close();
		return false;
	}
	/// Переход к следующему результирующему набору.
	override bool nextResultSet()
	{
		enforce(_cmd, "PqDataReader закрыт!");
		enforce(_cmd.inWork, "Команда не выполняется!");
		enforce(_cmd.connection);
		enforce(_cmd.connection.state == DbConnectionState.Opened, "Соединение не открыто!");

		auto conn = _cmd.connection._con;
		assert(conn);
		int hr;

		scope(failure)
		{
			if(_res)
			{
				PQclear(_res);
				_res = null;
			}
			if(_cmd)
				_cmd.close();
		}

		if(_res != null)
		{
			assert(to!ExecStatusType(PQresultStatus(_res)) == ExecStatusType.PGRES_TUPLES_OK,
										"Предыдущий набор данных прочитан не полностью!");
			PQclear(_res);
			_res = null;
		}

		if(_onIdle !is null)
		{
			hr = PQconsumeInput(conn);
			enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
			while(PQisBusy(conn))
			{
				try
					_onIdle();
				catch(Throwable err) { Logger.log("postgres _onIdle error : " ~ err.msg); }
				hr = PQconsumeInput(conn);
				enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
			}
		}

		while((_res = PQgetResult(conn)) !is null)
		{
			switch (hr = to!ExecStatusType(PQresultStatus(_res)))
			{
				case ExecStatusType.PGRES_COMMAND_OK: PQclear(_res); break;
				case ExecStatusType.PGRES_SINGLE_TUPLE:
				{
					//assert(PQntuples(_res) == 1);
					_fields.length = PQnfields(_res);
					for(uint i = 0; i < _fields.length; i++)
					{
						PqRowField f = new PqRowField();
						f._r = this;
						f._index = i;
						f._type = PQftype(_res,i);
						_fields[i] = f;
					}
					_isFirstRowInSet = true;
					return true;
				} break;
				case ExecStatusType.PGRES_TUPLES_OK:
				{
					assert(PQntuples(_res) == 0);
					_isFirstRowInSet = true;
					if(_fields.length == 0)
						goto case ExecStatusType.PGRES_SINGLE_TUPLE;
					return true;
				} break;
				default:
				{
					string err = cast(string)fromStringz(PQerrorMessage(conn)).dup;
					throw new SqlException(err, hr, to!string(to!ExecStatusType(hr)));
				} break;
			}
		}

		this.close();
		return false;
	}

	/// Метод проверки значения поля на null.
	override bool isNull(uint fieldIndex) const
	{
		enforce(_res, "Чтение данных не доступно!");
		if(fieldIndex >= _fields.length)
			throw new OutOfRangeException("fieldIndex");
		return PQgetisnull(_res, 0, cast(int)fieldIndex) == 1;
	}
	/// Метод проверки значения поля на null.
	override bool isNull(string fieldName) const
	{
		int col = PQfnumber(_res, toStringz(fieldName));
		if(col == -1)
			throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");
		return isNull(col);
	}

	/// Закрывает DataReader
	override void close()
	{
		if(_fields.length)
		{
			foreach(f; _fields)
				f._r = null;
			_fields.length = 0;
			_fields = null;
		}
		if(_res)
		{
			PQclear(_res);
			_res = null;
		}
		if(_cmd)
		{
			_cmd.close();
			_cmd = null;
		}
	}
}

//-------------------------------------------------------------------------------------------
/// Класс поля строки результирующего надора данных.
class PqRowField : IDbRowField
{
private:
	PqDataReader _r = null;
	uint _index;
	Oid _type;

public:
	@property uint index() const { return _index; }
	override @property string name() const { return cast(string)fromStringz(PQfname(_r._res, _index)); }
	override @property int dbTypeCode() const { return cast(int)_type; }
	override @property string dbTypeName() const { return to!string(dbType);	}
	@property PgSqlType dbType() const { return to!PgSqlType(_type > 5000 ? 0 : _type); }
	override @property int columnSize() const { return PQfsize(_r._res, _index); }
	override @property int decimalDigits() const
	{
		// TODO : фигню показывает
		// см. https://www.postgresql.org/message-id/slrnd6hnhn.27a.andrew%2Bnonews%40trinity.supernews.net
		return PQfmod(_r._res, _index);
	}

	override @property bool isNullable() const { return false; /* TODO */ }
	override @property bool isAutoincrement() const { return false;  /* TODO */ }

	@property bool isSomeString() const { return PQfformat(_r._res, _index) == 0; }

	T value(T)() const
	{
		enforce(_r && _r._res, "Чтение данных не доступно!");
		long			datalen = PQgetlength(_r._res, 0, _index);
		ubyte* data = datalen ? PQgetvalue(_r._res, 0, _index) : null;

		if(datalen == 0 || data is null)
		{
			static if(__traits(compiles, to!T(null)))
				return null;
			else
				throw new Exception("Значение поля [" ~ name ~ "] равно null!");
		}

		enforce(isSomeString, "ToDo : binary mode");

		string val = cast(string)data[0..datalen];

		static if(is(T == byte[])) {	return (cast(byte*)data)[0..datalen];	}
		else static if(is(T == ubyte[])) { return data[0..datalen]; }
		else switch(_type)
		{
			case PgSqlType.TIMESTAMP :
			{
				import std.datetime;

				val = val.replace(" ", "T");
				SysTime dt = SysTime.fromISOExtString(val);
				static if(is(T == SysTime))
					return dt;
				else static if(is(T == DateTime))
					return cast(DateTime)dt;
				else static if(is(T == Date))
					return cast(Date)dt;
				else static if(is(T == TimeOfDay))
					return cast(TimeOfDay)dt;
			} break;
			case PgSqlType.DATE :
			{
				import std.datetime;
				static if(is(T == Date))
					return Date.fromISOExtString(val);
			} break;
			case PgSqlType.TIME :
			{
				import std.datetime;
				static if(is(T == TimeOfDay))
					return TimeOfDay.fromISOExtString(val);
			} break;
			case PgSqlType.BOOL : static if(is(T == bool))
			{
				return ["TRUE","t","true","y","yes","on","1"].canFind(val);
			} break;
			default: break;
		}

		static if(__traits(compiles, to!T(string.init)))
		{
			T tval = to!T(val.dup);
			return tval;
		}

		throw new Exception("Не удалось извлечь значение поля типа " ~ dbTypeName ~ "!");
	}

}

//-------------------------------------------------------------------------------------------
/// Метод извлечения данных из поля по его индексу.
T getValue(T)(IDbDataReader r, uint fieldIndex)
{
	CheckNull!r;
	if(fieldIndex >= r.fields.length)
		throw new OutOfRangeException("fieldIndex");
	PqRowField f = cast(PqRowField)(r.fields[fieldIndex]);

	if(f is null)
		throw new Exception("Поле строки равно null или не является типом PqRowField!");

	return f.value!T();
}
/// Метод извлечения данных из поля по его индексу.
/// Если значение поля NULL, возвращает ifNullValue.
T getValue(T)(IDbDataReader r, uint fieldIndex, T ifNullValue)
{
	CheckNull!r;
	if(fieldIndex >= r.fields.length)
		throw new OutOfRangeException("fieldIndex");
	PqRowField f = cast(PqRowField)(r.fields[fieldIndex]);

	if(f is null)
		throw new Exception("Поле строки равно null или не является типом PqRowField!");

	return r.isNull(fieldIndex) ? ifNullValue : f.value!T();
}

/// Метод извлечения данных из поля по его имени.
T getValue(T)(IDbDataReader reader, string fieldName)
{
	CheckNull!reader;
	CheckNullOrEmpty!fieldName;

	PqDataReader r = cast(PqDataReader)reader;
	if(r is null)
		throw new Exception("reader не является  объектом типа PqDataReader!");

	PqRowField f = r[fieldName];
	if(f is null)
		throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");
	return f.value!T();
}
/// Метод извлечения данных из поля по его имени.
/// Если значение поля NULL, возвращает ifNullValue.
T getValue(T)(IDbDataReader reader, string fieldName, T ifNullValue)
{
	CheckNull!reader;
	CheckNullOrEmpty!fieldName;

	PqDataReader r = cast(PqDataReader)reader;
	if(r is null)
		throw new Exception("reader не является  объектом типа PqDataReader!");

	PqRowField f = r[fieldName];
	if(f is null)
		throw new Exception("Поле с именем [" ~ fieldName ~ "] не найдено в списке полей!");

	return r.isNull(fieldName) ? ifNullValue : f.value!T();
}
