module pulsar.db.postgres.exports;

version(postgres):

extern (C):
nothrow:
@nogc:

alias Oid = uint;

/// Postgre SQL types
enum PgSqlType : Oid
{
	USERTYPE = 0,

	/* OIDS 1 - 99 */
	// boolean, 'true'/'false'
	BOOL = 16,
	// variable-length string, binary values escaped
	BYTEA = 17,
	// single character
	CHAR = 18,
	// 63-byte type for storing system identifiers
	NAME = 19,
	// ~18 digit integer, 8-byte storage
	INT8 = 20,
	// -32 thousand to 32 thousand, 2-byte storage
	INT2 = 21,
	// array of int2, used in system tables
	INT2VECTOR = 22,
	// -2 billion to 2 billion integer, 4-byte storage
	INT4 = 23,
	// registered procedure
	REGPROC = 24,
	// variable-length string, no limit specified
	TEXT = 25,
	// object identifier(oid), maximum 4 billion
	OID = 26,
	// (block, offset), physical location of tuple
	TID = 27,
	// transaction id
	XID = 28,
	// command identifier type, sequence in transaction id
	CID = 29,
	// array of oids, used in system tables
	OIDVECTOR = 30,

	/* OIDS 100 - 199 */
	JSON = 114,
	// XML content
	XML = 142,
	// string representing an internal node tree
	PGNODETREE = 194,
	// multivariate ndistinct coefficients
	PGNDISTINCT = 3361,
	// multivariate dependencies
	PGDEPENDENCIES = 3402,
	// internal type for passing CollectedCommand
	PGDDLCOMMAND = 32,

	/* OIDS 200 - 299 */

	/* OIDS 300 - 399 */

	/* OIDS 400 - 499 */

	/* OIDS 500 - 599 */

	/* OIDS 600 - 699 */
	// geometric point '(x, y)'
	POINT = 600,
	// geometric line segment '(pt1,pt2)'
	LSEG = 601,
	// geometric path '(pt1,...)'
	PATH = 602,
	// geometric box '(lower left,upper right)'
	BOX = 603,
	// geometric polygon '(pt1,...)'
	POLYGON = 604,
	// geometric line
	LINE = 628,

	/* OIDS 700 - 799 */

	// single-precision floating point number, 4-byte storage
	FLOAT4 = 700,
	// double-precision floating point number, 8-byte storage
	FLOAT8 = 701,
	// absolute, limited-range date and time (Unix system time)
	ABSTIME = 702,
	// relative, limited-range time interval (Unix delta time)
	RELTIME = 703,
	// (abstime,abstime), time interval
	TINTERVAL = 704,
	//
	UNKNOWN = 705,
	// geometric circle '(center,radius)'
	CIRCLE = 718,
	// monetary amounts, $d,ddd.cc
	CASH = 790,

	/* OIDS 800 - 899 */
	// XX:XX:XX:XX:XX:XX, MAC address
	MACADDR = 829,
	MACADDR_ = 1040,
	// IP address/netmask, host address, netmask optional
	INET = 869,
	INET_ = 1041,
	// network IP address/netmask, network address
	CIDR = 650,
	CIDR_ = 651,
	// XX:XX:XX:XX:XX:XX:XX:XX, MAC address
	MACADDR8 = 774,

	/* OIDS 900 - 999 */

	/* OIDS 1000 - 1099 */
	INT2ARRAY = 1005,
	INT4ARRAY = 1007,
	TEXTARRAY = 1009,
	OIDARRAY = 1028,
	FLOAT4ARRAY = 1021,
	// access control list
	ACLITEM = 1033,
	ACLITEM_ = 1034,

	CSTRINGARRAY = 1263,
	// char(length), blank-padded string, fixed storage length
	BPCHAR = 1042,
	// varchar(length), non-blank-padded string, variable storage length
	VARCHAR = 1043,
	// date
	DATE = 1082,
	// time of day
	TIME = 1083,

	/* OIDS 1100 - 1199 */
	// date and time
	TIMESTAMP = 1114,
	// date and time with time zone
	TIMESTAMPTZ = 1184,
	// @ <number> <units>, time interval
	INTERVAL = 1186,

	/* OIDS 1200 - 1299 */
	// time of day with time zone
	TIMETZ = 1266,

	/* OIDS 1500 - 1599 */
	// fixed-length bit string
	BIT = 1560,
	// variable-length bit string
	VARBIT = 1562,

	/* OIDS 1600 - 1699 */

	/* OIDS 1700 - 1799 */
	// numeric(precision, decimal), arbitrary precision number
	NUMERIC = 1700,
	// reference to cursor (portal name)
	REFCURSOR = 1790,

	/* OIDS 2200 - 2299 */

	// registered procedure (with args)
	REGPROCEDURE = 2202,
	// registered operator
	REGOPER = 2203,
	// registered operator (with args)
	REGOPERATOR = 2204,
	// registered class
	REGCLASS = 2205,
	// registered type
	REGTYPE = 2206,
	// registered role
	REGROLE = 4096,
	// registered namespace
	REGNAMESPACE = 4089,
	REGTYPEARRAY = 2211,
	// UUID datatype
	UUID = 2950,
	// PostgreSQL LSN datatype
	LSN = 3220,

	/* text search */
	// text representation for text search
	TSVECTOR = 3614,
	// GiST index internal text representation for text search
	GTSVECTOR = 3642,
	// query representation for text search
	TSQUERY = 3615,
	// registered text search configuration
	REGCONFIG = 3734,
	// registered text search dictionary
	REGDICTIONARY = 3769,

	/* jsonb */
	// Binary JSON
	JSONB = 3802,

	// txid snapshot
	TXID = 2949,

	/* range types */
	// range of integers
	INT4RANGE = 3904,

	/*
	* pseudo-types
	*
	* types with typtype='p' represent various special cases in the type system.
	*
	* These cannot be used to define table columns, but are valid as function
	* argument and result types (if supported by the function's implementation
	* language).
	*
	* Note: cstring is a borderline case; it is still considered a pseudo-type,
	* but there is now support for it in records and arrays. Perhaps we should
	* just treat it as a regular base type?
	*/
	RECORD = 2249,
	RECORDARRAY = 2287,
	CSTRING = 2275,
	ANY = 2276,
	ANYARRAY = 2277,
	VOID = 2278,
	TRIGGER = 2279,
	EVTTRIGGER = 3838,
	LANGUAGE_HANDLER = 2280,
	INTERNAL = 2281,
	OPAQUE = 2282,
	ANYELEMENT = 2283,
	ANYNONARRAY = 2776,
	ANYENUM = 3500,
	FDW_HANDLER = 3115,
	INDEX_AM_HANDLER = 325,
	TSM_HANDLER = 3310,
	ANYRANGE = 3831,
}

const(char*) PQerrorMessage(PGconn*);

//=== connection ===
struct PGconn {};
PGconn* PQconnectdb (const(char)* conninfo);
/// Переустанавливает канал связи с сервером.
void PQreset(PGconn *conn);
/// Завершает соединение и очищает данные
void PQfinish(PGconn *conn);

//--- connection info ---
enum ConnStatus : int
{
	CONNECTION_OK = 0,
	CONNECTION_BAD = 1,
	/// Ожидание, пока соединение будет установлено
	CONNECTION_STARTED = 2,
	/// Соединение установлено; ожидание отправки.
	CONNECTION_MADE = 3,
	/// Ожидание ответа от сервера.
	CONNECTION_AWAITING_RESPONSE = 4,
	/// Аутентификация получена; ожидание завершения запуска серверной части.
	CONNECTION_AUTH_OK = 5,
	/// Согласование значений параметров, зависящих от программной среды.
	CONNECTION_SETENV = 6,
	/// Согласование SSL-шифрования.
	CONNECTION_SSL_STARTUP = 7,
	CONNECTION_NEEDED = 8, /* Internal state: connect() needed */
	CONNECTION_CHECK_WRITABLE,	/* Checking if session is read-write. */
	CONNECTION_CONSUME,			/* Consuming any extra messages. */
	CONNECTION_GSS_STARTUP,		/* Negotiating GSSAPI. */
	CONNECTION_CHECK_TARGET,	/* Checking target server properties. */
	CONNECTION_CHECK_STANDBY	/* Checking if server is in standby mode. */
}
ConnStatus PQstatus (const(PGconn)* conn);

/** Возвращает текущее значение параметра сервера.
	server_version, server_encoding, client_encoding, application_name,
	is_superuser, session_authorization, DateStyle, IntervalStyle, TimeZone,
	integer_datetimes, standard_conforming_strings
**/
char* PQparameterStatus(const(PGconn)*, const(char)*);

//--- connection test ---
PGPing PQping(const(char)* conninfo);
enum PGPing : int
{
	/// Сервер работает и, по-видимому, принимает подключения.
	PQPING_OK,
	/// Сервер работает, но находится в состоянии, которое запрещает подключения
	/// (запуск, завершение работы или восстановление после аварийного отказа).
	PQPING_REJECT,
	/// Контакт с сервером не удался.
	PQPING_NO_RESPONSE,
	/** Никакой попытки установить контакт с сервером сделано не было,
					поскольку предоставленные параметры были явно некорректными,
					или имела место какая-то проблема на стороне клиента (например, нехватка памяти).
	*/
	PQPING_NO_ATTEMTP
}

//=== Query ===
struct PGresult {};
/// Передаёт команду серверу и ожидает результата (синхронно).
PGresult* PQexec(PGconn*, const char* command);

/** Отправляет команду серверу, не ожидая получения результата.
				Если команда была отправлена успешно, то функция возвратит значение 1,
				в противном случае она возвратит 0 (тогда нужно воспользоваться функцией
				PQerrorMessage для получения дополнительной информации о сбое).
				После успешного вызова PQsendQuery вызовите PQgetResult один или несколько раз,
				чтобы получить результат.
				Функцию PQsendQuery нельзя вызвать повторно (на том же самом соединении) до тех пор,
				пока PQgetResult не вернёт нулевой указатель, означающий, что выполнение команды
				завершено.
				https://postgrespro.ru/docs/postgrespro/9.6/libpq-async.html
**/
int PQsendQuery(PGconn* conn, const char* command);

/** Выбирает однострочный режим для текущего выполняющегося запроса.
				Эту функцию можно вызывать только непосредственно после функции PQsendQuery или
				одной из её родственных функций, до выполнения любой другой операции на этом
				подключении, такой, как PQconsumeInput или PQgetResult.
				Будучи вызванной своевременно, функция активирует однострочный режим для текущего
				запроса и возвращает 1. В противном случае режим остаётся не изменённым,
				а функция возвращает 0. В любом случае режим возвращается в нормальное состояние
				после завершения текущего запроса.
**/
int PQsetSingleRowMode(PGconn* conn);

/** Если сервер готов передать данные, принять их.
				PQconsumeInput обычно возвращает 1, показывая, что «ошибки нет»,
				но возвращает 0, если имела место какая-либо проблема (в таком случае можно
				обратиться к функции PQerrorMessage за уточнением)
	**/
int PQconsumeInput(PGconn* conn);

/** Возвращает 1, если команда занята работой,
				то есть функция PQgetResult была бы заблокирована в ожидании ввода.
				Возвращаемое значение 0 показывает, что функция PQgetResult при её вызове
				гарантированно не будет заблокирована.
**/
int PQisBusy(PGconn* conn);

/** Ожидает получения следующего результата после предшествующего вызова PQsendQuery.
				Поддерживает получение нескольких наборов данных (например, если в запросе несколько SELECT).
					Когда команда завершена и результатов больше не будет, тогда возвращается нулевой указатель.
				Функция PQgetResult должна вызываться повторно до тех пор, пока она не вернёт
				нулевой указатель, означающий, что команда завершена.
					Не забывайте освобождать память, занимаемую каждым результирующим объектом,
				с помощью функции PQclear, когда работа с этим объектом закончена.
**/
PGresult *PQgetResult(PGconn *conn);

/// Возвращает статус результата выполнения команды.
ExecStatusType PQresultStatus(PGresult*);
/// Статус выполнения запрса
enum ExecStatusType : int
{
	/// Строка, отправленная серверу, была пустой.
	PGRES_EMPTY_QUERY = 0,
	/// Успешное завершение команды, не возвращающей никаких данных.
	PGRES_COMMAND_OK,
	/// Успешное завершение команды, возвращающей данные (такой, как SELECT или SHOW).
	PGRES_TUPLES_OK,
	/// Начат перенос данных Copy Out (с сервера).
	PGRES_COPY_OUT,
	/// Начат перенос данных Copy In (на сервер).
	PGRES_COPY_IN,
	/// Ответ сервера не был распознан.
	PGRES_BAD_RESPONSE,
	/// Произошла нефатальная ошибка (уведомление или предупреждение).
	PGRES_NONFATAL_ERROR,
	/// Произошла фатальная ошибка.
	PGRES_FATAL_ERROR,
	/// Начат перенос данных Copy In/Out (на сервер и с сервера).
	PGRES_COPY_BOTH,
	/// Структура PGresult содержит только одну результирующую строку,
	/// возвращённую текущей командой. Этот статус имеет место только тогда,
	/// когда для данного запроса был выбран режим построчного вывода
	PGRES_SINGLE_TUPLE,
	PGRES_PIPELINE_SYNC,		/* pipeline synchronization point */
	PGRES_PIPELINE_ABORTED		/* Command didn't run because of an abort earlier in a pipeline */
}

/// Создаёт структуру данных, содержащую информацию, необходимую для отмены команды,
///  запущенной через конкретное подключение к базе данных.
struct PGcancel {};
PGcancel* PQgetCancel(PGconn *conn);

/// Освобождает память, занимаемую структурой данных, созданной функцией PQgetCancel.
void PQfreeCancel(PGcancel* cancel);

/** Требует, чтобы сервер прекратил обработку текущей команды.
				Возвращаемое значение равно 1, если запрос на отмену был успешно отправлен, и 0 в противном случае.
				В случае неудачной отправки errbuf заполняется пояснительным сообщением об ошибке.
				errbuf должен быть массивом символов, имеющим размер errbufsize (рекомендуемый размер составляет 256 байтов).
**/
int PQcancel(PGcancel *cancel, char *errbuf, int errbufsize);


//=== Result ===
/// Возвращает описание статуса
char* PQresStatus(ExecStatusType status);
/// Возвращает индивидуальное поле из отчёта об ошибке.
char* PQresultErrorField(const PGresult* res, int fieldcode);
enum ResultErrorFieldCode : int
{
	PG_DIAG_SEVERITY =          'S',
	PG_DIAG_SEVERITY_NONLOCALIZED = 'V',
	PG_DIAG_SQLSTATE =          'C',
	PG_DIAG_MESSAGE_PRIMARY =   'M',
	PG_DIAG_MESSAGE_DETAIL =    'D',
	PG_DIAG_MESSAGE_HINT =      'H',
	PG_DIAG_STATEMENT_POSITION ='P',
	PG_DIAG_INTERNAL_POSITION = 'p',
	PG_DIAG_INTERNAL_QUERY =    'q',
	PG_DIAG_CONTEXT =           'W',
	PG_DIAG_SCHEMA_NAME =       's',
	PG_DIAG_TABLE_NAME =        't',
	PG_DIAG_COLUMN_NAME =       'c',
	PG_DIAG_DATATYPE_NAME =     'd',
	PG_DIAG_CONSTRAINT_NAME =   'n',
	PG_DIAG_SOURCE_FILE =       'F',
	PG_DIAG_SOURCE_LINE =       'L',
	PG_DIAG_SOURCE_FUNCTION =   'R'
}

/// Возвращает текст ошибки
char* PQresultErrorMessage(const(PGresult)*);

/// Возвращает число строк, которые затронула SQL-команда.
char* PQcmdTuples(const(PGresult)*);

/// Возвращает число строк в результирующем наборе
int PQntuples(const(PGresult)*);

/// Возвращает число столбцов в результирующем наборе
int PQnfields(const(PGresult)*);

/// Возвращает имя столбца, соответствующего данному номеру столбца.
const(char*) PQfname(const(PGresult)*, int);

/// Возвращает номер столбца, соответствующего данному имени столбца.
int PQfnumber(const(PGresult*) res, const(char*) column_name);

/// Возвращает код формата, показывающий формат данного столбца (0- строка, 1 - двоичные)
int PQfformat(const(PGresult*) res, int column_number);

/// Возвращает модификатор типа для столбца.
int PQfmod(const(PGresult*) res, int column_number);

/** PQfsize возвращает размер пространства, выделенного для этого столбца в
				строке базы данных, другими словами, это размер внутреннего представления этого
				типа данных на сервере.
				Отрицательное значение говорит о том, что тип данных имеет переменную длину.
**/
int PQfsize(const(PGresult*) res, int column_number);

/** Возвращает тип данных, соответствующий данному номеру столбца.
				Возвращаемое целое значение является внутренним номером OID для этого типа.
				Вы может сделать запрос к системной таблице pg_type, чтобы получить имена и свойства
				различных типов данных.
		**/
Oid PQftype(const(PGresult*) res,  int column_number);

/// Проверяет поле на предмет отсутствия значения (null).
/// Возвращает 1, если значение отсутствует (null), иначе 0.
int PQgetisnull(const(PGresult*) res,	int row_number,	int column_number);


/** Возвращает значение одного поля из одной строки.
	Вызывающая функция не должна напрямую освобождать память, на которую указывает возвращаемый указатель.
	Для данных в текстовом формате значение, возвращаемое функцией PQgetvalue,
		является значением поля, представленным в виде символьной строки с завершающим нулевым символом.
	Для данных в двоичном формате используется двоичное представление значения.
	Пустая строка возвращается в том случае, когда значение поля отсутствует (null).
	См. PQgetisnull, чтобы отличать отсутствие значения (null) от значения, равного пустой строке.
**/
ubyte* PQgetvalue(const(PGresult*), int row, int column);

/// Возвращает фактическую длину значения поля в байтах
int PQgetlength(const(PGresult*) res,	int row_number, int column_number);

/// Очищает PGresult и возвращает память системе.
void PQclear(PGresult*);

//=== Tools ===

//*******

/*

/// Возвращает параметры подключения, используемые действующим соединением.
PQconninfoOption* PQconninfo(PGconn *conn);
struct PQconninfoOption
{
	char* keyword;
	char* envvar;
	char* compiled;
	char* val;
	char* label;
	char* dispchar;
	int dispsize;
}



PGresult* PQprepare(PGconn*, const char* stmtName, const char* query, int nParams, const void* paramTypes);

PGresult* PQexecPrepared(PGconn*, const char* stmtName, int nParams, const char** paramValues, const int* paramLengths, const int* paramFormats, int resultFormat);



size_t PQescapeString (char *to, const char *from, size_t length);



		*/


