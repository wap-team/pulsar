module pulsar.db.postgres.connection;

version(postgres):

import pulsar.atoms;
import pulsar.atoms.events;


import pulsar.db.sql;
import pulsar.db.postgres.exports;
import pulsar.db.postgres.command;

alias OnIdleHandler = void delegate();

/// Класс соединения с базой данных Postgre SQL
@noproxy class PqConnection : IDbConnection
{
private:
	string _conStr = null;
	ushort _timeout = 30;
	DbConnectionState _state = DbConnectionState.Closed;

package:
	PGconn* _con;
	OnIdleHandler _onIdle;

public:
	/// Строка соединения, возвращаемая после установки соединения.
	override @property	string connectionString() const { return _conStr; }
	/// Состояние соединения
	override @property DbConnectionState state() const { return _state; }
	/// Тайм-аут соединения, сек.
	override @property
	{
		ushort timeout() const { return _timeout; }
		void timeout(ushort value) {	_timeout = value;}
	}
	/// Делегат, вызываемый при ожидании получения данных с сервера.
	/// Подставляется в команды, если не указан в них явно.
	@property // onIdle
	{
		OnIdleHandler onIdle() const { return _onIdle; }
		void onIdle(OnIdleHandler value) { _onIdle = value; }
	}
	/// Событие, вызываемое при получении сообщения от сервера
	Event!(string) OnServerMessage;

public:
	this() { }
	~this() { close(); }

public:
	void open(string server, string dataBase, string user = null, string password = null)
	{
		CheckNullOrEmpty!(server, dataBase);
		if(_state == DbConnectionState.Opened)
			return;
		string str = format("host=%s dbname=%s connect_timeout=%d", server,dataBase,timeout);
		if(user.length > 0)
			str ~= " user=" ~ user;
		if(password.length > 0)
			str ~= " password=" ~ password;
		open(str);
	}
	/// Открывает соединение
	/// Examples: host=localhost port=5432 dbname=mydb connect_timeout=10
	/// See_Also: https://postgrespro.ru/docs/postgrespro/9.6/libpq-connect.html
	override void open(string connectionString)
	{
		if(state == DbConnectionState.Opened)
			return;
		CheckNullOrEmpty!connectionString;
		_con = PQconnectdb(toStringz(connectionString));
		if(_con is null)
			throw new Exception("Unable to allocate PG connection object");
		ConnStatus status = PQstatus(_con);
		if(status != ConnStatus.CONNECTION_OK)
		{
		 scope(exit)
			{
			 PQfinish(_con);
				_state = DbConnectionState.Disposed;
			}
			// dup нужен, так как PQfinish очистит память
			string errMsg = cast(string)fromStringz(PQerrorMessage(_con)).dup;
			throw new SqlException(errMsg, cast(int)status, "Соединение с сервером");
		}
		_state = DbConnectionState.Opened;
		_conStr = connectionString;
		//		query("SET NAMES 'utf8'"); // D does everything with utf8
	}

	/// Закрывает соединение
	override void close()
	{
		scope(exit) _state = DbConnectionState.Disposed;
		if(_con != null)
			PQfinish(_con);
		_con = null;
		//OnServerMessage(null, "Соединение с сервером Postgre SQL закрыто.");
	}

	/// Проверяет, живо ли еще соединение
	override bool isAlive()
	{
		if(state != DbConnectionState.Opened)
			return false;

		ConnStatus status = PQstatus(_con);
		return status == ConnStatus.CONNECTION_OK;
	}

	/** Возвращает параметр сервера по имени
		Доступные параметры: server_version, server_encoding, client_encoding, application_name,
																							is_superuser, session_authorization, DateStyle, IntervalStyle, TimeZone,
																							integer_datetimes, standard_conforming_strings
		**/
	string getServerParam(string param)
	{
		CheckNullOrEmpty!param;
		enforce(state == DbConnectionState.Opened, "Соединение не установлено!");
		return cast(string)fromStringz(PQparameterStatus(_con, toStringz(param)));
	}

	/// Создает объект комманды.
	override PqCommand getCommand(string commandText = "")
	{
		return new PqCommand(this, commandText);
	}

	/// Выполняет команду и возвращает количество строк, затронутых коммнадой
	override int exec(string commandText)
	{
		assert(commandText);
		PqCommand cmd = new PqCommand(this);
		scope(exit) cmd.close();
		int res = cmd.exec(commandText);
		return res;
	}
	/** Выполняет команду и возвращает reader для чтения результирующего набора.
					Усли указан benchmarkName, производится замер времени выполнения запроса
						и его логирование с этим именем.
	*/
	override ScopedDataReader execReader(string commandText, string benchmarkName = null)
	{
		CheckNullOrEmpty!commandText;

		StopWatch sw = StopWatch(AutoStart.no);
		if(benchmarkName.length > 0)
			sw.start();

		IDbCommand cmd = new PqCommand(this);
		IDbDataReader res = cmd.execReader(commandText);

		if(benchmarkName.length > 0)
		{
			sw.stop();
			Logger.log("[BENCHMARK][PqConnection][execReader] %s => %s msecs", benchmarkName, sw.peek.total!"msecs");
		}

		return ScopedDataReader(cmd,res);
	}
}
