module pulsar.db.postgres.command;

version(postgres):

import pulsar.atoms;
import pulsar.db.sql;
import pulsar.db.postgres.exports;
import pulsar.db.postgres.connection;
import pulsar.db.postgres.reader;

/**
	* Класс формирования и выполнения команды через ODBC
	*/
class PqCommand : IDbCommand
{
private:
	package PqConnection _con = null;
	package bool _inWork = false;
	string _text;
	ushort _timeout = 30;

public:
	package this() { }
	this(const(PqConnection) con, string text = "")
	{
		CheckNull!con;
		_con = con.unqual;
		_text = text;
	}
	~this()
	{
		close();
	}

public:
	/// Соединение с сервером баз данных, на котором исполняется команда
	override @property	PqConnection connection() { return _con; }
	/// Текст команды
	override @property
	{
		string text() const { return _text; }
		void text(string value) { _text = value; }
	}
	/// Тайм-аут выполнения команды, сек
	override @property
	{
		ushort timeout() const { return _timeout; }
		void timeout(ushort value) { _timeout = value; }
	}
	/// Признак выполнения команды
	@property bool inWork() const { return _inWork; }

public:
	/// Выполняет команду и возвращает количество строк, затронутых команадой
	override int exec() { return exec(connection.onIdle); }
	/// Выполняет команду и возвращает количество строк, затронутых командой
	override int exec(string commandText)
	{
		CheckNullOrEmpty!commandText;
		text = commandText;
		return exec(connection.onIdle);
	}
	/// Выполняет команду и возвращает количество строк, затронутых командой.
	/// Если указан, при ожиданиях выполняется делегат onIdle.
	int exec(OnIdleHandler onIdle)
	{
		enforce(_inWork == false, "Команда уже выполняется!");
		assert(_con && _con._con);
		enforce(_con.state == DbConnectionState.Opened, "Соединение не открыто!");
		if(_text is null || _text.length == 0)
			throw new Exception("Не указан текст команды!");

		// TODO : SET statement_timeout = '10s' ???

		PGresult* res;
		uint count;
		auto conn = _con._con;

		_inWork = true;
		scope(exit)
			_inWork = false;

		int hr = PQsendQuery(conn, toStringz(_text));
		enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
		do
		{
			if(onIdle !is null)
			{
				hr = PQconsumeInput(conn);
				enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
				while(PQisBusy(conn))
				{
					try
						onIdle();
					catch(Throwable) {}
					hr = PQconsumeInput(conn);
					enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
				}
			}

			while((res = PQgetResult(conn)) !is null)
			{
				switch (hr = to!ExecStatusType(PQresultStatus(res)))
				{
					case ExecStatusType.PGRES_COMMAND_OK:
					{
						char* rc = PQcmdTuples(res);
						if(rc !is null)
						{
							string s = cast(string)fromStringz(rc);
							if(s.length > 0)
								count += to!int(s);
						}
					} break;
					case ExecStatusType.PGRES_TUPLES_OK:  count += PQntuples(res); break;
					default: // Always call PQgetResult until it returns null, even on error.
					{
						PQclear(res);
						string err = cast(string)fromStringz(PQerrorMessage(conn)).dup;

						this.close();

						throw new SqlException(err, hr, to!string(to!ExecStatusType(hr)));
					} break;
				}
				PQclear(res);
			}
		} while(res !is null);

		return count;
	}

	/// Выполняет команду и возвращает reader для чтения результирующего набора.
	override PqDataReader execReader(){ return execReader(connection.onIdle); }
	/// Выполняет команду и возвращает reader для чтения результирующего набора.
	override PqDataReader execReader(string commandText)
	{
		CheckNullOrEmpty!commandText;
		_text = commandText;
		return execReader(connection.onIdle);
	}
	/// Выполняет команду и возвращает reader для чтения результирующего набора.
	/// Если указан, при ожиданиях выполняется делегат onIdle.
	PqDataReader execReader(OnIdleHandler onIdle)
	{
		enforce(_inWork == false, "Команда уже выполняется!");
		assert(_con && _con._con);
		enforce(_con.state == DbConnectionState.Opened, "Соединение не открыто!");

		if(_text is null || _text.length == 0)
			throw new Exception("Не указан текст команды!");
		// TODO : SET statement_timeout = '10s' ???


		PGresult* res;
		auto conn = _con._con;

		_inWork = true;

		int hr = PQsendQuery(conn, toStringz(_text));
		if(hr == 0)
			throw new SqlException(cast(string)fromStringz(PQerrorMessage(conn)), hr, to!string(to!ExecStatusType(hr)) );
		enforce(PQsetSingleRowMode(conn));

		if(onIdle !is null)
		{
			hr = PQconsumeInput(conn);
			enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
			while(PQisBusy(conn))
			{
				try
					onIdle();
				catch(Throwable) {}
				hr = PQconsumeInput(conn);
				enforce(hr, "[PG][ERROR] " ~ cast(string)fromStringz(PQerrorMessage(conn)));
			}
		}

		auto r = new PqDataReader(this, onIdle);
		r.nextResultSet();

		return r;
	}

	/// Закрывает команду, освобождая все ресурсы, связанные с запросом.
	override void close()
	{
		if(_inWork)
		{
			assert(_con && _con._con);
			enforce(_con.state == DbConnectionState.Opened, "Соединение не открыто!");

			auto conn = _con._con;

			PGcancel*  pc = PQgetCancel(conn);
			scope(exit)
				PQfreeCancel(pc);
			char[256] errMsg;
			if(pc && PQcancel(pc, errMsg.ptr, cast(int)errMsg.length) == 0)
				Logger.log("[PG][ERROR] PQcancel => %s", cast(string)fromStringz(errMsg.ptr));

			PGresult* res;
			while((res = PQgetResult(conn)) !is null)
				PQclear(res);

			_inWork = false;
		}
	}
	//-------------------------------------------------------------------------------------------
}
