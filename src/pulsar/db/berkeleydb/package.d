module pulsar.db.berkeleydb;

//pragma(lib, "db-6");

public import pulsar.db.berkeleydb.classes;

version(unittest)
{
/* import std.stdio;
	import std.conv;

	import pulsar.data.db.berkley.exports;
	import pulsar.atoms;

	struct a
	{
		float c;
		int bb;
	}
	struct b
	{
		a cc;
		int bb;
	}

	void xxx()
	{}

	void main(string[] args)
	{
		trace("unittest: pulsar.db.berkleydb");
		int ret;

		DataBase db = new DataBase();
		db.Open("Evolution.db");

		int key = 101;
		string data = "Hi, I BerkelyDB!";

		PutString(db, key, data);
		Exists(db, key);
		int key_1 = 102;
		Exists(db, key_1);
		string data_r;
		data_r = Get!string(db, key);
		writeln(data_r);

		int key_2 = 120;
		byte[] data_2 = new byte[10000];//new byte[1073741824];
		data_2[10] = 121;
		PutBytes(db, key_2, data_2);
		Exists(db, key_2);

		byte[] data_r_2;
		data_r_2 = Get!(byte[])(db, key_2);
		Logger.log(0,to!string(data_r_2[1]) ~ " " ~ to!string(data_r_2[10]));

		DBCursor cursor = db.GetCursor();
		writeln("Count:%s", cursor.Count());
		//Delete(db, key);
		//Exists(db, key);

		db.Close();
		readln();
	}

	void PutString(DataBase db, ref int key, ref string data)
	{
		Logger.log(0,"******* Step: Put string ******");

		DataBaseEntry!int d_key;
		d_key.Data = key;
		d_key.Size = int.sizeof;

		DataBaseEntry!string d_data;
		d_data.Data = data;
		d_data.Size = cast(uint)data.length;

		db.Put(d_key, d_data);
	}
	void PutBytes(DataBase db, ref int key, ref byte[] data)
	{
		Logger.log(0,"******* Step: Put bytes ******");

		DataBaseEntry!int d_key;
		d_key.Data = key;
		d_key.Size = int.sizeof;

		DataBaseEntry!(byte[]) d_data;
		d_data.Data = data;
		d_data.Size = cast(uint)data.length;

		db.Put(d_key, d_data);
	}
	void Exists(DataBase db, ref int key)
	{
		Logger.log(0,"******* Step: Exists key int******");

		bool exists = false;

		DataBaseEntry!int d_key;
		d_key.Data = key;
		d_key.Size = int.sizeof;

		exists = db.Exists(d_key);
		Logger.log(0,(exists == true ? "Ключ найден!  " : "Ключ не найден!  ") ~ to!string(key));
	}
	void Exists(DataBase db, ref string key)
	{
		Logger.log(0,"******* Step: Exists key string ******");

		bool exists = false;

		DataBaseEntry!string d_key;
		d_key.Data = key;
		d_key.Size = cast(uint)key.length;

		exists = db.Exists(d_key);
		Logger.log(0,(exists == true ? "Ключ найден!  " : "Ключ не найден!  ") ~ to!string(key));
	}
	T Get(T)(DataBase db, ref int key)
	{
		Logger.log(0,"******* Step: Get int key ******");
		DataBaseEntry!T data;
		data.Flags = DbtFlags.DB_DBT_REALLOC;

		DataBaseEntry!int d_key;
		d_key.Data = key;
		d_key.Size = int.sizeof;

		data = db.Get!(int, T)(d_key);
		return data.Data;
	}
	T Get(T)(DataBase db, ref string key)
	{
		Logger.log(0,"******* Step: Get string key ******");
		DataBaseEntry!T data;
		data.Flags = DbtFlags.DB_DBT_REALLOC;

		DataBaseEntry!string d_key;
		d_key.Data = key;
		d_key.Size = cast(uint)key.length;

		data = db.Get!(string, T)(d_key);
		return data.Data;
	}
	void Delete(DataBase db, ref int key)
	{
		Logger.log(0,"******* Step: Delete object int key******");

		DataBaseEntry!int d_key;
		d_key.Data = key;
		d_key.Size = int.sizeof;

		db.Del(d_key);
	}
	void Delete(DataBase db, ref string key)
	{
		Logger.log(0,"******* Step: Delete object string key ******");

		DataBaseEntry!string d_key;
		d_key.Data = key;
		d_key.Size = cast(uint)key.length;

		db.Del(d_key);
	}*/
}

version(unittest)
{
// import pulsar.atoms;
// import pulsar.data.db.berkeleydb.exports;

// void main(string[] args)
// {
//  trace("=== unittest: BerkeleyDB native ===");

//  int ret;
//  DB_ENV* env;


//  DB *dbp;
//  u_int32_t flags;

//  ret = db_create(&dbp, null, 0);
//  if(ret != 0){
//   trace("Ошибка, код: " ~ to!string(ret));
//  }

//  flags = DbOpenFlags.DB_CREATE;


//  uint dbflags;
//  dbflags |= DbFlags.DB_DUP;
//  //ret = db_set_flags(dbp, dbflags);
//  dbflags = 0;
//  flags = DbOpenFlags.DB_CREATE;

//  ret = heap_set_heapsize(dbp, 1, 0,0);
//  ret = __db_open(dbp, null, null, toDBCharStr("my_xx.db"), null, DbType.DB_BTREE, flags, 0, 0);
//  if(ret != 0){
//   trace("Ошибка, код: " ~ to!string(ret));
//  }



//  ret = db_get_flags(dbp, &dbflags);


//  //DB_FLAGS.DB_CHKSUM;
//  //DB_FLAGS.DB_DUP;
//  //DB_FLAGS.DB_DUPSORT;
//  //DB_FLAGS.DB_RENUMBER;
//  ubyte chksum, dup, dupsort, renumber;
//  chksum = dbflags && DbFlags.DB_CHKSUM;
//  dup = dbflags && DbFlags.DB_DUP;
//  dupsort = dbflags && DbFlags.DB_DUPSORT;
//  renumber = dbflags && DbFlags.DB_RENUMBER;


//  DBT key, data, data_x, key_x;
//  string s_data = "Hello World BerkleyDB!ф";
//  byte[] b_data;
//  b_data = new byte[50000];
//  b_data[0] = 120;
//  b_data[1] = 120;
//  b_data[2] = 120;
//  b_data[3] = 120;
//  float f_key = 30.5;

//  int f_data = 1;

//  key.data = &f_key;
//  key.size = float.sizeof;

//  data.data = &b_data;
//  data.size = cast(uint)b_data.length;

//  ret = db_put(dbp, null, null, &key, &data, 0);
//  if(ret != 0){
//   trace("Ошибка, код: " ~ to!string(ret));
//  }

//  string s_data_x;
//  byte[] b_data_x;
//  float f_key_1 = 30.5;

//  key_x.data = &f_key_1;
//  key_x.size = float.sizeof;

//  //s_data_x.length = 20;
//  b_data_x.length = 10;
//  data_x.data = &b_data_x;
//  data_x.ulen = 1000000;
//  data_x.flags = DbtFlags.DB_DBT_USERMEM;
//  ret = db_get(dbp, null, null, &key_x, &data_x, 0);

//  //byte* xx = cast(byte*)(data_x.data);
//  //b_data_x =


//  //writeln("Data: " ~ s_data_x);
//  //writeln("Data:");
//  //for(int i=0;i<b_data_x.length;i++)
//  //			write(b_data_x[i]);
//  trace("x");


//  if(ret != 0){
//   trace("Ошибка, код: " ~ to!string(ret));
//  }

//  if(dbp != null)
//  {
//   ret = __db_close(dbp, null, 0);
//   if(ret != 0){
//    trace("Ошибка, код: " ~ to!string(ret));
//   }
//  }

// }


}

version(unittest)
{
	import std.conv;
	import std.traits;
	import pulsar.atoms;
	import pulsar.data.db.berkeleydb.exports;

	void main(string[] args)
	{
		trace("=== unittest: BerkeleyDB classes ===");

		Environment env = new Environment();
		scope(exit)
			env.close();

		env.tmpDir = "/tmp/bdb_tmp";
		env.logDir = "/home/gold/DProj/bdb_data/log";
		env.open("/home/gold/DProj/bdb_data", EnvOpenFlags.DB_INIT_MPOOL |
																																								EnvOpenFlags.DB_USE_ENVIRON |
																																								EnvOpenFlags.DB_CREATE );
		DataBase db = env.openDb("test", DbType.DB_BTREE,	DbOpenFlags.DB_CREATE );

		db.truncate();

		int k1 = 0xFF;
		uint v1 = 0xFFFEFDFC;
		alias BA = ubyte[];
		db.put((cast(ubyte*)&k1)[0..int.sizeof],
									(cast(ubyte*)&v1)[0..uint.sizeof]);
		assert(db.exists((cast(ubyte*)&k1)[0..int.sizeof]));

		int k2 = 77;
		string v2 = "helЯlo";
		db.put((cast(ubyte*)&k2)[0..int.sizeof],cast(ubyte[])v2);
		assert(db.exists((cast(ubyte*)&k2)[0..int.sizeof]));

		db.sync();

		uint v11;
		ubyte[] bv11 = (cast(ubyte*)&v11)[0..uint.sizeof];
		db.get((cast(ubyte*)&k1)[0..int.sizeof],bv11);
		assert(v11 == v1);

		ubyte[] bv22;
		db.get((cast(ubyte*)&k2)[0..int.sizeof], bv22);
		assert(cast(string)bv22 == v2);

		v11 = 100;
		assert(!db.del((cast(ubyte*)&v11)[0..uint.sizeof]));
		assert(db.del((cast(ubyte*)&k2)[0..int.sizeof]));
		assert(!db.exists((cast(ubyte*)&k2)[0..int.sizeof]));

		string sx = "asdfghjkl";
		ubyte[] kx = cast(ubyte[])sx;

		ubyte[] data = new ubyte[1000000];
		foreach(i;0..data.length)
			data[i] = 46;
		//data = cast(ubyte)46;
		db.put(kx,data);

		ubyte[] data1;
		db.get(kx,data1);
	}


}
