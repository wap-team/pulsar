module pulsar.db.berkeleydb.exports;

version(berkeleydb):

import std.string;
import std.conv;


extern (C)
{

	alias u_int32_t = uint;
	alias db_pgno_t = u_int32_t;
	alias	db_recno_t = u_int32_t;	/* Record number type. */

	alias void* DB;
	alias void* DBC;
	alias void* DB_ENV;
	alias void* DB_THREAD_INFO;
	alias void* DB_TXN;
	alias DBTYPE = int;



	/// Флаги операций
	enum DbtFlags : uint
	{
		DB_DBT_APPMALLOC	= 0x0001,	/* Callback allocated memory. */
		// DB_DBT_BULK		    = 0x0002	/* Internal: Insert if duplicate. */
		// DB_DBT_DUPOK		0x0004	/* Internal: Insert if duplicate. */
		// DB_DBT_ISSET		0x0008	/* Lower level calls set value. */
		DB_DBT_MALLOC		  = 0x0010,	/* Return in malloc'd memory. */
		DB_DBT_MULTIPLE		= 0x0020,	/* References multiple records. */
		DB_DBT_PARTIAL		 = 0x0040,	/* Partial put/get. */
		DB_DBT_REALLOC		 = 0x0080,	/* Return in realloc'd memory. */
		DB_DBT_READONLY		= 0x0100,	/* Readonly, don't update. */
		// DB_DBT_STREAMING	0x0200	/* Internal: DBT is being streamed. */
		// DB_DBT_USERCOPY		0x0400	/* Use the user-supplied callback. */
		DB_DBT_USERMEM		 = 0x0800,	/* Return in user's memory. */
		DB_DBT_BLOB		    = 0x1000	/* Data item is a blob. */
		// DB_DBT_BLOB_REC		0x2000	/* Internal: Blob database record. */
	}
	struct DBT
	{
		void* data;			/* Key/data */
		u_int32_t size;			/* key/data length */

		u_int32_t ulen;			/* RO: length of user buffer. */
		u_int32_t dlen;			/* RO: get/put record length. */
		u_int32_t doff;			/* RO: get/put record offset. */

		void *app_data;


		u_int32_t flags;
	};

	// Server
	alias FM = void* function(size_t);
	int db_env_set_func_malloc(FM);

	// Error handling
	char * db_strerror(int error);

	//  Enviroment
	int db_env_create(DB_ENV** dbenvpp, u_int32_t flags);

	int __env_open_pp(DB_ENV *dbenv, char *db_home, u_int32_t flags, int mode);
	alias db_env_open = __env_open_pp;

	int __env_close_pp(DB_ENV *, u_int32_t );
	alias db_env_close = __env_close_pp;

	int __env_remove(DB_ENV *, char *, u_int32_t );
	alias db_env_remove = __env_remove;

	int __env_set_tmp_dir(DB_ENV *, char *);
	alias db_env_set_tmp_dir = __env_set_tmp_dir;

	int __log_set_lg_dir(DB_ENV *, char *);
	alias db_env_set_lg_dir = __log_set_lg_dir;

	// Databases
	int db_create(DB **, DB_ENV *, u_int32_t);

	int	__db_open_pp(DB *db, DB_TXN *txnid, const char *file,
																		const char *database, DBTYPE type, u_int32_t flags, int mode);
	alias db_open = __db_open_pp;

	int	__db_close_pp(DB* dbp, u_int32_t flags);
	alias db_close = __db_close_pp;

	int __db_put_pp(DB *, DB_TXN *, DBT *, DBT *, u_int32_t);
	alias db_put = __db_put_pp;

	int	__db_get_pp(DB *, DB_TXN *, DBT *, DBT *, u_int32_t);
	alias db_get = __db_get_pp;

	int __db_del_pp(DB *, DB_TXN *, DBT *, u_int32_t);
	alias db_del = __db_del_pp;

	int __db_exists(DB *, DB_TXN *, DBT *, u_int32_t);
	alias db_exists = __db_exists;

	int __db_sync_pp(DB *, u_int32_t);
	alias db_sync = __db_sync_pp;

	int __db_truncate_pp(DB *, DB_TXN *, u_int32_t *, u_int32_t);
	alias db_truncate = __db_truncate_pp;

	int __db_cursor_pp(DB* dbp, DB_TXN* txn, DBC* cursorp, u_int32_t flags);
	alias __db_cursor_pp db_cursor;

	int __dbc_get_pp(DBC dbc, DBT *, DBT *, u_int32_t flags);
	alias __dbc_get_pp db_cursor_get;

	int __dbc_count_pp(DBC* dbc, db_recno_t* count_p, u_int32_t flags);
	alias __dbc_count_pp dbc_count;

	int __dbc_close_pp(DBC dbc);
	alias __dbc_close_pp dbc_close;



/+ struct __db_env {
		ENV *env;			/* Linked ENV structure */
		db_mutex_t mtx_db_env;		/* DB_ENV structure mutex */

		/* Error message callback */
//  void (*db_errcall) __P((const DB_ENV *, const char *, const char *));
		void  function(DB_ENV *, char *, char *) db_errcall;
		FILE* db_errfile;	/* Error message file stream */
		char* db_errpfx;	/* Error message prefix */

		/* Other message callback */
///  void (*db_msgcall) __P((const DB_ENV *, const char *));
		FILE* db_msgfile;	/* Other message file stream */

		/* Other application callback functions */
		int  function(DB_ENV *, DBT *, DB_LSN *, db_recops )app_dispatch;
		void  function(DB_ENV *, u_int32_t , void *) db_event_func;
		void  function(DB_ENV *, int , int )db_feedback;
		void  function(void *) db_free;
		void  function(DB_ENV *, int )db_paniccall;
		void * function(size_t )db_malloc;
		void * function(void *, size_t )db_realloc;
		int  function(DB_ENV *, pid_t , db_threadid_t , u_int32_t )is_alive;
		void  function(DB_ENV *, pid_t *, db_threadid_t *)thread_id;
		char * function(DB_ENV *, pid_t , db_threadid_t , char *)thread_id_string;

//  /* Application specified paths */
//  char	*db_blob_dir;		/* Blob file directory */
//  char	*db_log_dir;		/* Database log file directory */
//  char	*db_md_dir;		/* Persistent metadata directory */
		char *db_tmp_dir;		/* Database tmp file directory */

//  char    *db_create_dir;		/* Create directory for data files */
		char **db_data_dir;		/* Database data file directories */
		int	 data_cnt;		/* Database data file slots */
		int	 data_next;		/* Next database data file slot */

//  char	*intermediate_dir_mode;	/* Intermediate directory perms */

		int	 shm_key;		/* shmget key */

//  char	*passwd;		/* Cryptography support */
//  size_t	 passwd_len;

//  /* Private handle references */
		void *app_private;		/* Application-private handle */
//  void	*api1_internal;		/* C++, Perl API private */
//  void	*api2_internal;		/* Java API private */

		u_int32_t verbose;	/* DB_VERB_XXX flags */

//  u_int32_t	blob_threshold;	/* Blob threshold record size */

//  /* Mutex configuration */
		u_int32_t mutex_align;	/* Mutex alignment */
		u_int32_t mutex_cnt;	/* Number of mutexes to configure */
		u_int32_t	mutex_inc;	/* Number of mutexes to add */
		u_int32_t	mutex_max;	/* Max number of mutexes */
		u_int32_t	mutex_tas_spins;/* Test-and-set spin count */

		/* Locking configuration */
		u_int8_t       *lk_conflicts;	/* Two dimensional conflict matrix */
		int		lk_modes;	/* Number of lock modes in table */
		u_int32_t	lk_detect;	/* Deadlock detect on all conflicts */
		u_int32_t	lk_max;	/* Maximum number of locks */
		u_int32_t	lk_max_lockers;/* Maximum number of lockers */
		u_int32_t	lk_max_objects;/* Maximum number of locked objects */
//  u_int32_t	lk_init;	/* Initial number of locks */
//  u_int32_t	lk_init_lockers;/* Initial number of lockers */
//  u_int32_t	lk_init_objects;/* Initial number of locked objects */
//  u_int32_t	lk_partitions ;/* Number of object partitions */
//  db_timeout_t	lk_timeout;	/* Lock timeout period */
//  /* Used during initialization */
//  u_int32_t	locker_t_size;	/* Locker hash table size. */
//  u_int32_t	object_t_size;	/* Object hash table size. */

//  /* Logging configuration */
		u_int32_t	lg_bsize;	/* Buffer size */
//  u_int32_t	lg_fileid_init;	/* Initial allocation for fname structs */
		int		lg_filemode;	/* Log file permission mode */
		u_int32_t	lg_regionmax;	/* Region size */
		u_int32_t	lg_size;	/* Log file size */
//  u_int32_t	lg_flags;	/* Log configuration */

//  /* Memory pool configuration */
		u_int32_t	mp_gbytes;	/* Cache size: GB */
		u_int32_t	mp_bytes;	/* Cache size: bytes */
//  u_int32_t	mp_max_gbytes;	/* Maximum cache size: GB */
//  u_int32_t	mp_max_bytes;	/* Maximum cache size: bytes */
		size_t		mp_mmapsize;	/* Maximum file size for mmap */
		int		mp_maxopenfd;	/* Maximum open file descriptors */
		int		mp_maxwrite;	/* Maximum buffers to write */
		u_int		mp_ncache;	/* Initial number of cache regions */
//  u_int32_t	mp_pagesize;	/* Average page size */
//  u_int32_t	mp_tablesize;	/* Approximate hash table size */
//  u_int32_t	mp_mtxcount;	/* Number of mutexs */
//      /* Sleep after writing max buffers */
//  db_timeout_t	mp_maxwrite_sleep;

//  /* Transaction configuration */
//  u_int32_t	tx_init;	/* Initial number of transactions */
		u_int32_t	tx_max;		/* Maximum number of transactions */
		time_t		tx_timestamp;	/* Recover to specific timestamp */
		db_timeout_t	tx_timeout;	/* Timeout for transactions */

//  /* Thread tracking configuration */
//  u_int32_t	thr_init;	/* Thread count */
		u_int32_t	thr_max;	/* Thread max */
//  roff_t		memory_max;	/* Maximum region memory */

	/*
		* The following fields are not strictly user-owned, but they outlive
		* the ENV structure, and so are stored here.
		*/
		DB_FH		*registry;	/* DB_REGISTER file handle */
		u_int32_t	registry_off;	/*
//       * Offset of our slot.  We can't use
//       * off_t because its size depends on
//       * build settings.
//       */
//  db_timeout_t	envreg_timeout; /* DB_REGISTER wait timeout */

///  u_int32_t flags;

//  /* DB_ENV PUBLIC HANDLE LIST BEGIN */
//  int  (*add_data_dir) __P((DB_ENV *, const char *));
//  int  (*backup)	__P((DB_ENV *, const char *, u_int32_t));
		int  function(DB_ENV *, DB_TXN **)cdsgroup_begin;
		int  function(DB_ENV *, u_int32_t )close;
//  int  (*dbbackup) __P((DB_ENV *, const char *, const char *, u_int32_t));
		int  function(DB_ENV *, DB_TXN *, char *, char *, u_int32_t )dbremove;
		int  function(DB_ENV *, DB_TXN *, char *, char *, char *, u_int32_t )dbrename;
		void  function(DB_ENV *, int , char *,...)err;
		void  function(DB_ENV *, char *,...)errx;
		int  function(DB_ENV *, u_int32_t )failchk;
		int  function(DB_ENV *, char *, u_int32_t )fileid_reset;
//  int  (*get_alloc) __P((DB_ENV *, void *(**)(size_t),
//   void *(**)(void *, size_t), void (**)(void *)));
//  int  (*get_app_dispatch)
//   __P((DB_ENV *, int (**)(DB_ENV *, DBT *, DB_LSN *, db_recops)));
//  int  (*get_blob_dir) __P((DB_ENV *, const char **));
//  int  (*get_blob_threshold) __P((DB_ENV*, u_int32_t *));
//  int  (*get_cache_max) __P((DB_ENV *, u_int32_t *, u_int32_t *));
		int  function(DB_ENV *, u_int32_t *, u_int32_t *, int *)get_cachesize;
//  int  (*get_create_dir) __P((DB_ENV *, const char **));
		int  function(DB_ENV *, char ***)get_data_dirs;
//  int  (*get_data_len) __P((DB_ENV *, u_int32_t *));
//  int  (*get_backup_callbacks) __P((DB_ENV *,
//   int (**)(DB_ENV *, const char *, const char *, void **),
//   int (**)(DB_ENV *, u_int32_t, u_int32_t, u_int32_t, u_int8_t *, void *),
//   int (**)(DB_ENV *, const char *, void *)));
//  int  (*get_backup_config) __P((DB_ENV *, DB_BACKUP_CONFIG, u_int32_t *));
//  int  (*get_encrypt_flags) __P((DB_ENV *, u_int32_t *));
//  void (*get_errcall) __P((DB_ENV *,
//   void (**)(const DB_ENV *, const char *, const char *)));
//  void (*get_errfile) __P((DB_ENV *, FILE **));
//  void (*get_errpfx) __P((DB_ENV *, const char **));
//  int  (*get_flags) __P((DB_ENV *, u_int32_t *));
//  int  (*get_feedback) __P((DB_ENV *, void (**)(DB_ENV *, int, int)));
//  int  (*get_home) __P((DB_ENV *, const char **));
//  int  (*get_intermediate_dir_mode) __P((DB_ENV *, const char **));
//  int  (*get_isalive) __P((DB_ENV *,
//   int (**)(DB_ENV *, pid_t, db_threadid_t, u_int32_t)));
//  int  (*get_lg_bsize) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lg_dir) __P((DB_ENV *, const char **));
//  int  (*get_lg_filemode) __P((DB_ENV *, int *));
//  int  (*get_lg_max) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lg_regionmax) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lk_conflicts) __P((DB_ENV *, const u_int8_t **, int *));
//  int  (*get_lk_detect) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lk_max_lockers) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lk_max_locks) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lk_max_objects) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lk_partitions) __P((DB_ENV *, u_int32_t *));
//  int  (*get_lk_priority) __P((DB_ENV *, u_int32_t, u_int32_t *));
//  int  (*get_lk_tablesize) __P((DB_ENV *, u_int32_t *));
//  int  (*get_memory_init) __P((DB_ENV *, DB_MEM_CONFIG, u_int32_t *));
//  int  (*get_memory_max) __P((DB_ENV *, u_int32_t *, u_int32_t *));
//  int  (*get_metadata_dir) __P((DB_ENV *, const char **));
		int  function(DB_ENV *, int *)get_mp_max_openfd;
		int  function(DB_ENV *, int *, int *)get_mp_max_write;
		int  function(DB_ENV *, size_t *)get_mp_mmapsize;
//  int  (*get_mp_mtxcount) __P((DB_ENV *, u_int32_t *));
//  int  (*get_mp_pagesize) __P((DB_ENV *, u_int32_t *));
//  int  (*get_mp_tablesize) __P((DB_ENV *, u_int32_t *));
//  void (*get_msgcall)
//   __P((DB_ENV *, void (**)(const DB_ENV *, const char *)));
//  void (*get_msgfile) __P((DB_ENV *, FILE **));
//  int  (*get_open_flags) __P((DB_ENV *, u_int32_t *));
		int  function(DB_ENV *, int *)get_shm_key;
//  int  (*get_thread_count) __P((DB_ENV *, u_int32_t *));
//  int  (*get_thread_id_fn)
//   __P((DB_ENV *, void (**)(DB_ENV *, pid_t *, db_threadid_t *)));
//  int  (*get_thread_id_string_fn) __P((DB_ENV *,
//   char *(**)(DB_ENV *, pid_t, db_threadid_t, char *)));
//  int  (*get_timeout) __P((DB_ENV *, db_timeout_t *, u_int32_t));
//  int  (*get_tmp_dir) __P((DB_ENV *, const char **));
//  int  (*get_tx_max) __P((DB_ENV *, u_int32_t *));
//  int  (*get_tx_timestamp) __P((DB_ENV *, time_t *));
//  int  (*get_verbose) __P((DB_ENV *, u_int32_t, int *));
//  int  (*is_bigendian) __P((void));
		int  function(DB_ENV *, u_int32_t , u_int32_t , int *)lock_detect;
		int  function(DB_ENV *, u_int32_t , u_int32_t , DBT *, db_lockmode_t , DB_LOCK *)lock_get;
		int  function(DB_ENV *, u_int32_t *)lock_id;
		int  function(DB_ENV *, u_int32_t )lock_id_free;
		int  function(DB_ENV *, DB_LOCK *)lock_put;
		int  function(DB_ENV *, DB_LOCK_STAT **, u_int32_t )lock_stat;
		int  function(DB_ENV *, u_int32_t )lock_stat_print;
		int  function(DB_ENV *, u_int32_t , u_int32_t , DB_LOCKREQ *, int , DB_LOCKREQ **)lock_vec;
		int  function(DB_ENV *, char ***, u_int32_t )log_archive;
		int  function(DB_ENV *, DB_LOGC **, u_int32_t )log_cursor;
		int  function(DB_ENV *, DB_LSN *, char *, size_t )log_file;
		int  function(DB_ENV *, DB_LSN *)log_flush;
//  int  (*log_get_config) __P((DB_ENV *, u_int32_t, int *));
		int  function(DB_ENV *, DB_TXN *, char *,...)log_printf;
		int  function(DB_ENV *, DB_LSN *, DBT *, u_int32_t )log_put;
//  int  (*log_put_record) __P((DB_ENV *, DB *, DB_TXN *, DB_LSN *,
//   u_int32_t, u_int32_t, u_int32_t, u_int32_t,
//   DB_LOG_RECSPEC *, ...));
//  int  (*log_read_record) __P((DB_ENV *, DB **,
//   void *, void *, DB_LOG_RECSPEC *, u_int32_t, void **));
//  int  (*log_set_config) __P((DB_ENV *, u_int32_t, int));
		int  function(DB_ENV *, DB_LOG_STAT **, u_int32_t )log_stat;
		int  function(DB_ENV *, u_int32_t )log_stat_print;
//  int  (*log_verify) __P((DB_ENV *, const DB_LOG_VERIFY_CONFIG *));
		int  function(DB_ENV *, char *, u_int32_t )lsn_reset;
		int  function(DB_ENV *, DB_MPOOLFILE **, u_int32_t )memp_fcreate;
		int  function(DB_ENV *, int , int  function(DB_ENV *, db_pgno_t , void *, DBT *), int  function(DB_ENV *, db_pgno_t , void *, DBT *))memp_register;
		int  function(DB_ENV *, DB_MPOOL_STAT **, DB_MPOOL_FSTAT ***, u_int32_t )memp_stat;
		int  function(DB_ENV *, u_int32_t )memp_stat_print;
		int  function(DB_ENV *, DB_LSN *)memp_sync;
		int  function(DB_ENV *, int , int *)memp_trickle;
		int  function(DB_ENV *, u_int32_t , db_mutex_t *)mutex_alloc;
		int  function(DB_ENV *, db_mutex_t )mutex_free;
		int  function(DB_ENV *, u_int32_t *)mutex_get_align;
		int  function(DB_ENV *, u_int32_t *)mutex_get_increment;
//  int  (*mutex_get_init) __P((DB_ENV *, u_int32_t *));
		int  function(DB_ENV *, u_int32_t *)mutex_get_max;
		int  function(DB_ENV *, u_int32_t *)mutex_get_tas_spins;
		int  function(DB_ENV *, db_mutex_t )mutex_lock;
		int  function(DB_ENV *, u_int32_t )mutex_set_align;
		int  function(DB_ENV *, u_int32_t )mutex_set_increment;
//  int  (*mutex_set_init) __P((DB_ENV *, u_int32_t));
		int  function(DB_ENV *, u_int32_t )mutex_set_max;
		int  function(DB_ENV *, u_int32_t )mutex_set_tas_spins;
		int  function(DB_ENV *, DB_MUTEX_STAT **, u_int32_t )mutex_stat;
		int  function(DB_ENV *, u_int32_t )mutex_stat_print;
		int  function(DB_ENV *, db_mutex_t )mutex_unlock;
		int  function(DB_ENV *, char *, u_int32_t , int )open;
		int  function(DB_ENV *, char *, u_int32_t )remove;
		int  function(DB_ENV *, int , int , int *, u_int32_t )rep_elect;
		int  function(DB_ENV *)rep_flush;
//  int  (*rep_get_clockskew) __P((DB_ENV *, u_int32_t *, u_int32_t *));
		int  function(DB_ENV *, u_int32_t , int *)rep_get_config;
		int  function(DB_ENV *, u_int32_t *, u_int32_t *)rep_get_limit;
		int  function(DB_ENV *, int *)rep_get_nsites;
		int  function(DB_ENV *, int *)rep_get_priority;
//  int  (*rep_get_request) __P((DB_ENV *, u_int32_t *, u_int32_t *));
		int  function(DB_ENV *, int , u_int32_t *)rep_get_timeout;
		int  function(DB_ENV *, DBT *, DBT *, int *, DB_LSN *)rep_process_message;
//  int  (*rep_set_clockskew) __P((DB_ENV *, u_int32_t, u_int32_t));
		int  function(DB_ENV *, u_int32_t , int )rep_set_config;
		int  function(DB_ENV *, u_int32_t , u_int32_t )rep_set_limit;
		int  function(DB_ENV *, int )rep_set_nsites;
		int  function(DB_ENV *, int )rep_set_priority;
//  int  (*rep_set_request) __P((DB_ENV *, u_int32_t, u_int32_t));
		int  function(DB_ENV *, int , db_timeout_t )rep_set_timeout;
		int  function(DB_ENV *, int , int  function(DB_ENV *, DBT *, DBT *, DB_LSN *, int , u_int32_t ))rep_set_transport;
//  int  (*rep_set_view) __P((DB_ENV *, int (*)(DB_ENV *,
//   const char *, int *, u_int32_t)));
		int  function(DB_ENV *, DBT *, u_int32_t )rep_start;
		int  function(DB_ENV *, DB_REP_STAT **, u_int32_t )rep_stat;
		int  function(DB_ENV *, u_int32_t )rep_stat_print;
		int  function(DB_ENV *, u_int32_t )rep_sync;
//  int  (*repmgr_channel) __P((DB_ENV *, int, DB_CHANNEL **, u_int32_t));
		int  function(DB_ENV *, int *)repmgr_get_ack_policy;
//  int  (*repmgr_get_incoming_queue_max)
//   __P((DB_ENV *, u_int32_t *, u_int32_t *));
//  int  (*repmgr_local_site) __P((DB_ENV *, DB_SITE **));
//  int  (*repmgr_msg_dispatch) __P((DB_ENV *,
//   void (*)(DB_ENV *, DB_CHANNEL *, DBT *, u_int32_t, u_int32_t),
//   u_int32_t));
//  int  (*repmgr_set_ack_policy) __P((DB_ENV *, int));
//  int  (*repmgr_set_incoming_queue_max)
//   __P((DB_ENV *, u_int32_t, u_int32_t));
//  int  (*repmgr_site)
//   __P((DB_ENV *, const char *, u_int, DB_SITE**, u_int32_t));
//  int  (*repmgr_site_by_eid) __P((DB_ENV *, int, DB_SITE**));
		int  function(DB_ENV *, u_int *, DB_REPMGR_SITE **)repmgr_site_list;
//  int  (*repmgr_start) __P((DB_ENV *, int, u_int32_t));
//  int  (*repmgr_stat) __P((DB_ENV *, DB_REPMGR_STAT **, u_int32_t));
//  int  (*repmgr_stat_print) __P((DB_ENV *, u_int32_t));
//  int  (*set_alloc) __P((DB_ENV *, void *(*)(size_t),
//   void *(*)(void *, size_t), void (*)(void *)));
//  int  (*set_app_dispatch)
//   __P((DB_ENV *, int (*)(DB_ENV *, DBT *, DB_LSN *, db_recops)));
//  int  (*set_blob_dir) __P((DB_ENV *, const char *));
//  int  (*set_blob_threshold) __P((DB_ENV *, u_int32_t, u_int32_t));
//  int  (*set_cache_max) __P((DB_ENV *, u_int32_t, u_int32_t));
//  int  (*set_cachesize) __P((DB_ENV *, u_int32_t, u_int32_t, int));
//  int  (*set_create_dir) __P((DB_ENV *, const char *));
//  int  (*set_data_dir) __P((DB_ENV *, const char *));
//  int  (*set_data_len) __P((DB_ENV *, u_int32_t));
//  int  (*set_backup_callbacks) __P((DB_ENV *,
//   int (*)(DB_ENV *, const char *, const char *, void **),
//   int (*)(DB_ENV *, u_int32_t,
//       u_int32_t, u_int32_t, u_int8_t *, void *),
//   int (*)(DB_ENV *, const char *, void *)));
//  int  (*set_backup_config) __P((DB_ENV *, DB_BACKUP_CONFIG, u_int32_t));
//  int  (*set_encrypt) __P((DB_ENV *, const char *, u_int32_t));
//  void (*set_errcall) __P((DB_ENV *,
//   void (*)(const DB_ENV *, const char *, const char *)));
//  void (*set_errfile) __P((DB_ENV *, FILE *));
//  void (*set_errpfx) __P((DB_ENV *, const char *));
//  int  (*set_event_notify)
//   __P((DB_ENV *, void (*)(DB_ENV *, u_int32_t, void *)));
//  int  (*set_feedback) __P((DB_ENV *, void (*)(DB_ENV *, int, int)));
//  int  (*set_flags) __P((DB_ENV *, u_int32_t, int));
//  int  (*set_intermediate_dir_mode) __P((DB_ENV *, const char *));
//  int  (*set_isalive) __P((DB_ENV *,
//   int (*)(DB_ENV *, pid_t, db_threadid_t, u_int32_t)));
//  int  (*set_lg_bsize) __P((DB_ENV *, u_int32_t));
//  int  (*set_lg_dir) __P((DB_ENV *, const char *));
//  int  (*set_lg_filemode) __P((DB_ENV *, int));
//  int  (*set_lg_max) __P((DB_ENV *, u_int32_t));
//  int  (*set_lg_regionmax) __P((DB_ENV *, u_int32_t));
//  int  (*set_lk_conflicts) __P((DB_ENV *, u_int8_t *, int));
//  int  (*set_lk_detect) __P((DB_ENV *, u_int32_t));
//  int  (*set_lk_max_lockers) __P((DB_ENV *, u_int32_t));
//  int  (*set_lk_max_locks) __P((DB_ENV *, u_int32_t));
//  int  (*set_lk_max_objects) __P((DB_ENV *, u_int32_t));
//  int  (*set_lk_partitions) __P((DB_ENV *, u_int32_t));
//  int  (*set_lk_priority) __P((DB_ENV *, u_int32_t, u_int32_t));
//  int  (*set_lk_tablesize) __P((DB_ENV *, u_int32_t));
//  int  (*set_memory_init) __P((DB_ENV *, DB_MEM_CONFIG, u_int32_t));
//  int  (*set_memory_max) __P((DB_ENV *, u_int32_t, u_int32_t));
//  int  (*set_metadata_dir) __P((DB_ENV *, const char *));
//  int  (*set_mp_max_openfd) __P((DB_ENV *, int));
//  int  (*set_mp_max_write) __P((DB_ENV *, int, db_timeout_t));
//  int  (*set_mp_mmapsize) __P((DB_ENV *, size_t));
//  int  (*set_mp_mtxcount) __P((DB_ENV *, u_int32_t));
//  int  (*set_mp_pagesize) __P((DB_ENV *, u_int32_t));
//  int  (*set_mp_tablesize) __P((DB_ENV *, u_int32_t));
//  void (*set_msgcall)
//   __P((DB_ENV *, void (*)(const DB_ENV *, const char *)));
//  void (*set_msgfile) __P((DB_ENV *, FILE *));
//  int  (*set_paniccall) __P((DB_ENV *, void (*)(DB_ENV *, int)));
//  int  (*set_shm_key) __P((DB_ENV *, long));
//  int  (*set_thread_count) __P((DB_ENV *, u_int32_t));
//  int  (*set_thread_id)
//   __P((DB_ENV *, void (*)(DB_ENV *, pid_t *, db_threadid_t *)));
//  int  (*set_thread_id_string) __P((DB_ENV *,
//   char *(*)(DB_ENV *, pid_t, db_threadid_t, char *)));
//  int  (*set_timeout) __P((DB_ENV *, db_timeout_t, u_int32_t));
//  int  (*set_tmp_dir) __P((DB_ENV *, const char *));
//  int  (*set_tx_max) __P((DB_ENV *, u_int32_t));
//  int  (*set_tx_timestamp) __P((DB_ENV *, time_t *));
//  int  (*set_verbose) __P((DB_ENV *, u_int32_t, int));
//  int  (*txn_applied) __P((DB_ENV *,
//   DB_TXN_TOKEN *, db_timeout_t, u_int32_t));
//  int  (*stat_print) __P((DB_ENV *, u_int32_t));
//  int  (*txn_begin) __P((DB_ENV *, DB_TXN *, DB_TXN **, u_int32_t));
//  int  (*txn_checkpoint) __P((DB_ENV *, u_int32_t, u_int32_t, u_int32_t));
//  int  (*txn_recover) __P((DB_ENV *,
//   DB_PREPLIST *, long, long *, u_int32_t));
//  int  (*txn_stat) __P((DB_ENV *, DB_TXN_STAT **, u_int32_t));
//  int  (*txn_stat_print) __P((DB_ENV *, u_int32_t));
//  /* DB_ENV PUBLIC HANDLE LIST END */

//  /* DB_ENV PRIVATE HANDLE LIST BEGIN */
//  int  (*prdbt) __P((DBT *, int, const char *, void *,
//   int (*)(void *, const void *), int, int, int));
//  /* DB_ENV PRIVATE HANDLE LIST END */
	}
+/

/+ struct 555__db_env
	{

			int  function(DBT *, u_int32_t , void *, u_int32_t , u_int32_t )dbt_usercopy;


			_N17 *mutex_iq;
		u_int mutex_iq_next;
		u_int mutex_iq_max;

		db_timeout_t lk_timeout;

		int mp_maxwrite_sleep;



		u_int32_t thr_nbucket;
		void *thr_hashtab;
		pid_t pid_cache;
		char *db_home;

		int db_mode;
		int dir_mode;
		void *env_lref;
		u_int32_t open_flags;
		void *reginfo;
		DB_FH *lockfhp;





		int  function(DB_ENV *, DBT *, DB_LSN *, db_recops , void *)*recover_dtab;
		size_t recover_dtab_size;
		void *cl_handle;
		u_int cl_id;
		int db_ref;

		db_mutex_t mtx_dblist;
		__dblist dblist;
		_N18 links;
		__xa_txn xa_txn;
		int xa_rmid;
		char *passwd;
		size_t passwd_len;
		void *crypto_handle;
		db_mutex_t mtx_mt;
		int mti;
		u_long *mt;
		void *api1_internal;
		void *api2_internal;
		DB_LOCKTAB *lk_handle;
		DB_LOG *lg_handle;
		DB_MPOOL *mp_handle;
		DB_MUTEXMGR *mutex_handle;
		DB_REP *rep_handle;
		DB_TXNMGR *tx_handle;



		int  function(DB_ENV *, u_int32_t *)get_encrypt_flags;
		void  function(DB_ENV *, FILE **)get_errfile;
		void  function(DB_ENV *, char **)get_errpfx;
		int  function(DB_ENV *, u_int32_t *)get_flags;
		int  function(DB_ENV *, char **)get_home;
		int  function(DB_ENV *, u_int32_t *)get_lg_bsize;
		int  function(DB_ENV *, char **)get_lg_dir;
		int  function(DB_ENV *, int *)get_lg_filemode;
		int  function(DB_ENV *, u_int32_t *)get_lg_max;
		int  function(DB_ENV *, u_int32_t *)get_lg_regionmax;
		int  function(DB_ENV *, u_int8_t **, int *)get_lk_conflicts;
		int  function(DB_ENV *, u_int32_t *)get_lk_detect;
		int  function(DB_ENV *, u_int32_t *)get_lk_max_lockers;
		int  function(DB_ENV *, u_int32_t *)get_lk_max_locks;
		int  function(DB_ENV *, u_int32_t *)get_lk_max_objects;

		void  function(DB_ENV *, FILE **)get_msgfile;
		int  function(DB_ENV *, u_int32_t *)get_open_flags;
		int  function(DB_ENV *, db_timeout_t *, u_int32_t )get_timeout;
		int  function(DB_ENV *, char **)get_tmp_dir;
		int  function(DB_ENV *, u_int32_t *)get_tx_max;
		int  function(DB_ENV *, time_t *)get_tx_timestamp;
		int  function(DB_ENV *, u_int32_t , int *)get_verbose;
		int  function()is_bigendian;

		int  function(DB_ENV *, char *, u_int , int *, u_int32_t )repmgr_add_remote_site;
		int  function(DB_ENV *, int )repmgr_set_ack_policy;
		int  function(DB_ENV *, char *, u_int , u_int32_t )repmgr_set_local_site;
		int  function(DB_ENV *, int , u_int32_t )repmgr_start;
		int  function(DB_ENV *, void * function(size_t ), void * function(void *, size_t ), void  function(void *))set_alloc;
		int  function(DB_ENV *, int  function(DB_ENV *, DBT *, DB_LSN *, db_recops ))set_app_dispatch;
		int  function(DB_ENV *, u_int32_t , u_int32_t , int )set_cachesize;
		int  function(DB_ENV *, char *)set_data_dir;
		int  function(DB_ENV *, char *, u_int32_t )set_encrypt;
		void  function(DB_ENV *, void  function(DB_ENV *, char *, char *))set_errcall;
		void  function(DB_ENV *, FILE *)set_errfile;
		void  function(DB_ENV *, char *)set_errpfx;
		int  function(DB_ENV *, void  function(DB_ENV *, u_int32_t , void *))set_event_notify;
		int  function(DB_ENV *, void  function(DB_ENV *, int , int ))set_feedback;
		int  function(DB_ENV *, u_int32_t , int )set_flags;
		int  function(DB_ENV *, int , u_int32_t )set_intermediate_dir;
		int  function(DB_ENV *, int  function(DB_ENV *, pid_t , db_threadid_t , u_int32_t ))set_isalive;
		int  function(DB_ENV *, u_int32_t )set_lg_bsize;
		int  function(DB_ENV *, char *)set_lg_dir;
		int  function(DB_ENV *, int )set_lg_filemode;
		int  function(DB_ENV *, u_int32_t )set_lg_max;
		int  function(DB_ENV *, u_int32_t )set_lg_regionmax;
		int  function(DB_ENV *, u_int8_t *, int )set_lk_conflicts;
		int  function(DB_ENV *, u_int32_t )set_lk_detect;
		int  function(DB_ENV *, u_int32_t )set_lk_max_lockers;
		int  function(DB_ENV *, u_int32_t )set_lk_max_locks;
		int  function(DB_ENV *, u_int32_t )set_lk_max_objects;
		int  function(DB_ENV *, int )set_mp_max_openfd;
		int  function(DB_ENV *, int , int )set_mp_max_write;
		int  function(DB_ENV *, size_t )set_mp_mmapsize;
		void  function(DB_ENV *, void  function(DB_ENV *, char *))set_msgcall;
		void  function(DB_ENV *, FILE *)set_msgfile;
		int  function(DB_ENV *, void  function(DB_ENV *, int ))set_paniccall;
		int  function(DB_ENV *, u_int32_t , u_int32_t )set_rep_request;
		int  function(DB_ENV *, void *, char *, int , int , u_int32_t )set_rpc_server;
		int  function(DB_ENV *, int )set_shm_key;
		int  function(DB_ENV *, u_int32_t )set_thread_count;
		int  function(DB_ENV *, void  function(DB_ENV *, pid_t *, db_threadid_t *))set_thread_id;
		int  function(DB_ENV *, char * function(DB_ENV *, pid_t , db_threadid_t , char *))set_thread_id_string;
		int  function(DB_ENV *, db_timeout_t , u_int32_t )set_timeout;
		int  function(DB_ENV *, char *)set_tmp_dir;
		int  function(DB_ENV *, u_int32_t )set_tx_max;
		int  function(DB_ENV *, time_t *)set_tx_timestamp;
		int  function(DB_ENV *, u_int32_t , int )set_verbose;
		int  function(DB_ENV *, u_int32_t )stat_print;
		int  function(DB_ENV *, DB_TXN *, DB_TXN **, u_int32_t )txn_begin;
		int  function(DB_ENV *, u_int32_t , u_int32_t , u_int32_t )txn_checkpoint;
		int  function(DB_ENV *, DB_PREPLIST *, int , int *, u_int32_t )txn_recover;
		int  function(DB_ENV *, DB_TXN_STAT **, u_int32_t )txn_stat;
		int  function(DB_ENV *, u_int32_t )txn_stat_print;
		int  function(DBT *, int , char *, void *, int  function(void *, void *), int )prdbt;
		int test_abort;
		int test_check;
		int test_copy;
		u_int32_t flags;
	}
+/


	alias heap_set_heapsize = __heap_set_heapsize;
	int __heap_set_heapsize(DB* dbp, u_int32_t gbytes, u_int32_t bytes, u_int32_t flags);


	alias __db_set_flags db_set_flags;
	int __db_set_flags(DB* dbp, u_int32_t flags);
	alias __db_get_flags db_get_flags;
	int	__db_get_flags(DB* dbp, u_int32_t* flagsp);

	// https://docs.oracle.com/database/bdb181/html/api_reference/C/envversion.html
	char* db_version(int*, int*, int*);
	// https://docs.oracle.com/database/bdb181/html/api_reference/C/envfullversion.html
	char* db_full_version(int* family, int* release, int* major, int* minor, int* patch);
}

/*
	* DB access method and cursor operation values.  Each value is an operation
	* code to which additional bit flags are added.
	*/
enum	uint	DB_AFTER		= 1	;/* Dbc.put */
enum	uint	DB_APPEND	= 2	;/* Db.put */
enum	uint	DB_BEFORE		= 3	;/* Dbc.put */
enum	uint	DB_CONSUME		= 4	;/* Db.get */
enum	uint	DB_CONSUME_WAIT		= 5	;/* Db.get */
enum	uint	DB_CURRENT		= 6	;/* Dbc.get, Dbc.put, DbLogc.get */
enum	uint	DB_FIRST		 =7	;/* Dbc.get, DbLogc->get */
enum	uint	DB_GET_BOTH	=	 8	;/* Db.get, Dbc.get */
enum	uint	DB_GET_BOTHC	=	 9	;/* Dbc.get (internal) */
enum	uint	DB_GET_BOTH_RANGE=	10	;/* Db.get, Dbc.get */
enum	uint	DB_GET_RECNO		=11	;/* Dbc.get */
enum	uint	DB_JOIN_ITEM		=12	;/* Dbc.get; don't do primary lookup */
enum	uint	DB_KEYFIRST		=13	;/* Dbc.put */
enum	uint	DB_KEYLAST		=14	;/* Dbc.put */
enum	uint	DB_LAST			=15	;/* Dbc.get, DbLogc->get */
enum	uint DB_NEXT	=	16; /* Dbc.get, DbLogc->get */
enum	uint DB_NEXT_DUP		=17	;/* Dbc.get */
enum	uint DB_NEXT_NODUP	=	18	;/* Dbc.get */
enum	uint DB_NODUPDATA	=	19	;/* Db.put, Dbc.put */
enum	uint DB_NOOVERWRITE	=	20	;/* Db.put */
enum	uint DB_OVERWRITE_DUP	=21	;/* Dbc.put, Db.put; no DB_KEYEXIST */
enum	uint DB_POSITION		=22	;/* Dbc.dup */
enum	uint DB_PREV			=23	;/* Dbc.get, DbLogc->get */
enum	uint DB_PREV_DUP	=	24	;/* Dbc.get */
enum	uint DB_PREV_NODUP	=	25	;/* Dbc.get */
enum	uint DB_SET		=	26	;/* Dbc.get, DbLogc->get */
enum	uint DB_SET_RANGE	=	27	;/* Dbc.get */
enum	uint DB_SET_RECNO	=	28	;/* Db.get, Dbc.get */
enum	uint DB_UPDATE_SECONDARY=	29	;/* Dbc.get, Dbc.del (internal) */
enum	uint DB_SET_LTE	=	30	;/* Dbc.get (internal) */
enum	uint DB_GET_BOTH_LTE	=	31	;/* Dbc.get (internal) */


/+
/* Converted to D from db.h by htod */
module bdb.c.db;
/* DO NOT EDIT: automatically built by dist/s_windows. */
/*
	* See the file LICENSE for redistribution information.
	*
	* Copyright (c) 1996-2006
	*	Oracle Corporation.  All rights reserved.
	*
	* $Id: db.in,v 12.108 2006/09/13 14:53:37 mjc Exp $
	*
	* db.h include file layout:
	*	General.
	*	Database Environment.
	*	Locking subsystem.
	*	Logging subsystem.
	*	Shared buffer cache (mpool) subsystem.
	*	Transaction subsystem.
	*	Access methods.
	*	Access method cursors.
	*	Dbm/Ndbm, Hsearch historic interfaces.
	*/

//C     #ifndef _DB_H_
//C     #define	_DB_H_

//C     #ifndef __NO_SYSTEM_INCLUDES
//C     #include <sys/types.h>
import std.ctype;
//C     #include <stddef.h>
import std.c.stddef;
//C     #include <stdio.h>
import std.c.stdio;
//C     #endif

import std.c.time;

alias long off_t;

/*
	* Turn off inappropriate compiler warnings
	*/
//C     #ifdef _MSC_VER
/*
	* This warning is explicitly disabled in Visual C++ by default.
	* It is necessary to explicitly enable the /Wall flag to generate this
	* warning.
	* Since this is a shared include file it should compile without warnings
	* at the highest warning level, so third party applications can use
	* higher warning levels cleanly.
	*
	* 4820: 'bytes' bytes padding added after member 'member'
	*       The type and order of elements caused the compiler to
	*       add padding to the end of a struct.
	*/
//C     #pragma warning(push)
//C     #pragma warning(disable: 4820)
//C     #endif /* _MSC_VER */
//C     #if defined(__cplusplus)
//C     extern "C" {
//C     #endif


//C     #undef __P
//C     #define	__P(protos)	protos

/*
	* Berkeley DB version information.
	*/
//C     #define	DB_VERSION_MAJOR	4
//C     #define	DB_VERSION_MINOR	5
const DB_VERSION_MAJOR = 4;
//C     #define	DB_VERSION_PATCH	20
const DB_VERSION_MINOR = 5;
//C     #define	DB_VERSION_STRING	"Berkeley DB 4.5.20: (September 20, 2006)"
const DB_VERSION_PATCH = 20;
const char[] DB_VERSION_STRING = "Berkeley DB 4.5.20: (September 20, 2006)";

/*
	* !!!
	* Berkeley DB uses specifically sized types.  If they're not provided by
	* the system, typedef them here.
	*
	* We protect them against multiple inclusion using __BIT_TYPES_DEFINED__,
	* as does BIND and Kerberos, since we don't know for sure what #include
	* files the user is using.
	*
	* !!!
	* We also provide the standard u_int, u_long etc., if they're not provided
	* by the system.
	*/
//C     #ifndef	__BIT_TYPES_DEFINED__
//C     #define	__BIT_TYPES_DEFINED__
//C     typedef unsigned char u_int8_t;
extern (C):
alias ubyte u_int8_t;
//C     typedef short int16_t;
alias short int16_t;
//C     typedef unsigned short u_int16_t;
alias ushort u_int16_t;
//C     typedef int int32_t;
alias int int32_t;
//C     typedef unsigned int u_int32_t;
alias uint u_int32_t;
//C     typedef __int64 int64_t;
alias long int64_t;
//C     typedef unsigned __int64 u_int64_t;
alias ulong u_int64_t;
//C     #endif

//C     #ifndef _WINSOCKAPI_
//C     typedef unsigned char u_char;
alias ubyte u_char;
//C     typedef unsigned short u_short;
alias ushort u_short;
//C     typedef unsigned int u_int;
alias uint u_int;
//C     typedef unsigned long u_long;
alias uint u_long;
//C     #endif
//C     #ifdef _WIN64
//C     typedef int64_t ssize_t;
version(X86_64)
{
	alias long ssi;
}
else
{
//C     #else
//C     typedef int32_t ssize_t;
	alias int32_t ssize_t;
}
//C     #endif

/*
	* uintmax_t --
	* Largest unsigned type, used to align structures in memory.  We don't store
	* floating point types in structures, so integral types should be sufficient
	* (and we don't have to worry about systems that store floats in other than
	* power-of-2 numbers of bytes).  Additionally this fixes compilers that rewrite
	* structure assignments and ANSI C memcpy calls to be in-line instructions
	* that happen to require alignment.
	*
	* uintptr_t --
	* Unsigned type that's the same size as a pointer.  There are places where
	* DB modifies pointers by discarding the bottom bits to guarantee alignment.
	* We can't use uintmax_t, it may be larger than the pointer, and compilers
	* get upset about that.  So far we haven't run on any machine where there's
	* no unsigned type the same size as a pointer -- here's hoping.
	*/
//C     typedef u_int64_t uintmax_t;
alias u_int64_t uintmax_t;
//C     #ifdef _WIN64
//C     typedef u_int64_t uintptr_t;
version(X86_64)
{
	alias u_int64_t uintptr_t;
}
else
{
//C     #else
//C     typedef u_int32_t uintptr_t;
	alias u_int32_t uintptr_t;
}
//C     #endif

/*
	* Sequences are only available on machines with 64-bit integral types.
	*/
//C     typedef int64_t db_seq_t;
alias int64_t db_seq_t;

/* Thread and process identification. */
//C     typedef u_int32_t db_threadid_t;
alias u_int32_t db_threadid_t;
//C     typedef int pid_t;
alias int pid_t;

/* Basic types that are exported or quasi-exported. */
//C     typedef	u_int32_t	db_pgno_t;	/* Page number type. */
alias u_int32_t db_pgno_t;
//C     typedef	u_int16_t	db_indx_t;	/* Page offset type. */
alias u_int16_t db_indx_t;
//C     #define	DB_MAX_PAGES	0xffffffff	/* >= # of pages in a file */

const DB_MAX_PAGES = 0xffffffff;
//C     typedef	u_int32_t	db_recno_t;	/* Record number type. */
alias u_int32_t db_recno_t;
//C     #define	DB_MAX_RECORDS	0xffffffff	/* >= # of records in a tree */

const DB_MAX_RECORDS = 0xffffffff;
//C     typedef u_int32_t	db_timeout_t;	/* Type of a timeout. */
alias u_int32_t db_timeout_t;

/*
	* Region offsets are the difference between a pointer in a region and the
	* region's base address.  With private environments, both addresses are the
	* result of calling malloc, and we can't assume anything about what malloc
	* will return, so region offsets have to be able to hold differences between
	* arbitrary pointers.
	*/
//C     typedef	uintptr_t	roff_t;
alias uintptr_t roff_t;

/*
	* Forward structure declarations, so we can declare pointers and
	* applications can get type checking.
	*/
//C     struct __db;		typedef struct __db DB;
alias __db DB;
//C     struct __db_bt_stat;	typedef struct __db_bt_stat DB_BTREE_STAT;
alias __db_bt_stat DB_BTREE_STAT;
//C     struct __db_cipher;	typedef struct __db_cipher DB_CIPHER;
struct __db_cipher{}
alias __db_cipher DB_CIPHER;
//C     struct __db_compact;	typedef struct __db_compact DB_COMPACT;
alias __db_compact DB_COMPACT;
//C     struct __db_dbt;	typedef struct __db_dbt DBT;
alias __db_dbt DBT;
//C     struct __db_env;	typedef struct __db_env DB_ENV;
alias __db_env DB_ENV;
//C     struct __db_h_stat;	typedef struct __db_h_stat DB_HASH_STAT;
alias __db_h_stat DB_HASH_STAT;
//C     struct __db_ilock;	typedef struct __db_ilock DB_LOCK_ILOCK;
alias __db_ilock DB_LOCK_ILOCK;
//C     struct __db_lock_stat;	typedef struct __db_lock_stat DB_LOCK_STAT;
alias __db_lock_stat DB_LOCK_STAT;
//C     struct __db_lock_u;	typedef struct __db_lock_u DB_LOCK;
alias __db_lock_u DB_LOCK;
//C     struct __db_lockreq;	typedef struct __db_lockreq DB_LOCKREQ;
alias __db_lockreq DB_LOCKREQ;
//C     struct __db_locktab;	typedef struct __db_locktab DB_LOCKTAB;
struct __db_locktab{}
alias __db_locktab DB_LOCKTAB;
//C     struct __db_log;	typedef struct __db_log DB_LOG;
struct __db_log{}
alias __db_log DB_LOG;
//C     struct __db_log_cursor;	typedef struct __db_log_cursor DB_LOGC;
alias __db_log_cursor DB_LOGC;
//C     struct __db_log_stat;	typedef struct __db_log_stat DB_LOG_STAT;
alias __db_log_stat DB_LOG_STAT;
//C     struct __db_lsn;	typedef struct __db_lsn DB_LSN;
alias __db_lsn DB_LSN;
//C     struct __db_mpool;	typedef struct __db_mpool DB_MPOOL;
struct __db_mpool{}
alias __db_mpool DB_MPOOL;
//C     struct __db_mpool_fstat;typedef struct __db_mpool_fstat DB_MPOOL_FSTAT;
alias __db_mpool_fstat DB_MPOOL_FSTAT;
//C     struct __db_mpool_stat;	typedef struct __db_mpool_stat DB_MPOOL_STAT;
alias __db_mpool_stat DB_MPOOL_STAT;
//C     struct __db_mpoolfile;	typedef struct __db_mpoolfile DB_MPOOLFILE;
alias __db_mpoolfile DB_MPOOLFILE;
//C     struct __db_mutex_stat;	typedef struct __db_mutex_stat DB_MUTEX_STAT;
alias __db_mutex_stat DB_MUTEX_STAT;
//C     struct __db_mutex_t;	typedef struct __db_mutex_t DB_MUTEX;
struct __db_mutex_t{}
alias __db_mutex_t DB_MUTEX;
//C     struct __db_mutexmgr;	typedef struct __db_mutexmgr DB_MUTEXMGR;
struct __db_mutexmgr{}
alias __db_mutexmgr DB_MUTEXMGR;
//C     struct __db_preplist;	typedef struct __db_preplist DB_PREPLIST;
alias __db_preplist DB_PREPLIST;
//C     struct __db_qam_stat;	typedef struct __db_qam_stat DB_QUEUE_STAT;
alias __db_qam_stat DB_QUEUE_STAT;
//C     struct __db_rep;	typedef struct __db_rep DB_REP;
struct __db_rep{}
alias __db_rep DB_REP;
//C     struct __db_rep_stat;	typedef struct __db_rep_stat DB_REP_STAT;
alias __db_rep_stat DB_REP_STAT;
//C     struct __db_repmgr_site; 			typedef struct __db_repmgr_site DB_REPMGR_SITE;
alias __db_repmgr_site DB_REPMGR_SITE;
//C     struct __db_seq_record; typedef struct __db_seq_record DB_SEQ_RECORD;
alias __db_seq_record DB_SEQ_RECORD;
//C     struct __db_seq_stat;	typedef struct __db_seq_stat DB_SEQUENCE_STAT;
alias __db_seq_stat DB_SEQUENCE_STAT;
//C     struct __db_sequence;	typedef struct __db_sequence DB_SEQUENCE;
alias __db_sequence DB_SEQUENCE;
//C     struct __db_txn;	typedef struct __db_txn DB_TXN;
alias __db_txn DB_TXN;
//C     struct __db_txn_active;	typedef struct __db_txn_active DB_TXN_ACTIVE;
alias __db_txn_active DB_TXN_ACTIVE;
//C     struct __db_txn_stat;	typedef struct __db_txn_stat DB_TXN_STAT;
alias __db_txn_stat DB_TXN_STAT;
//C     struct __db_txnmgr;	typedef struct __db_txnmgr DB_TXNMGR;
struct __db_txnmgr{}
alias __db_txnmgr DB_TXNMGR;
//C     struct __dbc;		typedef struct __dbc DBC;
alias __dbc DBC;
//C     struct __dbc_internal;	typedef struct __dbc_internal DBC_INTERNAL;
struct __dbc_internal{}
alias __dbc_internal DBC_INTERNAL;
//C     struct __fh_t;		typedef struct __fh_t DB_FH;
struct __fh_t{}
alias __fh_t DB_FH;
//C     struct __fname;		typedef struct __fname FNAME;
struct __fname{}
alias __fname FNAME;
//C     struct __key_range;	typedef struct __key_range DB_KEY_RANGE;
alias __key_range DB_KEY_RANGE;
//C     struct __mpoolfile;	typedef struct __mpoolfile MPOOLFILE;
struct __mpoolfile{}
alias __mpoolfile MPOOLFILE;

/* Key/data structure -- a Data-Base Thang. */
//C     struct __db_dbt {
//C     	void	 *data;			/* Key/data */
//C     	u_int32_t size;			/* key/data length */

//C     	u_int32_t ulen;			/* RO: length of user buffer. */
//C     	u_int32_t dlen;			/* RO: get/put record length. */
//C     	u_int32_t doff;			/* RO: get/put record offset. */

//C     	void *app_data;

//C     #define	DB_DBT_APPMALLOC	0x001	/* Callback allocated memory. */
//C     #define	DB_DBT_ISSET		0x002	/* Lower level calls set value. */
const DB_DBT_APPMALLOC = 0x001;
//C     #define	DB_DBT_MALLOC		0x004	/* Return in malloc'd memory. */
const DB_DBT_ISSET = 0x002;
//C     #define	DB_DBT_PARTIAL		0x008	/* Partial put/get. */
const DB_DBT_MALLOC = 0x004;
//C     #define	DB_DBT_REALLOC		0x010	/* Return in realloc'd memory. */
const DB_DBT_PARTIAL = 0x008;
//C     #define	DB_DBT_USERCOPY		0x020	/* Use the user-supplied callback. */
const DB_DBT_REALLOC = 0x010;
//C     #define	DB_DBT_USERMEM		0x040	/* Return in user's memory. */
const DB_DBT_USERCOPY = 0x020;
//C     #define	DB_DBT_DUPOK		0x080	/* Insert if duplicate. */
const DB_DBT_USERMEM = 0x040;
//C     	u_int32_t flags;
const DB_DBT_DUPOK = 0x080;
//C     };
struct __db_dbt
{
				void *data;
				u_int32_t size;
				u_int32_t ulen;
				u_int32_t dlen;
				u_int32_t doff;
				void *app_data;
				u_int32_t flags;
}

/*
	* Common flags --
	*	Interfaces which use any of these common flags should never have
	*	interface specific flags in this range.
	*/
//C     #define	DB_CREATE	      0x0000001	/* Create file as necessary. */
//C     #define	DB_DURABLE_UNKNOWN    0x0000002 /* Durability on open (internal). */
const DB_CREATE = 0x0000001;
//C     #define	DB_FORCE	      0x0000004	/* Force (anything). */
const DB_DURABLE_UNKNOWN = 0x0000002;
//C     #define	DB_MULTIVERSION	      0x0000008 /* Multiversion concurrency control. */
const DB_FORCE = 0x0000004;
//C     #define	DB_NOMMAP	      0x0000010	/* Don't mmap underlying file. */
const DB_MULTIVERSION = 0x0000008;
//C     #define	DB_RDONLY	      0x0000020	/* Read-only (O_RDONLY). */
const DB_NOMMAP = 0x0000010;
//C     #define	DB_RECOVER	      0x0000040	/* Run normal recovery. */
const DB_RDONLY = 0x0000020;
//C     #define	DB_THREAD	      0x0000080	/* Applications are threaded. */
const DB_RECOVER = 0x0000040;
//C     #define	DB_TRUNCATE	      0x0000100	/* Discard existing DB (O_TRUNC). */
const DB_THREAD = 0x0000080;
//C     #define	DB_TXN_NOSYNC	      0x0000200	/* Do not sync log on commit. */
const DB_TRUNCATE = 0x0000100;
//C     #define	DB_TXN_NOT_DURABLE    0x0000400	/* Do not log changes. */
const DB_TXN_NOSYNC = 0x0000200;
//C     #define	DB_TXN_WRITE_NOSYNC   0x0000800	/* Write the log but don't sync. */
const DB_TXN_NOT_DURABLE = 0x0000400;
//C     #define	DB_USE_ENVIRON	      0x0001000	/* Use the environment. */
const DB_TXN_WRITE_NOSYNC = 0x0000800;
//C     #define	DB_USE_ENVIRON_ROOT   0x0002000	/* Use the environment if root. */
const DB_USE_ENVIRON = 0x0001000;

const DB_USE_ENVIRON_ROOT = 0x0002000;
/*
	* Common flags --
	*	Interfaces which use any of these common flags should never have
	*	interface specific flags in this range.
	*
	* DB_AUTO_COMMIT:
	*	DB_ENV->set_flags, DB->open
	*      (Note: until the 4.3 release, legal to DB->associate, DB->del,
	*	DB->put, DB->remove, DB->rename and DB->truncate, and others.)
	* DB_READ_COMMITTED:
	*	DB->cursor, DB->get, DB->join, DBcursor->c_get, DB_ENV->txn_begin
	* DB_READ_UNCOMMITTED:
	*	DB->cursor, DB->get, DB->join, DB->open, DBcursor->c_get,
	*	DB_ENV->txn_begin
	* DB_TXN_SNAPSHOT:
	*	DB_ENV->set_flags, DB_ENV->txn_begin, DB->cursor
	*
	* !!!
	* The DB_READ_COMMITTED and DB_READ_UNCOMMITTED bit masks can't be changed
	* without also changing the masks for the flags that can be OR'd into DB
	* access method and cursor operation values.
	*/
//C     #define	DB_AUTO_COMMIT	      0x02000000/* Implied transaction. */

const DB_AUTO_COMMIT = 0x02000000;
//C     #define	DB_READ_COMMITTED     0x04000000/* Degree 2 isolation. */
//C     #define	DB_DEGREE_2	      0x04000000/*	Historic name. */
const DB_READ_COMMITTED = 0x04000000;

const DB_DEGREE_2 = 0x04000000;
//C     #define	DB_READ_UNCOMMITTED   0x08000000/* Degree 1 isolation. */
//C     #define	DB_DIRTY_READ	      0x08000000/*	Historic name. */
const DB_READ_UNCOMMITTED = 0x08000000;

const DB_DIRTY_READ = 0x08000000;
//C     #define	DB_TXN_SNAPSHOT	      0x10000000/* Snapshot isolation. */

const DB_TXN_SNAPSHOT = 0x10000000;
/*
	* Flags common to db_env_create and db_create.
	*/
//C     #define	DB_CXX_NO_EXCEPTIONS  0x0000001	/* C++: return error values. */

const DB_CXX_NO_EXCEPTIONS = 0x0000001;
/*
	* Flags private to db_env_create.
	*	   Shared flags up to 0x0000001 */
//C     #define	DB_RPCCLIENT	      0x0000002	/* An RPC client environment. */

const DB_RPCCLIENT = 0x0000002;
/*
	* Flags private to db_create.
	*	   Shared flags up to 0x0000001 */
//C     #define	DB_XA_CREATE	      0x0000002	/* Open in an XA environment. */

const DB_XA_CREATE = 0x0000002;
/*
	* Flags private to DB_ENV->open.
	*	   Shared flags up to 0x0002000 */
//C     #define	DB_INIT_CDB	      0x0004000	/* Concurrent Access Methods. */
//C     #define	DB_INIT_LOCK	      0x0008000	/* Initialize locking. */
const DB_INIT_CDB = 0x0004000;
//C     #define	DB_INIT_LOG	      0x0010000	/* Initialize logging. */
const DB_INIT_LOCK = 0x0008000;
//C     #define	DB_INIT_MPOOL	      0x0020000	/* Initialize mpool. */
const DB_INIT_LOG = 0x0010000;
//C     #define	DB_INIT_REP	      0x0040000	/* Initialize replication. */
const DB_INIT_MPOOL = 0x0020000;
//C     #define	DB_INIT_TXN	      0x0080000	/* Initialize transactions. */
const DB_INIT_REP = 0x0040000;
//C     #define	DB_LOCKDOWN	      0x0100000	/* Lock memory into physical core. */
const DB_INIT_TXN = 0x0080000;
//C     #define	DB_PRIVATE	      0x0200000	/* DB_ENV is process local. */
const DB_LOCKDOWN = 0x0100000;
//C     #define	DB_RECOVER_FATAL      0x0400000	/* Run catastrophic recovery. */
const DB_PRIVATE = 0x0200000;
//C     #define	DB_REGISTER	      0x0800000	/* Multi-process registry. */
const DB_RECOVER_FATAL = 0x0400000;
//C     #define	DB_SYSTEM_MEM	      0x1000000	/* Use system-backed memory. */
const DB_REGISTER = 0x0800000;

const DB_SYSTEM_MEM = 0x1000000;
//C     #define	DB_JOINENV	      0x0	/* Compatibility. */

const DB_JOINENV = 0x0;
/*
	* Flags private to DB->open.
	*	   Shared flags up to 0x0002000 */
//C     #define	DB_EXCL		      0x0004000	/* Exclusive open (O_EXCL). */
//C     #define	DB_FCNTL_LOCKING      0x0008000	/* UNDOC: fcntl(2) locking. */
const DB_EXCL = 0x0004000;
//C     #define	DB_NO_AUTO_COMMIT     0x0010000	/* Override env-wide AUTOCOMMIT. */
const DB_FCNTL_LOCKING = 0x0008000;
//C     #define	DB_RDWRMASTER	      0x0020000	/* UNDOC: allow subdb master open R/W */
const DB_NO_AUTO_COMMIT = 0x0010000;
//C     #define	DB_WRITEOPEN	      0x0040000	/* UNDOC: open with write lock. */
const DB_RDWRMASTER = 0x0020000;

const DB_WRITEOPEN = 0x0040000;
/*
	* Flags private to DB->associate.
	*	   Shared flags up to 0x0002000 */
//C     #define	DB_IMMUTABLE_KEY      0x0004000	/* Secondary key is immutable. */
/*	      Shared flags at 0x1000000 */
const DB_IMMUTABLE_KEY = 0x0004000;

/*
	* Flags private to DB_ENV->txn_begin.
	*	   Shared flags up to 0x0002000 */
//C     #define	DB_TXN_NOWAIT	      0x0004000	/* Do not wait for locks in this TXN. */
//C     #define	DB_TXN_SYNC	      0x0008000	/* Always sync log on commit. */
const DB_TXN_NOWAIT = 0x0004000;

const DB_TXN_SYNC = 0x0008000;
/*
	* Flags private to DB_ENV->set_encrypt.
	*/
//C     #define	DB_ENCRYPT_AES	      0x0000001	/* AES, assumes SHA1 checksum */

const DB_ENCRYPT_AES = 0x0000001;
/*
	* Flags private to DB_ENV->set_flags.
	*	   Shared flags up to 0x00002000 */
//C     #define	DB_CDB_ALLDB	      0x00004000/* Set CDB locking per environment. */
//C     #define	DB_DIRECT_DB	      0x00008000/* Don't buffer databases in the OS. */
const DB_CDB_ALLDB = 0x00004000;
//C     #define	DB_DIRECT_LOG	      0x00010000/* Don't buffer log files in the OS. */
const DB_DIRECT_DB = 0x00008000;
//C     #define	DB_DSYNC_DB	      0x00020000/* Set O_DSYNC on the databases. */
const DB_DIRECT_LOG = 0x00010000;
//C     #define	DB_DSYNC_LOG	      0x00040000/* Set O_DSYNC on the log. */
const DB_DSYNC_DB = 0x00020000;
//C     #define	DB_LOG_AUTOREMOVE     0x00080000/* Automatically remove log files. */
const DB_DSYNC_LOG = 0x00040000;
//C     #define	DB_LOG_INMEMORY       0x00100000/* Store logs in buffers in memory. */
const DB_LOG_AUTOREMOVE = 0x00080000;
//C     #define	DB_NOLOCKING	      0x00200000/* Set locking/mutex behavior. */
const DB_LOG_INMEMORY = 0x00100000;
//C     #define	DB_NOPANIC	      0x00400000/* Set panic state per DB_ENV. */
const DB_NOLOCKING = 0x00200000;
//C     #define	DB_OVERWRITE	      0x00800000/* Overwrite unlinked region files. */
const DB_NOPANIC = 0x00400000;
//C     #define	DB_PANIC_ENVIRONMENT  0x01000000/* Set panic state per environment. */
const DB_OVERWRITE = 0x00800000;
/*	      Shared flags at 0x02000000 */
const DB_PANIC_ENVIRONMENT = 0x01000000;
/*	      Shared flags at 0x04000000 */
/*	      Shared flags at 0x08000000 */
/*	      Shared flags at 0x10000000 */
//C     #define	DB_REGION_INIT	      0x20000000/* Page-fault regions on open. */
//C     #define	DB_TIME_NOTGRANTED    0x40000000/* Return NOTGRANTED on timeout. */
const DB_REGION_INIT = 0x20000000;
//C     #define	DB_YIELDCPU	      0x80000000/* Yield the CPU (a lot). */
const DB_TIME_NOTGRANTED = 0x40000000;

const DB_YIELDCPU = 0x80000000;
/*
	* Flags private to DB->set_feedback's callback.
	*/
//C     #define	DB_UPGRADE	      0x0000001	/* Upgrading. */
//C     #define	DB_VERIFY	      0x0000002	/* Verifying. */
const DB_UPGRADE = 0x0000001;

const DB_VERIFY = 0x0000002;
/*
	* Flags private to DB->compact.
	*	   Shared flags up to 0x00002000
	*/
//C     #define	DB_FREELIST_ONLY      0x00004000 /* Just sort and truncate. */
//C     #define	DB_FREE_SPACE         0x00008000 /* Free space . */
const DB_FREELIST_ONLY = 0x00004000;
//C     #define	DB_COMPACT_FLAGS            (DB_FREELIST_ONLY | DB_FREE_SPACE)
const DB_FREE_SPACE = 0x00008000;

/*
	* Flags private to DB_MPOOLFILE->open.
	*	   Shared flags up to 0x0002000 */
//C     #define	DB_DIRECT	      0x0004000	/* Don't buffer the file in the OS. */
//C     #define	DB_EXTENT	      0x0008000	/* internal: dealing with an extent. */
const DB_DIRECT = 0x0004000;
//C     #define	DB_ODDFILESIZE	      0x0010000	/* Truncate file to N * pgsize. */
const DB_EXTENT = 0x0008000;

const DB_ODDFILESIZE = 0x0010000;
/*
	* Flags private to DB->set_flags.
	*	   Shared flags up to 0x00002000 */
//C     #define	DB_CHKSUM	      0x00004000 /* Do checksumming */
//C     #define	DB_DUP		      0x00008000 /* Btree, Hash: duplicate keys. */
const DB_CHKSUM = 0x00004000;
//C     #define	DB_DUPSORT	      0x00010000 /* Btree, Hash: duplicate keys. */
const DB_DUP = 0x00008000;
//C     #define	DB_ENCRYPT	      0x00020000 /* Btree, Hash: duplicate keys. */
const DB_DUPSORT = 0x00010000;
//C     #define	DB_INORDER	      0x00040000 /* Queue: strict ordering on consume */
const DB_ENCRYPT = 0x00020000;
//C     #define	DB_RECNUM	      0x00080000 /* Btree: record numbers. */
const DB_INORDER = 0x00040000;
//C     #define	DB_RENUMBER	      0x00100000 /* Recno: renumber on insert/delete. */
const DB_RECNUM = 0x00080000;
//C     #define	DB_REVSPLITOFF	      0x00200000 /* Btree: turn off reverse splits. */
const DB_RENUMBER = 0x00100000;
//C     #define	DB_SNAPSHOT	      0x00400000 /* Recno: snapshot the input. */
const DB_REVSPLITOFF = 0x00200000;

const DB_SNAPSHOT = 0x00400000;
/*
	* Flags private to the DB_ENV->stat_print, DB->stat and DB->stat_print methods.
	*/
//C     #define	DB_FAST_STAT	      0x0000001 /* Don't traverse the database. */
//C     #define	DB_STAT_ALL	      0x0000002	/* Print: Everything. */
const DB_FAST_STAT = 0x0000001;
//C     #define	DB_STAT_CLEAR	      0x0000004	/* Clear stat after returning values. */
const DB_STAT_ALL = 0x0000002;
//C     #define	DB_STAT_LOCK_CONF     0x0000008	/* Print: Lock conflict matrix. */
const DB_STAT_CLEAR = 0x0000004;
//C     #define	DB_STAT_LOCK_LOCKERS  0x0000010	/* Print: Lockers. */
const DB_STAT_LOCK_CONF = 0x0000008;
//C     #define	DB_STAT_LOCK_OBJECTS  0x0000020	/* Print: Lock objects. */
const DB_STAT_LOCK_LOCKERS = 0x0000010;
//C     #define	DB_STAT_LOCK_PARAMS   0x0000040	/* Print: Lock parameters. */
const DB_STAT_LOCK_OBJECTS = 0x0000020;
//C     #define	DB_STAT_MEMP_HASH     0x0000080	/* Print: Mpool hash buckets. */
const DB_STAT_LOCK_PARAMS = 0x0000040;
//C     #define	DB_STAT_NOERROR       0x0000100 /* Internal: continue on error. */
const DB_STAT_MEMP_HASH = 0x0000080;
//C     #define	DB_STAT_SUBSYSTEM     0x0000200 /* Print: Subsystems too. */
const DB_STAT_NOERROR = 0x0000100;

const DB_STAT_SUBSYSTEM = 0x0000200;
/*
	* Flags private to DB->join.
	*/
//C     #define	DB_JOIN_NOSORT	      0x0000001	/* Don't try to optimize join. */

const DB_JOIN_NOSORT = 0x0000001;
/*
	* Flags private to DB->verify.
	*/
//C     #define	DB_AGGRESSIVE	      0x0000001	/* Salvage whatever could be data.*/
//C     #define	DB_NOORDERCHK	      0x0000002	/* Skip sort order/hashing check. */
const DB_AGGRESSIVE = 0x0000001;
//C     #define	DB_ORDERCHKONLY	      0x0000004	/* Only perform the order check. */
const DB_NOORDERCHK = 0x0000002;
//C     #define	DB_PR_PAGE	      0x0000008	/* Show page contents (-da). */
const DB_ORDERCHKONLY = 0x0000004;
//C     #define	DB_PR_RECOVERYTEST    0x0000010	/* Recovery test (-dr). */
const DB_PR_PAGE = 0x0000008;
//C     #define	DB_PRINTABLE	      0x0000020	/* Use printable format for salvage. */
const DB_PR_RECOVERYTEST = 0x0000010;
//C     #define	DB_SALVAGE	      0x0000040	/* Salvage what looks like data. */
const DB_PRINTABLE = 0x0000020;
//C     #define	DB_UNREF	      0x0000080	/* Report unreferenced pages. */
const DB_SALVAGE = 0x0000040;
/*
const DB_UNREF = 0x0000080;
	* !!!
	* These must not go over 0x8000, or they will collide with the flags
	* used by __bam_vrfy_subtree.
	*/

/*
	* Flags private to DB->rep_set_transport's send callback.
	*/
//C     #define	DB_REP_ANYWHERE	      0x0000001	/* Message can be serviced anywhere. */
//C     #define	DB_REP_NOBUFFER	      0x0000002	/* Do not buffer this message. */
const DB_REP_ANYWHERE = 0x0000001;
//C     #define	DB_REP_PERMANENT      0x0000004	/* Important--app. may want to flush. */
const DB_REP_NOBUFFER = 0x0000002;
//C     #define	DB_REP_REREQUEST      0x0000008	/* This msg already been requested. */
const DB_REP_PERMANENT = 0x0000004;

const DB_REP_REREQUEST = 0x0000008;
/*******************************************************
	* Mutexes.
	*******************************************************/
//C     typedef u_int32_t	db_mutex_t;
alias u_int32_t db_mutex_t;

/*
	* Flag arguments for DbEnv.mutex_alloc, DbEnv.is_alive and for the
	* DB_MUTEX structure.
	*/
//C     #define	DB_MUTEX_ALLOCATED	0x01	/* Mutex currently allocated. */
//C     #define	DB_MUTEX_LOCKED		0x02	/* Mutex currently locked. */
const DB_MUTEX_ALLOCATED = 0x01;
//C     #define	DB_MUTEX_LOGICAL_LOCK	0x04	/* Mutex backs a database lock. */
const DB_MUTEX_LOCKED = 0x02;
//C     #define	DB_MUTEX_PROCESS_ONLY	0x08	/* Mutex private to a process. */
const DB_MUTEX_LOGICAL_LOCK = 0x04;
//C     #define	DB_MUTEX_SELF_BLOCK	0x10	/* Must be able to block self. */
const DB_MUTEX_PROCESS_ONLY = 0x08;

const DB_MUTEX_SELF_BLOCK = 0x10;
//C     struct __db_mutex_stat {
	/* The following fields are maintained in the region's copy. */
//C     	u_int32_t st_mutex_align;	/* Mutex alignment */
//C     	u_int32_t st_mutex_tas_spins;	/* Mutex test-and-set spins */
//C     	u_int32_t st_mutex_cnt;		/* Mutex count */
//C     	u_int32_t st_mutex_free;	/* Available mutexes */
//C     	u_int32_t st_mutex_inuse;	/* Mutexes in use */
//C     	u_int32_t st_mutex_inuse_max;	/* Maximum mutexes ever in use */

	/* The following fields are filled-in from other places. */
//C     	u_int32_t st_region_wait;	/* Region lock granted after wait. */
//C     	u_int32_t st_region_nowait;	/* Region lock granted without wait. */
//C     	roff_t	  st_regsize;		/* Region size. */
//C     };
struct __db_mutex_stat
{
				u_int32_t st_mutex_align;
				u_int32_t st_mutex_tas_spins;
				u_int32_t st_mutex_cnt;
				u_int32_t st_mutex_free;
				u_int32_t st_mutex_inuse;
				u_int32_t st_mutex_inuse_max;
				u_int32_t st_region_wait;
				u_int32_t st_region_nowait;
				roff_t st_regsize;
}

/* This is the length of the buffer passed to DB_ENV->thread_id_string() */
//C     #define	DB_THREADID_STRLEN	128

const DB_THREADID_STRLEN = 128;
/*******************************************************
	* Locking.
	*******************************************************/
//C     #define	DB_LOCKVERSION	1

const DB_LOCKVERSION = 1;
//C     #define	DB_FILE_ID_LEN		20	/* Unique file ID length. */

const DB_FILE_ID_LEN = 20;
/*
	* Deadlock detector modes; used in the DB_ENV structure to configure the
	* locking subsystem.
	*/
//C     #define	DB_LOCK_NORUN		0
//C     #define	DB_LOCK_DEFAULT		1	/* Default policy. */
const DB_LOCK_NORUN = 0;
//C     #define	DB_LOCK_EXPIRE		2	/* Only expire locks, no detection. */
const DB_LOCK_DEFAULT = 1;
//C     #define	DB_LOCK_MAXLOCKS	3	/* Select locker with max locks. */
const DB_LOCK_EXPIRE = 2;
//C     #define	DB_LOCK_MAXWRITE	4	/* Select locker with max writelocks. */
const DB_LOCK_MAXLOCKS = 3;
//C     #define	DB_LOCK_MINLOCKS	5	/* Select locker with min locks. */
const DB_LOCK_MAXWRITE = 4;
//C     #define	DB_LOCK_MINWRITE	6	/* Select locker with min writelocks. */
const DB_LOCK_MINLOCKS = 5;
//C     #define	DB_LOCK_OLDEST		7	/* Select oldest locker. */
const DB_LOCK_MINWRITE = 6;
//C     #define	DB_LOCK_RANDOM		8	/* Select random locker. */
const DB_LOCK_OLDEST = 7;
//C     #define	DB_LOCK_YOUNGEST	9	/* Select youngest locker. */
const DB_LOCK_RANDOM = 8;

const DB_LOCK_YOUNGEST = 9;
/* Flag values for lock_vec(), lock_get(). */
//C     #define	DB_LOCK_ABORT		0x001	/* Internal: Lock during abort. */
//C     #define	DB_LOCK_NOWAIT		0x002	/* Don't wait on unavailable lock. */
const DB_LOCK_ABORT = 0x001;
//C     #define	DB_LOCK_RECORD		0x004	/* Internal: record lock. */
const DB_LOCK_NOWAIT = 0x002;
//C     #define	DB_LOCK_SET_TIMEOUT	0x008	/* Internal: set lock timeout. */
const DB_LOCK_RECORD = 0x004;
//C     #define	DB_LOCK_SWITCH		0x010	/* Internal: switch existing lock. */
const DB_LOCK_SET_TIMEOUT = 0x008;
//C     #define	DB_LOCK_UPGRADE		0x020	/* Internal: upgrade existing lock. */
const DB_LOCK_SWITCH = 0x010;

const DB_LOCK_UPGRADE = 0x020;
/*
	* Simple R/W lock modes and for multi-granularity intention locking.
	*
	* !!!
	* These values are NOT random, as they are used as an index into the lock
	* conflicts arrays, i.e., DB_LOCK_IWRITE must be == 3, and DB_LOCK_IREAD
	* must be == 4.
	*/
//C     typedef enum {
//C     	DB_LOCK_NG=0,			/* Not granted. */
//C     	DB_LOCK_READ=1,			/* Shared/read. */
//C     	DB_LOCK_WRITE=2,		/* Exclusive/write. */
//C     	DB_LOCK_WAIT=3,			/* Wait for event */
//C     	DB_LOCK_IWRITE=4,		/* Intent exclusive/write. */
//C     	DB_LOCK_IREAD=5,		/* Intent to share/read. */
//C     	DB_LOCK_IWR=6,			/* Intent to read and write. */
//C     	DB_LOCK_READ_UNCOMMITTED=7,	/* Degree 1 isolation. */
//C     	DB_LOCK_WWRITE=8		/* Was Written. */
//C     } db_lockmode_t;
enum
{
				DB_LOCK_NG,
				DB_LOCK_READ,
				DB_LOCK_WRITE,
				DB_LOCK_WAIT,
				DB_LOCK_IWRITE,
				DB_LOCK_IREAD,
				DB_LOCK_IWR,
				DB_LOCK_READ_UNCOMMITTED,
				DB_LOCK_WWRITE,
}
alias int db_lockmode_t;

/*
	* Request types.
	*/
//C     typedef enum {
//C     	DB_LOCK_DUMP=0,			/* Display held locks. */
//C     	DB_LOCK_GET=1,			/* Get the lock. */
//C     	DB_LOCK_GET_TIMEOUT=2,		/* Get lock with a timeout. */
//C     	DB_LOCK_INHERIT=3,		/* Pass locks to parent. */
//C     	DB_LOCK_PUT=4,			/* Release the lock. */
//C     	DB_LOCK_PUT_ALL=5,		/* Release locker's locks. */
//C     	DB_LOCK_PUT_OBJ=6,		/* Release locker's locks on obj. */
//C     	DB_LOCK_PUT_READ=7,		/* Release locker's read locks. */
//C     	DB_LOCK_TIMEOUT=8,		/* Force a txn to timeout. */
//C     	DB_LOCK_TRADE=9,		/* Trade locker ids on a lock. */
//C     	DB_LOCK_UPGRADE_WRITE=10	/* Upgrade writes for dirty reads. */
//C     } db_lockop_t;
enum
{
				DB_LOCK_DUMP,
				DB_LOCK_GET,
				DB_LOCK_GET_TIMEOUT,
				DB_LOCK_INHERIT,
				DB_LOCK_PUT,
				DB_LOCK_PUT_ALL,
				DB_LOCK_PUT_OBJ,
				DB_LOCK_PUT_READ,
				DB_LOCK_TIMEOUT,
				DB_LOCK_TRADE,
				DB_LOCK_UPGRADE_WRITE,
}
alias int db_lockop_t;

/*
	* Status of a lock.
	*/
//C     typedef enum  {
//C     	DB_LSTAT_ABORTED=1,		/* Lock belongs to an aborted txn. */
//C     	DB_LSTAT_EXPIRED=2,		/* Lock has expired. */
//C     	DB_LSTAT_FREE=3,		/* Lock is unallocated. */
//C     	DB_LSTAT_HELD=4,		/* Lock is currently held. */
//C     	DB_LSTAT_PENDING=5,
/* Lock was waiting and has been
						* promoted; waiting for the owner
						* to run and upgrade it to held. */
//C     	DB_LSTAT_WAITING=6		/* Lock is on the wait queue. */
//C     }db_status_t;
enum
{
				DB_LSTAT_ABORTED = 1,
				DB_LSTAT_EXPIRED,
				DB_LSTAT_FREE,
				DB_LSTAT_HELD,
				DB_LSTAT_PENDING,
				DB_LSTAT_WAITING,
}
alias int db_status_t;

/* Lock statistics structure. */
//C     struct __db_lock_stat {
//C     	u_int32_t st_id;		/* Last allocated locker ID. */
//C     	u_int32_t st_cur_maxid;		/* Current maximum unused ID. */
//C     	u_int32_t st_maxlocks;		/* Maximum number of locks in table. */
//C     	u_int32_t st_maxlockers;	/* Maximum num of lockers in table. */
//C     	u_int32_t st_maxobjects;	/* Maximum num of objects in table. */
//C     	int	  st_nmodes;		/* Number of lock modes. */
//C     	u_int32_t st_nlocks;		/* Current number of locks. */
//C     	u_int32_t st_maxnlocks;		/* Maximum number of locks so far. */
//C     	u_int32_t st_nlockers;		/* Current number of lockers. */
//C     	u_int32_t st_maxnlockers;	/* Maximum number of lockers so far. */
//C     	u_int32_t st_nobjects;		/* Current number of objects. */
//C     	u_int32_t st_maxnobjects;	/* Maximum number of objects so far. */
//C     	u_int32_t st_nrequests;		/* Number of lock gets. */
//C     	u_int32_t st_nreleases;		/* Number of lock puts. */
//C     	u_int32_t st_nupgrade;		/* Number of lock upgrades. */
//C     	u_int32_t st_ndowngrade;	/* Number of lock downgrades. */
//C     	u_int32_t st_lock_wait;		/* Lock conflicts w/ subsequent wait */
//C     	u_int32_t st_lock_nowait;	/* Lock conflicts w/o subsequent wait */
//C     	u_int32_t st_ndeadlocks;	/* Number of lock deadlocks. */
//C     	db_timeout_t st_locktimeout;	/* Lock timeout. */
//C     	u_int32_t st_nlocktimeouts;	/* Number of lock timeouts. */
//C     	db_timeout_t st_txntimeout;	/* Transaction timeout. */
//C     	u_int32_t st_ntxntimeouts;	/* Number of transaction timeouts. */
//C     	u_int32_t st_region_wait;	/* Region lock granted after wait. */
//C     	u_int32_t st_region_nowait;	/* Region lock granted without wait. */
//C     	roff_t	  st_regsize;		/* Region size. */
//C     };
struct __db_lock_stat
{
				u_int32_t st_id;
				u_int32_t st_cur_maxid;
				u_int32_t st_maxlocks;
				u_int32_t st_maxlockers;
				u_int32_t st_maxobjects;
				int st_nmodes;
				u_int32_t st_nlocks;
				u_int32_t st_maxnlocks;
				u_int32_t st_nlockers;
				u_int32_t st_maxnlockers;
				u_int32_t st_nobjects;
				u_int32_t st_maxnobjects;
				u_int32_t st_nrequests;
				u_int32_t st_nreleases;
				u_int32_t st_nupgrade;
				u_int32_t st_ndowngrade;
				u_int32_t st_lock_wait;
				u_int32_t st_lock_nowait;
				u_int32_t st_ndeadlocks;
				db_timeout_t st_locktimeout;
				u_int32_t st_nlocktimeouts;
				db_timeout_t st_txntimeout;
				u_int32_t st_ntxntimeouts;
				u_int32_t st_region_wait;
				u_int32_t st_region_nowait;
				roff_t st_regsize;
}

/*
	* DB_LOCK_ILOCK --
	*	Internal DB access method lock.
	*/
//C     struct __db_ilock {
//C     	db_pgno_t pgno;			/* Page being locked. */
//C     	u_int8_t fileid[DB_FILE_ID_LEN];/* File id. */
//C     #define	DB_HANDLE_LOCK	1
//C     #define	DB_RECORD_LOCK	2
const DB_HANDLE_LOCK = 1;
//C     #define	DB_PAGE_LOCK	3
const DB_RECORD_LOCK = 2;
//C     	u_int32_t type;			/* Type of lock. */
const DB_PAGE_LOCK = 3;
//C     };
struct __db_ilock
{
				db_pgno_t pgno;
				u_int8_t [20]fileid;
				u_int32_t type;
}

/*
	* DB_LOCK --
	*	The structure is allocated by the caller and filled in during a
	*	lock_get request (or a lock_vec/DB_LOCK_GET).
	*/
//C     struct __db_lock_u {
//C     	roff_t		off;		/* Offset of the lock in the region */
//C     	u_int32_t	ndx;
/* Index of the object referenced by
						* this lock; used for locking. */
//C     	u_int32_t	gen;		/* Generation number of this lock. */
//C     	db_lockmode_t	mode;		/* mode of this lock. */
//C     };
struct __db_lock_u
{
				roff_t off;
				u_int32_t ndx;
				u_int32_t gen;
				db_lockmode_t mode;
}

/* Lock request structure. */
//C     struct __db_lockreq {
//C     	db_lockop_t	 op;		/* Operation. */
//C     	db_lockmode_t	 mode;		/* Requested mode. */
//C     	db_timeout_t	 timeout;	/* Time to expire lock. */
//C     	DBT		*obj;		/* Object being locked. */
//C     	DB_LOCK		 lock;		/* Lock returned. */
//C     };
struct __db_lockreq
{
				db_lockop_t op;
				db_lockmode_t mode;
				db_timeout_t timeout;
				DBT *obj;
				DB_LOCK lock;
}

/*******************************************************
	* Logging.
	*******************************************************/
//C     #define	DB_LOGVERSION	12		/* Current log version. */
//C     #define	DB_LOGOLDVER	8		/* Oldest log version supported. */
const DB_LOGVERSION = 12;
//C     #define	DB_LOGMAGIC	0x040988
const DB_LOGOLDVER = 8;

const DB_LOGMAGIC = 0x040988;
/* Flag values for DB_ENV->log_archive(). */
//C     #define	DB_ARCH_ABS	0x001		/* Absolute pathnames. */
//C     #define	DB_ARCH_DATA	0x002		/* Data files. */
const DB_ARCH_ABS = 0x001;
//C     #define	DB_ARCH_LOG	0x004		/* Log files. */
const DB_ARCH_DATA = 0x002;
//C     #define	DB_ARCH_REMOVE	0x008	/* Remove log files. */
const DB_ARCH_LOG = 0x004;

const DB_ARCH_REMOVE = 0x008;
/* Flag values for DB_ENV->log_put(). */
//C     #define	DB_FLUSH		0x001	/* Flush data to disk (public). */
//C     #define	DB_LOG_CHKPNT		0x002	/* Flush supports a checkpoint */
const DB_FLUSH = 0x001;
//C     #define	DB_LOG_COMMIT		0x004	/* Flush supports a commit */
const DB_LOG_CHKPNT = 0x002;
//C     #define	DB_LOG_NOCOPY		0x008	/* Don't copy data */
const DB_LOG_COMMIT = 0x004;
//C     #define	DB_LOG_NOT_DURABLE	0x010	/* Do not log; keep in memory */
const DB_LOG_NOCOPY = 0x008;
//C     #define	DB_LOG_WRNOSYNC		0x020	/* Write, don't sync log_put */
const DB_LOG_NOT_DURABLE = 0x010;

const DB_LOG_WRNOSYNC = 0x020;
/*
	* A DB_LSN has two parts, a fileid which identifies a specific file, and an
	* offset within that file.  The fileid is an unsigned 4-byte quantity that
	* uniquely identifies a file within the log directory -- currently a simple
	* counter inside the log.  The offset is also an unsigned 4-byte value.  The
	* log manager guarantees the offset is never more than 4 bytes by switching
	* to a new log file before the maximum length imposed by an unsigned 4-byte
	* offset is reached.
	*/
//C     struct __db_lsn {
//C     	u_int32_t	file;		/* File ID. */
//C     	u_int32_t	offset;		/* File offset. */
//C     };
struct __db_lsn
{
				u_int32_t file;
				u_int32_t offset;
}

/*
	* Application-specified log record types start at DB_user_BEGIN, and must not
	* equal or exceed DB_debug_FLAG.
	*
	* DB_debug_FLAG is the high-bit of the u_int32_t that specifies a log record
	* type.  If the flag is set, it's a log record that was logged for debugging
	* purposes only, even if it reflects a database change -- the change was part
	* of a non-durable transaction.
	*/
//C     #define	DB_user_BEGIN		10000
//C     #define	DB_debug_FLAG		0x80000000
const DB_user_BEGIN = 10000;

const DB_debug_FLAG = 0x80000000;
/*
	* DB_LOGC --
	*	Log cursor.
	*/
//C     struct __db_log_cursor {
//C     	DB_ENV	 *dbenv;		/* Enclosing dbenv. */

//C     	DB_FH	 *c_fhp;		/* File handle. */
//C     	DB_LSN	  c_lsn;		/* Cursor: LSN */
//C     	u_int32_t c_len;		/* Cursor: record length */
//C     	u_int32_t c_prev;		/* Cursor: previous record's offset */

//C     	DBT	  c_dbt;		/* Return DBT. */
//C     	DB_LSN    p_lsn;		/* Persist LSN. */
//C     	u_int32_t p_version;		/* Persist version. */

//C     	u_int8_t *bp;			/* Allocated read buffer. */
//C     	u_int32_t bp_size;		/* Read buffer length in bytes. */
//C     	u_int32_t bp_rlen;		/* Read buffer valid data length. */
//C     	DB_LSN	  bp_lsn;		/* Read buffer first byte LSN. */

//C     	u_int32_t bp_maxrec;		/* Max record length in the log file. */

	/* DB_LOGC PUBLIC HANDLE LIST BEGIN */
//C     	int (*close) __P((DB_LOGC *, u_int32_t));
//C     	int (*get) __P((DB_LOGC *, DB_LSN *, DBT *, u_int32_t));
//C     	int (*version) __P((DB_LOGC *, u_int32_t *, u_int32_t));
	/* DB_LOGC PUBLIC HANDLE LIST END */

//C     #define	DB_LOG_DISK		0x01	/* Log record came from disk. */
//C     #define	DB_LOG_LOCKED		0x02	/* Log region already locked */
const DB_LOG_DISK = 0x01;
//C     #define	DB_LOG_SILENT_ERR	0x04	/* Turn-off error messages. */
const DB_LOG_LOCKED = 0x02;
//C     	u_int32_t flags;
const DB_LOG_SILENT_ERR = 0x04;
//C     };
struct __db_log_cursor
{
				DB_ENV *dbenv;
				DB_FH *c_fhp;
				DB_LSN c_lsn;
				u_int32_t c_len;
				u_int32_t c_prev;
				DBT c_dbt;
				DB_LSN p_lsn;
				u_int32_t p_version;
				u_int8_t *bp;
				u_int32_t bp_size;
				u_int32_t bp_rlen;
				DB_LSN bp_lsn;
				u_int32_t bp_maxrec;
				int  function(DB_LOGC *, u_int32_t )close;
				int  function(DB_LOGC *, DB_LSN *, DBT *, u_int32_t )get;
				int  function(DB_LOGC *, u_int32_t *, u_int32_t )version_;
				u_int32_t flags;
}

/* Log statistics structure. */
//C     struct __db_log_stat {
//C     	u_int32_t st_magic;		/* Log file magic number. */
//C     	u_int32_t st_version;		/* Log file version number. */
//C     	int	  st_mode;		/* Log file permissions mode. */
//C     	u_int32_t st_lg_bsize;		/* Log buffer size. */
//C     	u_int32_t st_lg_size;		/* Log file size. */
//C     	u_int32_t st_record;		/* Records entered into the log. */
//C     	u_int32_t st_w_bytes;		/* Bytes to log. */
//C     	u_int32_t st_w_mbytes;		/* Megabytes to log. */
//C     	u_int32_t st_wc_bytes;		/* Bytes to log since checkpoint. */
//C     	u_int32_t st_wc_mbytes;		/* Megabytes to log since checkpoint. */
//C     	u_int32_t st_wcount;		/* Total I/O writes to the log. */
//C     	u_int32_t st_wcount_fill;	/* Overflow writes to the log. */
//C     	u_int32_t st_rcount;		/* Total I/O reads from the log. */
//C     	u_int32_t st_scount;		/* Total syncs to the log. */
//C     	u_int32_t st_region_wait;	/* Region lock granted after wait. */
//C     	u_int32_t st_region_nowait;	/* Region lock granted without wait. */
//C     	u_int32_t st_cur_file;		/* Current log file number. */
//C     	u_int32_t st_cur_offset;	/* Current log file offset. */
//C     	u_int32_t st_disk_file;		/* Known on disk log file number. */
//C     	u_int32_t st_disk_offset;	/* Known on disk log file offset. */
//C     	roff_t	  st_regsize;		/* Region size. */
//C     	u_int32_t st_maxcommitperflush;	/* Max number of commits in a flush. */
//C     	u_int32_t st_mincommitperflush;	/* Min number of commits in a flush. */
//C     };
struct __db_log_stat
{
				u_int32_t st_magic;
				u_int32_t st_version;
				int st_mode;
				u_int32_t st_lg_bsize;
				u_int32_t st_lg_size;
				u_int32_t st_record;
				u_int32_t st_w_bytes;
				u_int32_t st_w_mbytes;
				u_int32_t st_wc_bytes;
				u_int32_t st_wc_mbytes;
				u_int32_t st_wcount;
				u_int32_t st_wcount_fill;
				u_int32_t st_rcount;
				u_int32_t st_scount;
				u_int32_t st_region_wait;
				u_int32_t st_region_nowait;
				u_int32_t st_cur_file;
				u_int32_t st_cur_offset;
				u_int32_t st_disk_file;
				u_int32_t st_disk_offset;
				roff_t st_regsize;
				u_int32_t st_maxcommitperflush;
				u_int32_t st_mincommitperflush;
}

/*
	* We need to record the first log record of a transaction.  For user
	* defined logging this macro returns the place to put that information,
	* if it is need in rlsnp, otherwise it leaves it unchanged.  We also
	* need to track the last record of the transaction, this returns the
	* place to put that info.
	*/
//C     #define	DB_SET_TXN_LSNP(txn, blsnp, llsnp)			((txn)->set_txn_lsnp(txn, blsnp, llsnp))

/*******************************************************
	* Shared buffer cache (mpool).
	*******************************************************/
/* Flag values for DB_MPOOLFILE->get. */
//C     #define	DB_MPOOL_CREATE		0x001	/* Create a page. */
//C     #define	DB_MPOOL_DIRTY		0x002	/* Get page for an update. */
const DB_MPOOL_CREATE = 0x001;
//C     #define	DB_MPOOL_EDIT		0x004	/* Modify without copying. */
const DB_MPOOL_DIRTY = 0x002;
//C     #define	DB_MPOOL_FREE		0x008	/* Free page if present. */
const DB_MPOOL_EDIT = 0x004;
//C     #define	DB_MPOOL_LAST		0x010	/* Return the last page. */
const DB_MPOOL_FREE = 0x008;
//C     #define	DB_MPOOL_NEW		0x020	/* Create a new page. */
const DB_MPOOL_LAST = 0x010;

const DB_MPOOL_NEW = 0x020;
/* Flag values for DB_MPOOLFILE->put, DB_MPOOLFILE->set. */
//C     #define	DB_MPOOL_DISCARD	0x001	/* Don't cache the page. */

const DB_MPOOL_DISCARD = 0x001;
/* Flags values for DB_MPOOLFILE->set_flags. */
//C     #define	DB_MPOOL_NOFILE		0x001	/* Never open a backing file. */
//C     #define	DB_MPOOL_UNLINK		0x002	/* Unlink the file on last close. */
const DB_MPOOL_NOFILE = 0x001;

const DB_MPOOL_UNLINK = 0x002;
/* Priority values for DB_MPOOLFILE->set_priority. */
//C     typedef enum {
//C     	DB_PRIORITY_VERY_LOW=1,
//C     	DB_PRIORITY_LOW=2,
//C     	DB_PRIORITY_DEFAULT=3,
//C     	DB_PRIORITY_HIGH=4,
//C     	DB_PRIORITY_VERY_HIGH=5
//C     } DB_CACHE_PRIORITY;
enum
{
				DB_PRIORITY_VERY_LOW = 1,
				DB_PRIORITY_LOW,
				DB_PRIORITY_DEFAULT,
				DB_PRIORITY_HIGH,
				DB_PRIORITY_VERY_HIGH,
}
alias int DB_CACHE_PRIORITY;

/* Per-process DB_MPOOLFILE information. */
//C     struct __db_mpoolfile {
//C     	DB_FH	  *fhp;			/* Underlying file handle. */

	/*
		* !!!
		* The ref, pinref and q fields are protected by the region lock.
		*/
//C     	u_int32_t  ref;			/* Reference count. */

//C     	u_int32_t pinref;		/* Pinned block reference count. */

	/*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_ENTRY(__db_mpoolfile) q;
		*/
//C     	struct {
//C     		struct __db_mpoolfile *tqe_next;
//C     		struct __db_mpoolfile **tqe_prev;
//C     	} q;				/* Linked list of DB_MPOOLFILE's. */
struct _N5
{
				__db_mpoolfile *tqe_next;
				__db_mpoolfile **tqe_prev;
}

	/*
		* !!!
		* The rest of the fields (with the exception of the MP_FLUSH flag)
		* are not thread-protected, even when they may be modified at any
		* time by the application.  The reason is the DB_MPOOLFILE handle
		* is single-threaded from the viewpoint of the application, and so
		* the only fields needing to be thread-protected are those accessed
		* by checkpoint or sync threads when using DB_MPOOLFILE structures
		* to flush buffers from the cache.
		*/
//C     	DB_ENV	       *dbenv;		/* Overlying DB_ENV. */
//C     	MPOOLFILE      *mfp;		/* Underlying MPOOLFILE. */

//C     	u_int32_t	clear_len;	/* Cleared length on created pages. */
//C     	u_int8_t			/* Unique file ID. */
//C     			fileid[DB_FILE_ID_LEN];
//C     	int		ftype;		/* File type. */
//C     	int32_t		lsn_offset;	/* LSN offset in page. */
//C     	u_int32_t	gbytes, bytes;	/* Maximum file size. */
//C     	DBT	       *pgcookie;	/* Byte-string passed to pgin/pgout. */
//C     	int32_t		priority;	/* Cache priority. */

//C     	void	       *addr;		/* Address of mmap'd region. */
//C     	size_t		len;		/* Length of mmap'd region. */

//C     	u_int32_t	config_flags;	/* Flags to DB_MPOOLFILE->set_flags. */

	/* DB_MPOOLFILE PUBLIC HANDLE LIST BEGIN */
//C     	int (*close) __P((DB_MPOOLFILE *, u_int32_t));
//C     	int (*get)
//C     	    __P((DB_MPOOLFILE *, db_pgno_t *, DB_TXN *, u_int32_t, void *));
//C     	int (*open) __P((DB_MPOOLFILE *, const char *, u_int32_t, int, size_t));
//C     	int (*put) __P((DB_MPOOLFILE *, void *, u_int32_t));
//C     	int (*set) __P((DB_MPOOLFILE *, void *, u_int32_t));
//C     	int (*get_clear_len) __P((DB_MPOOLFILE *, u_int32_t *));
//C     	int (*set_clear_len) __P((DB_MPOOLFILE *, u_int32_t));
//C     	int (*get_fileid) __P((DB_MPOOLFILE *, u_int8_t *));
//C     	int (*set_fileid) __P((DB_MPOOLFILE *, u_int8_t *));
//C     	int (*get_flags) __P((DB_MPOOLFILE *, u_int32_t *));
//C     	int (*set_flags) __P((DB_MPOOLFILE *, u_int32_t, int));
//C     	int (*get_ftype) __P((DB_MPOOLFILE *, int *));
//C     	int (*set_ftype) __P((DB_MPOOLFILE *, int));
//C     	int (*get_lsn_offset) __P((DB_MPOOLFILE *, int32_t *));
//C     	int (*set_lsn_offset) __P((DB_MPOOLFILE *, int32_t));
//C     	int (*get_maxsize) __P((DB_MPOOLFILE *, u_int32_t *, u_int32_t *));
//C     	int (*set_maxsize) __P((DB_MPOOLFILE *, u_int32_t, u_int32_t));
//C     	int (*get_pgcookie) __P((DB_MPOOLFILE *, DBT *));
//C     	int (*set_pgcookie) __P((DB_MPOOLFILE *, DBT *));
//C     	int (*get_priority) __P((DB_MPOOLFILE *, DB_CACHE_PRIORITY *));
//C     	int (*set_priority) __P((DB_MPOOLFILE *, DB_CACHE_PRIORITY));
//C     	int (*sync) __P((DB_MPOOLFILE *));
	/* DB_MPOOLFILE PUBLIC HANDLE LIST END */

	/*
		* MP_FILEID_SET, MP_OPEN_CALLED and MP_READONLY do not need to be
		* thread protected because they are initialized before the file is
		* linked onto the per-process lists, and never modified.
		*
		* MP_FLUSH is thread protected because it is potentially read/set by
		* multiple threads of control.
		*/
//C     #define	MP_FILEID_SET	0x001		/* Application supplied a file ID. */
//C     #define	MP_FLUSH	0x002		/* Was opened to flush a buffer. */
const MP_FILEID_SET = 0x001;
//C     #define	MP_MULTIVERSION	0x004		/* Opened for multiversion access. */
const MP_FLUSH = 0x002;
//C     #define	MP_OPEN_CALLED	0x008		/* File opened. */
const MP_MULTIVERSION = 0x004;
//C     #define	MP_READONLY	0x010		/* File is readonly. */
const MP_OPEN_CALLED = 0x008;
//C     	u_int32_t  flags;
const MP_READONLY = 0x010;
//C     };
struct __db_mpoolfile
{
				DB_FH *fhp;
				u_int32_t ref_;
				u_int32_t pinref;
				_N5 q;
				DB_ENV *dbenv;
				MPOOLFILE *mfp;
				u_int32_t clear_len;
				u_int8_t [20]fileid;
				int ftype;
				int32_t lsn_offset;
				u_int32_t gbytes;
				u_int32_t bytes;
				DBT *pgcookie;
				int32_t priority;
				void *addr;
				size_t len;
				u_int32_t config_flags;
				int  function(DB_MPOOLFILE *, u_int32_t )close;
				int  function(DB_MPOOLFILE *, db_pgno_t *, DB_TXN *, u_int32_t , void *)get;
				int  function(DB_MPOOLFILE *, char *, u_int32_t , int , size_t )open;
				int  function(DB_MPOOLFILE *, void *, u_int32_t )put;
				int  function(DB_MPOOLFILE *, void *, u_int32_t )set;
				int  function(DB_MPOOLFILE *, u_int32_t *)get_clear_len;
				int  function(DB_MPOOLFILE *, u_int32_t )set_clear_len;
				int  function(DB_MPOOLFILE *, u_int8_t *)get_fileid;
				int  function(DB_MPOOLFILE *, u_int8_t *)set_fileid;
				int  function(DB_MPOOLFILE *, u_int32_t *)get_flags;
				int  function(DB_MPOOLFILE *, u_int32_t , int )set_flags;
				int  function(DB_MPOOLFILE *, int *)get_ftype;
				int  function(DB_MPOOLFILE *, int )set_ftype;
				int  function(DB_MPOOLFILE *, int32_t *)get_lsn_offset;
				int  function(DB_MPOOLFILE *, int32_t )set_lsn_offset;
				int  function(DB_MPOOLFILE *, u_int32_t *, u_int32_t *)get_maxsize;
				int  function(DB_MPOOLFILE *, u_int32_t , u_int32_t )set_maxsize;
				int  function(DB_MPOOLFILE *, DBT *)get_pgcookie;
				int  function(DB_MPOOLFILE *, DBT *)set_pgcookie;
				int  function(DB_MPOOLFILE *, DB_CACHE_PRIORITY *)get_priority;
				int  function(DB_MPOOLFILE *, DB_CACHE_PRIORITY )set_priority;
				int  function(DB_MPOOLFILE *)sync;
				u_int32_t flags;
}

/* Mpool statistics structure. */
//C     struct __db_mpool_stat {
//C     	u_int32_t st_gbytes;		/* Total cache size: GB. */
//C     	u_int32_t st_bytes;		/* Total cache size: B. */
//C     	u_int32_t st_ncache;		/* Number of caches. */
//C     	roff_t	  st_regsize;		/* Region size. */
//C     	size_t	  st_mmapsize;		/* Maximum file size for mmap. */
//C     	int	  st_maxopenfd;		/* Maximum number of open fd's. */
//C     	int	  st_maxwrite;		/* Maximum buffers to write. */
//C     	int	  st_maxwrite_sleep;	/* Sleep after writing max buffers. */
//C     	u_int32_t st_map;		/* Pages from mapped files. */
//C     	u_int32_t st_cache_hit;		/* Pages found in the cache. */
//C     	u_int32_t st_cache_miss;	/* Pages not found in the cache. */
//C     	u_int32_t st_page_create;	/* Pages created in the cache. */
//C     	u_int32_t st_page_in;		/* Pages read in. */
//C     	u_int32_t st_page_out;		/* Pages written out. */
//C     	u_int32_t st_ro_evict;		/* Clean pages forced from the cache. */
//C     	u_int32_t st_rw_evict;		/* Dirty pages forced from the cache. */
//C     	u_int32_t st_page_trickle;	/* Pages written by memp_trickle. */
//C     	u_int32_t st_pages;		/* Total number of pages. */
//C     	u_int32_t st_page_clean;	/* Clean pages. */
//C     	u_int32_t st_page_dirty;	/* Dirty pages. */
//C     	u_int32_t st_hash_buckets;	/* Number of hash buckets. */
//C     	u_int32_t st_hash_searches;	/* Total hash chain searches. */
//C     	u_int32_t st_hash_longest;	/* Longest hash chain searched. */
//C     	u_int32_t st_hash_examined;	/* Total hash entries searched. */
//C     	u_int32_t st_hash_nowait;	/* Hash lock granted with nowait. */
//C     	u_int32_t st_hash_wait;		/* Hash lock granted after wait. */
//C     	u_int32_t st_hash_max_nowait;	/* Max hash lock granted with nowait. */
//C     	u_int32_t st_hash_max_wait;	/* Max hash lock granted after wait. */
//C     	u_int32_t st_region_nowait;	/* Region lock granted with nowait. */
//C     	u_int32_t st_region_wait;	/* Region lock granted after wait. */
//C     	u_int32_t st_mvcc_frozen;		/* Buffers frozen. */
//C     	u_int32_t st_mvcc_thawed;		/* Buffers thawed. */
//C     	u_int32_t st_mvcc_freed;		/* Frozen buffers freed. */
//C     	u_int32_t st_alloc;		/* Number of page allocations. */
//C     	u_int32_t st_alloc_buckets;	/* Buckets checked during allocation. */
//C     	u_int32_t st_alloc_max_buckets;	/* Max checked during allocation. */
//C     	u_int32_t st_alloc_pages;	/* Pages checked during allocation. */
//C     	u_int32_t st_alloc_max_pages;	/* Max checked during allocation. */
//C     	u_int32_t st_io_wait;		/* Thread waited on buffer I/O. */
//C     };
struct __db_mpool_stat
{
				u_int32_t st_gbytes;
				u_int32_t st_bytes;
				u_int32_t st_ncache;
				roff_t st_regsize;
				size_t st_mmapsize;
				int st_maxopenfd;
				int st_maxwrite;
				int st_maxwrite_sleep;
				u_int32_t st_map;
				u_int32_t st_cache_hit;
				u_int32_t st_cache_miss;
				u_int32_t st_page_create;
				u_int32_t st_page_in;
				u_int32_t st_page_out;
				u_int32_t st_ro_evict;
				u_int32_t st_rw_evict;
				u_int32_t st_page_trickle;
				u_int32_t st_pages;
				u_int32_t st_page_clean;
				u_int32_t st_page_dirty;
				u_int32_t st_hash_buckets;
				u_int32_t st_hash_searches;
				u_int32_t st_hash_longest;
				u_int32_t st_hash_examined;
				u_int32_t st_hash_nowait;
				u_int32_t st_hash_wait;
				u_int32_t st_hash_max_nowait;
				u_int32_t st_hash_max_wait;
				u_int32_t st_region_nowait;
				u_int32_t st_region_wait;
				u_int32_t st_mvcc_frozen;
				u_int32_t st_mvcc_thawed;
				u_int32_t st_mvcc_freed;
				u_int32_t st_alloc;
				u_int32_t st_alloc_buckets;
				u_int32_t st_alloc_max_buckets;
				u_int32_t st_alloc_pages;
				u_int32_t st_alloc_max_pages;
				u_int32_t st_io_wait;
}

/* Mpool file statistics structure. */
//C     struct __db_mpool_fstat {
//C     	char *file_name;		/* File name. */
//C     	u_int32_t st_pagesize;		/* Page size. */
//C     	u_int32_t st_map;		/* Pages from mapped files. */
//C     	u_int32_t st_cache_hit;		/* Pages found in the cache. */
//C     	u_int32_t st_cache_miss;	/* Pages not found in the cache. */
//C     	u_int32_t st_page_create;	/* Pages created in the cache. */
//C     	u_int32_t st_page_in;		/* Pages read in. */
//C     	u_int32_t st_page_out;		/* Pages written out. */
//C     };
struct __db_mpool_fstat
{
				char *file_name;
				u_int32_t st_pagesize;
				u_int32_t st_map;
				u_int32_t st_cache_hit;
				u_int32_t st_cache_miss;
				u_int32_t st_page_create;
				u_int32_t st_page_in;
				u_int32_t st_page_out;
}

/*******************************************************
	* Transactions and recovery.
	*******************************************************/
//C     #define	DB_TXNVERSION	1

const DB_TXNVERSION = 1;
//C     typedef enum {
//C     	DB_TXN_ABORT=0,			/* Public. */
//C     	DB_TXN_APPLY=1,			/* Public. */
//C     	DB_TXN_BACKWARD_ALLOC=2,	/* Internal. */
//C     	DB_TXN_BACKWARD_ROLL=3,		/* Public. */
//C     	DB_TXN_FORWARD_ROLL=4,		/* Public. */
//C     	DB_TXN_OPENFILES=5,		/* Internal. */
//C     	DB_TXN_POPENFILES=6,		/* Internal. */
//C     	DB_TXN_PRINT=7			/* Public. */
//C     } db_recops;
enum
{
				DB_TXN_ABORT,
				DB_TXN_APPLY,
				DB_TXN_BACKWARD_ALLOC,
				DB_TXN_BACKWARD_ROLL,
				DB_TXN_FORWARD_ROLL,
				DB_TXN_OPENFILES,
				DB_TXN_POPENFILES,
				DB_TXN_PRINT,
}
alias int db_recops;

/*
	* BACKWARD_ALLOC is used during the forward pass to pick up any aborted
	* allocations for files that were created during the forward pass.
	* The main difference between _ALLOC and _ROLL is that the entry for
	* the file not exist during the rollforward pass.
	*/
//C     #define	DB_UNDO(op)	((op) == DB_TXN_ABORT ||					(op) == DB_TXN_BACKWARD_ROLL || (op) == DB_TXN_BACKWARD_ALLOC)
//C     #define	DB_REDO(op)	((op) == DB_TXN_FORWARD_ROLL || (op) == DB_TXN_APPLY)

//C     struct __db_txn {
//C     	DB_TXNMGR	*mgrp;		/* Pointer to transaction manager. */
//C     	DB_TXN		*parent;	/* Pointer to transaction's parent. */

//C     	u_int32_t	txnid;		/* Unique transaction id. */
//C     	char		*name;		/* Transaction name */

//C     	db_threadid_t	tid;		/* Thread id for use in MT XA. */
//C     	void		*td;		/* Detail structure within region. */
//C     	db_timeout_t	lock_timeout;	/* Timeout for locks for this txn. */
//C     	db_timeout_t	expire;		/* Time transaction expires. */
//C     	void		*txn_list;	/* Undo information for parent. */

	/*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_ENTRY(__db_txn) links;
		* TAILQ_ENTRY(__db_txn) xalinks;
		*/
//C     	struct {
//C     		struct __db_txn *tqe_next;
//C     		struct __db_txn **tqe_prev;
//C     	} links;			/* Links transactions off manager. */
struct _N7
{
				__db_txn *tqe_next;
				__db_txn **tqe_prev;
}
//C     	struct {
//C     		struct __db_txn *tqe_next;
//C     		struct __db_txn **tqe_prev;
//C     	} xalinks;			/* Links active XA transactions. */
struct _N8
{
				__db_txn *tqe_next;
				__db_txn **tqe_prev;
}

	/*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_HEAD(__kids, __db_txn) kids;
		*/
//C     	struct __kids {
//C     		struct __db_txn *tqh_first;
//C     		struct __db_txn **tqh_last;
//C     	} kids;
struct __kids
{
				__db_txn *tqh_first;
				__db_txn **tqh_last;
}

	/*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_HEAD(__events, __txn_event) events;
		*/
//C     	struct {
//C     		struct __txn_event *tqh_first;
//C     		struct __txn_event **tqh_last;
//C     	} events;
struct __txn_event{}
struct _N9
{
				__txn_event *tqh_first;
				__txn_event **tqh_last;
}

	/*
		* !!!
		* Explicit representations of structures from queue.h.
		* STAILQ_HEAD(__logrec, __txn_logrec) logs;
		*/
//C     	struct {
//C     		struct __txn_logrec *stqh_first;
//C     		struct __txn_logrec **stqh_last;
//C     	} logs;				/* Links deferred events. */
struct __txn_logrec{}
struct _N10
{
				__txn_logrec *stqh_first;
				__txn_logrec **stqh_last;
}

	/*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_ENTRY(__db_txn) klinks;
		*/
//C     	struct {
//C     		struct __db_txn *tqe_next;
//C     		struct __db_txn **tqe_prev;
//C     	} klinks;
struct _N11
{
				__db_txn *tqe_next;
				__db_txn **tqe_prev;
}

//C     	void	*api_internal;		/* C++ API private. */
//C     	void	*xml_internal;		/* XML API private. */

//C     	u_int32_t	cursors;	/* Number of cursors open for txn */

	/* DB_TXN PUBLIC HANDLE LIST BEGIN */
//C     	int	  (*abort) __P((DB_TXN *));
//C     	int	  (*commit) __P((DB_TXN *, u_int32_t));
//C     	int	  (*discard) __P((DB_TXN *, u_int32_t));
//C     	int	  (*get_name) __P((DB_TXN *, const char **));
//C     	u_int32_t (*id) __P((DB_TXN *));
//C     	int	  (*prepare) __P((DB_TXN *, u_int8_t *));
//C     	int	  (*set_name) __P((DB_TXN *, const char *));
//C     	int	  (*set_timeout) __P((DB_TXN *, db_timeout_t, u_int32_t));
	/* DB_TXN PUBLIC HANDLE LIST END */

	/* DB_TXN PRIVATE HANDLE LIST BEGIN */
//C     	void	  (*set_txn_lsnp) __P((DB_TXN *txn, DB_LSN **, DB_LSN **));
	/* DB_TXN PRIVATE HANDLE LIST END */

//C     #define	TXN_CHILDCOMMIT		0x0001	/* Txn has committed. */
//C     #define	TXN_CDSGROUP		0x0002	/* CDS group handle. */
const TXN_CHILDCOMMIT = 0x0001;
//C     #define	TXN_COMPENSATE		0x0004	/* Compensating transaction. */
const TXN_CDSGROUP = 0x0002;
//C     #define	TXN_DEADLOCK		0x0008	/* Txn has deadlocked. */
const TXN_COMPENSATE = 0x0004;
//C     #define	TXN_LOCKTIMEOUT		0x0010	/* Txn has a lock timeout. */
const TXN_DEADLOCK = 0x0008;
//C     #define	TXN_MALLOC		0x0020	/* Structure allocated by TXN system. */
const TXN_LOCKTIMEOUT = 0x0010;
//C     #define	TXN_NOSYNC		0x0040	/* Do not sync on prepare and commit. */
const TXN_MALLOC = 0x0020;
//C     #define	TXN_NOWAIT		0x0080	/* Do not wait on locks. */
const TXN_NOSYNC = 0x0040;
//C     #define	TXN_PRIVATE		0x0100	/* Txn owned by cursor.. */
const TXN_NOWAIT = 0x0080;
//C     #define	TXN_READ_COMMITTED	0x0200	/* Txn has degree 2 isolation. */
const TXN_PRIVATE = 0x0100;
//C     #define	TXN_READ_UNCOMMITTED	0x0400	/* Txn has degree 1 isolation. */
const TXN_READ_COMMITTED = 0x0200;
//C     #define	TXN_RESTORED		0x0800	/* Txn has been restored. */
const TXN_READ_UNCOMMITTED = 0x0400;
//C     #define	TXN_SNAPSHOT		0x1000	/* Snapshot Isolation. */
const TXN_RESTORED = 0x0800;
//C     #define	TXN_SYNC		0x2000	/* Write and sync on prepare/commit. */
const TXN_SNAPSHOT = 0x1000;
//C     #define	TXN_WRITE_NOSYNC	0x4000	/* Write only on prepare/commit. */
const TXN_SYNC = 0x2000;
//C     	u_int32_t	flags;
const TXN_WRITE_NOSYNC = 0x4000;
//C     };
struct __db_txn
{
				DB_TXNMGR *mgrp;
				DB_TXN *parent;
				u_int32_t txnid;
				char *name;
				db_threadid_t tid;
				void *td;
				db_timeout_t lock_timeout;
				db_timeout_t expire;
				void *txn_list;
				_N7 links;
				_N8 xalinks;
				__kids kids;
				_N9 events;
				_N10 logs;
				_N11 klinks;
				void *api_internal;
				void *xml_internal;
				u_int32_t cursors;
				int  function(DB_TXN *)abort;
				int  function(DB_TXN *, u_int32_t )commit;
				int  function(DB_TXN *, u_int32_t )discard;
				int  function(DB_TXN *, char **)get_name;
				u_int32_t  function(DB_TXN *)id;
				int  function(DB_TXN *, u_int8_t *)prepare;
				int  function(DB_TXN *, char *)set_name;
				int  function(DB_TXN *, db_timeout_t , u_int32_t )set_timeout;
				void  function(DB_TXN *txn, DB_LSN **, DB_LSN **)set_txn_lsnp;
				u_int32_t flags;
}

//C     #define	TXN_SYNC_FLAGS (TXN_SYNC | TXN_NOSYNC | TXN_WRITE_NOSYNC)

/*
	* Structure used for two phase commit interface.  Berkeley DB support for two
	* phase commit is compatible with the X/Open XA interface.
	*
	* The XA #define XIDDATASIZE defines the size of a global transaction ID.  We
	* have our own version here (for name space reasons) which must have the same
	* value.
	*/
//C     #define	DB_XIDDATASIZE	128
//C     struct __db_preplist {
const DB_XIDDATASIZE = 128;
//C     	DB_TXN	*txn;
//C     	u_int8_t gid[DB_XIDDATASIZE];
//C     };
struct __db_preplist
{
				DB_TXN *txn;
				u_int8_t [128]gid;
}

/* Transaction statistics structure. */
//C     struct __db_txn_active {
//C     	u_int32_t txnid;		/* Transaction ID */
//C     	u_int32_t parentid;		/* Transaction ID of parent */
//C     	pid_t     pid;			/* Process owning txn ID */
//C     	db_threadid_t tid;		/* Thread owning txn ID */

//C     	DB_LSN	  lsn;			/* LSN when transaction began */

//C     	DB_LSN	  read_lsn;		/* Read LSN for MVCC */
//C     	u_int32_t mvcc_ref;		/* MVCC reference count */

//C     #define	TXN_ABORTED		1
//C     #define	TXN_COMMITTED		2
const TXN_ABORTED = 1;
//C     #define	TXN_PREPARED		3
const TXN_COMMITTED = 2;
//C     #define	TXN_RUNNING		4
const TXN_PREPARED = 3;
//C     	u_int32_t status;		/* Status of the transaction */
const TXN_RUNNING = 4;

//C     #define	TXN_XA_ABORTED		1
//C     #define	TXN_XA_DEADLOCKED	2
const TXN_XA_ABORTED = 1;
//C     #define	TXN_XA_ENDED		3
const TXN_XA_DEADLOCKED = 2;
//C     #define	TXN_XA_PREPARED		4
const TXN_XA_ENDED = 3;
//C     #define	TXN_XA_STARTED		5
const TXN_XA_PREPARED = 4;
//C     #define	TXN_XA_SUSPENDED	6
const TXN_XA_STARTED = 5;
//C     	u_int32_t xa_status;		/* XA status */
const TXN_XA_SUSPENDED = 6;

//C     	u_int8_t  xid[DB_XIDDATASIZE];	/* Global transaction ID */
//C     	char	  name[51];		/* 50 bytes of name, nul termination */
//C     };
struct __db_txn_active
{
				u_int32_t txnid;
				u_int32_t parentid;
				pid_t pid;
				db_threadid_t tid;
				DB_LSN lsn;
				DB_LSN read_lsn;
				u_int32_t mvcc_ref;
				u_int32_t status;
				u_int32_t xa_status;
				u_int8_t [128]xid;
				char [51]name;
}

//C     struct __db_txn_stat {
//C     	DB_LSN	  st_last_ckp;		/* lsn of the last checkpoint */
//C     	time_t	  st_time_ckp;		/* time of last checkpoint */
//C     	u_int32_t st_last_txnid;	/* last transaction id given out */
//C     	u_int32_t st_maxtxns;		/* maximum txns possible */
//C     	u_int32_t st_naborts;		/* number of aborted transactions */
//C     	u_int32_t st_nbegins;		/* number of begun transactions */
//C     	u_int32_t st_ncommits;		/* number of committed transactions */
//C     	u_int32_t st_nactive;		/* number of active transactions */
//C     	u_int32_t st_nsnapshot;		/* number of snapshot transactions */
//C     	u_int32_t st_nrestores;
/* number of restored transactions
								after recovery. */
//C     	u_int32_t st_maxnactive;	/* maximum active transactions */
//C     	u_int32_t st_maxnsnapshot;	/* maximum snapshot transactions */
//C     	DB_TXN_ACTIVE *st_txnarray;	/* array of active transactions */
//C     	u_int32_t st_region_wait;	/* Region lock granted after wait. */
//C     	u_int32_t st_region_nowait;	/* Region lock granted without wait. */
//C     	roff_t	  st_regsize;		/* Region size. */
//C     };
struct __db_txn_stat
{
				DB_LSN st_last_ckp;
				time_t st_time_ckp;
				u_int32_t st_last_txnid;
				u_int32_t st_maxtxns;
				u_int32_t st_naborts;
				u_int32_t st_nbegins;
				u_int32_t st_ncommits;
				u_int32_t st_nactive;
				u_int32_t st_nsnapshot;
				u_int32_t st_nrestores;
				u_int32_t st_maxnactive;
				u_int32_t st_maxnsnapshot;
				DB_TXN_ACTIVE *st_txnarray;
				u_int32_t st_region_wait;
				u_int32_t st_region_nowait;
				roff_t st_regsize;
}

/*******************************************************
	* Replication.
	*******************************************************/
/* Special, out-of-band environment IDs. */
//C     #define	DB_EID_BROADCAST	-1
//C     #define	DB_EID_INVALID		-2
const DB_EID_BROADCAST = -1;

const DB_EID_INVALID = -2;
/* rep_config flag values. */
//C     #define	DB_REP_CONF_BULK	0x0001	/* Bulk transfer. */
//C     #define	DB_REP_CONF_DELAYCLIENT	0x0002	/* Delay client synchronization. */
const DB_REP_CONF_BULK = 0x0001;
//C     #define	DB_REP_CONF_NOAUTOINIT	0x0004	/* No automatic client init. */
const DB_REP_CONF_DELAYCLIENT = 0x0002;
//C     #define	DB_REP_CONF_NOWAIT	0x0008	/* Don't wait, return error. */
const DB_REP_CONF_NOAUTOINIT = 0x0004;

const DB_REP_CONF_NOWAIT = 0x0008;
/*
	* Operation code values for rep_start and/or repmgr_start.  Just one of the
	* following values should be passed in the flags parameter.  (If we ever need
	* additional, independent bit flags for these methods, we can start allocating
	* them from the high-order byte of the flags word, as we currently do elsewhere
	* for DB_AFTER through DB_WRITELOCK and DB_AUTO_COMMIT, etc.)
	*/
//C     #define	DB_REP_CLIENT			1
//C     #define	DB_REP_ELECTION			2
const DB_REP_CLIENT = 1;
//C     #define	DB_REP_FULL_ELECTION		3
const DB_REP_ELECTION = 2;
//C     #define	DB_REP_MASTER			4
const DB_REP_FULL_ELECTION = 3;

const DB_REP_MASTER = 4;
/* Acknowledgement policies. */
//C     #define	DB_REPMGR_ACKS_ALL		1
//C     #define	DB_REPMGR_ACKS_ALL_PEERS	2
const DB_REPMGR_ACKS_ALL = 1;
//C     #define	DB_REPMGR_ACKS_NONE		3
const DB_REPMGR_ACKS_ALL_PEERS = 2;
//C     #define	DB_REPMGR_ACKS_ONE		4
const DB_REPMGR_ACKS_NONE = 3;
//C     #define	DB_REPMGR_ACKS_ONE_PEER		5
const DB_REPMGR_ACKS_ONE = 4;
//C     #define	DB_REPMGR_ACKS_QUORUM		6
const DB_REPMGR_ACKS_ONE_PEER = 5;

const DB_REPMGR_ACKS_QUORUM = 6;
/* Replication Framework timeout configuration values. */
//C     #define	DB_REP_ACK_TIMEOUT	1
//C     #define	DB_REP_ELECTION_TIMEOUT 2
const DB_REP_ACK_TIMEOUT = 1;
//C     #define	DB_REP_ELECTION_RETRY	3
const DB_REP_ELECTION_TIMEOUT = 2;
//C     #define	DB_REP_CONNECTION_RETRY 4
const DB_REP_ELECTION_RETRY = 3;

const DB_REP_CONNECTION_RETRY = 4;
/* Event notification types. */
//C     #define	DB_EVENT_NO_SUCH_EVENT		0 /* out-of-band sentinel value */
//C     #define	DB_EVENT_PANIC			1
const DB_EVENT_NO_SUCH_EVENT = 0;
//C     #define	DB_EVENT_REP_CLIENT		2
const DB_EVENT_PANIC = 1;
//C     #define	DB_EVENT_REP_MASTER		3
const DB_EVENT_REP_CLIENT = 2;
//C     #define	DB_EVENT_REP_NEWMASTER		4
const DB_EVENT_REP_MASTER = 3;
//C     #define	DB_EVENT_REP_STARTUPDONE	5
const DB_EVENT_REP_NEWMASTER = 4;
//C     #define	DB_EVENT_WRITE_FAILED		6
const DB_EVENT_REP_STARTUPDONE = 5;

const DB_EVENT_WRITE_FAILED = 6;
/* Flag value for repmgr_add_remote_site. */
//C     #define	DB_REPMGR_PEER          0x01

const DB_REPMGR_PEER = 0x01;
/* Replication Manager site status. */
//C     struct __db_repmgr_site {
//C             int eid;
//C             char *host;
//C             u_int port;

//C     #define	DB_REPMGR_CONNECTED	0x01
//C     #define	DB_REPMGR_DISCONNECTED	0x02
const DB_REPMGR_CONNECTED = 0x01;
//C             u_int32_t status;
const DB_REPMGR_DISCONNECTED = 0x02;
//C     };
struct __db_repmgr_site
{
				int eid;
				char *host;
				u_int port;
				u_int32_t status;
}

/* Replication statistics. */
//C     struct __db_rep_stat {
	/* !!!
		* Many replication statistics fields cannot be protected by a mutex
		* without an unacceptable performance penalty, since most message
		* processing is done without the need to hold a region-wide lock.
		* Fields whose comments end with a '+' may be updated without holding
		* the replication or log mutexes (as appropriate), and thus may be
		* off somewhat (or, on unreasonable architectures under unlucky
		* circumstances, garbaged).
		*/
//C     	u_int32_t st_status;		/* Current replication status. */
//C     	DB_LSN st_next_lsn;		/* Next LSN to use or expect. */
//C     	DB_LSN st_waiting_lsn;		/* LSN we're awaiting, if any. */
//C     	db_pgno_t st_next_pg;		/* Next pg we expect. */
//C     	db_pgno_t st_waiting_pg;	/* pg we're awaiting, if any. */

//C     	u_int32_t st_dupmasters;
/* # of times a duplicate master
								condition was detected.+ */
//C     	int st_env_id;			/* Current environment ID. */
//C     	int st_env_priority;		/* Current environment priority. */
//C     	u_int32_t st_bulk_fills;	/* Bulk buffer fills. */
//C     	u_int32_t st_bulk_overflows;	/* Bulk buffer overflows. */
//C     	u_int32_t st_bulk_records;	/* Bulk records stored. */
//C     	u_int32_t st_bulk_transfers;	/* Transfers of bulk buffers. */
//C     	u_int32_t st_client_rerequests;	/* Number of forced rerequests. */
//C     	u_int32_t st_client_svc_req;
/* Number of client service requests
								received by this client. */
//C     	u_int32_t st_client_svc_miss;
/* Number of client service requests
								missing on this client. */
//C     	u_int32_t st_gen;		/* Current generation number. */
//C     	u_int32_t st_egen;		/* Current election gen number. */
//C     	u_int32_t st_log_duplicated;	/* Log records received multiply.+ */
//C     	u_int32_t st_log_queued;	/* Log records currently queued.+ */
//C     	u_int32_t st_log_queued_max;	/* Max. log records queued at once.+ */
//C     	u_int32_t st_log_queued_total;	/* Total # of log recs. ever queued.+ */
//C     	u_int32_t st_log_records;	/* Log records received and put.+ */
//C     	u_int32_t st_log_requested;	/* Log recs. missed and requested.+ */
//C     	int st_master;			/* Env. ID of the current master. */
//C     	u_int32_t st_master_changes;	/* # of times we've switched masters. */
//C     	u_int32_t st_msgs_badgen;	/* Messages with a bad generation #.+ */
//C     	u_int32_t st_msgs_processed;	/* Messages received and processed.+ */
//C     	u_int32_t st_msgs_recover;
/* Messages ignored because this site
								was a client in recovery.+ */
//C     	u_int32_t st_msgs_send_failures;/* # of failed message sends.+ */
//C     	u_int32_t st_msgs_sent;		/* # of successful message sends.+ */
//C     	u_int32_t st_newsites;		/* # of NEWSITE msgs. received.+ */
//C     	int st_nsites;
/* Current number of sites we will
								assume during elections. */
//C     	u_int32_t st_nthrottles;	/* # of times we were throttled. */
//C     	u_int32_t st_outdated;
/* # of times we detected and returned
								an OUTDATED condition.+ */
//C     	u_int32_t st_pg_duplicated;	/* Pages received multiply.+ */
//C     	u_int32_t st_pg_records;	/* Pages received and stored.+ */
//C     	u_int32_t st_pg_requested;	/* Pages missed and requested.+ */
//C     	u_int32_t st_startup_complete;	/* Site completed client sync-up. */
//C     	u_int32_t st_txns_applied;	/* # of transactions applied.+ */

	/* Elections generally. */
//C     	u_int32_t st_elections;		/* # of elections held.+ */
//C     	u_int32_t st_elections_won;	/* # of elections won by this site.+ */

	/* Statistics about an in-progress election. */
//C     	int st_election_cur_winner;	/* Current front-runner. */
//C     	u_int32_t st_election_gen;	/* Election generation number. */
//C     	DB_LSN st_election_lsn;		/* Max. LSN of current winner. */
//C     	int st_election_nsites;		/* # of "registered voters". */
//C     	int st_election_nvotes;		/* # of "registered voters" needed. */
//C     	int st_election_priority;	/* Current election priority. */
//C     	int st_election_status;		/* Current election status. */
//C     	u_int32_t st_election_tiebreaker;/* Election tiebreaker value. */
//C     	int st_election_votes;		/* Votes received in this round. */
//C     	u_int32_t st_election_sec;	/* Last election time seconds. */
//C     	u_int32_t st_election_usec;	/* Last election time useconds. */
//C     };
struct __db_rep_stat
{
				u_int32_t st_status;
				DB_LSN st_next_lsn;
				DB_LSN st_waiting_lsn;
				db_pgno_t st_next_pg;
				db_pgno_t st_waiting_pg;
				u_int32_t st_dupmasters;
				int st_env_id;
				int st_env_priority;
				u_int32_t st_bulk_fills;
				u_int32_t st_bulk_overflows;
				u_int32_t st_bulk_records;
				u_int32_t st_bulk_transfers;
				u_int32_t st_client_rerequests;
				u_int32_t st_client_svc_req;
				u_int32_t st_client_svc_miss;
				u_int32_t st_gen;
				u_int32_t st_egen;
				u_int32_t st_log_duplicated;
				u_int32_t st_log_queued;
				u_int32_t st_log_queued_max;
				u_int32_t st_log_queued_total;
				u_int32_t st_log_records;
				u_int32_t st_log_requested;
				int st_master;
				u_int32_t st_master_changes;
				u_int32_t st_msgs_badgen;
				u_int32_t st_msgs_processed;
				u_int32_t st_msgs_recover;
				u_int32_t st_msgs_send_failures;
				u_int32_t st_msgs_sent;
				u_int32_t st_newsites;
				int st_nsites;
				u_int32_t st_nthrottles;
				u_int32_t st_outdated;
				u_int32_t st_pg_duplicated;
				u_int32_t st_pg_records;
				u_int32_t st_pg_requested;
				u_int32_t st_startup_complete;
				u_int32_t st_txns_applied;
				u_int32_t st_elections;
				u_int32_t st_elections_won;
				int st_election_cur_winner;
				u_int32_t st_election_gen;
				DB_LSN st_election_lsn;
				int st_election_nsites;
				int st_election_nvotes;
				int st_election_priority;
				int st_election_status;
				u_int32_t st_election_tiebreaker;
				int st_election_votes;
				u_int32_t st_election_sec;
				u_int32_t st_election_usec;
}

/*******************************************************
	* Sequences.
	*******************************************************/
/*
	* The storage record for a sequence.
	*/
//C     struct __db_seq_record {
//C     	u_int32_t	seq_version;	/* Version size/number. */
//C     #define	DB_SEQ_DEC		0x00000001	/* Decrement sequence. */
//C     #define	DB_SEQ_INC		0x00000002	/* Increment sequence. */
const DB_SEQ_DEC = 0x00000001;
//C     #define	DB_SEQ_RANGE_SET	0x00000004	/* Range set (internal). */
const DB_SEQ_INC = 0x00000002;
//C     #define	DB_SEQ_WRAP		0x00000008	/* Wrap sequence at min/max. */
const DB_SEQ_RANGE_SET = 0x00000004;
//C     #define	DB_SEQ_WRAPPED		0x00000010	/* Just wrapped (internal). */
const DB_SEQ_WRAP = 0x00000008;
//C     	u_int32_t	flags;		/* Flags. */
const DB_SEQ_WRAPPED = 0x00000010;
//C     	db_seq_t	seq_value;	/* Current value. */
//C     	db_seq_t	seq_max;	/* Max permitted. */
//C     	db_seq_t	seq_min;	/* Min permitted. */
//C     };
struct __db_seq_record
{
				u_int32_t seq_version;
				u_int32_t flags;
				db_seq_t seq_value;
				db_seq_t seq_max;
				db_seq_t seq_min;
}

/*
	* Handle for a sequence object.
	*/
//C     struct __db_sequence {
//C     	DB		*seq_dbp;	/* DB handle for this sequence. */
//C     	db_mutex_t	mtx_seq;	/* Mutex if sequence is threaded. */
//C     	DB_SEQ_RECORD	*seq_rp;	/* Pointer to current data. */
//C     	DB_SEQ_RECORD	seq_record;	/* Data from DB_SEQUENCE. */
//C     	int32_t		seq_cache_size; /* Number of values cached. */
//C     	db_seq_t	seq_last_value;	/* Last value cached. */
//C     	DBT		seq_key;	/* DBT pointing to sequence key. */
//C     	DBT		seq_data;	/* DBT pointing to seq_record. */

	/* API-private structure: used by C++ and Java. */
//C     	void		*api_internal;

	/* DB_SEQUENCE PUBLIC HANDLE LIST BEGIN */
//C     	int		(*close) __P((DB_SEQUENCE *, u_int32_t));
//C     	int		(*get) __P((DB_SEQUENCE *,
//C     			      DB_TXN *, int32_t, db_seq_t *, u_int32_t));
//C     	int		(*get_cachesize) __P((DB_SEQUENCE *, int32_t *));
//C     	int		(*get_db) __P((DB_SEQUENCE *, DB **));
//C     	int		(*get_flags) __P((DB_SEQUENCE *, u_int32_t *));
//C     	int		(*get_key) __P((DB_SEQUENCE *, DBT *));
//C     	int		(*get_range) __P((DB_SEQUENCE *,
//C     			     db_seq_t *, db_seq_t *));
//C     	int		(*initial_value) __P((DB_SEQUENCE *, db_seq_t));
//C     	int		(*open) __P((DB_SEQUENCE *,
//C     			    DB_TXN *, DBT *, u_int32_t));
//C     	int		(*remove) __P((DB_SEQUENCE *, DB_TXN *, u_int32_t));
//C     	int		(*set_cachesize) __P((DB_SEQUENCE *, int32_t));
//C     	int		(*set_flags) __P((DB_SEQUENCE *, u_int32_t));
//C     	int		(*set_range) __P((DB_SEQUENCE *, db_seq_t, db_seq_t));
//C     	int		(*stat) __P((DB_SEQUENCE *,
//C     			    DB_SEQUENCE_STAT **, u_int32_t));
//C     	int		(*stat_print) __P((DB_SEQUENCE *, u_int32_t));
	/* DB_SEQUENCE PUBLIC HANDLE LIST END */
//C     };
struct __db_sequence
{
				DB *seq_dbp;
				db_mutex_t mtx_seq;
				DB_SEQ_RECORD *seq_rp;
				DB_SEQ_RECORD seq_record;
				int32_t seq_cache_size;
				db_seq_t seq_last_value;
				DBT seq_key;
				DBT seq_data;
				void *api_internal;
				int  function(DB_SEQUENCE *, u_int32_t )close;
				int  function(DB_SEQUENCE *, DB_TXN *, int32_t , db_seq_t *, u_int32_t )get;
				int  function(DB_SEQUENCE *, int32_t *)get_cachesize;
				int  function(DB_SEQUENCE *, DB **)get_db;
				int  function(DB_SEQUENCE *, u_int32_t *)get_flags;
				int  function(DB_SEQUENCE *, DBT *)get_key;
				int  function(DB_SEQUENCE *, db_seq_t *, db_seq_t *)get_range;
				int  function(DB_SEQUENCE *, db_seq_t )initial_value;
				int  function(DB_SEQUENCE *, DB_TXN *, DBT *, u_int32_t )open;
				int  function(DB_SEQUENCE *, DB_TXN *, u_int32_t )remove;
				int  function(DB_SEQUENCE *, int32_t )set_cachesize;
				int  function(DB_SEQUENCE *, u_int32_t )set_flags;
				int  function(DB_SEQUENCE *, db_seq_t , db_seq_t )set_range;
				int  function(DB_SEQUENCE *, DB_SEQUENCE_STAT **, u_int32_t )stat;
				int  function(DB_SEQUENCE *, u_int32_t )stat_print;
}

//C     struct __db_seq_stat {
//C     	u_int32_t st_wait;		/* Sequence lock granted w/o wait. */
//C     	u_int32_t st_nowait;		/* Sequence lock granted after wait. */
//C     	db_seq_t  st_current;		/* Current value in db. */
//C     	db_seq_t  st_value;		/* Current cached value. */
//C     	db_seq_t  st_last_value;	/* Last cached value. */
//C     	db_seq_t  st_min;		/* Minimum value. */
//C     	db_seq_t  st_max;		/* Maximum value. */
//C     	int32_t   st_cache_size;	/* Cache size. */
//C     	u_int32_t st_flags;		/* Flag value. */
//C     };
struct __db_seq_stat
{
				u_int32_t st_wait;
				u_int32_t st_nowait;
				db_seq_t st_current;
				db_seq_t st_value;
				db_seq_t st_last_value;
				db_seq_t st_min;
				db_seq_t st_max;
				int32_t st_cache_size;
				u_int32_t st_flags;
}

/*******************************************************
	* Access methods.
	*******************************************************/
//C     typedef enum {
//C     	DB_BTREE=1,
//C     	DB_HASH=2,
//C     	DB_RECNO=3,
//C     	DB_QUEUE=4,
//C     	DB_UNKNOWN=5			/* Figure it out on open. */
//C     } DBTYPE;
enum
{
				DB_BTREE = 1,
				DB_HASH,
				DB_RECNO,
				DB_QUEUE,
				DB_UNKNOWN,
}
alias int DBTYPE;

//C     #define	DB_RENAMEMAGIC	0x030800	/* File has been renamed. */

const DB_RENAMEMAGIC = 0x030800;
//C     #define	DB_BTREEVERSION	9		/* Current btree version. */
//C     #define	DB_BTREEOLDVER	8		/* Oldest btree version supported. */
const DB_BTREEVERSION = 9;
//C     #define	DB_BTREEMAGIC	0x053162
const DB_BTREEOLDVER = 8;

const DB_BTREEMAGIC = 0x053162;
//C     #define	DB_HASHVERSION	8		/* Current hash version. */
//C     #define	DB_HASHOLDVER	7		/* Oldest hash version supported. */
const DB_HASHVERSION = 8;
//C     #define	DB_HASHMAGIC	0x061561
const DB_HASHOLDVER = 7;

const DB_HASHMAGIC = 0x061561;
//C     #define	DB_QAMVERSION	4		/* Current queue version. */
//C     #define	DB_QAMOLDVER	3		/* Oldest queue version supported. */
const DB_QAMVERSION = 4;
//C     #define	DB_QAMMAGIC	0x042253
const DB_QAMOLDVER = 3;

const DB_QAMMAGIC = 0x042253;
//C     #define	DB_SEQUENCE_VERSION 2		/* Current sequence version. */
//C     #define	DB_SEQUENCE_OLDVER  1		/* Oldest sequence version supported. */
const DB_SEQUENCE_VERSION = 2;

const DB_SEQUENCE_OLDVER = 1;
/*
	* DB access method and cursor operation values.  Each value is an operation
	* code to which additional bit flags are added.
	*/
const	DB_AFTER		= 1	;/* Dbc.put */
const	DB_APPEND	= 2	;/* Db.put */
const	DB_BEFORE		= 3	;/* Dbc.put */
const	DB_CONSUME		= 4	;/* Db.get */
const	DB_CONSUME_WAIT		= 5	;/* Db.get */
const	DB_CURRENT		= 6	;/* Dbc.get, Dbc.put, DbLogc.get */
const	DB_FIRST		 =7	;/* Dbc.get, DbLogc->get */
const	DB_GET_BOTH	=	 8	;/* Db.get, Dbc.get */
const	DB_GET_BOTHC	=	 9	;/* Dbc.get (internal) */
const	DB_GET_BOTH_RANGE=	10	;/* Db.get, Dbc.get */
const	DB_GET_RECNO		=11	;/* Dbc.get */
const	DB_JOIN_ITEM		=12	;/* Dbc.get; don't do primary lookup */
const	DB_KEYFIRST		=13	;/* Dbc.put */
const	DB_KEYLAST		=14	;/* Dbc.put */
const	DB_LAST			=15	;/* Dbc.get, DbLogc->get */
const	DB_NEXT	=	16; /* Dbc.get, DbLogc->get */
const	DB_NEXT_DUP		=17	;/* Dbc.get */
const	DB_NEXT_NODUP	=	18	;/* Dbc.get */
const	DB_NODUPDATA	=	19	;/* Db.put, Dbc.put */
const	DB_NOOVERWRITE	=	20	;/* Db.put */
const	DB_OVERWRITE_DUP	=21	;/* Dbc.put, Db.put; no DB_KEYEXIST */
const	DB_POSITION		=22	;/* Dbc.dup */
const	DB_PREV			=23	;/* Dbc.get, DbLogc->get */
const	DB_PREV_DUP	=	24	;/* Dbc.get */
const	DB_PREV_NODUP	=	25	;/* Dbc.get */
const	DB_SET		=	26	;/* Dbc.get, DbLogc->get */
const	DB_SET_RANGE	=	27	;/* Dbc.get */
const	DB_SET_RECNO	=	28	;/* Db.get, Dbc.get */
const	DB_UPDATE_SECONDARY=	29	;/* Dbc.get, Dbc.del (internal) */
const	DB_SET_LTE	=	30	;/* Dbc.get (internal) */
const	DB_GET_BOTH_LTE	=	31	;/* Dbc.get (internal) */

/* This has to change when the max opcode hits 255. */
//C     #define	DB_OPFLAGS_MASK	0x000000ff	/* Mask for operations flags. */

const DB_OPFLAGS_MASK = 0x000000ff;
/*
	* Masks for flags that can be OR'd into DB access method and cursor
	* operation values.  Three top bits have already been taken:
	*
	* DB_AUTO_COMMIT	0x02000000
	* DB_READ_COMMITTED	0x04000000
	* DB_READ_UNCOMMITTED	0x08000000
	*/
//C     #define	DB_MULTIPLE	0x10000000	/* Return multiple data values. */
//C     #define	DB_MULTIPLE_KEY	0x20000000	/* Return multiple data/key pairs. */
const DB_MULTIPLE = 0x10000000;
//C     #define	DB_RMW		0x40000000	/* Acquire write lock immediately. */
const DB_MULTIPLE_KEY = 0x20000000;

const DB_RMW = 0x40000000;
/*
	* DB (user visible) error return codes.
	*
	* !!!
	* We don't want our error returns to conflict with other packages where
	* possible, so pick a base error value that's hopefully not common.  We
	* document that we own the error name space from -30,800 to -30,999.
	*/
/* DB (public) error return codes. */
//#define	DB_BUFFER_SMALL		(-30999)/* User memory too small for return. */
//#define	DB_DONOTINDEX		(-30998)/* "Null" return from 2ndary callbk. */
//#define	DB_FOREIGN_CONFLICT	(-30997)/* A foreign db constraint triggered. */
//#define	DB_HEAP_FULL		(-30996)/* No free space in a heap file. */
//#define	DB_KEYEMPTY		(-30995)/* Key/data deleted or never created. */
//#define	DB_KEYEXIST		(-30994)/* The key/data pair already exists. */
//#define	DB_LOCK_DEADLOCK	(-30993)/* Deadlock. */
//#define	DB_LOCK_NOTGRANTED	(-30992)/* Lock unavailable. */
//#define	DB_LOG_BUFFER_FULL	(-30991)/* In-memory log buffer full. */
//#define	DB_LOG_VERIFY_BAD	(-30990)/* Log verification failed. */
//#define	DB_META_CHKSUM_FAIL	(-30989)/* Metadata page checksum failed. */
//#define	DB_NOSERVER		(-30988)/* Server panic return. */
//#define	DB_NOTFOUND		(-30987)/* Key/data pair not found (EOF). */
//#define	DB_OLD_VERSION		(-30986)/* Out-of-date version. */
//#define	DB_PAGE_NOTFOUND	(-30985)/* Requested page not found. */
//#define	DB_REP_DUPMASTER	(-30984)/* There are two masters. */
//#define	DB_REP_HANDLE_DEAD	(-30983)/* Rolled back a commit. */
const int DB_REP_HANDLE_DEAD = -30983;
//#define	DB_REP_HOLDELECTION	(-30982)/* Time to hold an election. */
//#define	DB_REP_IGNORE		(-30981)/* This msg should be ignored.*/
//#define	DB_REP_ISPERM		(-30980)/* Cached not written perm written.*/
//#define	DB_REP_JOIN_FAILURE	(-30979)/* Unable to join replication group. */
//#define	DB_REP_LEASE_EXPIRED	(-30978)/* Master lease has expired. */
//#define	DB_REP_LOCKOUT		(-30977)/* API/Replication lockout now. */
const int DB_REP_LOCKOUT = -30977;
//#define	DB_REP_NEWSITE		(-30976)/* New site entered system. */
//#define	DB_REP_NOTPERM		(-30975)/* Permanent log record not written. */
//#define	DB_REP_UNAVAIL		(-30974)/* Site cannot currently be reached. */
//#define	DB_REP_WOULDROLLBACK	(-30973)/* UNDOC: rollback inhibited by app. */
//#define	DB_RUNRECOVERY		(-30972)/* Panic return. */
//#define	DB_SECONDARY_BAD	(-30971)/* Secondary index corrupt. */
//#define	DB_SLICE_CORRUPT	(-30970)/* A part of a sliced env is corrupt. */
//#define	DB_TIMEOUT		(-30969)/* Timed out on read consistency. */
//#define	DB_VERIFY_BAD		(-30968)/* Verify failed; bad format. */
//#define	DB_VERSION_MISMATCH	(-30967)/* Environment version mismatch. */

///* DB (private) error return codes. */
//#define	DB_ALREADY_ABORTED	(-30899)
//				        /* Spare error number (-30898). */
//#define	DB_DELETED		(-30897)/* Recovery file marked deleted. */
//#define	DB_EVENT_NOT_HANDLED	(-30896)/* Forward event to application. */
//#define	DB_NEEDSPLIT		(-30895)/* Page needs to be split. */
//#define	DB_NOINTMP		(-30886)/* Sequences not supported in temporary
//					   or in-memory databases. */
//#define	DB_REP_BULKOVF		(-30894)/* Rep bulk buffer overflow. */
//#define	DB_REP_LOGREADY		(-30893)/* Rep log ready for recovery. */
//#define	DB_REP_NEWMASTER	(-30892)/* We have learned of a new master. */
//#define	DB_REP_PAGEDONE		(-30891)/* This page was already done. */
//#define	DB_SURPRISE_KID		(-30890)/* Child commit where parent
//					   didn't know it was a parent. */
//#define	DB_SWAPBYTES		(-30889)/* Database needs byte swapping. */
//#define	DB_TXN_CKP		(-30888)/* Encountered ckp record in log. */
//#define	DB_VERIFY_FATAL		(-30887)/* DB->verify cannot proceed. */

/* Database handle. */
//C     struct __db {
	/*******************************************************
		* Public: owned by the application.
		*******************************************************/
//C     	u_int32_t pgsize;		/* Database logical page size. */

					/* Callbacks. */
//C     	int (*db_append_recno) __P((DB *, DBT *, db_recno_t));
//C     	void (*db_feedback) __P((DB *, int, int));
//C     	int (*dup_compare) __P((DB *, const DBT *, const DBT *));

//C     	void	*app_private;		/* Application-private handle. */

	/*******************************************************
		* Private: owned by DB.
		*******************************************************/
//C     	DB_ENV	*dbenv;			/* Backing environment. */

//C     	DBTYPE	 type;			/* DB access method type. */

//C     	DB_MPOOLFILE *mpf;		/* Backing buffer pool. */

//C     	db_mutex_t mutex;		/* Synchronization for free threading */

//C     	char *fname, *dname;		/* File/database passed to DB->open. */
//C     	u_int32_t open_flags;		/* Flags passed to DB->open. */

//C     	u_int8_t fileid[DB_FILE_ID_LEN];/* File's unique ID for locking. */

//C     	u_int32_t adj_fileid;		/* File's unique ID for curs. adj. */

//C     #define	DB_LOGFILEID_INVALID	-1
//C     	FNAME *log_filename;		/* File's naming info for logging. */
const DB_LOGFILEID_INVALID = -1;

//C     	db_pgno_t meta_pgno;		/* Meta page number */
//C     	u_int32_t lid;			/* Locker id for handle locking. */
//C     	u_int32_t cur_lid;		/* Current handle lock holder. */
//C     	u_int32_t associate_lid;	/* Locker id for DB->associate call. */
//C     	DB_LOCK	 handle_lock;		/* Lock held on this handle. */

//C     	u_int	 cl_id;			/* RPC: remote client id. */

//C     	time_t	 timestamp;		/* Handle timestamp for replication. */
//C     	u_int32_t fid_gen;		/* Rep generation number for fids. */

	/*
		* Returned data memory for DB->get() and friends.
		*/
//C     	DBT	 my_rskey;		/* Secondary key. */
//C     	DBT	 my_rkey;		/* [Primary] key. */
//C     	DBT	 my_rdata;		/* Data. */

	/*
		* !!!
		* Some applications use DB but implement their own locking outside of
		* DB.  If they're using fcntl(2) locking on the underlying database
		* file, and we open and close a file descriptor for that file, we will
		* discard their locks.  The DB_FCNTL_LOCKING flag to DB->open is an
		* undocumented interface to support this usage which leaves any file
		* descriptors we open until DB->close.  This will only work with the
		* DB->open interface and simple caches, e.g., creating a transaction
		* thread may open/close file descriptors this flag doesn't protect.
		* Locking with fcntl(2) on a file that you don't own is a very, very
		* unsafe thing to do.  'Nuff said.
		*/
//C     	DB_FH	*saved_open_fhp;	/* Saved file handle. */

	/*
		* Linked list of DBP's, linked from the DB_ENV, used to keep track
		* of all open db handles for cursor adjustment.
		*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_ENTRY(__db) dblistlinks;
		*/
//C     	struct {
//C     		struct __db *tqe_next;
//C     		struct __db **tqe_prev;
//C     	} dblistlinks;
struct _N13
{
				__db *tqe_next;
				__db **tqe_prev;
}

	/*
		* Cursor queues.
		*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_HEAD(__cq_fq, __dbc) free_queue;
		* TAILQ_HEAD(__cq_aq, __dbc) active_queue;
		* TAILQ_HEAD(__cq_jq, __dbc) join_queue;
		*/
//C     	struct __cq_fq {
//C     		struct __dbc *tqh_first;
//C     		struct __dbc **tqh_last;
//C     	} free_queue;
struct __cq_fq
{
				__dbc *tqh_first;
				__dbc **tqh_last;
}
//C     	struct __cq_aq {
//C     		struct __dbc *tqh_first;
//C     		struct __dbc **tqh_last;
//C     	} active_queue;
struct __cq_aq
{
				__dbc *tqh_first;
				__dbc **tqh_last;
}
//C     	struct __cq_jq {
//C     		struct __dbc *tqh_first;
//C     		struct __dbc **tqh_last;
//C     	} join_queue;
struct __cq_jq
{
				__dbc *tqh_first;
				__dbc **tqh_last;
}

	/*
		* Secondary index support.
		*
		* Linked list of secondary indices -- set in the primary.
		*
		* !!!
		* Explicit representations of structures from queue.h.
		* LIST_HEAD(s_secondaries, __db);
		*/
//C     	struct {
//C     		struct __db *lh_first;
//C     	} s_secondaries;
struct _N14
{
				__db *lh_first;
}

	/*
		* List entries for secondaries, and reference count of how
		* many threads are updating this secondary (see __db_c_put).
		*
		* !!!
		* Note that these are synchronized by the primary's mutex, but
		* filled in in the secondaries.
		*
		* !!!
		* Explicit representations of structures from queue.h.
		* LIST_ENTRY(__db) s_links;
		*/
//C     	struct {
//C     		struct __db *le_next;
//C     		struct __db **le_prev;
//C     	} s_links;
struct _N15
{
				__db *le_next;
				__db **le_prev;
}
//C     	u_int32_t s_refcnt;

	/* Secondary callback and free functions -- set in the secondary. */
//C     	int	(*s_callback) __P((DB *, const DBT *, const DBT *, DBT *));

	/* Reference to primary -- set in the secondary. */
//C     	DB	*s_primary;

//C     #define	DB_ASSOC_IMMUTABLE_KEY    0x00000001 /* Secondary key is immutable. */

const DB_ASSOC_IMMUTABLE_KEY = 0x00000001;
	/* Flags passed to associate -- set in the secondary. */
//C     	u_int32_t s_assoc_flags;

	/* API-private structure: used by DB 1.85, C++, Java, Perl and Tcl */
//C     	void	*api_internal;

	/* Subsystem-private structure. */
//C     	void	*bt_internal;		/* Btree/Recno access method. */
//C     	void	*h_internal;		/* Hash access method. */
//C     	void	*q_internal;		/* Queue access method. */
//C     	void	*xa_internal;		/* XA. */

	/* DB PUBLIC HANDLE LIST BEGIN */
//C     	int  (*associate) __P((DB *, DB_TXN *, DB *,
//C     		int (*)(DB *, const DBT *, const DBT *, DBT *), u_int32_t));
//C     	int  (*close) __P((DB *, u_int32_t));
//C     	int  (*compact) __P((DB *,
//C     		DB_TXN *, DBT *, DBT *, DB_COMPACT *, u_int32_t, DBT *));
//C     	int  (*cursor) __P((DB *, DB_TXN *, DBC **, u_int32_t));
//C     	int  (*del) __P((DB *, DB_TXN *, DBT *, u_int32_t));
//C     	void (*err) __P((DB *, int, const char *, ...));
//C     	void (*errx) __P((DB *, const char *, ...));
//C     	int  (*fd) __P((DB *, int *));
//C     	int  (*get) __P((DB *, DB_TXN *, DBT *, DBT *, u_int32_t));
//C     	int  (*get_bt_minkey) __P((DB *, u_int32_t *));
//C     	int  (*get_byteswapped) __P((DB *, int *));
//C     	int  (*get_cachesize) __P((DB *, u_int32_t *, u_int32_t *, int *));
//C     	int  (*get_dbname) __P((DB *, const char **, const char **));
//C     	int  (*get_encrypt_flags) __P((DB *, u_int32_t *));
//C     	DB_ENV *(*get_env) __P((DB *));
//C     	void (*get_errfile) __P((DB *, FILE **));
//C     	void (*get_errpfx) __P((DB *, const char **));
//C     	int  (*get_flags) __P((DB *, u_int32_t *));
//C     	int  (*get_h_ffactor) __P((DB *, u_int32_t *));
//C     	int  (*get_h_nelem) __P((DB *, u_int32_t *));
//C     	int  (*get_lorder) __P((DB *, int *));
//C     	DB_MPOOLFILE *(*get_mpf) __P((DB *));
//C     	void (*get_msgfile) __P((DB *, FILE **));
//C     	int  (*get_open_flags) __P((DB *, u_int32_t *));
//C     	int  (*get_pagesize) __P((DB *, u_int32_t *));
//C     	int  (*get_q_extentsize) __P((DB *, u_int32_t *));
//C     	int  (*get_re_delim) __P((DB *, int *));
//C     	int  (*get_re_len) __P((DB *, u_int32_t *));
//C     	int  (*get_re_pad) __P((DB *, int *));
//C     	int  (*get_re_source) __P((DB *, const char **));
//C     	int  (*get_transactional) __P((DB *));
//C     	int  (*get_type) __P((DB *, DBTYPE *));
//C     	int  (*join) __P((DB *, DBC **, DBC **, u_int32_t));
//C     	int  (*key_range)
//C     		__P((DB *, DB_TXN *, DBT *, DB_KEY_RANGE *, u_int32_t));
//C     	int  (*open) __P((DB *,
//C     		DB_TXN *, const char *, const char *, DBTYPE, u_int32_t, int));
//C     	int  (*pget) __P((DB *, DB_TXN *, DBT *, DBT *, DBT *, u_int32_t));
//C     	int  (*put) __P((DB *, DB_TXN *, DBT *, DBT *, u_int32_t));
//C     	int  (*remove) __P((DB *, const char *, const char *, u_int32_t));
//C     	int  (*rename) __P((DB *,
//C     		const char *, const char *, const char *, u_int32_t));
//C     	int  (*set_alloc) __P((DB *, void *(*)(size_t),
//C     		void *(*)(void *, size_t), void (*)(void *)));
//C     	int  (*set_append_recno) __P((DB *, int (*)(DB *, DBT *, db_recno_t)));
//C     	int  (*set_bt_compare)
//C     		__P((DB *, int (*)(DB *, const DBT *, const DBT *)));
//C     	int  (*set_bt_minkey) __P((DB *, u_int32_t));
//C     	int  (*set_bt_prefix)
//C     		__P((DB *, size_t (*)(DB *, const DBT *, const DBT *)));
//C     	int  (*set_cachesize) __P((DB *, u_int32_t, u_int32_t, int));
//C     	int  (*set_dup_compare)
//C     		__P((DB *, int (*)(DB *, const DBT *, const DBT *)));
//C     	int  (*set_encrypt) __P((DB *, const char *, u_int32_t));
//C     	void (*set_errcall) __P((DB *,
//C     		void (*)(const DB_ENV *, const char *, const char *)));
//C     	void (*set_errfile) __P((DB *, FILE *));
//C     	void (*set_errpfx) __P((DB *, const char *));
//C     	int  (*set_feedback) __P((DB *, void (*)(DB *, int, int)));
//C     	int  (*set_flags) __P((DB *, u_int32_t));
//C     	int  (*set_h_ffactor) __P((DB *, u_int32_t));
//C     	int  (*set_h_hash)
//C     		__P((DB *, u_int32_t (*)(DB *, const void *, u_int32_t)));
//C     	int  (*set_h_nelem) __P((DB *, u_int32_t));
//C     	int  (*set_lorder) __P((DB *, int));
//C     	void (*set_msgcall) __P((DB *, void (*)(const DB_ENV *, const char *)));
//C     	void (*set_msgfile) __P((DB *, FILE *));
//C     	int  (*set_pagesize) __P((DB *, u_int32_t));
//C     	int  (*set_paniccall) __P((DB *, void (*)(DB_ENV *, int)));
//C     	int  (*set_q_extentsize) __P((DB *, u_int32_t));
//C     	int  (*set_re_delim) __P((DB *, int));
//C     	int  (*set_re_len) __P((DB *, u_int32_t));
//C     	int  (*set_re_pad) __P((DB *, int));
//C     	int  (*set_re_source) __P((DB *, const char *));
//C     	int  (*stat) __P((DB *, DB_TXN *, void *, u_int32_t));
//C     	int  (*stat_print) __P((DB *, u_int32_t));
//C     	int  (*sync) __P((DB *, u_int32_t));
//C     	int  (*truncate) __P((DB *, DB_TXN *, u_int32_t *, u_int32_t));
//C     	int  (*upgrade) __P((DB *, const char *, u_int32_t));
//C     	int  (*verify)
//C     		__P((DB *, const char *, const char *, FILE *, u_int32_t));
	/* DB PUBLIC HANDLE LIST END */

	/* DB PRIVATE HANDLE LIST BEGIN */
//C     	int  (*dump) __P((DB *, const char *,
//C     		int (*)(void *, const void *), void *, int, int));
//C     	int  (*db_am_remove) __P((DB *, DB_TXN *, const char *, const char *));
//C     	int  (*db_am_rename) __P((DB *, DB_TXN *,
//C     	    const char *, const char *, const char *));
	/* DB PRIVATE HANDLE LIST END */

	/*
		* Never called; these are a place to save function pointers
		* so that we can undo an associate.
		*/
//C     	int  (*stored_get) __P((DB *, DB_TXN *, DBT *, DBT *, u_int32_t));
//C     	int  (*stored_close) __P((DB *, u_int32_t));

//C     #define	DB_OK_BTREE	0x01
//C     #define	DB_OK_HASH	0x02
const DB_OK_BTREE = 0x01;
//C     #define	DB_OK_QUEUE	0x04
const DB_OK_HASH = 0x02;
//C     #define	DB_OK_RECNO	0x08
const DB_OK_QUEUE = 0x04;
//C     	u_int32_t	am_ok;		/* Legal AM choices. */
const DB_OK_RECNO = 0x08;

	/*
		* This field really ought to be an AM_FLAG, but we have
		* have run out of bits.  If/when we decide to split up
		* the flags, we can incorporate it.
		*/
//C     	int	 preserve_fid;		/* Do not free fileid on close. */

//C     #define	DB_AM_CHKSUM		0x00000001 /* Checksumming */
//C     #define	DB_AM_CL_WRITER		0x00000002 /* Allow writes in client replica */
const DB_AM_CHKSUM = 0x00000001;
//C     #define	DB_AM_COMPENSATE	0x00000004 /* Created by compensating txn */
const DB_AM_CL_WRITER = 0x00000002;
//C     #define	DB_AM_CREATED		0x00000008 /* Database was created upon open */
const DB_AM_COMPENSATE = 0x00000004;
//C     #define	DB_AM_CREATED_MSTR	0x00000010 /* Encompassing file was created */
const DB_AM_CREATED = 0x00000008;
//C     #define	DB_AM_DBM_ERROR		0x00000020 /* Error in DBM/NDBM database */
const DB_AM_CREATED_MSTR = 0x00000010;
//C     #define	DB_AM_DELIMITER		0x00000040 /* Variable length delimiter set */
const DB_AM_DBM_ERROR = 0x00000020;
//C     #define	DB_AM_DISCARD		0x00000080 /* Discard any cached pages */
const DB_AM_DELIMITER = 0x00000040;
//C     #define	DB_AM_DUP		0x00000100 /* DB_DUP */
const DB_AM_DISCARD = 0x00000080;
//C     #define	DB_AM_DUPSORT		0x00000200 /* DB_DUPSORT */
const DB_AM_DUP = 0x00000100;
//C     #define	DB_AM_ENCRYPT		0x00000400 /* Encryption */
const DB_AM_DUPSORT = 0x00000200;
//C     #define	DB_AM_FIXEDLEN		0x00000800 /* Fixed-length records */
const DB_AM_ENCRYPT = 0x00000400;
//C     #define	DB_AM_INMEM		0x00001000 /* In-memory; no sync on close */
const DB_AM_FIXEDLEN = 0x00000800;
//C     #define	DB_AM_INORDER		0x00002000 /* DB_INORDER */
const DB_AM_INMEM = 0x00001000;
//C     #define	DB_AM_IN_RENAME		0x00004000 /* File is being renamed */
const DB_AM_INORDER = 0x00002000;
//C     #define	DB_AM_NOT_DURABLE	0x00008000 /* Do not log changes */
const DB_AM_IN_RENAME = 0x00004000;
//C     #define	DB_AM_OPEN_CALLED	0x00010000 /* DB->open called */
const DB_AM_NOT_DURABLE = 0x00008000;
//C     #define	DB_AM_PAD		0x00020000 /* Fixed-length record pad */
const DB_AM_OPEN_CALLED = 0x00010000;
//C     #define	DB_AM_PGDEF		0x00040000 /* Page size was defaulted */
const DB_AM_PAD = 0x00020000;
//C     #define	DB_AM_RDONLY		0x00080000 /* Database is readonly */
const DB_AM_PGDEF = 0x00040000;
//C     #define	DB_AM_READ_UNCOMMITTED	0x00100000 /* Support degree 1 isolation */
const DB_AM_RDONLY = 0x00080000;
//C     #define	DB_AM_RECNUM		0x00200000 /* DB_RECNUM */
const DB_AM_READ_UNCOMMITTED = 0x00100000;
//C     #define	DB_AM_RECOVER		0x00400000 /* DB opened by recovery routine */
const DB_AM_RECNUM = 0x00200000;
//C     #define	DB_AM_RENUMBER		0x00800000 /* DB_RENUMBER */
const DB_AM_RECOVER = 0x00400000;
//C     #define	DB_AM_REVSPLITOFF	0x01000000 /* DB_REVSPLITOFF */
const DB_AM_RENUMBER = 0x00800000;
//C     #define	DB_AM_SECONDARY		0x02000000 /* Database is a secondary index */
const DB_AM_REVSPLITOFF = 0x01000000;
//C     #define	DB_AM_SNAPSHOT		0x04000000 /* DB_SNAPSHOT */
const DB_AM_SECONDARY = 0x02000000;
//C     #define	DB_AM_SUBDB		0x08000000 /* Subdatabases supported */
const DB_AM_SNAPSHOT = 0x04000000;
//C     #define	DB_AM_SWAP		0x10000000 /* Pages need to be byte-swapped */
const DB_AM_SUBDB = 0x08000000;
//C     #define	DB_AM_TXN		0x20000000 /* Opened in a transaction */
const DB_AM_SWAP = 0x10000000;
//C     #define	DB_AM_VERIFYING		0x40000000 /* DB handle is in the verifier */
const DB_AM_TXN = 0x20000000;
//C     	u_int32_t orig_flags;		   /* Flags at  open, for refresh */
const DB_AM_VERIFYING = 0x40000000;
//C     	u_int32_t flags;
//C     };
struct __db
{
				u_int32_t pgsize;
				int  function(DB *, DBT *, db_recno_t )db_append_recno;
				void  function(DB *, int , int )db_feedback;
				int  function(DB *, DBT *, DBT *)dup_compare;
				void *app_private;
				DB_ENV *dbenv;
				DBTYPE type;
				DB_MPOOLFILE *mpf;
				db_mutex_t mutex;
				char *fname;
				char *dname;
				u_int32_t open_flags;
				u_int8_t [20]fileid;
				u_int32_t adj_fileid;
				FNAME *log_filename;
				db_pgno_t meta_pgno;
				u_int32_t lid;
				u_int32_t cur_lid;
				u_int32_t associate_lid;
				DB_LOCK handle_lock;
				u_int cl_id;
				time_t timestamp;
				u_int32_t fid_gen;
				DBT my_rskey;
				DBT my_rkey;
				DBT my_rdata;
				DB_FH *saved_open_fhp;
				_N13 dblistlinks;
				__cq_fq free_queue;
				__cq_aq active_queue;
				__cq_jq join_queue;
				_N14 s_secondaries;
				_N15 s_links;
				u_int32_t s_refcnt;
				int  function(DB *, DBT *, DBT *, DBT *)s_callback;
				DB *s_primary;
				u_int32_t s_assoc_flags;
				void *api_internal;
				void *bt_internal;
				void *h_internal;
				void *q_internal;
				void *xa_internal;
				int  function(DB *, DB_TXN *, DB *, int  function(DB *, DBT *, DBT *, DBT *), u_int32_t )associate;
				int  function(DB *, u_int32_t )close;
				int  function(DB *, DB_TXN *, DBT *, DBT *, DB_COMPACT *, u_int32_t , DBT *)compact;
				int  function(DB *, DB_TXN *, DBC **, u_int32_t )cursor;
				int  function(DB *, DB_TXN *, DBT *, u_int32_t )del;
				void  function(DB *, int , char *,...)err;
				void  function(DB *, char *,...)errx;
				int  function(DB *, int *)fd;
				int  function(DB *, DB_TXN *, DBT *, DBT *, u_int32_t )get;
				int  function(DB *, u_int32_t *)get_bt_minkey;
				int  function(DB *, int *)get_byteswapped;
				int  function(DB *, u_int32_t *, u_int32_t *, int *)get_cachesize;
				int  function(DB *, char **, char **)get_dbname;
				int  function(DB *, u_int32_t *)get_encrypt_flags;
				DB_ENV * function(DB *)get_env;
				void  function(DB *, FILE **)get_errfile;
				void  function(DB *, char **)get_errpfx;
				int  function(DB *, u_int32_t *)get_flags;
				int  function(DB *, u_int32_t *)get_h_ffactor;
				int  function(DB *, u_int32_t *)get_h_nelem;
				int  function(DB *, int *)get_lorder;
				DB_MPOOLFILE * function(DB *)get_mpf;
				void  function(DB *, FILE **)get_msgfile;
				int  function(DB *, u_int32_t *)get_open_flags;
				int  function(DB *, u_int32_t *)get_pagesize;
				int  function(DB *, u_int32_t *)get_q_extentsize;
				int  function(DB *, int *)get_re_delim;
				int  function(DB *, u_int32_t *)get_re_len;
				int  function(DB *, int *)get_re_pad;
				int  function(DB *, char **)get_re_source;
				int  function(DB *)get_transactional;
				int  function(DB *, DBTYPE *)get_type;
				int  function(DB *, DBC **, DBC **, u_int32_t )join;
				int  function(DB *, DB_TXN *, DBT *, DB_KEY_RANGE *, u_int32_t )key_range;
				int  function(DB *, DB_TXN *, char *, char *, DBTYPE , u_int32_t , int )open;
				int  function(DB *, DB_TXN *, DBT *, DBT *, DBT *, u_int32_t )pget;
				int  function(DB *, DB_TXN *, DBT *, DBT *, u_int32_t )put;
				int  function(DB *, char *, char *, u_int32_t )remove;
				int  function(DB *, char *, char *, char *, u_int32_t )rename;
				int  function(DB *, void * function(size_t ), void * function(void *, size_t ), void  function(void *))set_alloc;
				int  function(DB *, int  function(DB *, DBT *, db_recno_t ))set_append_recno;
				int  function(DB *, int  function(DB *, DBT *, DBT *))set_bt_compare;
				int  function(DB *, u_int32_t )set_bt_minkey;
				int  function(DB *, size_t  function(DB *, DBT *, DBT *))set_bt_prefix;
				int  function(DB *, u_int32_t , u_int32_t , int )set_cachesize;
				int  function(DB *, int  function(DB *, DBT *, DBT *))set_dup_compare;
				int  function(DB *, char *, u_int32_t )set_encrypt;
				void  function(DB *, void  function(DB_ENV *, char *, char *))set_errcall;
				void  function(DB *, FILE *)set_errfile;
				void  function(DB *, char *)set_errpfx;
				int  function(DB *, void  function(DB *, int , int ))set_feedback;
				int  function(DB *, u_int32_t )set_flags;
				int  function(DB *, u_int32_t )set_h_ffactor;
				int  function(DB *, u_int32_t  function(DB *, void *, u_int32_t ))set_h_hash;
				int  function(DB *, u_int32_t )set_h_nelem;
				int  function(DB *, int )set_lorder;
				void  function(DB *, void  function(DB_ENV *, char *))set_msgcall;
				void  function(DB *, FILE *)set_msgfile;
				int  function(DB *, u_int32_t )set_pagesize;
				int  function(DB *, void  function(DB_ENV *, int ))set_paniccall;
				int  function(DB *, u_int32_t )set_q_extentsize;
				int  function(DB *, int )set_re_delim;
				int  function(DB *, u_int32_t )set_re_len;
				int  function(DB *, int )set_re_pad;
				int  function(DB *, char *)set_re_source;
				int  function(DB *, DB_TXN *, void *, u_int32_t )stat;
				int  function(DB *, u_int32_t )stat_print;
				int  function(DB *, u_int32_t )sync;
				int  function(DB *, DB_TXN *, u_int32_t *, u_int32_t )truncate;
				int  function(DB *, char *, u_int32_t )upgrade;
				int  function(DB *, char *, char *, FILE *, u_int32_t )verify;
				int  function(DB *, char *, int  function(void *, void *), void *, int , int )dump;
				int  function(DB *, DB_TXN *, char *, char *)db_am_remove;
				int  function(DB *, DB_TXN *, char *, char *, char *)db_am_rename;
				int  function(DB *, DB_TXN *, DBT *, DBT *, u_int32_t )stored_get;
				int  function(DB *, u_int32_t )stored_close;
				u_int32_t am_ok;
				int preserve_fid;
				u_int32_t orig_flags;
				u_int32_t flags;
}

/*
	* Macros for bulk get.  These are only intended for the C API.
	* For C++, use DbMultiple*Iterator.
	*/
//C     #define	DB_MULTIPLE_INIT(pointer, dbt)						(pointer = (u_int8_t *)(dbt)->data +					    (dbt)->ulen - sizeof(u_int32_t))
//C     #define	DB_MULTIPLE_NEXT(pointer, dbt, retdata, retdlen)			do {										if (*((u_int32_t *)(pointer)) == (u_int32_t)-1) {				retdata = NULL;								pointer = NULL;								break;								}									retdata = (u_int8_t *)							    (dbt)->data + *(u_int32_t *)(pointer);				(pointer) = (u_int32_t *)(pointer) - 1;					retdlen = *(u_int32_t *)(pointer);					(pointer) = (u_int32_t *)(pointer) - 1;					if (retdlen == 0 &&							    retdata == (u_int8_t *)(dbt)->data)						retdata = NULL;						} while (0)
//C     #define	DB_MULTIPLE_KEY_NEXT(pointer, dbt, retkey, retklen, retdata, retdlen) 	do {										if (*((u_int32_t *)(pointer)) == (u_int32_t)-1) {				retdata = NULL;								retkey = NULL;								pointer = NULL;								break;								}									retkey = (u_int8_t *)							    (dbt)->data + *(u_int32_t *)(pointer);				(pointer) = (u_int32_t *)(pointer) - 1;					retklen = *(u_int32_t *)(pointer);					(pointer) = (u_int32_t *)(pointer) - 1;					retdata = (u_int8_t *)							    (dbt)->data + *(u_int32_t *)(pointer);				(pointer) = (u_int32_t *)(pointer) - 1;					retdlen = *(u_int32_t *)(pointer);					(pointer) = (u_int32_t *)(pointer) - 1;				} while (0)

//C     #define	DB_MULTIPLE_RECNO_NEXT(pointer, dbt, recno, retdata, retdlen)   	do {										if (*((u_int32_t *)(pointer)) == (u_int32_t)0) {				recno = 0;								retdata = NULL;								pointer = NULL;								break;								}									recno = *(u_int32_t *)(pointer);					(pointer) = (u_int32_t *)(pointer) - 1;					retdata = (u_int8_t *)							    (dbt)->data + *(u_int32_t *)(pointer);				(pointer) = (u_int32_t *)(pointer) - 1;					retdlen = *(u_int32_t *)(pointer);					(pointer) = (u_int32_t *)(pointer) - 1;				} while (0)

/*******************************************************
	* Access method cursors.
	*******************************************************/
//C     struct __dbc {
//C     	DB *dbp;			/* Related DB access method. */
//C     	DB_TXN	 *txn;			/* Associated transaction. */

	/*
		* Active/free cursor queues.
		*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_ENTRY(__dbc) links;
		*/
//C     	struct {
//C     		DBC *tqe_next;
//C     		DBC **tqe_prev;
//C     	} links;
struct _N16
{
				DBC *tqe_next;
				DBC **tqe_prev;
}

	/*
		* The DBT *'s below are used by the cursor routines to return
		* data to the user when DBT flags indicate that DB should manage
		* the returned memory.  They point at a DBT containing the buffer
		* and length that will be used, and "belonging" to the handle that
		* should "own" this memory.  This may be a "my_*" field of this
		* cursor--the default--or it may be the corresponding field of
		* another cursor, a DB handle, a join cursor, etc.  In general, it
		* will be whatever handle the user originally used for the current
		* DB interface call.
		*/
//C     	DBT	 *rskey;		/* Returned secondary key. */
//C     	DBT	 *rkey;			/* Returned [primary] key. */
//C     	DBT	 *rdata;		/* Returned data. */

//C     	DBT	  my_rskey;		/* Space for returned secondary key. */
//C     	DBT	  my_rkey;		/* Space for returned [primary] key. */
//C     	DBT	  my_rdata;		/* Space for returned data. */

//C     	void	 *lref;			/* Reference to default locker. */
//C     	u_int32_t locker;		/* Locker for this operation. */
//C     	DBT	  lock_dbt;		/* DBT referencing lock. */
//C     	DB_LOCK_ILOCK lock;		/* Object to be locked. */
//C     	DB_LOCK	  mylock;		/* CDB lock held on this cursor. */

//C     	u_int	  cl_id;		/* Remote client id. */

//C     	DBTYPE	  dbtype;		/* Cursor type. */

//C     	DBC_INTERNAL *internal;		/* Access method private. */

	/* DBC PUBLIC HANDLE LIST BEGIN */
//C     	int (*c_close) __P((DBC *));
//C     	int (*c_count) __P((DBC *, db_recno_t *, u_int32_t));
//C     	int (*c_del) __P((DBC *, u_int32_t));
//C     	int (*c_dup) __P((DBC *, DBC **, u_int32_t));
//C     	int (*c_get) __P((DBC *, DBT *, DBT *, u_int32_t));
//C     	int (*c_pget) __P((DBC *, DBT *, DBT *, DBT *, u_int32_t));
//C     	int (*c_put) __P((DBC *, DBT *, DBT *, u_int32_t));
	/* DBC PUBLIC HANDLE LIST END */

	/* DBC PRIVATE HANDLE LIST BEGIN */
//C     	int (*c_am_bulk) __P((DBC *, DBT *, u_int32_t));
//C     	int (*c_am_close) __P((DBC *, db_pgno_t, int *));
//C     	int (*c_am_del) __P((DBC *));
//C     	int (*c_am_destroy) __P((DBC *));
//C     	int (*c_am_get) __P((DBC *, DBT *, DBT *, u_int32_t, db_pgno_t *));
//C     	int (*c_am_put) __P((DBC *, DBT *, DBT *, u_int32_t, db_pgno_t *));
//C     	int (*c_am_writelock) __P((DBC *));
	/* DBC PRIVATE HANDLE LIST END */

/*
	* DBC_DONTLOCK and DBC_RECOVER are used during recovery and transaction
	* abort.  If a transaction is being aborted or recovered then DBC_RECOVER
	* will be set and locking and logging will be disabled on this cursor.  If
	* we are performing a compensating transaction (e.g. free page processing)
	* then DB_DONTLOCK will be set to inhibit locking, but logging will still
	* be required. DB_DONTLOCK is also used if the whole database is locked.
	*/
//C     #define	DBC_ACTIVE		0x0001	/* Cursor in use. */
//C     #define	DBC_DONTLOCK		0x0002	/* Don't lock on this cursor. */
const DBC_ACTIVE = 0x0001;
//C     #define	DBC_MULTIPLE		0x0004	/* Return Multiple data. */
const DBC_DONTLOCK = 0x0002;
//C     #define	DBC_MULTIPLE_KEY	0x0008	/* Return Multiple keys and data. */
const DBC_MULTIPLE = 0x0004;
//C     #define	DBC_OPD			0x0010	/* Cursor references off-page dups. */
const DBC_MULTIPLE_KEY = 0x0008;
//C     #define	DBC_OWN_LID		0x0020	/* Free lock id on destroy. */
const DBC_OPD = 0x0010;
//C     #define	DBC_READ_COMMITTED	0x0040	/* Cursor has degree 2 isolation. */
const DBC_OWN_LID = 0x0020;
//C     #define	DBC_READ_UNCOMMITTED	0x0080	/* Cursor has degree 1 isolation. */
const DBC_READ_COMMITTED = 0x0040;
//C     #define	DBC_RECOVER		0x0100	/* Recovery cursor; don't log/lock. */
const DBC_READ_UNCOMMITTED = 0x0080;
//C     #define	DBC_RMW			0x0200	/* Acquire write flag in read op. */
const DBC_RECOVER = 0x0100;
//C     #define	DBC_TRANSIENT		0x0400	/* Cursor is transient. */
const DBC_RMW = 0x0200;
//C     #define	DBC_WRITECURSOR		0x0800	/* Cursor may be used to write (CDB). */
const DBC_TRANSIENT = 0x0400;
//C     #define	DBC_WRITER		0x1000	/* Cursor immediately writing (CDB). */
const DBC_WRITECURSOR = 0x0800;
//C     	u_int32_t flags;
const DBC_WRITER = 0x1000;
//C     };
struct __dbc
{
				DB *dbp;
				DB_TXN *txn;
				_N16 links;
				DBT *rskey;
				DBT *rkey;
				DBT *rdata;
				DBT my_rskey;
				DBT my_rkey;
				DBT my_rdata;
				void *lref;
				u_int32_t locker;
				DBT lock_dbt;
				DB_LOCK_ILOCK lock;
				DB_LOCK mylock;
				u_int cl_id;
				DBTYPE dbtype;
				DBC_INTERNAL *internal;
				int  function(DBC *)c_close;
				int  function(DBC *, db_recno_t *, u_int32_t )c_count;
				int  function(DBC *, u_int32_t )c_del;
				int  function(DBC *, DBC **, u_int32_t )c_dup;
				int  function(DBC *, DBT *, DBT *, u_int32_t )c_get;
				int  function(DBC *, DBT *, DBT *, DBT *, u_int32_t )c_pget;
				int  function(DBC *, DBT *, DBT *, u_int32_t )c_put;
				int  function(DBC *, DBT *, u_int32_t )c_am_bulk;
				int  function(DBC *, db_pgno_t , int *)c_am_close;
				int  function(DBC *)c_am_del;
				int  function(DBC *)c_am_destroy;
				int  function(DBC *, DBT *, DBT *, u_int32_t , db_pgno_t *)c_am_get;
				int  function(DBC *, DBT *, DBT *, u_int32_t , db_pgno_t *)c_am_put;
				int  function(DBC *)c_am_writelock;
				u_int32_t flags;
}

/* Key range statistics structure */
//C     struct __key_range {
//C     	double less;
//C     	double equal;
//C     	double greater;
//C     };
struct __key_range
{
				double less;
				double equal;
				double greater;
}

/* Btree/Recno statistics structure. */
//C     struct __db_bt_stat {
//C     	u_int32_t bt_magic;		/* Magic number. */
//C     	u_int32_t bt_version;		/* Version number. */
//C     	u_int32_t bt_metaflags;		/* Metadata flags. */
//C     	u_int32_t bt_nkeys;		/* Number of unique keys. */
//C     	u_int32_t bt_ndata;		/* Number of data items. */
//C     	u_int32_t bt_pagesize;		/* Page size. */
//C     	u_int32_t bt_minkey;		/* Minkey value. */
//C     	u_int32_t bt_re_len;		/* Fixed-length record length. */
//C     	u_int32_t bt_re_pad;		/* Fixed-length record pad. */
//C     	u_int32_t bt_levels;		/* Tree levels. */
//C     	u_int32_t bt_int_pg;		/* Internal pages. */
//C     	u_int32_t bt_leaf_pg;		/* Leaf pages. */
//C     	u_int32_t bt_dup_pg;		/* Duplicate pages. */
//C     	u_int32_t bt_over_pg;		/* Overflow pages. */
//C     	u_int32_t bt_empty_pg;		/* Empty pages. */
//C     	u_int32_t bt_free;		/* Pages on the free list. */
//C     	u_int32_t bt_int_pgfree;	/* Bytes free in internal pages. */
//C     	u_int32_t bt_leaf_pgfree;	/* Bytes free in leaf pages. */
//C     	u_int32_t bt_dup_pgfree;	/* Bytes free in duplicate pages. */
//C     	u_int32_t bt_over_pgfree;	/* Bytes free in overflow pages. */
//C     };
struct __db_bt_stat
{
				u_int32_t bt_magic;
				u_int32_t bt_version;
				u_int32_t bt_metaflags;
				u_int32_t bt_nkeys;
				u_int32_t bt_ndata;
				u_int32_t bt_pagesize;
				u_int32_t bt_minkey;
				u_int32_t bt_re_len;
				u_int32_t bt_re_pad;
				u_int32_t bt_levels;
				u_int32_t bt_int_pg;
				u_int32_t bt_leaf_pg;
				u_int32_t bt_dup_pg;
				u_int32_t bt_over_pg;
				u_int32_t bt_empty_pg;
				u_int32_t bt_free;
				u_int32_t bt_int_pgfree;
				u_int32_t bt_leaf_pgfree;
				u_int32_t bt_dup_pgfree;
				u_int32_t bt_over_pgfree;
}

//C     struct __db_compact {
	/* Input Parameters. */
//C     	u_int32_t	compact_fillpercent;	/* Desired fillfactor: 1-100 */
//C     	db_timeout_t	compact_timeout;	/* Lock timeout. */
//C     	u_int32_t	compact_pages;		/* Max pages to process. */
	/* Output Stats. */
//C     	u_int32_t	compact_pages_free;	/* Number of pages freed. */
//C     	u_int32_t	compact_pages_examine;	/* Number of pages examine. */
//C     	u_int32_t	compact_levels;		/* Number of levels removed. */
//C     	u_int32_t	compact_deadlock;	/* Number of deadlocks. */
//C     	db_pgno_t	compact_pages_truncated; /* Pages truncated to OS. */
	/* Internal. */
//C     	db_pgno_t	compact_truncate;	/* Page number for truncation */
//C     };
struct __db_compact
{
				u_int32_t compact_fillpercent;
				db_timeout_t compact_timeout;
				u_int32_t compact_pages;
				u_int32_t compact_pages_free;
				u_int32_t compact_pages_examine;
				u_int32_t compact_levels;
				u_int32_t compact_deadlock;
				db_pgno_t compact_pages_truncated;
				db_pgno_t compact_truncate;
}

/* Hash statistics structure. */
//C     struct __db_h_stat {
//C     	u_int32_t hash_magic;		/* Magic number. */
//C     	u_int32_t hash_version;		/* Version number. */
//C     	u_int32_t hash_metaflags;	/* Metadata flags. */
//C     	u_int32_t hash_nkeys;		/* Number of unique keys. */
//C     	u_int32_t hash_ndata;		/* Number of data items. */
//C     	u_int32_t hash_pagesize;	/* Page size. */
//C     	u_int32_t hash_ffactor;		/* Fill factor specified at create. */
//C     	u_int32_t hash_buckets;		/* Number of hash buckets. */
//C     	u_int32_t hash_free;		/* Pages on the free list. */
//C     	u_int32_t hash_bfree;		/* Bytes free on bucket pages. */
//C     	u_int32_t hash_bigpages;	/* Number of big key/data pages. */
//C     	u_int32_t hash_big_bfree;	/* Bytes free on big item pages. */
//C     	u_int32_t hash_overflows;	/* Number of overflow pages. */
//C     	u_int32_t hash_ovfl_free;	/* Bytes free on ovfl pages. */
//C     	u_int32_t hash_dup;		/* Number of dup pages. */
//C     	u_int32_t hash_dup_free;	/* Bytes free on duplicate pages. */
//C     };
struct __db_h_stat
{
				u_int32_t hash_magic;
				u_int32_t hash_version;
				u_int32_t hash_metaflags;
				u_int32_t hash_nkeys;
				u_int32_t hash_ndata;
				u_int32_t hash_pagesize;
				u_int32_t hash_ffactor;
				u_int32_t hash_buckets;
				u_int32_t hash_free;
				u_int32_t hash_bfree;
				u_int32_t hash_bigpages;
				u_int32_t hash_big_bfree;
				u_int32_t hash_overflows;
				u_int32_t hash_ovfl_free;
				u_int32_t hash_dup;
				u_int32_t hash_dup_free;
}

/* Queue statistics structure. */
//C     struct __db_qam_stat {
//C     	u_int32_t qs_magic;		/* Magic number. */
//C     	u_int32_t qs_version;		/* Version number. */
//C     	u_int32_t qs_metaflags;		/* Metadata flags. */
//C     	u_int32_t qs_nkeys;		/* Number of unique keys. */
//C     	u_int32_t qs_ndata;		/* Number of data items. */
//C     	u_int32_t qs_pagesize;		/* Page size. */
//C     	u_int32_t qs_extentsize;	/* Pages per extent. */
//C     	u_int32_t qs_pages;		/* Data pages. */
//C     	u_int32_t qs_re_len;		/* Fixed-length record length. */
//C     	u_int32_t qs_re_pad;		/* Fixed-length record pad. */
//C     	u_int32_t qs_pgfree;		/* Bytes free in data pages. */
//C     	u_int32_t qs_first_recno;	/* First not deleted record. */
//C     	u_int32_t qs_cur_recno;		/* Next available record number. */
//C     };
struct __db_qam_stat
{
				u_int32_t qs_magic;
				u_int32_t qs_version;
				u_int32_t qs_metaflags;
				u_int32_t qs_nkeys;
				u_int32_t qs_ndata;
				u_int32_t qs_pagesize;
				u_int32_t qs_extentsize;
				u_int32_t qs_pages;
				u_int32_t qs_re_len;
				u_int32_t qs_re_pad;
				u_int32_t qs_pgfree;
				u_int32_t qs_first_recno;
				u_int32_t qs_cur_recno;
}

/*******************************************************
	* Environment.
	*******************************************************/
//C     #define	DB_REGION_MAGIC	0x120897	/* Environment magic number. */

const DB_REGION_MAGIC = 0x120897;
/* Database Environment handle. */
//C     struct __db_env {
	/*******************************************************
		* Public: owned by the application.
		*******************************************************/
					/* Error message callback. */
//C     	void (*db_errcall) __P((const DB_ENV *, const char *, const char *));
//C     	FILE		*db_errfile;	/* Error message file stream. */
//C     	const char	*db_errpfx;	/* Error message prefix. */

//C     	FILE		*db_msgfile;	/* Statistics message file stream. */
					/* Statistics message callback. */
//C     	void (*db_msgcall) __P((const DB_ENV *, const char *));

					/* Other Callbacks. */
//C     	void (*db_feedback) __P((DB_ENV *, int, int));
//C     	void (*db_paniccall) __P((DB_ENV *, int));
//C     	void (*db_event_func) __P((DB_ENV *, u_int32_t, void *));

					/* App-specified alloc functions. */
//C     	void *(*db_malloc) __P((size_t));
//C     	void *(*db_realloc) __P((void *, size_t));
//C     	void (*db_free) __P((void *));

	/* Application callback to copy data to/from a custom data source. */
//C     #define	DB_USERCOPY_GETDATA	0x0001
//C     #define	DB_USERCOPY_SETDATA	0x0002
const DB_USERCOPY_GETDATA = 0x0001;
//C     	int (*dbt_usercopy)
const DB_USERCOPY_SETDATA = 0x0002;
//C     	    __P((DBT *, u_int32_t, void *, u_int32_t, u_int32_t));

	/*
		* Currently, the verbose list is a bit field with room for 32
		* entries.  There's no reason that it needs to be limited, if
		* there are ever more than 32 entries, convert to a bit array.
		*/
//C     #define	DB_VERB_DEADLOCK	0x0001	/* Deadlock detection information. */
//C     #define	DB_VERB_RECOVERY	0x0002	/* Recovery information. */
const DB_VERB_DEADLOCK = 0x0001;
//C     #define	DB_VERB_REGISTER	0x0004	/* Dump waits-for table. */
const DB_VERB_RECOVERY = 0x0002;
//C     #define	DB_VERB_REPLICATION	0x0008	/* Replication information. */
const DB_VERB_REGISTER = 0x0004;
//C     #define	DB_VERB_WAITSFOR	0x0010	/* Dump waits-for table. */
const DB_VERB_REPLICATION = 0x0008;
//C     	u_int32_t	 verbose;	/* Verbose output. */
const DB_VERB_WAITSFOR = 0x0010;

//C     	void		*app_private;	/* Application-private handle. */

//C     	int (*app_dispatch)		/* User-specified recovery dispatch. */
//C     	    __P((DB_ENV *, DBT *, DB_LSN *, db_recops));

	/* Mutexes. */
//C     	u_int32_t	mutex_align;	/* Mutex alignment */
//C     	u_int32_t	mutex_cnt;	/* Number of mutexes to configure */
//C     	u_int32_t	mutex_inc;	/* Number of mutexes to add */
//C     	u_int32_t	mutex_tas_spins;/* Test-and-set spin count */

//C     	struct {
//C     		int	  alloc_id;	/* Allocation ID argument */
//C     		u_int32_t flags;	/* Flags argument */
//C     	} *mutex_iq;			/* Initial mutexes queue */
struct _N17
{
				int alloc_id;
				u_int32_t flags;
}
//C     	u_int		mutex_iq_next;	/* Count of initial mutexes */
//C     	u_int		mutex_iq_max;	/* Maximum initial mutexes */

	/* Locking. */
//C     	u_int8_t	*lk_conflicts;	/* Two dimensional conflict matrix. */
//C     	int		 lk_modes;	/* Number of lock modes in table. */
//C     	u_int32_t	 lk_max;	/* Maximum number of locks. */
//C     	u_int32_t	 lk_max_lockers;/* Maximum number of lockers. */
//C     	u_int32_t	 lk_max_objects;/* Maximum number of locked objects. */
//C     	u_int32_t	 lk_detect;	/* Deadlock detect on all conflicts. */
//C     	db_timeout_t	 lk_timeout;	/* Lock timeout period. */

	/* Logging. */
//C     	u_int32_t	 lg_bsize;	/* Buffer size. */
//C     	u_int32_t	 lg_size;	/* Log file size. */
//C     	u_int32_t	 lg_regionmax;	/* Region size. */
//C     	int		 lg_filemode;	/* Log file permission mode. */

	/* Memory pool. */
//C     	u_int32_t	 mp_gbytes;	/* Cachesize: GB. */
//C     	u_int32_t	 mp_bytes;	/* Cachesize: Bytes. */
//C     	u_int		 mp_ncache;	/* Number of cache regions. */
//C     	size_t		 mp_mmapsize;	/* Maximum file size for mmap. */
//C     	int		 mp_maxopenfd;	/* Maximum open file descriptors. */
//C     	int		 mp_maxwrite;	/* Maximum buffers to write. */
//C     	int				/* Sleep after writing max buffers. */
//C     			 mp_maxwrite_sleep;

	/* Transactions. */
//C     	u_int32_t	 tx_max;	/* Maximum number of transactions. */
//C     	time_t		 tx_timestamp;	/* Recover to specific timestamp. */
//C     	db_timeout_t	 tx_timeout;	/* Timeout for transactions. */

	/* Thread tracking. */
//C     	u_int32_t	thr_nbucket;	/* Number of hash buckets. */
//C     	u_int32_t	thr_max;	/* Max before garbage collection. */
//C     	void		*thr_hashtab;	/* Hash table of DB_THREAD_INFO. */

	/*******************************************************
		* Private: owned by DB.
		*******************************************************/
//C     	pid_t		pid_cache;	/* Cached process ID. */

					/* User files, paths. */
//C     	char		*db_home;	/* Database home. */
//C     	char		*db_log_dir;	/* Database log file directory. */
//C     	char		*db_tmp_dir;	/* Database tmp file directory. */

//C     	char	       **db_data_dir;	/* Database data file directories. */
//C     	int		 data_cnt;	/* Database data file slots. */
//C     	int		 data_next;	/* Next Database data file slot. */

//C     	int		 db_mode;	/* Default open permissions. */
//C     	int		 dir_mode;	/* Intermediate directory perms. */
//C     	void		*env_lref;	/* Locker in non-threaded handles. */
//C     	u_int32_t	 open_flags;	/* Flags passed to DB_ENV->open. */

//C     	void		*reginfo;	/* REGINFO structure reference. */
//C     	DB_FH		*lockfhp;	/* fcntl(2) locking file handle. */

//C     	DB_FH		*registry;	/* DB_REGISTER file handle. */
//C     	u_int32_t	registry_off;
/*
						* Offset of our slot.  We can't use
						* off_t because its size depends on
						* build settings.
						*/

					/* Return IDs. */
//C     	void	       (*thread_id) __P((DB_ENV *, pid_t *, db_threadid_t *));
					/* Return if IDs alive. */
//C     	int	       (*is_alive)
//C     			__P((DB_ENV *, pid_t, db_threadid_t, u_int32_t));
					/* Format IDs into a string. */
//C     	char	       *(*thread_id_string)
//C     			__P((DB_ENV *, pid_t, db_threadid_t, char *));

//C     	int	      (**recover_dtab)	/* Dispatch table for recover funcs. */
//C     			    __P((DB_ENV *, DBT *, DB_LSN *, db_recops, void *));
//C     	size_t		 recover_dtab_size;
					/* Slots in the dispatch table. */

//C     	void		*cl_handle;	/* RPC: remote client handle. */
//C     	u_int		 cl_id;		/* RPC: remote client env id. */

//C     	int		 db_ref;	/* DB reference count. */

//C     	long		 shm_key;	/* shmget(2) key. */

	/*
		* List of open DB handles for this DB_ENV, used for cursor
		* adjustment.  Must be protected for multi-threaded support.
		*
		* !!!
		* As this structure is allocated in per-process memory, the
		* mutex may need to be stored elsewhere on architectures unable
		* to support mutexes in heap memory, e.g. HP/UX 9.
		*
		* !!!
		* Explicit representation of structure in queue.h.
		* TAILQ_HEAD(__dblist, __db);
		*/
//C     	db_mutex_t mtx_dblist;		/* Mutex. */
//C     	struct __dblist {
//C     		struct __db *tqh_first;
//C     		struct __db **tqh_last;
//C     	} dblist;
struct __dblist
{
				__db *tqh_first;
				__db **tqh_last;
}

	/*
		* XA support.
		*
		* !!!
		* Explicit representations of structures from queue.h.
		* TAILQ_ENTRY(__db_env) links;
		* TAILQ_HEAD(xa_txn, __db_txn);
		*/
//C     	struct {
//C     		struct __db_env *tqe_next;
//C     		struct __db_env **tqe_prev;
//C     	} links;
struct _N18
{
				__db_env *tqe_next;
				__db_env **tqe_prev;
}
//C     	struct __xa_txn {	/* XA Active Transactions. */
//C     		struct __db_txn *tqh_first;
//C     		struct __db_txn **tqh_last;
//C     	} xa_txn;
struct __xa_txn
{
				__db_txn *tqh_first;
				__db_txn **tqh_last;
}
//C     	int		 xa_rmid;	/* XA Resource Manager ID. */

//C     	char		*passwd;	/* Cryptography support. */
//C     	size_t		 passwd_len;
//C     	void		*crypto_handle;	/* Primary handle. */
//C     	db_mutex_t	 mtx_mt;	/* Mersenne Twister mutex. */
//C     	int		 mti;		/* Mersenne Twister index. */
//C     	u_long		*mt;		/* Mersenne Twister state vector. */

	/* API-private structure. */
//C     	void		*api1_internal;	/* C++, Perl API private */
//C     	void		*api2_internal;	/* Java API private */

//C     	DB_LOCKTAB	*lk_handle;	/* Lock handle. */
//C     	DB_LOG		*lg_handle;	/* Log handle. */
//C     	DB_MPOOL	*mp_handle;	/* Mpool handle. */
//C     	DB_MUTEXMGR	*mutex_handle;	/* Mutex handle. */
//C     	DB_REP		*rep_handle;	/* Replication handle. */
//C     	DB_TXNMGR	*tx_handle;	/* Txn handle. */

	/* DB_ENV PUBLIC HANDLE LIST BEGIN */
//C     	int  (*cdsgroup_begin) __P((DB_ENV *, DB_TXN **));
//C     	int  (*close) __P((DB_ENV *, u_int32_t));
//C     	int  (*dbremove) __P((DB_ENV *,
//C     		DB_TXN *, const char *, const char *, u_int32_t));
//C     	int  (*dbrename) __P((DB_ENV *,
//C     		DB_TXN *, const char *, const char *, const char *, u_int32_t));
//C     	void (*err) __P((const DB_ENV *, int, const char *, ...));
//C     	void (*errx) __P((const DB_ENV *, const char *, ...));
//C     	int  (*failchk) __P((DB_ENV *, u_int32_t));
//C     	int  (*fileid_reset) __P((DB_ENV *, const char *, u_int32_t));
//C     	int  (*get_cachesize) __P((DB_ENV *, u_int32_t *, u_int32_t *, int *));
//C     	int  (*get_data_dirs) __P((DB_ENV *, const char ***));
//C     	int  (*get_encrypt_flags) __P((DB_ENV *, u_int32_t *));
//C     	void (*get_errfile) __P((DB_ENV *, FILE **));
//C     	void (*get_errpfx) __P((DB_ENV *, const char **));
//C     	int  (*get_flags) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_home) __P((DB_ENV *, const char **));
//C     	int  (*get_lg_bsize) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_lg_dir) __P((DB_ENV *, const char **));
//C     	int  (*get_lg_filemode) __P((DB_ENV *, int *));
//C     	int  (*get_lg_max) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_lg_regionmax) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_lk_conflicts) __P((DB_ENV *, const u_int8_t **, int *));
//C     	int  (*get_lk_detect) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_lk_max_lockers) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_lk_max_locks) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_lk_max_objects) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_mp_max_openfd) __P((DB_ENV *, int *));
//C     	int  (*get_mp_max_write) __P((DB_ENV *, int *, int *));
//C     	int  (*get_mp_mmapsize) __P((DB_ENV *, size_t *));
//C     	void (*get_msgfile) __P((DB_ENV *, FILE **));
//C     	int  (*get_open_flags) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_shm_key) __P((DB_ENV *, long *));
//C     	int  (*get_timeout) __P((DB_ENV *, db_timeout_t *, u_int32_t));
//C     	int  (*get_tmp_dir) __P((DB_ENV *, const char **));
//C     	int  (*get_tx_max) __P((DB_ENV *, u_int32_t *));
//C     	int  (*get_tx_timestamp) __P((DB_ENV *, time_t *));
//C     	int  (*get_verbose) __P((DB_ENV *, u_int32_t, int *));
//C     	int  (*is_bigendian) __P((void));
//C     	int  (*lock_detect) __P((DB_ENV *, u_int32_t, u_int32_t, int *));
//C     	int  (*lock_get) __P((DB_ENV *,
//C     		u_int32_t, u_int32_t, const DBT *, db_lockmode_t, DB_LOCK *));
//C     	int  (*lock_id) __P((DB_ENV *, u_int32_t *));
//C     	int  (*lock_id_free) __P((DB_ENV *, u_int32_t));
//C     	int  (*lock_put) __P((DB_ENV *, DB_LOCK *));
//C     	int  (*lock_stat) __P((DB_ENV *, DB_LOCK_STAT **, u_int32_t));
//C     	int  (*lock_stat_print) __P((DB_ENV *, u_int32_t));
//C     	int  (*lock_vec) __P((DB_ENV *,
//C     		u_int32_t, u_int32_t, DB_LOCKREQ *, int, DB_LOCKREQ **));
//C     	int  (*log_archive) __P((DB_ENV *, char **[], u_int32_t));
//C     	int  (*log_cursor) __P((DB_ENV *, DB_LOGC **, u_int32_t));
//C     	int  (*log_file) __P((DB_ENV *, const DB_LSN *, char *, size_t));
//C     	int  (*log_flush) __P((DB_ENV *, const DB_LSN *));
//C     	int  (*log_printf) __P((DB_ENV *, DB_TXN *, const char *, ...));
//C     	int  (*log_put) __P((DB_ENV *, DB_LSN *, const DBT *, u_int32_t));
//C     	int  (*log_stat) __P((DB_ENV *, DB_LOG_STAT **, u_int32_t));
//C     	int  (*log_stat_print) __P((DB_ENV *, u_int32_t));
//C     	int  (*lsn_reset) __P((DB_ENV *, const char *, u_int32_t));
//C     	int  (*memp_fcreate) __P((DB_ENV *, DB_MPOOLFILE **, u_int32_t));
//C     	int  (*memp_register) __P((DB_ENV *, int, int (*)(DB_ENV *,
//C     		db_pgno_t, void *, DBT *), int (*)(DB_ENV *,
//C     		db_pgno_t, void *, DBT *)));
//C     	int  (*memp_stat) __P((DB_ENV *,
//C     		DB_MPOOL_STAT **, DB_MPOOL_FSTAT ***, u_int32_t));
//C     	int  (*memp_stat_print) __P((DB_ENV *, u_int32_t));
//C     	int  (*memp_sync) __P((DB_ENV *, DB_LSN *));
//C     	int  (*memp_trickle) __P((DB_ENV *, int, int *));
//C     	int  (*mutex_alloc) __P((DB_ENV *, u_int32_t, db_mutex_t *));
//C     	int  (*mutex_free) __P((DB_ENV *, db_mutex_t));
//C     	int  (*mutex_get_align) __P((DB_ENV *, u_int32_t *));
//C     	int  (*mutex_get_increment) __P((DB_ENV *, u_int32_t *));
//C     	int  (*mutex_get_max) __P((DB_ENV *, u_int32_t *));
//C     	int  (*mutex_get_tas_spins) __P((DB_ENV *, u_int32_t *));
//C     	int  (*mutex_lock) __P((DB_ENV *, db_mutex_t));
//C     	int  (*mutex_set_align) __P((DB_ENV *, u_int32_t));
//C     	int  (*mutex_set_increment) __P((DB_ENV *, u_int32_t));
//C     	int  (*mutex_set_max) __P((DB_ENV *, u_int32_t));
//C     	int  (*mutex_set_tas_spins) __P((DB_ENV *, u_int32_t));
//C     	int  (*mutex_stat) __P((DB_ENV *, DB_MUTEX_STAT **, u_int32_t));
//C     	int  (*mutex_stat_print) __P((DB_ENV *, u_int32_t));
//C     	int  (*mutex_unlock) __P((DB_ENV *, db_mutex_t));
//C     	int  (*open) __P((DB_ENV *, const char *, u_int32_t, int));
//C     	int  (*remove) __P((DB_ENV *, const char *, u_int32_t));
//C     	int  (*rep_elect)
//C     		__P((DB_ENV *, int, int, int *, u_int32_t));
//C     	int  (*rep_flush) __P((DB_ENV *));
//C     	int  (*rep_get_config) __P((DB_ENV *, u_int32_t, int *));
//C     	int  (*rep_get_limit) __P((DB_ENV *, u_int32_t *, u_int32_t *));
//C     	int  (*rep_get_nsites) __P((DB_ENV *, int *));
//C     	int  (*rep_get_priority) __P((DB_ENV *, int *));
//C     	int  (*rep_get_timeout) __P((DB_ENV *, int, u_int32_t *));
//C     	int  (*rep_process_message)
//C     		__P((DB_ENV *, DBT *, DBT *, int *, DB_LSN *));
//C     	int  (*rep_set_config) __P((DB_ENV *, u_int32_t, int));
//C     	int  (*rep_set_limit) __P((DB_ENV *, u_int32_t, u_int32_t));
//C     	int  (*rep_set_nsites) __P((DB_ENV *, int));
//C     	int  (*rep_set_priority) __P((DB_ENV *, int));
//C     	int  (*rep_set_timeout) __P((DB_ENV *, int, db_timeout_t));
//C     	int  (*rep_set_transport) __P((DB_ENV *, int, int (*)(DB_ENV *,
//C     		const DBT *, const DBT *, const DB_LSN *, int, u_int32_t)));
//C     	int  (*rep_start) __P((DB_ENV *, DBT *, u_int32_t));
//C     	int  (*rep_stat) __P((DB_ENV *, DB_REP_STAT **, u_int32_t));
//C     	int  (*rep_stat_print) __P((DB_ENV *, u_int32_t));
//C     	int  (*rep_sync) __P((DB_ENV *, u_int32_t));
//C     	int  (*repmgr_add_remote_site) __P((DB_ENV *, const char *, u_int,
//C     		int *, u_int32_t));
//C     	int  (*repmgr_get_ack_policy) __P((DB_ENV *, int *));
//C     	int  (*repmgr_set_ack_policy) __P((DB_ENV *, int));
//C     	int  (*repmgr_set_local_site) __P((DB_ENV *, const char *, u_int,
//C     		u_int32_t));
//C     	int  (*repmgr_site_list) __P((DB_ENV *, u_int *,
//C     		DB_REPMGR_SITE **));
//C     	int  (*repmgr_start) __P((DB_ENV *, int, u_int32_t));
//C     	int  (*set_alloc) __P((DB_ENV *, void *(*)(size_t),
//C     		void *(*)(void *, size_t), void (*)(void *)));
//C     	int  (*set_app_dispatch)
//C     		__P((DB_ENV *, int (*)(DB_ENV *, DBT *, DB_LSN *, db_recops)));
//C     	int  (*set_cachesize) __P((DB_ENV *, u_int32_t, u_int32_t, int));
//C     	int  (*set_data_dir) __P((DB_ENV *, const char *));
//C     	int  (*set_encrypt) __P((DB_ENV *, const char *, u_int32_t));
//C     	void (*set_errcall) __P((DB_ENV *,
//C     		void (*)(const DB_ENV *, const char *, const char *)));
//C     	void (*set_errfile) __P((DB_ENV *, FILE *));
//C     	void (*set_errpfx) __P((DB_ENV *, const char *));
//C     	int  (*set_event_notify)
//C     		__P((DB_ENV *, void (*)(DB_ENV *, u_int32_t, void *)));
//C     	int  (*set_feedback) __P((DB_ENV *, void (*)(DB_ENV *, int, int)));
//C     	int  (*set_flags) __P((DB_ENV *, u_int32_t, int));
//C     	int  (*set_intermediate_dir) __P((DB_ENV *, int, u_int32_t));
//C     	int  (*set_isalive) __P((DB_ENV *,
//C     		int (*)(DB_ENV *, pid_t, db_threadid_t, u_int32_t)));
//C     	int  (*set_lg_bsize) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_lg_dir) __P((DB_ENV *, const char *));
//C     	int  (*set_lg_filemode) __P((DB_ENV *, int));
//C     	int  (*set_lg_max) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_lg_regionmax) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_lk_conflicts) __P((DB_ENV *, u_int8_t *, int));
//C     	int  (*set_lk_detect) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_lk_max_lockers) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_lk_max_locks) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_lk_max_objects) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_mp_max_openfd) __P((DB_ENV *, int));
//C     	int  (*set_mp_max_write) __P((DB_ENV *, int, int));
//C     	int  (*set_mp_mmapsize) __P((DB_ENV *, size_t));
//C     	void (*set_msgcall)
//C     		__P((DB_ENV *, void (*)(const DB_ENV *, const char *)));
//C     	void (*set_msgfile) __P((DB_ENV *, FILE *));
//C     	int  (*set_paniccall) __P((DB_ENV *, void (*)(DB_ENV *, int)));
//C     	int  (*set_rep_request) __P((DB_ENV *, u_int32_t, u_int32_t));
//C     	int  (*set_rpc_server)
//C     		__P((DB_ENV *, void *, const char *, long, long, u_int32_t));
//C     	int  (*set_shm_key) __P((DB_ENV *, long));
//C     	int  (*set_thread_count) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_thread_id) __P((DB_ENV *,
//C     		void (*)(DB_ENV *, pid_t *, db_threadid_t *)));
//C     	int  (*set_thread_id_string) __P((DB_ENV *,
//C     		char *(*)(DB_ENV *, pid_t, db_threadid_t, char *)));
//C     	int  (*set_timeout) __P((DB_ENV *, db_timeout_t, u_int32_t));
//C     	int  (*set_tmp_dir) __P((DB_ENV *, const char *));
//C     	int  (*set_tx_max) __P((DB_ENV *, u_int32_t));
//C     	int  (*set_tx_timestamp) __P((DB_ENV *, time_t *));
//C     	int  (*set_verbose) __P((DB_ENV *, u_int32_t, int));
//C     	int  (*stat_print) __P((DB_ENV *, u_int32_t));
//C     	int  (*txn_begin) __P((DB_ENV *, DB_TXN *, DB_TXN **, u_int32_t));
//C     	int  (*txn_checkpoint) __P((DB_ENV *, u_int32_t, u_int32_t, u_int32_t));
//C     	int  (*txn_recover)
//C     		__P((DB_ENV *, DB_PREPLIST *, long, long *, u_int32_t));
//C     	int  (*txn_stat) __P((DB_ENV *, DB_TXN_STAT **, u_int32_t));
//C     	int  (*txn_stat_print) __P((DB_ENV *, u_int32_t));
	/* DB_ENV PUBLIC HANDLE LIST END */

	/* DB_ENV PRIVATE HANDLE LIST BEGIN */
//C     	int  (*prdbt) __P((DBT *,
//C     		int, const char *, void *, int (*)(void *, const void *), int));
	/* DB_ENV PRIVATE HANDLE LIST END */

//C     #define	DB_TEST_ELECTINIT	 1	/* after __rep_elect_init */
//C     #define	DB_TEST_ELECTVOTE1	 2	/* after sending VOTE1 */
const DB_TEST_ELECTINIT = 1;
//C     #define	DB_TEST_POSTDESTROY	 3	/* after destroy op */
const DB_TEST_ELECTVOTE1 = 2;
//C     #define	DB_TEST_POSTLOG		 4	/* after logging all pages */
const DB_TEST_POSTDESTROY = 3;
//C     #define	DB_TEST_POSTLOGMETA	 5	/* after logging meta in btree */
const DB_TEST_POSTLOG = 4;
//C     #define	DB_TEST_POSTOPEN	 6	/* after __os_open */
const DB_TEST_POSTLOGMETA = 5;
//C     #define	DB_TEST_POSTSYNC	 7	/* after syncing the log */
const DB_TEST_POSTOPEN = 6;
//C     #define	DB_TEST_PREDESTROY	 8	/* before destroy op */
const DB_TEST_POSTSYNC = 7;
//C     #define	DB_TEST_PREOPEN		 9	/* before __os_open */
const DB_TEST_PREDESTROY = 8;
//C     #define	DB_TEST_RECYCLE		 10	/* test rep and txn_recycle */
const DB_TEST_PREOPEN = 9;
//C     #define	DB_TEST_SUBDB_LOCKS	 11	/* subdb locking tests */
const DB_TEST_RECYCLE = 10;
//C     	int		 test_abort;	/* Abort value for testing. */
const DB_TEST_SUBDB_LOCKS = 11;
//C     	int		 test_check;	/* Checkpoint value for testing. */
//C     	int		 test_copy;	/* Copy value for testing. */

//C     #define	DB_ENV_AUTO_COMMIT	0x00000001 /* DB_AUTO_COMMIT. */
//C     #define	DB_ENV_CDB		0x00000002 /* DB_INIT_CDB. */
const DB_ENV_AUTO_COMMIT = 0x00000001;
//C     #define	DB_ENV_CDB_ALLDB	0x00000004 /* CDB environment wide locking. */
const DB_ENV_CDB = 0x00000002;
//C     #define	DB_ENV_CREATE		0x00000008 /* DB_CREATE set. */
const DB_ENV_CDB_ALLDB = 0x00000004;
//C     #define	DB_ENV_DBLOCAL		0x00000010 /* Environment for a private DB. */
const DB_ENV_CREATE = 0x00000008;
//C     #define	DB_ENV_DIRECT_DB	0x00000020 /* DB_DIRECT_DB set. */
const DB_ENV_DBLOCAL = 0x00000010;
//C     #define	DB_ENV_DIRECT_LOG	0x00000040 /* DB_DIRECT_LOG set. */
const DB_ENV_DIRECT_DB = 0x00000020;
//C     #define	DB_ENV_DSYNC_DB		0x00000080 /* DB_DSYNC_DB set. */
const DB_ENV_DIRECT_LOG = 0x00000040;
//C     #define	DB_ENV_DSYNC_LOG	0x00000100 /* DB_DSYNC_LOG set. */
const DB_ENV_DSYNC_DB = 0x00000080;
//C     #define	DB_ENV_FATAL		0x00000200 /* Doing fatal recovery in env. */
const DB_ENV_DSYNC_LOG = 0x00000100;
//C     #define	DB_ENV_LOCKDOWN		0x00000400 /* DB_LOCKDOWN set. */
const DB_ENV_FATAL = 0x00000200;
//C     #define	DB_ENV_LOG_AUTOREMOVE   0x00000800 /* DB_LOG_AUTOREMOVE set. */
const DB_ENV_LOCKDOWN = 0x00000400;
//C     #define	DB_ENV_LOG_INMEMORY     0x00001000 /* DB_LOG_INMEMORY set. */
const DB_ENV_LOG_AUTOREMOVE = 0x00000800;
//C     #define	DB_ENV_MULTIVERSION	0x00002000 /* DB_MULTIVERSION set. */
const DB_ENV_LOG_INMEMORY = 0x00001000;
//C     #define	DB_ENV_NOLOCKING	0x00004000 /* DB_NOLOCKING set. */
const DB_ENV_MULTIVERSION = 0x00002000;
//C     #define	DB_ENV_NOMMAP		0x00008000 /* DB_NOMMAP set. */
const DB_ENV_NOLOCKING = 0x00004000;
//C     #define	DB_ENV_NOPANIC		0x00010000 /* Okay if panic set. */
const DB_ENV_NOMMAP = 0x00008000;
//C     #define	DB_ENV_OPEN_CALLED	0x00020000 /* DB_ENV->open called. */
const DB_ENV_NOPANIC = 0x00010000;
//C     #define	DB_ENV_OVERWRITE	0x00040000 /* DB_OVERWRITE set. */
const DB_ENV_OPEN_CALLED = 0x00020000;
//C     #define	DB_ENV_PRIVATE		0x00080000 /* DB_PRIVATE set. */
const DB_ENV_OVERWRITE = 0x00040000;
//C     #define	DB_ENV_REGION_INIT	0x00100000 /* DB_REGION_INIT set. */
const DB_ENV_PRIVATE = 0x00080000;
//C     #define	DB_ENV_RPCCLIENT	0x00200000 /* DB_RPCCLIENT set. */
const DB_ENV_REGION_INIT = 0x00100000;
//C     #define	DB_ENV_RPCCLIENT_GIVEN	0x00400000 /* User-supplied RPC client struct */
const DB_ENV_RPCCLIENT = 0x00200000;
//C     #define	DB_ENV_SYSTEM_MEM	0x00800000 /* DB_SYSTEM_MEM set. */
const DB_ENV_RPCCLIENT_GIVEN = 0x00400000;
//C     #define	DB_ENV_THREAD		0x01000000 /* DB_THREAD set. */
const DB_ENV_SYSTEM_MEM = 0x00800000;
//C     #define	DB_ENV_TIME_NOTGRANTED	0x02000000 /* DB_TIME_NOTGRANTED set. */
const DB_ENV_THREAD = 0x01000000;
//C     #define	DB_ENV_TXN_NOSYNC	0x04000000 /* DB_TXN_NOSYNC set. */
const DB_ENV_TIME_NOTGRANTED = 0x02000000;
//C     #define	DB_ENV_TXN_SNAPSHOT	0x08000000 /* DB_TXN_SNAPSHOT set. */
const DB_ENV_TXN_NOSYNC = 0x04000000;
//C     #define	DB_ENV_TXN_WRITE_NOSYNC	0x10000000 /* DB_TXN_WRITE_NOSYNC set. */
const DB_ENV_TXN_SNAPSHOT = 0x08000000;
//C     #define	DB_ENV_YIELDCPU		0x20000000 /* DB_YIELDCPU set. */
const DB_ENV_TXN_WRITE_NOSYNC = 0x10000000;
//C     	u_int32_t flags;
const DB_ENV_YIELDCPU = 0x20000000;
//C     };
struct __db_env
{
				void  function(DB_ENV *, char *, char *)db_errcall;
				FILE *db_errfile;
				char *db_errpfx;
				FILE *db_msgfile;
				void  function(DB_ENV *, char *)db_msgcall;
				void  function(DB_ENV *, int , int )db_feedback;
				void  function(DB_ENV *, int )db_paniccall;
				void  function(DB_ENV *, u_int32_t , void *)db_event_func;
				void * function(size_t )db_malloc;
				void * function(void *, size_t )db_realloc;
				void  function(void *)db_free;
				int  function(DBT *, u_int32_t , void *, u_int32_t , u_int32_t )dbt_usercopy;
				u_int32_t verbose;
				void *app_private;
				int  function(DB_ENV *, DBT *, DB_LSN *, db_recops )app_dispatch;
				u_int32_t mutex_align;
				u_int32_t mutex_cnt;
				u_int32_t mutex_inc;
				u_int32_t mutex_tas_spins;
				_N17 *mutex_iq;
				u_int mutex_iq_next;
				u_int mutex_iq_max;
				u_int8_t *lk_conflicts;
				int lk_modes;
				u_int32_t lk_max;
				u_int32_t lk_max_lockers;
				u_int32_t lk_max_objects;
				u_int32_t lk_detect;
				db_timeout_t lk_timeout;
				u_int32_t lg_bsize;
				u_int32_t lg_size;
				u_int32_t lg_regionmax;
				int lg_filemode;
				u_int32_t mp_gbytes;
				u_int32_t mp_bytes;
				u_int mp_ncache;
				size_t mp_mmapsize;
				int mp_maxopenfd;
				int mp_maxwrite;
				int mp_maxwrite_sleep;
				u_int32_t tx_max;
				time_t tx_timestamp;
				db_timeout_t tx_timeout;
				u_int32_t thr_nbucket;
				u_int32_t thr_max;
				void *thr_hashtab;
				pid_t pid_cache;
				char *db_home;
				char *db_log_dir;
				char *db_tmp_dir;
				char **db_data_dir;
				int data_cnt;
				int data_next;
				int db_mode;
				int dir_mode;
				void *env_lref;
				u_int32_t open_flags;
				void *reginfo;
				DB_FH *lockfhp;
				DB_FH *registry;
				u_int32_t registry_off;
				void  function(DB_ENV *, pid_t *, db_threadid_t *)thread_id;
				int  function(DB_ENV *, pid_t , db_threadid_t , u_int32_t )is_alive;
				char * function(DB_ENV *, pid_t , db_threadid_t , char *)thread_id_string;
				int  function(DB_ENV *, DBT *, DB_LSN *, db_recops , void *)*recover_dtab;
				size_t recover_dtab_size;
				void *cl_handle;
				u_int cl_id;
				int db_ref;
				int shm_key;
				db_mutex_t mtx_dblist;
				__dblist dblist;
				_N18 links;
				__xa_txn xa_txn;
				int xa_rmid;
				char *passwd;
				size_t passwd_len;
				void *crypto_handle;
				db_mutex_t mtx_mt;
				int mti;
				u_long *mt;
				void *api1_internal;
				void *api2_internal;
				DB_LOCKTAB *lk_handle;
				DB_LOG *lg_handle;
				DB_MPOOL *mp_handle;
				DB_MUTEXMGR *mutex_handle;
				DB_REP *rep_handle;
				DB_TXNMGR *tx_handle;
				int  function(DB_ENV *, DB_TXN **)cdsgroup_begin;
				int  function(DB_ENV *, u_int32_t )close;
				int  function(DB_ENV *, DB_TXN *, char *, char *, u_int32_t )dbremove;
				int  function(DB_ENV *, DB_TXN *, char *, char *, char *, u_int32_t )dbrename;
				void  function(DB_ENV *, int , char *,...)err;
				void  function(DB_ENV *, char *,...)errx;
				int  function(DB_ENV *, u_int32_t )failchk;
				int  function(DB_ENV *, char *, u_int32_t )fileid_reset;
				int  function(DB_ENV *, u_int32_t *, u_int32_t *, int *)get_cachesize;
				int  function(DB_ENV *, char ***)get_data_dirs;
				int  function(DB_ENV *, u_int32_t *)get_encrypt_flags;
				void  function(DB_ENV *, FILE **)get_errfile;
				void  function(DB_ENV *, char **)get_errpfx;
				int  function(DB_ENV *, u_int32_t *)get_flags;
				int  function(DB_ENV *, char **)get_home;
				int  function(DB_ENV *, u_int32_t *)get_lg_bsize;
				int  function(DB_ENV *, char **)get_lg_dir;
				int  function(DB_ENV *, int *)get_lg_filemode;
				int  function(DB_ENV *, u_int32_t *)get_lg_max;
				int  function(DB_ENV *, u_int32_t *)get_lg_regionmax;
				int  function(DB_ENV *, u_int8_t **, int *)get_lk_conflicts;
				int  function(DB_ENV *, u_int32_t *)get_lk_detect;
				int  function(DB_ENV *, u_int32_t *)get_lk_max_lockers;
				int  function(DB_ENV *, u_int32_t *)get_lk_max_locks;
				int  function(DB_ENV *, u_int32_t *)get_lk_max_objects;
				int  function(DB_ENV *, int *)get_mp_max_openfd;
				int  function(DB_ENV *, int *, int *)get_mp_max_write;
				int  function(DB_ENV *, size_t *)get_mp_mmapsize;
				void  function(DB_ENV *, FILE **)get_msgfile;
				int  function(DB_ENV *, u_int32_t *)get_open_flags;
				int  function(DB_ENV *, int *)get_shm_key;
				int  function(DB_ENV *, db_timeout_t *, u_int32_t )get_timeout;
				int  function(DB_ENV *, char **)get_tmp_dir;
				int  function(DB_ENV *, u_int32_t *)get_tx_max;
				int  function(DB_ENV *, time_t *)get_tx_timestamp;
				int  function(DB_ENV *, u_int32_t , int *)get_verbose;
				int  function()is_bigendian;
				int  function(DB_ENV *, u_int32_t , u_int32_t , int *)lock_detect;
				int  function(DB_ENV *, u_int32_t , u_int32_t , DBT *, db_lockmode_t , DB_LOCK *)lock_get;
				int  function(DB_ENV *, u_int32_t *)lock_id;
				int  function(DB_ENV *, u_int32_t )lock_id_free;
				int  function(DB_ENV *, DB_LOCK *)lock_put;
				int  function(DB_ENV *, DB_LOCK_STAT **, u_int32_t )lock_stat;
				int  function(DB_ENV *, u_int32_t )lock_stat_print;
				int  function(DB_ENV *, u_int32_t , u_int32_t , DB_LOCKREQ *, int , DB_LOCKREQ **)lock_vec;
				int  function(DB_ENV *, char ***, u_int32_t )log_archive;
				int  function(DB_ENV *, DB_LOGC **, u_int32_t )log_cursor;
				int  function(DB_ENV *, DB_LSN *, char *, size_t )log_file;
				int  function(DB_ENV *, DB_LSN *)log_flush;
				int  function(DB_ENV *, DB_TXN *, char *,...)log_printf;
				int  function(DB_ENV *, DB_LSN *, DBT *, u_int32_t )log_put;
				int  function(DB_ENV *, DB_LOG_STAT **, u_int32_t )log_stat;
				int  function(DB_ENV *, u_int32_t )log_stat_print;
				int  function(DB_ENV *, char *, u_int32_t )lsn_reset;
				int  function(DB_ENV *, DB_MPOOLFILE **, u_int32_t )memp_fcreate;
				int  function(DB_ENV *, int , int  function(DB_ENV *, db_pgno_t , void *, DBT *), int  function(DB_ENV *, db_pgno_t , void *, DBT *))memp_register;
				int  function(DB_ENV *, DB_MPOOL_STAT **, DB_MPOOL_FSTAT ***, u_int32_t )memp_stat;
				int  function(DB_ENV *, u_int32_t )memp_stat_print;
				int  function(DB_ENV *, DB_LSN *)memp_sync;
				int  function(DB_ENV *, int , int *)memp_trickle;
				int  function(DB_ENV *, u_int32_t , db_mutex_t *)mutex_alloc;
				int  function(DB_ENV *, db_mutex_t )mutex_free;
				int  function(DB_ENV *, u_int32_t *)mutex_get_align;
				int  function(DB_ENV *, u_int32_t *)mutex_get_increment;
				int  function(DB_ENV *, u_int32_t *)mutex_get_max;
				int  function(DB_ENV *, u_int32_t *)mutex_get_tas_spins;
				int  function(DB_ENV *, db_mutex_t )mutex_lock;
				int  function(DB_ENV *, u_int32_t )mutex_set_align;
				int  function(DB_ENV *, u_int32_t )mutex_set_increment;
				int  function(DB_ENV *, u_int32_t )mutex_set_max;
				int  function(DB_ENV *, u_int32_t )mutex_set_tas_spins;
				int  function(DB_ENV *, DB_MUTEX_STAT **, u_int32_t )mutex_stat;
				int  function(DB_ENV *, u_int32_t )mutex_stat_print;
				int  function(DB_ENV *, db_mutex_t )mutex_unlock;
				int  function(DB_ENV *, char *, u_int32_t , int )open;
				int  function(DB_ENV *, char *, u_int32_t )remove;
				int  function(DB_ENV *, int , int , int *, u_int32_t )rep_elect;
				int  function(DB_ENV *)rep_flush;
				int  function(DB_ENV *, u_int32_t , int *)rep_get_config;
				int  function(DB_ENV *, u_int32_t *, u_int32_t *)rep_get_limit;
				int  function(DB_ENV *, int *)rep_get_nsites;
				int  function(DB_ENV *, int *)rep_get_priority;
				int  function(DB_ENV *, int , u_int32_t *)rep_get_timeout;
				int  function(DB_ENV *, DBT *, DBT *, int *, DB_LSN *)rep_process_message;
				int  function(DB_ENV *, u_int32_t , int )rep_set_config;
				int  function(DB_ENV *, u_int32_t , u_int32_t )rep_set_limit;
				int  function(DB_ENV *, int )rep_set_nsites;
				int  function(DB_ENV *, int )rep_set_priority;
				int  function(DB_ENV *, int , db_timeout_t )rep_set_timeout;
				int  function(DB_ENV *, int , int  function(DB_ENV *, DBT *, DBT *, DB_LSN *, int , u_int32_t ))rep_set_transport;
				int  function(DB_ENV *, DBT *, u_int32_t )rep_start;
				int  function(DB_ENV *, DB_REP_STAT **, u_int32_t )rep_stat;
				int  function(DB_ENV *, u_int32_t )rep_stat_print;
				int  function(DB_ENV *, u_int32_t )rep_sync;
				int  function(DB_ENV *, char *, u_int , int *, u_int32_t )repmgr_add_remote_site;
				int  function(DB_ENV *, int *)repmgr_get_ack_policy;
				int  function(DB_ENV *, int )repmgr_set_ack_policy;
				int  function(DB_ENV *, char *, u_int , u_int32_t )repmgr_set_local_site;
				int  function(DB_ENV *, u_int *, DB_REPMGR_SITE **)repmgr_site_list;
				int  function(DB_ENV *, int , u_int32_t )repmgr_start;
				int  function(DB_ENV *, void * function(size_t ), void * function(void *, size_t ), void  function(void *))set_alloc;
				int  function(DB_ENV *, int  function(DB_ENV *, DBT *, DB_LSN *, db_recops ))set_app_dispatch;
				int  function(DB_ENV *, u_int32_t , u_int32_t , int )set_cachesize;
				int  function(DB_ENV *, char *)set_data_dir;
				int  function(DB_ENV *, char *, u_int32_t )set_encrypt;
				void  function(DB_ENV *, void  function(DB_ENV *, char *, char *))set_errcall;
				void  function(DB_ENV *, FILE *)set_errfile;
				void  function(DB_ENV *, char *)set_errpfx;
				int  function(DB_ENV *, void  function(DB_ENV *, u_int32_t , void *))set_event_notify;
				int  function(DB_ENV *, void  function(DB_ENV *, int , int ))set_feedback;
				int  function(DB_ENV *, u_int32_t , int )set_flags;
				int  function(DB_ENV *, int , u_int32_t )set_intermediate_dir;
				int  function(DB_ENV *, int  function(DB_ENV *, pid_t , db_threadid_t , u_int32_t ))set_isalive;
				int  function(DB_ENV *, u_int32_t )set_lg_bsize;
				int  function(DB_ENV *, char *)set_lg_dir;
				int  function(DB_ENV *, int )set_lg_filemode;
				int  function(DB_ENV *, u_int32_t )set_lg_max;
				int  function(DB_ENV *, u_int32_t )set_lg_regionmax;
				int  function(DB_ENV *, u_int8_t *, int )set_lk_conflicts;
				int  function(DB_ENV *, u_int32_t )set_lk_detect;
				int  function(DB_ENV *, u_int32_t )set_lk_max_lockers;
				int  function(DB_ENV *, u_int32_t )set_lk_max_locks;
				int  function(DB_ENV *, u_int32_t )set_lk_max_objects;
				int  function(DB_ENV *, int )set_mp_max_openfd;
				int  function(DB_ENV *, int , int )set_mp_max_write;
				int  function(DB_ENV *, size_t )set_mp_mmapsize;
				void  function(DB_ENV *, void  function(DB_ENV *, char *))set_msgcall;
				void  function(DB_ENV *, FILE *)set_msgfile;
				int  function(DB_ENV *, void  function(DB_ENV *, int ))set_paniccall;
				int  function(DB_ENV *, u_int32_t , u_int32_t )set_rep_request;
				int  function(DB_ENV *, void *, char *, int , int , u_int32_t )set_rpc_server;
				int  function(DB_ENV *, int )set_shm_key;
				int  function(DB_ENV *, u_int32_t )set_thread_count;
				int  function(DB_ENV *, void  function(DB_ENV *, pid_t *, db_threadid_t *))set_thread_id;
				int  function(DB_ENV *, char * function(DB_ENV *, pid_t , db_threadid_t , char *))set_thread_id_string;
				int  function(DB_ENV *, db_timeout_t , u_int32_t )set_timeout;
				int  function(DB_ENV *, char *)set_tmp_dir;
				int  function(DB_ENV *, u_int32_t )set_tx_max;
				int  function(DB_ENV *, time_t *)set_tx_timestamp;
				int  function(DB_ENV *, u_int32_t , int )set_verbose;
				int  function(DB_ENV *, u_int32_t )stat_print;
				int  function(DB_ENV *, DB_TXN *, DB_TXN **, u_int32_t )txn_begin;
				int  function(DB_ENV *, u_int32_t , u_int32_t , u_int32_t )txn_checkpoint;
				int  function(DB_ENV *, DB_PREPLIST *, int , int *, u_int32_t )txn_recover;
				int  function(DB_ENV *, DB_TXN_STAT **, u_int32_t )txn_stat;
				int  function(DB_ENV *, u_int32_t )txn_stat_print;
				int  function(DBT *, int , char *, void *, int  function(void *, void *), int )prdbt;
				int test_abort;
				int test_check;
				int test_copy;
				u_int32_t flags;
}

//C     #ifndef DB_DBM_HSEARCH
//C     #define	DB_DBM_HSEARCH	0		/* No historic interfaces by default. */
//C     #endif
const DB_DBM_HSEARCH = 0;
//C     #if DB_DBM_HSEARCH != 0
/*******************************************************
	* Dbm/Ndbm historic interfaces.
	*******************************************************/
//C     typedef struct __db DBM;

//C     #define	DBM_INSERT	0		/* Flags to dbm_store(). */
//C     #define	DBM_REPLACE	1

/*
	* The DB support for ndbm(3) always appends this suffix to the
	* file name to avoid overwriting the user's original database.
	*/
//C     #define	DBM_SUFFIX	".db"

//C     #if defined(_XPG4_2)
//C     typedef struct {
//C     	char *dptr;
//C     	size_t dsize;
//C     } datum;
//C     #else
//C     typedef struct {
//C     	char *dptr;
//C     	int dsize;
//C     } datum;
//C     #endif

/*
	* Translate NDBM calls into DB calls so that DB doesn't step on the
	* application's name space.
	*/
//C     #define	dbm_clearerr(a)		__db_ndbm_clearerr(a)
//C     #define	dbm_close(a)		__db_ndbm_close(a)
//C     #define	dbm_delete(a, b)	__db_ndbm_delete(a, b)
//C     #define	dbm_dirfno(a)		__db_ndbm_dirfno(a)
//C     #define	dbm_error(a)		__db_ndbm_error(a)
//C     #define	dbm_fetch(a, b)		__db_ndbm_fetch(a, b)
//C     #define	dbm_firstkey(a)		__db_ndbm_firstkey(a)
//C     #define	dbm_nextkey(a)		__db_ndbm_nextkey(a)
//C     #define	dbm_open(a, b, c)	__db_ndbm_open(a, b, c)
//C     #define	dbm_pagfno(a)		__db_ndbm_pagfno(a)
//C     #define	dbm_rdonly(a)		__db_ndbm_rdonly(a)
//C     #define	dbm_store(a, b, c, d) 	__db_ndbm_store(a, b, c, d)

/*
	* Translate DBM calls into DB calls so that DB doesn't step on the
	* application's name space.
	*
	* The global variables dbrdonly, dirf and pagf were not retained when 4BSD
	* replaced the dbm interface with ndbm, and are not supported here.
	*/
//C     #define	dbminit(a)	__db_dbm_init(a)
//C     #define	dbmclose	__db_dbm_close
//C     #if !defined(__cplusplus)
//C     #define	delete(a)	__db_dbm_delete(a)
//C     #endif
//C     #define	fetch(a)	__db_dbm_fetch(a)
//C     #define	firstkey	__db_dbm_firstkey
//C     #define	nextkey(a)	__db_dbm_nextkey(a)
//C     #define	store(a, b)	__db_dbm_store(a, b)

/*******************************************************
	* Hsearch historic interface.
	*******************************************************/
//C     typedef enum {
//C     	FIND, ENTER
//C     } ACTION;

//C     typedef struct entry {
//C     	char *key;
//C     	char *data;
//C     } ENTRY;

//C     #define	hcreate(a)	__db_hcreate(a)
//C     #define	hdestroy	__db_hdestroy
//C     #define	hsearch(a, b)	__db_hsearch(a, b)

//C     #endif /* DB_DBM_HSEARCH */

//C     #if defined(__cplusplus)
//C     }
//C     #endif

/* Restore default compiler warnings */
//C     #ifdef _MSC_VER
//C     #pragma warning(pop)
//C     #endif
//C     #endif /* !_DB_H_ */

/* DO NOT EDIT: automatically built by dist/s_include. */
//C     #ifndef	_DB_EXT_PROT_IN_
//C     #define	_DB_EXT_PROT_IN_

//C     #if defined(__cplusplus)
//C     extern "C" {
//C     #endif

//C     int db_create __P((DB **, DB_ENV *, u_int32_t));
int  db_create(DB **, DB_ENV *, u_int32_t );
//C     char *db_strerror __P((int));
char * db_strerror(int );
//C     int db_env_create __P((DB_ENV **, u_int32_t));
int  db_env_create(DB_ENV **, u_int32_t );
//C     int log_compare __P((const DB_LSN *, const DB_LSN *));
int  log_compare(DB_LSN *, DB_LSN *);
//C     int db_env_set_func_close __P((int (*)(int)));
int  db_env_set_func_close(int  function(int ));
//C     int db_env_set_func_dirfree __P((void (*)(char **, int)));
int  db_env_set_func_dirfree(void  function(char **, int ));
//C     int db_env_set_func_dirlist __P((int (*)(const char *, char ***, int *)));
int  db_env_set_func_dirlist(int  function(char *, char ***, int *));
//C     int db_env_set_func_exists __P((int (*)(const char *, int *)));
int  db_env_set_func_exists(int  function(char *, int *));
//C     int db_env_set_func_free __P((void (*)(void *)));
int  db_env_set_func_free(void  function(void *));
//C     int db_env_set_func_fsync __P((int (*)(int)));
int  db_env_set_func_fsync(int  function(int ));
//C     int db_env_set_func_ftruncate __P((int (*)(int, off_t)));
int  db_env_set_func_ftruncate(int  function(int , off_t ));
//C     int db_env_set_func_ioinfo __P((int (*)(const char *, int, u_int32_t *, u_int32_t *, u_int32_t *)));
int  db_env_set_func_ioinfo(int  function(char *, int , u_int32_t *, u_int32_t *, u_int32_t *));
//C     int db_env_set_func_malloc __P((void *(*)(size_t)));
int  db_env_set_func_malloc(void * function(size_t ));
//C     int db_env_set_func_map __P((int (*)(char *, size_t, int, int, void **)));
int  db_env_set_func_map(int  function(char *, size_t , int , int , void **));
//C     int db_env_set_func_pread __P((ssize_t (*)(int, void *, size_t, off_t)));
int  db_env_set_func_pread(ssize_t  function(int , void *, size_t , off_t ));
//C     int db_env_set_func_pwrite __P((ssize_t (*)(int, const void *, size_t, off_t)));
int  db_env_set_func_pwrite(ssize_t  function(int , void *, size_t , off_t ));
//C     int db_env_set_func_open __P((int (*)(const char *, int, ...)));
int  db_env_set_func_open(int  function(char *, int ,...));
//C     int db_env_set_func_read __P((ssize_t (*)(int, void *, size_t)));
int  db_env_set_func_read(ssize_t  function(int , void *, size_t ));
//C     int db_env_set_func_realloc __P((void *(*)(void *, size_t)));
int  db_env_set_func_realloc(void * function(void *, size_t ));
//C     int db_env_set_func_rename __P((int (*)(const char *, const char *)));
int  db_env_set_func_rename(int  function(char *, char *));
//C     int db_env_set_func_seek __P((int (*)(int, off_t, int)));
int  db_env_set_func_seek(int  function(int , off_t , int ));
//C     int db_env_set_func_sleep __P((int (*)(u_long, u_long)));
int  db_env_set_func_sleep(int  function(u_long , u_long ));
//C     int db_env_set_func_unlink __P((int (*)(const char *)));
int  db_env_set_func_unlink(int  function(char *));
//C     int db_env_set_func_unmap __P((int (*)(void *, size_t)));
int  db_env_set_func_unmap(int  function(void *, size_t ));
//C     int db_env_set_func_write __P((ssize_t (*)(int, const void *, size_t)));
int  db_env_set_func_write(ssize_t  function(int , void *, size_t ));
//C     int db_env_set_func_yield __P((int (*)(void)));
int  db_env_set_func_yield(int  function());
//C     int db_sequence_create __P((DB_SEQUENCE **, DB *, u_int32_t));
int  db_sequence_create(DB_SEQUENCE **, DB *, u_int32_t );
//C     #if DB_DBM_HSEARCH != 0
//C     int	 __db_ndbm_clearerr __P((DBM *));
//C     void	 __db_ndbm_close __P((DBM *));
//C     int	 __db_ndbm_delete __P((DBM *, datum));
//C     int	 __db_ndbm_dirfno __P((DBM *));
//C     int	 __db_ndbm_error __P((DBM *));
//C     datum __db_ndbm_fetch __P((DBM *, datum));
//C     datum __db_ndbm_firstkey __P((DBM *));
//C     datum __db_ndbm_nextkey __P((DBM *));
//C     DBM	*__db_ndbm_open __P((const char *, int, int));
//C     int	 __db_ndbm_pagfno __P((DBM *));
//C     int	 __db_ndbm_rdonly __P((DBM *));
//C     int	 __db_ndbm_store __P((DBM *, datum, datum, int));
//C     int	 __db_dbm_close __P((void));
//C     int	 __db_dbm_delete __P((datum));
//C     datum __db_dbm_fetch __P((datum));
//C     datum __db_dbm_firstkey __P((void));
//C     int	 __db_dbm_init __P((char *));
//C     datum __db_dbm_nextkey __P((datum));
//C     int	 __db_dbm_store __P((datum, datum));
//C     #endif
//C     #if DB_DBM_HSEARCH != 0
//C     int __db_hcreate __P((size_t));
//C     ENTRY *__db_hsearch __P((ENTRY, ACTION));
//C     void __db_hdestroy __P((void));
//C     #endif

//C     #if defined(__cplusplus)
//C     }
//C     #endif
//C     #endif /* !_DB_EXT_PROT_IN_ */
+/
