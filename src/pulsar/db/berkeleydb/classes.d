module pulsar.db.berkeleydb.classes;

version(berkeleydb):

import std.traits;
import std.conv;
import std.datetime;
import std.exception;

//import pulsar.atoms;
//import pulsar.containers.index;
import pulsar.db.berkeleydb.exports;
import pulsar.db.berkeleydb.tools;

/// Флаги открытия среды
enum EnvOpenFlags : uint
{
	NONE                = 0x00000000,
	DB_CREATE           = 0x00000001,
	DB_RECOVER          = 0x00000002,
	DB_USE_ENVIRON      = 0x00000004,
	DB_USE_ENVIRON_ROOT = 0x00000008,
	DB_FAILCHK          = 0x00000010,
	DB_THREAD           = 0x00000020,
	DB_INIT_CDB				     = 0x00000080,
	DB_INIT_LOCK        = 0x00000100,
	DB_INIT_LOG         = 0x00000200,
	DB_INIT_MPOOL       = 0x00000400,
	DB_INIT_REP         = 0x00001000,
	DB_INIT_TXN         = 0x00002000,
	DB_LOCKDOWN         = 0x00004000,
	DB_PRIVATE          = 0x00010000,
	DB_RECOVER_FATAL    = 0x00020000,
	DB_REGISTER         = 0x00040000,
	DB_SYSTEM_MEM       = 0x00080000
}
/// Флаги закрытия среды
enum EnvCloseFlags : uint
{
	DB_NONE = 0 ,
	DB_FORCESYNC = 0x00000001
}

/// Флаги создания базы
enum DbCreateFlags : uint
{
	NONE = 0x00000000,
	/// Open in an XA environment.
	DB_XA_CREATE = 0x0000002
}
/// Флаги открытия базы
enum DbOpenFlags : uint
{
	NONE = 0x00000000,
	DB_AUTO_COMMIT      = 0x00000100,
	DB_CREATE           = 0x00000001, // Create the database if the database does not already exist
	DB_EXCL             = 0x00000004, // Return an error if the database already exists
	DB_MULTIVERSION     = 0x00000008,
	DB_NOMMAP           = 0x00000010,
	DB_RDONLY           = 0x00000400, // Open the database for reading only
	DB_READ_UNCOMMITTED = 0x00000200,
	DB_THREAD           = 0x00000020,
	DB_TRUNCATE         = 0x00040000
}
/// Флаги закрытия базы
enum DbCloseFlags : uint
{
	NONE = 0x00000000,
	/// Do not flush cached information to disk.
	DB_NOSYNC = 0x00000001
}

/// Тип базы данных (метода доступа)
enum DbType : DBTYPE
{
	DB_BTREE   = 1,
	DB_HASH    = 2,
	DB_RECNO   = 3,
	DB_QUEUE   = 4,
	DB_UNKNOWN = 5,			/* automatically determine db type */
	DB_HEAP    = 6
}

/// Флаги методов добавления значений в базу
enum DbPutFlags : uint
{
	DB_NONE          = 0,
	DB_APPEND        = 2,
	DB_NODUPDATA	    = 19,
	DB_NOOVERWRITE	  = 20,
	DB_MULTIPLE				  = 0x00000800,
	DB_MULTIPLE_KEY  = 0x00004000,
	DB_OVERWRITE_DUP = 21
}
/// Флаги методов извлечения значений из базы
enum DbGetFlags : uint
{
	DB_NONE          = 0,
	DB_CONSUME          = 4,
	DB_CONSUME_WAIT     = 5,
	DB_GET_BOTH         = 8,
	DB_SET_RECNO        = 28,
	DB_IGNORE_LEASE     = 0x00001000,
	DB_MULTIPLE         = 0x00000800,
	DB_READ_COMMITTED   = 0x00000400,
	DB_READ_UNCOMMITTED = 0x00000200,
	DB_RMW              = 0x00002000
}
/// Флаги методов удаления значений из базы
enum DbDelFlags : uint
{
	DB_NONE         = 0,
	DB_CONSUME      = 4,
	DB_MULTIPLE     = 0x00000800,
	DB_MULTIPLE_KEY	=	0x00004000
}
/// Коды возвратов методов значений из базы
enum DbRetuns : int
{
	DB_OK           = 0,
 DB_NOTFOUND     = -30987,
	DB_KEYEMPTY     = -30995,
	DB_BUFFER_SMALL	= -30999
}
//------------------------------------------------------

/// Класс окружения
class Environment
{
private:
	DB_ENV* _env;
	string _homeDir;
	string _tmpDir;
	string _logDir;
	//IndexList!(DataBase, ushort, "name") _opens;
	DataBase[string] _opens;
	bool _opened = false;

public:
	/// Путь к каталогу хранилища баз.
	/// Установка после открытия среды не действует до переоткрытия.
	@property // homeDir
	{
		string homeDir() { return _homeDir; }
		void homeDir(string value) { enforce(value.length); _homeDir = value; }
	}
	/// Путь к каталогу временных файлов.
	/// Установка после открытия среды не действует до переоткрытия.
	@property // tmpDir
	{
		string tmpDir() { return _tmpDir; }
		void tmpDir(string value) { _tmpDir = value; }
	}
	/// Путь к каталогу файлов логов.
	/// Установка после открытия среды не действует до переоткрытия.
	@property // logDir
	{
		string logDir() { return _logDir; }
		void logDir(string value) { _logDir = value; }
	}

public:
	this(bool createNativeEnvironment = true)
	{
		//_opens = new IndexList!(DataBase, ushort, "name")();
		if(createNativeEnvironment)
			if(int ret = db_env_create(&_env, 0))
				throw new Exception(bdbError(ret));
	}
	/// Путь к каталогу хранилища баз.
	this(string path)
	{
		this.homeDir = path;
		this();
		open();

	}
	~this()
	{
		close();
	}

	/// Открывает среду для работы
	/// path - Путь к каталогу хранилища баз.
	void open(string path = null)
	{
		open(path,EnvOpenFlags.NONE);
	}
	/// Открывает среду для работы
	/// path - Путь к каталогу хранилища баз.
	void open(string path, EnvOpenFlags flags)
	{
		if(path.length == 0)
			path = homeDir;
		if(path.length == 0)
			throw new Exception("Не указан каталог хранилища!");
		this.homeDir = path;

		if(_env)
		{
			if(_tmpDir.length > 0)
				if(int ret = db_env_set_tmp_dir(_env, toDBCharStr(_tmpDir)))
					throw new Exception(bdbError(ret));
			if(_logDir.length > 0)
				if(int ret = db_env_set_lg_dir(_env, toDBCharStr(_logDir)))
					throw new Exception(bdbError(ret));

			if(int ret = db_env_open(_env, toDBCharStr(path),
																												cast(uint)flags, 0))
				throw new Exception(bdbError(ret));
		}
		_opened = true;
	}

	/// Сбрасывает всю информацию из кеша на диск
	void sync()
	{
		if(_opened)
		{
			foreach(k; _opens)
				if(k._opened)
					k.sync();
		}
	}

	/// Закрывает все базы, но не закрывает Environment
	void clear()
	{
		if(_opened)
		{
			foreach(k; _opens.dup)
				if(k._opened)
					k.close();
		}
	}

	/// Закрывает среду
	void close(EnvCloseFlags flags = EnvCloseFlags.DB_NONE)
	{
		if(_opened)
		{
			foreach(k; _opens.dup)
				if(k._opened)
					k.close();
			if(_env)
				if(int ret = db_env_close(_env, cast(uint)flags))
					throw new Exception(bdbError(ret));
			_opened = false;
		}
	}

	void remove()
	{
		if(int ret = db_env_remove(_env, toDBCharStr(this.homeDir), 0))
			throw new Exception(bdbError(ret));
	}

	/// Создает объект базы (не саму базу) в данном окружени
	private DataBase create(DbCreateFlags flags = DbCreateFlags.NONE)
	{
		return new DataBase(this, flags);
	}

	/// Открывает базу в данном окружени
	DataBase openDb(string filename, DbType type, DbOpenFlags flags,
																	string basename = null)
	{
		DataBase db = _opens.get(text(filename, "-", basename), null);
		if(db)
			return db;
		if(_env is null)
			filename = this.homeDir ~ "/" ~ filename;
		db = new DataBase(this, DbCreateFlags.NONE);
		db.open(filename, type, flags,	basename);
		return db;
	}

}
//---------------------------------------------------------------------------

/// Класс базы
class DataBase
{
package:
	Environment _env;
	DB* dbp;
	bool _opened;
	string _name;

public:
	@property string name() { return _name; }

public:
	this(DbCreateFlags flags = DbCreateFlags.NONE)
	{
		this(null,flags);
	}
	private this(Environment env, DbCreateFlags flags = DbCreateFlags.NONE)
	{
		_env = env;
		if(int ret = db_create(&dbp, _env ? _env._env : null, cast(uint)flags))
			throw new Exception(bdbError(ret));
	}
	/// Деструктор по умолчанию
	~this()
	{
		close();
	}

	/// Открывает соединение с базой
	void open(string filename, DbType type, DbOpenFlags flags,
											string basename = null)
	{
		if(_opened)
			return;
		if(int ret = db_open(dbp, null, toDBCharStr(filename),toDBCharStr(basename),
																							cast(DBTYPE)type, cast(uint)flags, 0))
			throw new Exception(bdbError(ret));
		_opened = true;
		_name = text(filename, "-", basename);
		if(_env)
			_env._opens[_name] = this;
	}
	/// Закрывает соединение
	void close(DbCloseFlags flags = DbCloseFlags.NONE)
	{
		if(_opened)
		{
			sync();
			if(int ret = db_close(dbp, cast(uint)flags))
				throw new Exception(bdbError(ret));
			_opened = false;
			if(_env)
				_env._opens.remove(_name);
		}
	}

	/// Добавляет запись
	//void put(Ref key, Ref value, DbPutFlags flags = DbPutFlags.DB_NONE)
	void put(ubyte[] key, ubyte[] value, DbPutFlags flags = DbPutFlags.DB_NONE)
	{
		DBT k, v;
		k.data = key.ptr;
		k.size = cast(uint)key.length;

		v.data = value.ptr;
		v.size = cast(uint)value.length;

		if (int ret = db_put(dbp, null, &k, &v, cast(uint)flags))
			throw new Exception(bdbError(ret));
	}
	/// Извлекает запись по ключу
	bool get(ubyte[] key, ref ubyte[] value, DbGetFlags flags = DbGetFlags.DB_NONE)
	{
		DBT k, v;
		k.data = key.ptr;
		k.size = cast(uint)key.length;

		v.flags = cast(uint)DbtFlags.DB_DBT_USERMEM;
		v.data = value.ptr;
		v.ulen = cast(uint)value.length;
		//v.size = cast(uint)value.length;

		int ret = db_get(dbp, null, &k, &v, cast(uint)flags);
		switch(cast(DbRetuns)ret)
		{
			case DbRetuns.DB_OK :
				if(v.size != value.length)
					value.length = v.size;
				return true;
			case DbRetuns.DB_NOTFOUND : return false;
			case DbRetuns.DB_BUFFER_SMALL :
			{
				if(value.length > 0)
					throw new Exception(bdbError(ret));

				//value.malloc(v.size, false);
				value.length = v.size;
				v.data = value.ptr;
				v.ulen = v.size;
				if((ret = db_get(dbp, null, &k, &v, cast(uint)flags)) != 0)
					throw new Exception(bdbError(ret));
			}
			break;
			default: throw new Exception(bdbError(ret));
		}
		return true;
	}
	/// Удаляет запись по ключу
	bool del(ubyte[] key, DbDelFlags flags = DbDelFlags.DB_NONE)
	{
		DBT k;
		k.data = key.ptr;
		k.size = cast(uint)key.length;

		int ret = db_del(dbp, null, &k, flags);
		if (ret == DbRetuns.DB_NOTFOUND)
			return false;
		else if (ret != 0)
			throw new Exception(bdbError(ret));
		return true;
	}
	/// Проверяет на существование записи по ключу
	bool exists(ubyte[] key)
	{
		DBT k;
		k.data = key.ptr;
		k.size = cast(uint)key.length;

		int ret = db_exists(dbp, null, &k, 0);
		if(ret == DbRetuns.DB_NOTFOUND)
			return false;
		if(ret != 0)
			throw new Exception(bdbError(ret));
		return true;
	}

	/// Удаляет все записи в базе.
	/// Returns: количество удаленных записей.
	uint truncate()
	{
		uint x;
		if (int ret = db_truncate(dbp, null, &x, 0))
			throw new Exception(bdbError(ret));
		return x;
	}
	/// Сбрасывает всю информацию из кеша на диск
	void sync()
	{
		if (int ret = db_sync(dbp, 0))
			throw new Exception(bdbError(ret));
	}

	/// Возвращает итератор по ключам базы.
	KeysIter keys() { return KeysIter(dbp); }

}

/// Структура курсора (итератора) по записям в базе.
/// При копировании данной структуры создается новй итератор.
struct KeysIter
{
private:
	DB* db;
	DBC dbc;
	DBT k;
	ubyte[] buf;

	this(DB* db)
	{
		assert(db);
		this.db = db;

		popFront();
	}
public:
	this(this)
	{
		db = db;
		dbc = null;
		buf = null;
		popFront();
	}
	~this()
	{
		close();
	}

public:
	@property bool empty() const { return buf.length == 0; }
	@property const(ubyte[]) front( ) const	{ return buf; }
	void popFront()
	{
		if(dbc is null)
			open();

		ubyte[1] val;
		DBT v;

		int ret = db_cursor_get(dbc, &k, &v, DB_NEXT);
		if(ret == DbRetuns.DB_OK)
		{
			if(k.size != buf.length)
				resizeBuf();
		}
		else if(ret == DbRetuns.DB_BUFFER_SMALL)
		{
			if(k.size <= buf.length)
				resizeBuf();
			else
			{
				resizeBuf();
				if(db_cursor_get(dbc, &k, &v, DB_NEXT) != DbRetuns.DB_OK)
					throw new Exception(bdbError(ret));
			}
		}
		else if(ret == DbRetuns.DB_NOTFOUND)
		{
			buf.length = 0;
		}
		else
			throw new Exception(bdbError(ret));
	}
	auto save() { return this; }

private:
	void resizeBuf()
	{
		buf.length = k.size;
		k.data = buf.ptr;
		k.ulen = cast(uint)buf.length;
	}
	void open()
	{
		buf.length = 16;
		k.flags = cast(uint)DbtFlags.DB_DBT_USERMEM;
		k.data = buf.ptr;
		k.ulen = cast(uint)buf.length;

		//#define	DB_CURSOR_BULK				0x00000001

		int ret = db_cursor(db, null, &dbc, 1);
		if(ret != DbRetuns.DB_OK)
			//throw new Exception("Не удалось создать курсор базы!");
			throw new Exception(bdbError(ret));
	}
	void close()
	{
		if(dbc !is null)
		{
			auto ret = dbc_close(dbc);
			bdbError(ret);
		}
		dbc = null;
	}
}
