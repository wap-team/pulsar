module pulsar.db.berkeleydb.tools;

version(berkeleydb):

import std.conv;
import std.string;

import pulsar.db.berkeleydb.exports;

char* toDBCharStr(string txt)
{
	if(txt is null)
		return null;
	immutable(char)* cstr = txt.toStringz();
	return cast(char*)cstr;
}
string fromDBCharStr(void* txt, size_t len)
{
	if(len==0)
		return "";
	char* cstr = cast(char*)txt;
	return to!string(cstr[0..len]);
}
string fromDBCharStrZ(void* txt)
{
	int x = 0;
	for(;(cast(ubyte*)txt)[x] > 0; x++){}
	return fromDBCharStr(txt,x);
}
string bdbError(int errno)
{
	char* str = db_strerror(errno);
	return text("BDB error: (",errno,") " ~ fromDBCharStrZ(str));
}

/*

struct Ref
{
	import std.traits;
	import core.memory;

private:
	size_t _length;
	void* _ptr;

public:
	@property // length
	{
		size_t length()
		{
			return _length == size_t.max ? *(cast(size_t*)_ptr) : _length;
		}
		void length(size_t val)
		{
			if(_length != size_t.max)
				throw new Exception("Попытка изменить размер локальной переменной!");
			*(cast(size_t*)_ptr) = val;
		}
	}
	@property void* ptr()
	{
				return _length == size_t.max ? _ptr + size_t.sizeof : _ptr ;
	}

public:
	this(T)(ref T var)
		{
			_ptr = cast(void*)&var;
			static if(isArray!T)
				_length = size_t.max;
			else static if(isNumeric!T)
				_length = T.sizeof;
			else
				static assert(false,"Unsupported type - " ~ T.stringof);
		}
	void malloc(size_t size, bool clear = true)
	{
		if(_length != size_t.max)
			throw new Exception("Попытка изменить размер локальной переменной!");
		size_t* len = cast(size_t*)_ptr;
		auto p = cast(size_t*)(_ptr + size_t.sizeof);
		*len = size;
//		uint attr = 0;
//		if(p !is null)
//			attr = GC.getAttr(p);
		*p = *cast(size_t*)(size ? GC.malloc(size) : null);
		if(clear)
			foreach(i; 0..size)
				*(cast(byte*)p + i) = 0;

	}
}

*/
///unittest
//{
//	import pulsar.atoms;

//	int x = 10;
//	Ref r1 = Ref(x);

//	string s = "abc";
//	Ref r2 = Ref(s);

//	string ss;
//	Ref r3 = Ref(ss);
//	r3.malloc(4);
//}
