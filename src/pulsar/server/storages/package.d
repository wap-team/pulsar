module pulsar.server.storages;

import pulsar.atoms;

/// Базовый интерфейс хранилищ данных
interface IDataStorage
{
	final string typeName()
	{
		return (cast(Object)this).toString();
	}

	/// Версия api(сервера) хранилица данных (major.minor.patch или полное описание)
	string storageVersion(bool fullInfo = false);

	/// Определяет, имеет ли хранилище объект с указанным ключом.
	bool contains(const(ubyte[]) key, string baseName);

	/// Сохраняет объект в хранилище.
	/// Возвращает false, если хранилище не предназначено
	/// для хранения подобных объектов.
	bool save(const(Object) obj, const(ubyte[]) key, string baseName);

	/// Загружает объект из хранилища.
	/// Возвращает false если объект отсутствует в хранилище
	bool load(ref Object obj, const(ubyte[]) key, string baseName);

	/// Форсирует запись данных на диск.
	void flush();
	/// Закрывает хранилище
	void close();
}
/// Интерфейс хранилищ сущностей
interface IEssenceStorage : IDataStorage
{
	/// Сохраняет объект в хранилище.
	/// Возвращает false, если хранилище не предназначено
	/// для хранения подобных объектов.
	bool save(const(Object) obj, OID oid, string reason);

	/// Загружает объект из хранилища.
	/// Возвращает false если объект отсутствует в хранилище
	bool load(ref Object obj, OID oid, const(ClassInfo) type);
}

/// Сруктура правила определения ключа и/или базы хранения объектов
// TODO : Полноценно key на загрузку может заработать,
// если в заглушках глобальных объектов использовать ключ,
// найденный по этой фукции, а не OID.
struct MaintenanceRule
{
	string baseNameStr;
	string function(const(ClassInfo) type) baseNameFunc;
	ubyte[] function(const(Object) obj, const(ClassInfo)) keyFunc;
	/// Определяет, будет ли правило применяться для наследованных типов
	bool strict;

	string baseName(const(ClassInfo) type)
	{
		if(baseNameStr.length)
			return baseNameStr;
		return baseNameFunc is null ? type.toString() : baseNameFunc(type);
	}
	ubyte[] key(const(Object) obj, const(ClassInfo) type)
	{
		return keyFunc is null ? null : keyFunc(obj,type);
	}
}

/// Класс менеджера хранилищ
class DataStorageManager
{
public:
	import std.datetime.stopwatch: StopWatch;
	//StopWatch getRuleBench;

private:
	struct DataStorageItem
	{
		string nickname;
		IDataStorage storage;
		IEssenceStorage essenceStorage;
		alias storage this;
		// Можно будет заюзать, если надо будет отключить правила на хранилище
		// bool useRules
	}
	DataStorageItem[] _storages;
	MaintenanceRule[ClassInfo] rules;

public:
	this() {}

public:
	/// Возвращает хранилище с указанным именем
	IDataStorage get(string nickname)
	{
		foreach(s; _storages)
			if(s.nickname == nickname)
				return s;
		return null;
	}
	void add(IDataStorage storage, string name = null)
	{
		Object st = cast(Object)storage;
		_storages ~= DataStorageItem(name, storage, cast(IEssenceStorage)st);
	}

	int opApply(int delegate(IDataStorage) dg)
	{
		int result = 0;
		foreach(item;_storages)
		{
			result = dg(item.storage);
			if(result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, IDataStorage) dg)
	{
		int result = 0;
		for(size_t i = 0; i < _storages.length; i++)
		{
			result = dg(i,_storages[i].storage);
			if(result)
				break;
		}
		return result;
	}

	MaintenanceRule getRule(const(ClassInfo) type)
	{
//		getRuleBench.start();
//		scope(exit)
//				getRuleBench.stop();
		MaintenanceRule* r;
		if((r = cast(ClassInfo)type in rules) is null)
		{
			for(ClassInfo t = cast(ClassInfo)type.base; t ; t = cast(ClassInfo)t.base)
				if((r = t in rules) !is null)
				{
					if((*r).strict && t !is type)
						continue;
					rules[cast(ClassInfo)type] = *r;
					return *r;
				}
			rules[cast(ClassInfo)type] = MaintenanceRule.init;
			return MaintenanceRule.init;
		}
	else
		return *r;
	}
	void addRule(const(ClassInfo) type, MaintenanceRule rule)
	{
		CheckNull!type;
		enforce(rules.has(type) == false, "Правило для типа [" ~ type.toString() ~ "] уже добавлено!");
		rules[type.unqual] = rule;
	}

public:
	/// Загружает объект из хранилищ.
	/// Возвращает false, если объект отсутствует в хранилищах
	bool load(ref Object obj, const(ubyte[]) key, string baseName)
	{
		CheckNullOrEmpty!(key, baseName);
		foreach(st; _storages)
			if(st.load(obj, key, baseName))
				return true;
		return false;
	}
	bool load(ref Object obj, string key, string baseName)
	{
		return load(obj, cast(const(ubyte[]))key, baseName);
	}
	bool load(ref Object obj, string key, const(ClassInfo) type)
	{
		CheckNull!type;
		auto rule = getRule(type);
		return load(obj, cast(const(ubyte[]))key, rule.baseName(type));
	}
	bool load(ref Object obj, OID key, string baseName)
	{
		return load(obj, key.data, baseName);
	}
	bool load(ref Object obj, OID key, const(ClassInfo) type)
	{
		CheckNull!(key, type);
		foreach(st; _storages)
		{
			bool loaded;
			if(st.essenceStorage)
				loaded = st.essenceStorage.load(obj, key, type);
			else
			{
				auto rule = getRule(type);
				auto k = rule.key(obj,type);
				loaded = st.load(obj, k.length ? k : key.data , rule.baseName(type));
			}
			if(loaded)
				return true;
		}
		return false;
	}

	/// Сохраняет объект в хранилище.
	/// Возвращает false, если хранилище не предназначено для хранения подобных объектов.
	bool save(const(Object) obj, const(ubyte[]) key, string baseName)
	{
		return save(obj, OID.init, key, baseName, null);
	}
	bool save(const(Object) obj, string key, string baseName)
	{
		return save(obj, cast(const(ubyte[]))key, baseName);
	}
	bool save(const(Object) obj, string key)
	{
		CheckNull!obj;
		return save(obj, key, IProxy.typeId(obj).toString());
	}
	bool save(const(Object) obj, OID oid, string reason = null)
	{
		return save(obj, oid, null, null, reason);
	}
private:
	bool save(const(Object) obj, OID oid, const(ubyte[]) key, string baseName, string reason = null)
	{
		import std.datetime;

		CheckNull!(obj);
		assert(oid.empty == false || key.length > 0, "Не указан ключ!");
		bool saved;
		foreach(st; _storages)
		{
			bool res;
			if(st.essenceStorage && oid.empty == false)
				res = st.essenceStorage.save(obj, oid, reason);
			else
			{
				auto type = IProxy.typeId(obj);
				auto rule = getRule(type);
				if(key.length)
				res = st.save(obj, key, baseName.length ? baseName : rule.baseName(type));
				else
				{
					auto k = rule.key(obj, type);
					res = st.save(obj, k.length ? k : oid.data,
																								baseName.length ? baseName : rule.baseName(type));
				}
			}
			saved = saved || res;
		}
		return saved;
	}
}


