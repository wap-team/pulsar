module pulsar.server.storages.berkeleydb;

version(berkeleydb):

import std.file;
import std.path;
import std.parallelism;
import core.thread;
import undead.stream;

import pulsar.atoms;
import pulsar.gol.servant;
import pulsar.server.storages;
import pulsar.reflection;
import pulsar.serialization;
import pulsar.text.config;

/// Класс хранилища BerkeleyDB
class BerkeleyDBStorage : IDataStorage, IConfInit
{
	import pulsar.db.berkeleydb;

private:
	Environment _env;
	TaskPool _pool;
	string _dirName;
	bool _asyncSave;

public:
	@property Environment env() const
	{
		enforce(_env, "Хранилище не инициализировано!");
		return _env.unqual;
	}
	@property // dirName
	{
		string dirName() const { return _dirName; }
		void dirName(string value)
		{
			if(value.empty)
				throw new Exception("Не указан каталог хранилища!");
			if(!exists(value))
				throw new Exception("Каталог хранилища [" ~ value ~ "] не существует!");
			if(!isDir(value))
				throw new Exception("Путь [" ~ value ~ "] не является каталогом!");

			_dirName = value;

			initEnvironment();
		}
	}
	@property // asyncSave
	{
		bool asyncSave() const { return _asyncSave; }
		void asyncSave(bool value)
		{
			_asyncSave = value;
			if(_asyncSave && _pool is null)
				_pool = new TaskPool(1);
		}
	}

	/// Версия api(сервера) хранилица данных (major.minor.patch - описание)
	override string storageVersion(bool fullInfo = false)
	{
		import pulsar.db.berkeleydb.exports;
		import std.string: fromStringz;

		int family, release, major, minor, patch;
		char[] s_ver;
		if(fullInfo == false)
		{
			db_version(&major, &minor, &patch);
			return format("%s.%s.%s", major, minor, patch);
		}
		s_ver = fromStringz(db_full_version(&family, &release, &major, &minor, &patch));
		return format("%s", s_ver);
	}

	this() {  }
	~this()
	{
		if(_pool)
			_pool.finish(true);
		if(_env)
		{
			_env.sync();
			_env.close();
			_env = null;
		}
	}

	override void confInit(string[string] initData)
	{
		string s = initData.get("dirName",null);
		Logger.log(4, "dirName: %s", s);
		dirName = s;
		s = initData.get("asyncSave","true");
		Logger.log(4, "asyncSave: %s", s);
		asyncSave = to!bool(s);
	}

	override bool contains(const(ubyte[]) key, string baseName)
	{
		assert(false, "ToDo");
//		if(oid.empty)
//			return false;
//		return contains(oid.toString(), type);
	}

	override bool save(const(Object) obj, const(ubyte[]) key, string baseName)
	{
		CheckNull!obj;
		CheckNullOrEmpty!(key, baseName);

		if(baseName.endsWith(".bdb") == false)
			baseName = baseName ~ ".bdb";

		StopWatch sw = StopWatch(AutoStart.yes);

		ubyte[] k = (cast(ubyte[])key).dup;
		// Serialization
		MemoryStream ms = new MemoryStream();
		ms.reserve(1000);
		SerializationSettings sets;
		sets.mode = SerializationMode.Save;
		Serializer.run(obj, XmlArchiver.xmlWriter(ms), sets);

		sw.stop();

		// Save in bdb
		void putInBdb(string msgTemplate)
		{
			try
			{
				StopWatch sw = StopWatch(AutoStart.yes);
				DataBase db = env.openDb(baseName, DbType.DB_BTREE,	DbOpenFlags.DB_CREATE);
				enforce(db);
				db.put(k, ms.data);
				db.sync();
				sw.stop();
				trace(format(" └ " ~ msgTemplate, sw.peek().total!"msecs"));
			}
			catch(Throwable err)
			{
				version(Posix)
				{
					Logger.log();
					Logger.log();
					Logger.log(0,"!!!!!!! DATA STORAGE CRITICAL ERROR !!!!!!!");
					Logger.logError(err);
					import core.sys.posix.signal;
					import core.thread;
					_pool.stop();
					kill(getpid, SIGABRT);
				}
				else
				{
					static assert(false,"TODO");
				}
			}
		}

		string msg = format("BDB storage save(%s+%s): %s (%s)[%s]",
													//st.nickname.length < 3 ? st.nickname ~ " " : st.nickname,
													sw.peek().total!"msecs",
													"%s",
													unqual(obj).toString().replace("%", "%%"),
													IProxy.typeId(obj).toString(),
													// TODO : Лыжа корявая
													key.length == 16 ? fromData(key).toString() : cast(string)key);


		if(asyncSave)
			_pool.put(task(&putInBdb, msg));
		else
			putInBdb(msg);
		return true;
	}

	bool load(ref Object obj, const(ubyte[]) key, string baseName)
	{
		if(baseName.endsWith(".bdb") == false)
			baseName = baseName ~ ".bdb";
		DataBase db = env.openDb(baseName, DbType.DB_BTREE,	DbOpenFlags.DB_CREATE);
		if(db is null)
			return false;

		ubyte[] data;
		if(db.get(cast(ubyte[])key, data) == false)
			return false;

		string xml = cast(string)data;

//		if(xml.canFind("StockBizObjectProperty"))
//		{
//			trace(xml);
//		}

//		if(baseName == "sim.crm.clients.client.Client.bdb")
//		{
//			//trace(xml);
//			if(xml.canFind("sim.hrm.person.Person"))
//				xml = xml.replace(".Client\"",".ClientPerson\"");
//			else
//				xml = xml.replace(".Client\"",".ClientBizUnit\"");
//			xml = xml.replace("_person", "_pers");
//			trace(xml);
//		}

//		 StopWatch sw = StopWatch(AutoStart.yes);
		obj = deserialize(XmlArchiver.xmlReader(xml), obj);
//		sw.stop();
//		sw.print(" (deser %s) ");
		return true;
	}

	override void flush() { env.sync(); }
	override void close()
	{
		if(_pool)
			_pool.finish(true);
		env.close();
	}

private:
	void initEnvironment()
	{
		if(_env)
			_env.close();
		else
			_env = new Environment();
		_env.tmpDir = "/tmp/bdb_tmp";
		//_env.logDir = "/home/gold/DProj/bdb_data/log";
		_env.open(dirName, EnvOpenFlags.DB_INIT_CDB |
																					EnvOpenFlags.DB_INIT_MPOOL |
																					EnvOpenFlags.DB_USE_ENVIRON |
																					EnvOpenFlags.DB_CREATE |
																					EnvOpenFlags.DB_PRIVATE);

	}
}


