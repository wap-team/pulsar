module pulsar.server.storages.file;

import std.file;
import std.path;
import std.utf;

import pulsar.atoms;
import pulsar.gol.servant;
import pulsar.server.storages;
import pulsar.reflection;
import pulsar.serialization;
import pulsar.text.config;


// TODO : нужна поддержка асинхрона

/// Класс файлового хранилища
class FileStorage : IDataStorage, IConfInit
{
public:
	string dirName;

	/// Версия api(сервера) хранилица данных (major.minor.patch)
	override string storageVersion(bool fullInfo = false)
	{
		return "1.0.0";
	}

	override void confInit(string[string] initData)
	{
		dirName = initData.get("dirName",null);
		if(dirName is null)
			throw new Exception("Не указан каталог хранилища!");
		Logger.logChars("dir: " ~ dirName ~ " ...");
		if(!exists(dirName))
			throw new Exception("Каталог хранилища [" ~ dirName ~ "] не существует!");
		if(!isDir(dirName))
			throw new Exception("Путь [" ~ dirName ~ "] не является каталогом!");
	}

	override bool contains(const(ubyte[]) key, string baseName)
	{
		CheckNullOrEmpty!(key, baseName);
		string k = cast(string)key;
		validate(k);
		baseName = buildPath(dirName, baseName, k);
		return exists(baseName) && isFile(baseName);
	}
	override bool save(const(Object) obj, const(ubyte[]) key, string baseName)
	{
		CheckNull!obj;
		CheckNullOrEmpty!(key, baseName);

		string k = cast(string)key;
		validate(k);
		baseName = buildPath(dirName, baseName);
		mkdirRecurse(baseName);
		baseName = buildPath(baseName, k);

		SerializationSettings sets;
		sets.mode = SerializationMode.Save;
		Serializer.run(obj, XmlArchiver.fileWriter(baseName ~ "$"), sets);

		if(exists(baseName ~ "$"))
		{
			if(exists(baseName))
				remove(baseName);
			rename(baseName ~ "$", baseName);
		}
		return true;
	}
	override bool load(ref Object obj, const(ubyte[]) key, string baseName)
	{
		CheckNullOrEmpty!(key, baseName);

		string k = cast(string)key;
		validate(k);
		baseName = buildPath(dirName, baseName, k);

		if(!exists(baseName))
			return false;
		if(isDir(baseName))
			return false;
		obj = deserialize(XmlArchiver.fileReader(baseName), obj);
		return true;
	}

	override void flush(){}
	override void close() {}
}

