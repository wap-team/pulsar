module pulsar.server.log;

/**
	* Базовый класс логеров.
	* Статические методы работают с логером по умолчанию (Default).
	*/
class Logger
{
	import std.string;
	import std.traits;
	import std.conv;
	import pulsar.atoms.datetime;

private:
	static __gshared Logger _default;
	static __gshared uint _logOff = 0;

	shared static this() { _default = new ConsoleLogger(); }

public:
	/// Default: Логер по умолчанию.
	@property static {
		Logger Default() { return _default; }
		void Default(Logger value) { _default = value; }
	}
	/// Выключает логирование. Ошибки логируются в любом случае.
	static void logOff() nothrow { _logOff++; }
	/// Включает логирование
	static void logOn() nothrow { if(_logOff > 0) _logOff--; }

	bool noTime;
	//-------------------------------------------------------------------------------------------
protected:
	abstract void writeToLog(int level, string str);
	abstract void writeLineToLog(int level, string str);
public:
	/** Записывает данные в виде строки в лог.
			Если первый аргумент - целое число, то он принимается за уровень лога.
			Аргументы должны соответствовать правилам std.string.format

			Examples:
			---
			Logger.log("message");
			Logger.log(2,"message");
			Logger.log(3,"message %s %s", 11, "fff");
			---
		*/
	static void log(S...)(S args)
	{
		if(_logOff > 0)
			return;
		if(_default !is null)
			static if(args.length == 0)
				_default.writeLineToLog(0, "");
			else static if(is(S[0] == int))
				_default.writeLineToLog(args[0], (args.length > 1 ? format(args[1..$]) : ""));
			else static if(isSomeString!(S[0]) && args.length > 1)
				_default.writeLineToLog(0, format(args));
			else
				_default.writeLineToLog(0, to!string(args));
	}

	/** Записывает данные в виде символов в лог.
			Если первый аргумент - целое число, то он принимается за уровень лога.
			Аргументы должны соответствовать правилам std.string.format

			Examples:
			---
			Logger.logChars("message");
			Logger.logChars(2,"message");
			Logger.logChars(3,"message %s %s", 11, "fff");
			---
		*/
	static void logChars(S...)(S args)
	{
		if(_logOff > 0)
			return;
		if(_default !is null)
			static if(args.length == 0)
				_default.writeToLog(0, "");
		else static if(is(S[0] == int))
			_default.writeToLog(args[0], (args.length > 1 ? format(args[1..$]) : ""));
		else
			_default.writeToLog(0, format(args));
	}

	static void logInfo(S...)(S args, string caller = __FUNCTION__)
	{
		import std.stdio;
		//writeln("x",args);
		static if(args.length == 0)
			writeln();
		else if(args.length == 1)
			log("[INFO][" ~ caller ~ "] " ~ args[0]);
		else
			log("[INFO][" ~ caller ~ "] " ~ args[0], args[1..$]);
		stdout.flush();
	}

	static void logError(Throwable err) nothrow
	{
		try
		{
			import pulsar.atoms.signals : SignalThrowable;
			SignalThrowable st = cast(SignalThrowable)err;
			if(st)
				err = st.next;
			_default.writeLineToLog(0,"");
			_default.writeLineToLog(0,format("**** ERROR %s (%s) *******", typeid(err),Clock.currTime));
			if(st)
			{
				_default.writeLineToLog(0,format("* Signal  : %s", st.signal));
				_default.writeLineToLog(0,format("* Slot    : %s", st.slot));
			}
			_default.writeLineToLog(0,format("* File    : %s(%s) ", err.file, err.line));
			_default.writeLineToLog(0,format("* Message : %s ", err.msg.replace("\n", "").replace("\r", "")));
			import pulsar.web.http;
			auto httperr = cast(HttpStatusException)err;
			if(httperr)
			{
				if(httperr.request.length)
					_default.writeLineToLog(0,format("*  Request: %s ", httperr.request));
				if(httperr.context.length)
					_default.writeLineToLog(0,format("*  Context: %s ", httperr.context));
				if(httperr.responseBody.length)
					_default.writeLineToLog(0,format("* Response: %s ", httperr.responseBody));
			}
			_default.writeLineToLog(0,"----------------");
			import pulsar.atoms.exceptions;
			if(typeid(err) !is typeid(LogicError))
				_default.writeLineToLog(0,err.info.toString());
		}
		catch(Throwable tr)
		{
			import std.stdio;
			try
			{
				writeln(tr);
			}
			catch(Throwable){}
		}
	}

	/** Записывает строку в поток отладочной информации.
					По возможностям вывода аналогичен writeln. Вызывает flush данных в поток.
					Использовать только для отладки!
	*/
	static void trace(T...)(T args)
	{
		if(_logOff > 0)
			return;
		import std.stdio;
		writeln!T(args);
		stdout.flush();
	}
	static void traceChars(T...)(T args)
	{
		if(_logOff > 0)
			return;
		import std.stdio;
		write!T(args);
		stdout.flush();
	}
	// Использовать только для отладки!
	static void print(StopWatch sw, string format = null)
	{
		if(_logOff > 0)
			return;
		import std.stdio;
		if(format is null)
			write(sw.peek().total!"msecs");
		else
			writef(format,sw.peek().total!"msecs");
		stdout.flush();
	}
	static void println(StopWatch sw, string format = null)
	{
		if(_logOff > 0)
			return;
		import std.stdio;
		if(format is null)
			writeln(sw.peek().total!"msecs");
		else
			writefln(format,sw.peek().total!"msecs");
		stdout.flush();
	}

}

/**
	* Класс логера с выводом на консоль
	*/
class ConsoleLogger : Logger
{
	import std.stdio;
	enum char[] _shift = cast(char[])"          "; // 10

	override void writeToLog(int level, string str)
	{
		char[] shift;
		if(level > 10)
		{
			shift = new char[level];
			foreach(i; 0..level)
				shift[i] = ' ';
		}
		else
			shift = _shift[0..level];

	if(noTime == false && str.length > 0)
			writeTime();
		write(shift , str);

		stdout.flush();
		noTime = true;
	}
	override void writeLineToLog(int level, string str)
	{
		char[] shift;
		if(level > 10)
		{
			shift = new char[level];
			foreach(i; 0..level)
				shift[i] = ' ';
		}
		else
			shift = _shift[0..level];

		if(noTime == false && str.length > 0)
			writeTime();
		else
			noTime = false;
		writeln(shift , str);
		stdout.flush();
	}
	void writeTime()
	{
	import std.datetime;
		SysTime d = Clock.currTime;
//		write(d.day < 10 ? "0" : null, d.day, ".");
//		write(d.month < 10 ? "0" : null, cast(ubyte)d.month, ".");
//		write(d.year - 2000, " ");
		write(d.hour < 10 ? "0" : null, d.hour, ":");
		write(d.minute < 10 ? "0" : null, d.minute, ":");
		write(d.second < 10 ? "0" : null, d.second, ".");
		writef("%03d ", d.fracSecs.total!"msecs");
	}
}


