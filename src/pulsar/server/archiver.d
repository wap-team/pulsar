module pulsar.server.archiver;

interface IArchivable
{
	@property bool isArchived() const;
	@property bool canBeArchived() const;

	void toArchive();
}
