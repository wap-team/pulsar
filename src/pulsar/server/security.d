module pulsar.server.security;

import std.uuid;

import pulsar.atoms;
import pulsar.containers.index;
import pulsar.containers.sortedlist;
import pulsar.serialization.attributes;


/// Перечисление уровней доступа
enum AccessLevel : ubyte
{
	/// Нет доступа
	None   = 0,
	/// Доступ на чтение/просмотр
	Browse = 1,
	/// Доступ на чтение/просмотр
	Run = 1,

	/// Доступ на изменение, определяется формой
	Edit = 2,
	Level1 = 2,
	/// Доступ на изменение FullEdit
	FullEdit = 4,
	Level2 = 4,

	BrowseAndEdit = Browse | Edit ,
	BrowseAndFullEdit = Browse | FullEdit,
	BrowseAndAllEdit = Browse | Edit | FullEdit,

	/// Специальный доступ 1
	Special1 = 8,
	Level3 = 8,
	/// Специальный доступ 2
	Special2 = 16,
	Level4 = 16,
	/// Специальный доступ 3
	Special3 = 32,
	Level5 = 32,
	/// Специальный доступ 4
	Special4 = 64,
	Level6 = 64,

	// Полный доступ
	Full = 127,
	// Доступ запрещен
	Deny = 128
}
bool isSetBrowseAccess(AccessLevel l)
{
	return (l & AccessLevel.Browse) > 0 && l < AccessLevel.Deny;
}
bool isSetEditAccess(AccessLevel l)
{
	return (l & (AccessLevel.FullEdit|AccessLevel.Edit)) > 0 && l < AccessLevel.Deny;
}
bool isSetFullEditAccess(AccessLevel l)
{
	return (l & AccessLevel.FullEdit) > 0 && l < AccessLevel.Deny;
}
AccessLevel sum(AccessLevel l1, AccessLevel l2)
{
	if(l1.isSet(AccessLevel.Deny) && l2.isSet(AccessLevel.Deny))
		return l1 | l2;
	if(l1.isSet(AccessLevel.Deny))
		return cast(AccessLevel)(~cast(int)(l1) & l2);
	else if(l2.isSet(AccessLevel.Deny))
		return cast(AccessLevel)(~~cast(int)(l2) & l1);
	else
		return l1 | l2;
}
@property bool isEmpty(AccessLevel l)
{
	return l == AccessLevel.None || l == AccessLevel.Deny;
}

/// Базовый класс контекста безопасности
class SecurityContext { }
//------------------------------------------------------

/// Структура идентификатора безопасности (Security Identifier)
alias SID = UUID;

/// Структура дескриптора безопасности (Security Descriptor)
alias SD = UUID;

/// Класс группы идентификаторов безопасности (SID).
class SidGroup	: OIDObject
{
private:
	string _n;
	string _d;
	SID[] _sids;

public:
	// SID группы
	@property SID sid() const { return oid; }
	/// Имя группы.
	@property // name
	{
		string name() const { return _n; }
		void name(string value)	{ CheckNullOrEmpty!value; _n = value; }
	}
	/// Описание группы.
	@property // description
	{
		string description() const { return _d; }
		void description(string value) { _d = value;	}
	}
	/// SIDы группы
	@property const(SID[]) sids() const { return _sids; }

public:
	protected this() {}
	this(string name, string desc = null)
	{
		this.name = name;
		description = desc;
	}
	override string toString() const { return name; }

	bool has(SID sid) const { return _sids.canFind(sid); }

protected:
	void add(SID sid)
	{
		foreach(o; _sids)
			if(o == sid)
				return;
		_sids ~= sid;
	}
	bool remove(SID sid)
	{
		for(long i = _sids.length-1; i >=0 ; i--)
			if(_sids[i] == sid)
			{
				_sids[i] = _sids[$-1];
				_sids.length--;
				return true;
			}
		return false;
	}
}
/// Класс группы дескрипторов безопасности (SD).
class SdGroup	: OIDObject
{
private:
	string _n;
	string _d;
	SD[] _sds;

public:
	// SD группы
	@property SD sd() const { return oid; }
	/// Имя группы.
	@property // name
	{
		string name() const { return _n; }
		void name(string value)	{ CheckNullOrEmpty!value; _n = value; }
	}
	/// Описание группы.
	@property // description
	{
		string description() const { return _d; }
		void description(string value) { _d = value;	}
	}
	/// SDы группы
	@property const(SD[]) sds() const { return _sds; }

public:
	this(string name, string desc = null)
	{
		this.name = name;
		description = desc;
	}
	override string toString() const { return name; }

	bool has(SD sd) const { return _sds.canFind(sd); }

protected:
	void add(SD sd)
	{
		foreach(o; _sds)
			if(o == sd)
				return;
		_sds ~= sd;
	}
	bool remove(SD sd)
	{
		for(long i = _sds.length-1; i >=0 ; i--)
			if(_sds[i] == sd)
			{
				_sds[i] = _sds[$-1];
				_sds.length--;
				return true;
			}
		return false;
	}
}

/// Набор SID'ов
alias AccessToken = IndexList!OID;

/// Класс записи контроля доступа (Access Control Entry).
struct ACE
{
private:
	SD _sd;
	SID _sid;
	AccessLevel _al;

	@NonSerialized size_t psd;
	@NonSerialized size_t psid;

public:
	/// Дескриптор безопасности.
	@property SD sd() const { return _sd; }
	/// Идентификатор безопасности.
	@property SID sid() const { return _sid; }
	/// Уровень доступа
	@property // access
	{
		AccessLevel access() const { return _al; }
		void access(AccessLevel level) { _al = level; }
	}
	/// Определяет, является ли ACE запретительным.
	@property bool isDenyACE() const { return (_al & AccessLevel.Deny) > 0; }
	/// Определяет, является ли ACE пустым.
	@property bool isEmpty() const
	{
		return _sd == SD.init || _sid == SID.init;
	}
	/// Определяет, задает ли ACE доступ.
	@property bool isEmptyAccess() const
	{
		return _al == AccessLevel.None || _al == AccessLevel.Deny;
	}

//	/// Определяет наличие доступа
//	@property bool hasAccess(AccessLevel access) const { return (_access & access) > 0; }

public:
	this(SD sd, SID sid, AccessLevel access = AccessLevel.None)
	{
		enforce(!sd.empty);
		enforce(!sid.empty);
		_sd = sd;
		_sid = sid;
		_al = access;
	}

public:
	string toString() const
	{
		return format("SD:{%s} -> SID:{%s} = %08b", _sd, _sid, _al);
	}
}

//------------------------------------------------------
/// Базовый класс безопасности. Реализует работу с ACE и группами
class Security
{
private:
	import core.thread : Fiber;
	static __gshared SecurityContext[Fiber] _contexts;

	ACE[] _list;
	size_t[SD] _sdIx;
	size_t[SID] _sidIx;
	IndexList!(SidGroup,ushort,"sid") _sidGroups;
	IndexList!(SdGroup,ushort,"sd") _sdGroups;

public:
	static @property // context
	{
		SecurityContext context()
		{
			Fiber f = Fiber.getThis();
			return f ? _contexts.get(f, null) : null;
		}
		void context(SecurityContext c)
		{
			Fiber f = Fiber.getThis();
			assert(f);
			if(c is null)
				_contexts.remove(f);
			else
				_contexts[f] = c;
		}
	}

	/// Группы SIDов.
	@property auto sidGroups() const { return _sidGroups; }
	/// Группы SD.
	@property auto sdGroups() const { return _sdGroups; }

public:
	this()
	{
		_list.length = 1;
		_sidGroups = new IndexList!(SidGroup,ushort,"sid")();
		_sdGroups  = new IndexList!(SdGroup,ushort,"sd")();
	}

public:
	// --- ACEs methods ---
	/// Итератор по всем ACE с указанным SD
	@noproxy auto bySD(SD sd) const
	{
		static struct Iter
		{
			Security s;
			SD sd;
			int opApply(int delegate(ACE) dg) const
			{
				int result = 0;
				size_t pos = s._sdIx.get(sd, 0);
				for(; pos > 0; pos = s._list[pos].psd)
				{
					result = dg(s._list[pos]);
					if(result)
						break;
				}
				return result;
			}
		}
		return Iter(unqual(this),sd);
	}
	/// Итератор по всем ACE с указанным SID
	@noproxy auto bySID(SID sid) const
	{
		static struct Iter
		{
			Security s;
			SID sid;
			int opApply(int delegate(ACE) dg) const
			{
				int result = 0;
				size_t pos = s._sidIx.get(sid, 0);
				for(; pos > 0; pos = s._list[pos].psid)
				{
					result = dg(s._list[pos]);
					if(result)
						break;
				}
				return result;
			}
		}
		return Iter(unqual(this),sid);
	}
	void set(SD sd, SID sid, AccessLevel al) { set(ACE(sd, sid,al)); }
	void set(ACE ace)
	{
		assert(ace.isEmpty == false);
		size_t pos = _sdIx.get(ace._sd,0);
		for(; pos > 0;pos = _list[pos].psd)
			if(_list[pos]._sid == ace._sid)
			{
				ace.psd = _list[pos].psd;
				ace.psid = _list[pos].psid;
				_list[pos] = ace;
				return;
			}
		for(pos = 1; pos < _list.length; pos++)
			if(_list[pos].isEmpty)
				break;
		ace.psd  = _sdIx.get(ace._sd,0);
		ace.psid = _sidIx.get(ace._sid,0);
		if(pos >= _list.length)
			_list ~= ace;
		else
			_list[pos] = ace;
		_sdIx[ace._sd] = pos;
		_sidIx[ace._sid] = pos;

	}
	bool remove(ACE ace) { return remove(ace._sd, ace._sid); }
	bool remove(SD sd, SID sid)
	{
		size_t pos = _sdIx.get(sd,0);
		size_t prev = 0;
		for(; pos > 0; prev = pos, pos = _list[pos].psd)
			if(_list[pos]._sid == sid)
				break;
		if(pos == 0)
			return false;

		if(prev == 0) // последний SD и есть удаляемый
		{
			if(_list[pos].psd == 0)
				_sdIx.remove(sd);
			else
				_sdIx[sd] = _list[pos].psd;
		}
		else
			_list[prev].psd = _list[pos].psd;

		prev = _sidIx.get(sid,0);
		if(prev == pos) // последний SID
		{
			if(_list[prev].psid == 0)
				_sidIx.remove(sid);
			else
				_sidIx[sid] = _list[prev].psid;
		}
		else
		{
			while( _list[prev].psid != pos)
				prev = _list[prev].psid;
			assert(prev);
			_list[prev].psid = _list[pos].psid;
		}
		_list[pos] = ACE.init;
		return true;
	}
	void removeAllwithSD(SD sd)
	{
		size_t pos = _sdIx.get(sd, 0);
		while(pos > 0)
		{
			size_t ps = _sidIx.get(_list[pos]._sid,0);
			if(ps == pos) // последний SID
			{
				if(_list[ps].psid == 0)
					_sidIx.remove(_list[pos]._sid);
				else
					_sidIx[_list[pos]._sid] = _list[ps].psid;
			}
			else
			{
				while( _list[ps].psid != pos)
					ps = _list[ps].psid;
				assert(ps);
				_list[ps].psid = _list[pos].psid;
			}
			ps = _list[pos].psd;
			_list[pos] = ACE.init;
			pos = ps;
		}
		_sdIx.remove(sd);
	}
	void removeAllwithSID(SID sid)
	{
		size_t pos = _sidIx.get(sid, 0);
		while(pos > 0)
		{
			size_t ps = _sdIx.get(_list[pos]._sd,0);
			if(ps == pos) // последний SD
			{
				if(_list[ps].psd == 0)
					_sdIx.remove(_list[pos]._sd);
				else
					_sdIx[_list[pos]._sd] = _list[ps].psd;
			}
			else
			{
				while( _list[ps].psd != pos)
					ps = _list[ps].psd;
				assert(ps);
				_list[ps].psd = _list[pos].psd;
			}
			ps = _list[pos].psid;
			_list[pos] = ACE.init;
			pos = ps;
		}
		_sidIx.remove(sid);
	}

	// --- Groups methods ---
	auto getSidGroup(SID sid) const { return _sidGroups.sid.get(sid, null); }
	bool hasSidGroup(SID sid) const { return _sidGroups.sid.has(sid); }
	void addSidGroup(SidGroup group)
	{
		CheckNull!(group);
		_sidGroups.add(group);
	}
	void removeSidGroup(SidGroup group)
	{
		CheckNull!(group);
		foreach(SidGroup g; _sidGroups)
			g.remove(group.sid);
		removeAllwithSID(group.sid);
		_sidGroups.remove(group);
	}

	auto getSdGroup(SD sd) const { return _sdGroups.sd.get(sd, null); }
	bool hasSdGroup(SD sd) const { return _sdGroups.sd.has(sd); }
	void addSdGroup(SdGroup group)
	{
		CheckNull!group;
		_sdGroups.add(group);
	}
	void removeSdGroup(SdGroup group)
	{
		CheckNull!(group);
		foreach(SdGroup g; _sdGroups)
			g.remove(group.sd);
		removeAllwithSD(group.sd);
		_sdGroups.remove(group);
	}

	/// Добавляет группу безопасности в другую группу.
	void linkSid(SidGroup target, const(SidGroup) group)
	{
		CheckNull!(target,group);
		if(_sidGroups.has(target) == false || _sidGroups.has(group) == false)
			throw new Exception("Группы должны принадлежать Security и быть однотипными!");
		target.add(group.sid);
	}
	void linkSid(SidGroup target, SID sid)
	{
		CheckNull!(target);
		if(_sidGroups.has(target) == false)
			throw new Exception("Группа должна принадлежать Security!");
		if(target.sid == sid)
			throw new LogicError("Нельзя добавить группу саму в себя!");
		target.add(sid);
	}
	bool unlinkSid(SidGroup target, const(SidGroup) group)
	{
		CheckNull!(target,group);
		if(_sidGroups.has(target) == false || _sidGroups.has(group) == false)
			throw new Exception("Группы должны принадлежать Security и быть однотипными!");
		return target.remove(group.sid);
	}
	bool unlinkSid(SidGroup target, SID sid)
	{
		CheckNull!(target);
		if(_sidGroups.has(target) == false)
			throw new Exception("Группы должны принадлежать Security и быть однотипными!");
		return target.remove(sid);
	}
	void linkSd(SdGroup target, const(SdGroup) group)
	{
		CheckNull!(target,group);
		if(_sdGroups.has(target) == false || _sdGroups.has(group) == false)
			throw new Exception("Группы должны принадлежать Security и быть однотипными!");
		target.add(group.sd);
	}
	void linkSd(SdGroup target, SD sd)
	{
		CheckNull!(target);
		if(_sdGroups.has(target) == false)
			throw new Exception("Группа должна принадлежать Security!");
		if(target.sd == sd)
			throw new LogicError("Нельзя добавить группу саму в себя!");
		target.add(sd);
	}
	bool unlinkSd(SdGroup target, const(SdGroup) group)
	{
		CheckNull!(target,group);
		if(_sdGroups.has(target) == false || _sdGroups.has(group) == false)
			throw new Exception("Группы должны принадлежать Security и быть однотипными!");
		return target.remove(group.sd);
	}
	bool unlinkSd(SdGroup target, SD sd)
	{
		CheckNull!(target);
		if(_sdGroups.has(target) == false)
			throw new Exception("Группы должны принадлежать Security и быть однотипными!");
		return target.remove(sd);
	}

	// --- Calc access methods ---
	/// Расчитывает AccessToken для SID по группам
	AccessToken calcSIDsAccessToken(SID sid) const
	{
		auto ix = new AccessToken();
		ix.add(sid);
		for(uint pos = 0; pos < ix.count; pos++)
		{
			foreach(sg; _sidGroups)
			{
				if(sg.sid != ix[pos])
					foreach(sgo; sg._sids)
					{
						if(sgo == ix[pos])
						{
							ix.add(sg.sid);
							break;
						}
					}
			}
		}
		return ix;
	}
	/// Расчитывает AccessToken для SD по группам
	AccessToken calcSDsAccessToken(SD sd) const
	{
		auto ix = new AccessToken();
		ix.add(sd);
		for(uint pos = 0; pos < ix.count; pos++)
		{
			//trace("-- ", ix[pos], " --");
			foreach(sg; _sdGroups)
			{
				//trace(sg);
				if(sg.sd != ix[pos])
					foreach(sgo; sg._sds)
					{
						if(sgo == ix[pos])
						{
							ix.add(sg.sd);
							break;
						}
					}
			}
		}
		return ix;
	}


	//------
	/// Расчитывает уровень доступа для данного дескриптора безопасности.
	AccessLevel calcAccessLevel(const(AccessToken) atSids, SD sd, bool calcSdAccessToken = false) const
	{
		if(calcSdAccessToken)
			return calcAccessLevel(atSids,calcSDsAccessToken(sd));

		AccessLevel res = AccessLevel.None;
		foreach(ace; bySD(sd))
			if(atSids.has(ace.sid))
				res = res.sum(ace.access);
			return res;
	}
	/// Расчитывает уровень доступа для данного дескриптора безопасности.
	AccessLevel calcAccessLevel(const(AccessToken) atSids,
																													const(AccessToken) atSds) const
	{
		AccessLevel res = AccessLevel.None;

		foreach(ssd; atSds)
			foreach(ace; bySD(ssd))
				if(atSids.has(ace.sid))
					if(res == AccessLevel.None)
						res = ace.access;
					else
						res = res.sum(ace.access);
		return res;
	}

	//------
	void rebuild()
	{
		while(_sdIx.length)
			_sdIx.remove(_sdIx.byKey().front);
		while(_sidIx.length)
			_sidIx.remove(_sidIx.byKey().front);

		if(_list.length == 0)
			_list.length = 1;
		for(size_t i = _list.length-1; i > 0; i--)
			if(_list[i].isEmpty)
			{
				_list[i] = _list[$-1];
				_list.length--;
			}

		foreach(i, ref ace; _list[1..$])
		{
			ace.psd = 0;
			ace.psid = 0;

			if(ace.isEmpty)
				continue;

			size_t pos = _sdIx.get(ace._sd, 0);
			ace.psd = pos;
			_sdIx[ace._sd] = i+1;

			pos = _sidIx.get(ace._sid, 0);
			ace.psid = pos;
			_sidIx[ace._sid] = i+1;
		}
	}
	void onDeserialized()
	{
		if(_sidGroups is null)
			_sidGroups = new IndexList!(SidGroup,ushort,"sid")();
		if(_sdGroups is null)
			_sdGroups = new IndexList!(SdGroup,ushort,"sd")();

		rebuild();
	}
}










