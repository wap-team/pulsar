module pulsar.server.connectors.odbc;

import pulsar.atoms;
import pulsar.server.connectors;
import pulsar.server.connectors.sql;

version(odbc):

/// Базовый класс коннектора к базе данных ODBC.
class OdbcConnector : SqlConnector
{
	import pulsar.db.sql;
	import pulsar.db.odbc;

	override IDbConnection connectionFactory()
	{
		auto con = new OdbcConnection();
		con.OnServerMessage += &logMessage;
		return con;
	}

protected:
	void logMessage(string message) { Logger.log(0,"[ODBC]" ~ message);	}
}
