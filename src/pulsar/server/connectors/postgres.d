module pulsar.server.connectors.postgres;

import pulsar.atoms;
public import pulsar.db.sql;
public import pulsar.db.postgres;
public import pulsar.server.connectors;
public import pulsar.server.connectors.sql;

version(postgres):

/// Базовый класс коннектора к базе данных ODBC.
class PostgresConnector : SqlConnector
{
	import pulsar.db.postgres;
private:
	bool _async;

public:
	@property // async
	{
		bool async() const { return _async; }
		void async(bool value) { _async = value; }
	}

public:
	override IDbConnection connectionFactory()
	{
		auto con = new PqConnection();
		con.OnServerMessage += &logMessage;
		if(async)
			con.onIdle = ()
			{
				//traceChars(".");
				import pulsar.server;
				PulsarServer.sleep();
			};
		return con;
	}
	/// Возвращает краткую сводку параметров сервера.
	override @noproxy string[string] serverInfo()
	{
		auto con = newConnection();
		scope(exit) destroy(con);

		string[string] pars =
		[
			"server_version" : null,		    "server_encoding" : null,
			"client_encoding" : null,     "application_name" : null,
			"is_superuser" : null,        "session_authorization" : null,
			"DateStyle" : null,           "IntervalStyle" : null,
			"TimeZone" : null,         			"integer_datetimes"  : null,
			"standard_conforming_strings" : null
		];
		foreach(p; pars.byKey)
			pars[p] = con.getServerParam(p);

		return pars;
	}
	// IConfInit.confInit
	override void confInit(string[string] conf)
	{
		super.confInit(conf);
		string s = conf.get("async","false");
		async = to!bool(s);
		Logger.log(3, "async: %s", s);
	}
	/// Создает новое соединение с SQL сервером назначения.
	override PqConnection newConnection(bool open = true)
	{
		return enforce(cast(PqConnection)super.newConnection);
	}

public:
	/// Тест отсутсвия блокирования при работе с базой
	@noproxy void testAsync()
	{
		auto con = newConnection();
		scope(exit) destroy(con);
		auto r = con.execReader("SELECT *, pg_sleep(5) from pg_roles;");

		do
		{
			trace("\nName  \tDbType\tColumnSize\tDecimalDigits\tIsNullable\tIsAutoincrement");
			foreach(c;r.fields)
				trace(c.name, "\t", c.dbTypeName, "\t", c.columnSize, "\t", c.decimalDigits, "\t\t", c.isNullable, "\t", c.isAutoincrement);

			while(r.next())
			{
				string res;
				traceChars(".");
				for(int f = 0; f < r.fields.length; f++)
				{
					traceChars("[");
					if(r.isNull(f))
						traceChars("NULL]");
					else
					{
						auto id = r.getValue!string(f);
						traceChars(id, "]");
					}
				}
				trace();
			}
		} while(r.nextResultSet());
	}

protected:
	void logMessage(string message) { Logger.log(0,"[PostgreSQL]" ~ message);	}
}
