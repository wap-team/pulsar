module pulsar.server.connectors.http;

import pulsar.atoms;
import pulsar.text.config;
import pulsar.text.xml.iterator;
import pulsar.server.connectors;
public import pulsar.web.http;

/// Базовый класс коннектора к HTTP сервисам
class HttpConnector : IDataConnector, IConfInit
{
protected:
	string _baseUrl;

public:
	@property // baseUrl
	{
		string baseUrl() const { return _baseUrl; }
		void baseUrl(string value) { _baseUrl = value; }
	}

public:
	// IConfInit.confInit
	@noproxy override void confInit(string[string] conf)
	{
		string s = conf.get("baseUrl", null);
		if(s.length == 0)
			throw new Exception("Не указан baseUrl!");
		baseUrl = s;
		Logger.log(4, "baseUrl: %s", s);
		}
	@noproxy override string[string] serverInfo() { assert(false, "ToDo"); }

public:

	/** Основной метод выполнения HTTP запроса.
			Сам метод является синхронным, хотя запрос отправляется асинхронно.
			Если в зарпросе указан callback, выполняет его по завершении запроса.
		**/
	HttpResponse request(HttpRequest request, bool noThrow = false)
	{
		import vibe.vibe;
		import std.uri : encode;

		HttpResponse response;

		URL url = URL(request.url);
		//trace(request.method, " ", url);
		requestHTTP(url,
				(scope req)
				{
					req.method = to!(vibe.vibe.HTTPMethod)(to!string(request.method));
					// could add headers here before sending,
					auto contentType = req.headers.get("Content-Type", request.contentType.mimeString());
					foreach(k,v; request.headers)
						if(k.startsWith("_") == false)
							req.headers[k] = v;
					// write a POST body, or do similar things.
					if(request.content.length)
						req.writeBody(cast(ubyte[])request.content, contentType);
				},
				(scope res)
				{
					response.status = to!HttpStatus(res.statusCode);
					response.content = res.bodyReader.readAllUTF8();
				}
			);

		if(response.status >= 300 && noThrow == false)
			throw new HttpStatusException
			(
				response.status,
				format("Запрос [%s] вернул код %s (%s)!", url, to!ushort(response.status), response.status)
			)
				.setResponseBody(response.content);
//		if(request.responseCallback)
//			request.responseCallback(request, response);
		return response;
	}

	/// Строит основу запроса с XML телом
	HttpRequest buildXmlRequest(HttpMethod method, string path, string xml)
	{
		if(xml.startsWith("<?xml") == false)
			xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" ~ xml;
		HttpRequest req = HttpRequest(method, baseUrl);
		import std.uri;
		req.path = encode(path);
		req.contentType = ContentType.TextXml;
		req.content = xml;
		return req;
	}

	/** Строит запрос с XML телом и отправляет его.
			Возвращает строку с телом ответа.
	**/
	string sendXml(HttpMethod method, string path, string xml)
	{
		HttpRequest req = buildXmlRequest(method, path, xml);
		return request(req).content;
	}

	string get(string path, string[string] query = null)
	{
		HttpRequest req = HttpRequest(HttpMethod.GET, baseUrl);
		req.path = path;
		foreach(k,v; query)
			req.addQuery(k,v); //encode(k));
		return request(req).content;
	}
	XmlIterator!char getXml(string path, string[string] query = null)
	{
		return XmlIterator!char(get(path, query));
	}

	string post(string path, string[string] postArgs)
	{
		HttpRequest req = HttpRequest(HttpMethod.POST, baseUrl);
		req.path = path;
		req.contentType = ContentType.WwwForm;
		req.content = urlencoded(postArgs);
		return request(req).content;
	}
	string postXml(string path, string xml)
	{
		return sendXml(HttpMethod.POST, path, xml);
	}

	string patchXml(string path, string xml)
	{
		return sendXml(HttpMethod.PATCH, path, xml);
	}

	string putXml(string path, string xml)
	{
		return sendXml(HttpMethod.PUT, path, xml);

	}

protected:
	string urlencoded(string[string] args) const
	{
		import std.array : appender;
		import std.uri : encode;
		auto app = appender!string();
		foreach(k,v; args)
		{
			if(app.data.length)
				app.put("&");
			app.put(k); //encode(k));
			app.put("=");
			app.put(v); //encode(v));
		}
		return app.data;
	}
}
