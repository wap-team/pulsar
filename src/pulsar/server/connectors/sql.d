module pulsar.server.connectors.sql;

import pulsar.atoms;
import pulsar.text.config;

public import pulsar.server.connectors;

/// Базовый класс коннектора к базе данных SQL.
class SqlConnector : IDataConnector, IConfInit
{
	import core.sync.rwmutex;
	import pulsar.db.sql;

protected:
	@noproxy ReadWriteMutex mutex;
private:
	string _сonStr = null;
	string[string] _queries;

public:
	@noproxy abstract IDbConnection connectionFactory();
	@noproxy abstract string[string] serverInfo();

	/// Строка соединения с SQL сервером.
	@property // сonnectionString
	{
		string сonnectionString() const
		{
			synchronized(unqual(mutex).reader)
				return _сonStr;
		}
		void сonnectionString(string value)
		{
			synchronized(mutex.writer)
				_сonStr = value;
		}
	}

public:
	this()
	{
		mutex = new ReadWriteMutex();
		initQueries();
	}
	// IConfInit.confInit
	void confInit(string[string] conf)
	{
		string s = conf.get("connectionString", conf.get("ConStr", null));
		if(s)
			сonnectionString = s;
	}

public:
	/// Перезаписывает словарь запросов значениями запросов по умолчанию.
	void initQueries() { }

	/** Возвращает запрос, заполненный данными.
		* Params:
		* name = Имя запроса
		* args = Аргументы запроса
		*/
	string getQuery(Args...)(string name, Args args)
	{
		CheckNullOrEmpty!name;
		string s;
		synchronized(mutex.reader)
			s = _queries.get(name,null);
		if(s)
			return format(s, args);
		throw new Exception(format("Запрос [%s] не найден!", name));
	}
	string getQueryF(Args...)(string name, Args args)
	{
		CheckNullOrEmpty!name;
		string s;
		synchronized(mutex.reader)
			s = _queries.get(name,null);
		if(s)
		{
			import std.traits;
			foreach(i,X; Args)
			{
				//trace(X.stringof, " = ", args[i]);
				static if(isSomeString!X)
					if(args[i].length == 0)
						args[i] = "NULL";
					else if(args[i][0] == '@')
						args[i] = args[i][1..$];
					else if(args[i] != "NULL")
						args[i] = "'" ~ args[i].replace("'","''")		~ "'";

						//			foreach(ref x; args)
//				if(x != "NULL" && typeid(x) == typeid(string))
//					x = "'" ~ x		~ "'";
			}
			return format(s, args);
		}
		throw new Exception(format("Запрос [%s] не найден!", name));
	}

	void setQuery(string name, string query)
	{
		CheckNullOrEmpty!(name, query);
		synchronized(mutex.writer)
			_queries[name] = query;
	}

	/// Создает новое соединение с SQL сервером назначения.
	IDbConnection newConnection(bool open = true)
	{
		string cs = сonnectionString;
		if(cs.empty)
			throw new Exception("Не задана строка подключения к серверу назначения!");
		auto vcon = connectionFactory();
		enforce(vcon);
		if(open)
			vcon.open(cs);
		return vcon;
	}

	void onDeserialized()
	{
		mutex = new ReadWriteMutex();
	}
}

/*/// Базовый класс коннектора к основной и вспомогательной базам данных SQL .
class SqlDualConnector : SqlConnector
{
	import core.sync.rwmutex;
	import pulsar.db.sql;

protected:
	@noproxy ReadWriteMutex mutex;
private:
	string _victimConStr = null;
	string _storageConStr = null;
	string[string] _queries;

public:
	@noproxy abstract IDbConnection connectionFactory();
	//override IDbConnection connectionFactory() { throw new Exception("Not implemented!"); }

	/// Строка соединения с SQL сервером вспомогательных таблиц коннектора.
	@property // storageConnectionString
	{
		string storageConnectionString() const
		{
			synchronized(unqual(mutex).reader)
				return _storageConStr;
		}
		void storageConnectionString(string value)
		{
			synchronized(mutex.writer)
				_storageConStr = value;
		}
	}
	/// Строка соединения с SQL сервером назначения.
	@property // victimConnectionString
	{
		string victimConnectionString() const
		{
			synchronized(unqual(mutex).reader)
				return _victimConStr;
		}
		void victimConnectionString(string value)
		{
			synchronized(mutex.writer)
			_victimConStr = value;
		}
	}
	/// Словарь запросов коннектора.
	//@property string[string] queries() { return _queries; }

public:
	this()
	{
		mutex = new ReadWriteMutex();
		initQueries();
	}

public:
	/// Перезаписывает словарь запросов значениями запросов по умолчанию.
	void initQueries() { }

	string getQuery(Args...)(string name, Args args)
	{
		CheckNullOrEmpty!name;
		string s;
		synchronized(mutex.reader)
			s = _queries.get(name,null);
		if(s)
			return format(s, args);
		throw new Exception(format("Запрос [%s] не найден!", name));
	}
	void setQuery(string name, string query)
	{
		CheckNullOrEmpty!(name, query);
		synchronized(mutex.writer)
			_queries[name] = query;
	}

	/// Создает новое соединение с SQL сервером назначения.
	IDbConnection newVictimConnection(bool open = true)
	{
		string cs = victimConnectionString;
		if(cs.empty)
			throw new Exception("Не задана строка подключения к серверу назначения!");
		auto vcon = connectionFactory();
		enforce(vcon);
		if(open)
			vcon.open(cs);
		return vcon;
	}
	/// Создает новое соединение с SQL сервером вспомогательных таблиц коннектора.
	IDbConnection newStorageConnection(bool open = true)
	{
		string cs = storageConnectionString;
		if(cs.empty)
			throw new Exception("Не задана строка подключения к серверу вспомогательных таблиц!");
		auto vcon = connectionFactory();
		enforce(vcon);
		if(open)
			vcon.open(cs);
		return vcon;
	}

	void onDeserialized()
	{
		mutex = new ReadWriteMutex();
	}
}
*/
