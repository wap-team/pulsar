module pulsar.server.connectors;

import pulsar.text.config;

/// Базовый интерфейс объектов коннекторов
interface IDataConnector : IConfInit
{
	//IDbConnection connectionFactory();

	/// Возвращает краткую сводку параметров сервера.
	string[string] serverInfo();
}
