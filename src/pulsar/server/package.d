module pulsar.server;

import std.uuid;
import std.file;
import std.exception : enforce;
import core.thread;

import pulsar.atoms;
import pulsar.text.config;
import pulsar.text.xml;
import pulsar.gol;
import pulsar.mms;
import pulsar.reflection;
import pulsar.server.log;
public import pulsar.server.scheduler;
public import pulsar.server.storages;

alias PostRunFunc = void delegate();

static class PulsarServer
{
	alias FiberYield = void function();
	alias FiberSleep = void function(Duration);

static __gshared:
private:
	Scheduler _scheduler;
	PostRunFunc[] _postRun;

	FiberYield _yield;
	FiberSleep _sleep;

	struct FiberLock
	{
		Fiber fiber;
		SysTime lockTime;
	}
	FiberLock[void*] _locks;

	DataStorageManager _storageManager;

	bool _isStarted;

public:
	/// Менеджер хранилищ
	@property DataStorageManager storageManager() { return _storageManager; }
	/// Планировщик задач
	@property Scheduler scheduler() { return _scheduler; }

	@property bool isStarted() { return _isStarted; }

public:
	shared static this()
	{
		_storageManager = new DataStorageManager();
		_scheduler = new Scheduler();

		// GOL clean
		/*
		import std.functional : toDelegate;
		import std.datetime;
		SchedulerTask t = SchedulerTask();
		t.period = 10.seconds;
		t.description = "Очистка GOL";
		t.logErrorOnly = true;
		t.priority = SchedulerTaskPriority.Low;
		t.work = toDelegate(&GOL.clean);
		_scheduler.add(t);
		*/
	}

	bool init(string configFileName)
	{
		enforce(configFileName.length, "Не указано имя файла конфигурации!");

		enum string err = "Ошибка файла конфигурации: ";
		alias String = const(char[]);
		StopWatch sw = StopWatch(AutoStart.yes);

		Logger.log(0,"Инициализация сервера Пульсара...");

		Logger.logChars(1,"Загрузка файла конфигурации " ~ configFileName ~ " ... ");
		if(!(exists(configFileName) && isFile(configFileName)))
		{
			Logger.log();
			Logger.log(2,"Файл конфигурации " ~ configFileName ~ " не найден!");
			Logger.log(0,"Инициализация сервера прервана.");
			return false;
		}
		auto doc = new Document!char;
		doc.parse(readText(configFileName));
		Logger.log(0, "OK");

		Logger.log(1,"Загрузка настроек сервера ... ");
		foreach (node; doc.query["PulsarServerConfig"]["Options"].child())
		{
			switch(node.name)
			{
				case "GOL_recovery" :
				{
					GOL.recoveryMode = to!bool(node.value);
					if(GOL.recoveryMode)
						Logger.log(2, "!!!! ВКЛЮЧЕН РЕЖИМ ВОССТАНОВЛЕНИЯ GOL !!!!");
				} break;
				default: Logger.log(2, "Опция [%s] не обрабатывается!!!", node.name); break;
			}
		}

		Logger.log(1,"Инициализация хранилищ ... ");
		foreach (node; doc.query["PulsarServerConfig"]["Storages"].child("Storage"))
		{
			auto n = node.attributes.name("","type");
			if(!n)
				throw new Exception(err ~ "Для <Storage> не указан атрибут type!");
			string t = cast(string)(n.value);
			string name = (n = node.attributes.name("","name")) is null ? null :
																																																																	cast(string)(n.value);
			if(name)
				Logger.log(2,"%s (%s) ... ", name, t);
			else
				Logger.log(2,t ~ " ... ");

			ClassInfo tt = cast(ClassInfo)ClassInfo.find(t);
			if(tt is null)
				throw new Exception("Не удалось найти тип " ~ 	t ~ "!");

			IDataStorage st;
			if(name)
			{
				IMetaTypeAggregate imt = cast(IMetaTypeAggregate)MetaTypeLibrary[tt];
				if(imt && imt.hasServant())
				{
					MetaPtr mp = GOL.createAndAdd(imt, randomUUID(), name, true);
					st = cast(IDataStorage)(cast(Object)mp.ptr);
				}
			}
			if(st is null)
				st = cast(IDataStorage)(tt.create());

			auto confInit = cast(IConfInit)st; //(cast(Object)st);
			if(confInit)
			{
				string[string] iniData;
				foreach(ch; node.children)
					if(ch.name.length > 0)
						iniData[cast(string)(ch.name)] = cast(string)(ch.value);
				confInit.confInit(iniData);
			}

			Logger.log(3, "(i)version: %s", st.storageVersion(true));
			_storageManager.add(st, name);
			Logger.log(2, "%s ok (%s)", name, st.storageVersion(/*true*/));
		}

		Logger.log(1,"Загрузка стартовых объектов ... ");
		foreach (node; doc.query["PulsarServerConfig"].child("Autostart"))
		{
			sw.reset();
			auto nName = node.attributes.name("","name");
			auto nOid = node.attributes.name("","oid");
			auto nType = node.attributes.name("","type");
			auto noSave = node.attributes.name("","noSave");
			string name, oid;

			const(ClassInfo) ci = ClassInfo.find(nType.value);

			Object obj;
			if(nName)
			{
				name = cast(string)(nName.value);
				Logger.logChars(2,name ~ " ... ");
				obj = GOL.get(name, ci, false);
			}
			else if(nOid)
			{
				assert(false, "ToDo");
//				oid = cast(string)(nOid.value);
//				Logger.logChars(2,oid ~ " ... ");
//				obj = GOL.get(OID(oid));
			}
			else
				throw new Exception(err ~ "Для <Autostart> не указано имя или OID объекта!");

			if(obj is null)
			{
				if(!nType)
					throw new Exception(err ~ "Для <Autostart> не указан атрибут type!");
				string t = cast(string)(nType.value);

				IMetaType mt = MetaTypeLibrary[t];
				if(mt is null)
					throw new Exception("Метатип для типа [" ~ t ~ "] не зарегистрирован!");

				MetaPtr mp;
				if(nOid)
					mp = GOL.createAndAdd(cast(IMetaTypeAggregate)mt, OID(oid), name, noSave !is null);
				else
					mp = GOL.createAndAdd(cast(IMetaTypeAggregate)mt, randomUUID(), name, noSave !is null);
				obj = cast(Object)mp.ptr;

				Logger.log("создан");
			}
			else
				Logger.log("загружен (%s)", sw.peek().total!"msecs");

			IConfInit ici = cast(IConfInit)obj;
			if(ici)
			{
				string[string] args;
				foreach(n; node.children)
					args[n.name] = to!string(n.value);
				if(args.length)
					ici.confInit(args);
			}
		}

		sw.reset();
		Logger.logChars(2,"MMS ... ");
		MMS.load();
		Logger.log("загружен (%s)", sw.peek().total!"msecs");

		return true;
	}
	bool config()
	{
		Logger.log(1,"Конфигурирование объектов ... ");
		return true;
	}
	void run()
	{
		Logger.log(0,"Запуск сервера Пульсара ... ");
		MMS.run();
		Logger.log(1,"Запуск шины сообщений ... ");
		SignalBus.enabled = true;
		Logger.log(1,"Выполнение postRun функций ... ");
		foreach(f; _postRun)
			f();

		_isStarted = true;
//		trace("getRuleBench - count: ",  _storageManager.rules.length,
//								" msecs: ", _storageManager.getRuleBench.peek().msecs);
	}
	void shutdown()
	{
//		trace("getRuleBench - count: ",  _storageManager.rules.length,
//								" msecs: ", _storageManager.getRuleBench.peek().msecs);
		Logger.log(0,"Завершение работы сервера ...");
		Logger.log(1,"Остановка хранилищ ...");
		foreach(s; _storageManager)
		{
			Logger.log(2,"%s ...", s.typeName);
			s.close();
		}
		Logger.log(0,"Сервер Пульсара остановлен (%s).", Clock.currTime.toString());

		_isStarted = false;
	}
	/// Аварийная остановка
	void abort()
	{
		//import core.stdc.signal;
		//raise(SIGABRT);

		Logger.log(0, "Аварийное завершение программы ...");
		import core.stdc.stdlib;
		core.stdc.stdlib.abort();
	}



public:
	void addPostRun(PostRunFunc func)
	{
		assert(func);
		_postRun ~= func;
	}

public:
	static Duration defaultSleepPeriod = 20.msecs;
	static Duration maxLockPeriod = 2.minutes;

	@property bool isSetYield() { return _yield !is null; }
	@property void yield(FiberYield val) { _yield = val; }
	@property void yield() {  if(_yield) _yield(); }

	@property bool isSetSleep() { return _sleep !is null; }
	@property void sleep(FiberSleep val) { _sleep = val; }
	@property void sleep(Duration val = defaultSleepPeriod)
	{
		if(_sleep)
			_sleep(val);
	}

	void lock(const(Object) obj) { lock(obj.unqual); }
	void lock(Object obj)
	{
		//CheckNull!obj;
		if(obj is null)
			return;
		if(thread_isMainThread == false)
			throw new Exception("Блокировка данных возможна только из основного потока!");
		void* ptr = cast(void*)obj;
		Fiber f = Fiber.getThis();
		while(ptr in _locks)
		{
			if(Clock.currTime - _locks[ptr].lockTime > maxLockPeriod)
				throw new Exception("Объект [" ~ obj.toString() ~ "] блокирован больше положенного времени!");
			if(_locks[ptr].fiber is f)
				break;
			sleep(defaultSleepPeriod);
		}
		_locks[ptr] = FiberLock(f, Clock.currTime);
		//trace("lock ", obj, " (", _locks.length, ")");
	}
	void unlock(const(Object) obj) { unlock(obj.unqual); }
	void unlock(Object obj)
	{
		//CheckNull!obj;
		if(obj is null)
			return;
		if(thread_isMainThread == false)
			throw new Exception("Блокировка данных возможна только из основного потока!");

		void* ptr = cast(void*)obj;
		Fiber f = Fiber.getThis();
		if(_locks.get(ptr, FiberLock.init).fiber is f)
			_locks.remove(cast(void*)obj);
		//trace("unlock ", obj, " (", _locks.length, ")");
	}

	/// Запускает делегат в рабочем потоке и асинхронно ожидает его завершения.
	Ret await(Ret, Args...)(Ret delegate(Args) work, Args args)
	{
		//pragma(msg, Ret, " await", Args);

		import core.thread;
		assert(work);

		alias Work = Ret delegate(Args);

		static class AwaitThread : Thread
		{
		public:
			Work work;
			Throwable err;
			static if(!is(Ret == void))
				Ret ret;
			Args args;

			this(Work work, Args args)
			{
				static if(!is(Ret == void))
					this.ret = Ret.init;
				this.work = work;
				this.args = args;
				super(&run);
			}

		private:
			void run()
			{
				try
				{
					static if(is(Ret == void))
						work(args);
					else
						ret = work(args);
				}
				catch(Throwable e)
				{
					err = e;
				}
			}
		}

		auto t = new AwaitThread(work, args);
		t.start();
		while(t.isRunning)
			if(isSetSleep)
				PulsarServer.sleep(defaultSleepPeriod);
			else
				Thread.getThis().sleep(defaultSleepPeriod);
		if(t.err !is null)
			throw t.err;
		static if(is(Ret == void))
			return;
		else
			return t.ret;
	}
}
