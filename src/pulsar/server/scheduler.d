module pulsar.server.scheduler;

import std.algorithm: sort;
import std.datetime;

import pulsar.atoms;


/// Класс планировщика задач
class Scheduler
{
private:
	SchedulerTask[] _tasks;
	Duration _rerunIfError = dur!"seconds"(60);

public:
	/// Метод вызова для внешнего таймера.
	/// priority - приоритет задач, ниже которого задачи вызываться не будут.(Пока не работает)
	/// Возвращает время, через которое следует вызвать метод опять.
	Duration tick(SchedulerTaskPriority priority)
	{
		Duration res;
		void setRes(Duration d)
		{
			if(res == Duration.init || d < res)
				res = d;
		}

		foreach(ref t; _tasks)
			/*if(priority.isSet(t.priority) == false)
				continue;
			else */ if(t.lastRun + t.period <= cast(DateTime)Clock.currTime)
			{
				if(t.logErrorOnly == false)
					Logger.log(3, "--> Run task [%s] ...",t.description);
				try
				{
					t.work();
					t.lastRun = cast(DateTime)Clock.currTime;
					if(t.logErrorOnly == false)
						Logger.log(3, "--- Task [%s] done ", t.description);
				}
				catch(Throwable err)
				{
					if(t.logErrorOnly)
						Logger.log(3, "!!! Task error [%s]:",t.description);
					Logger.log(err.toString());
					auto now = cast(DateTime)Clock.currTime;
					t.lastRun = (now - t.period)  + _rerunIfError;
				}
				setRes(dur!"seconds"(1));
				break;
			}
			else
				setRes((t.lastRun + t.period) - cast(DateTime)Clock.currTime);

		if(res == Duration.init)
			return 10.seconds;
		return res < dur!"seconds"(1) ? dur!"seconds"(1) : res;
	}

	@property size_t count() const { return _tasks.length; }
	void add(SchedulerTask task)
	{
		_tasks ~= task;

		bool comp(SchedulerTask x, SchedulerTask y)
		{
			if(x.priority == y.priority)
				return x.period < y.period;
			return x.priority > y.priority;
		}
		sort!(comp)(_tasks);

		Logger.log(2, "добавлена задача планировщика \"%s\"",task.description);
	}
}

/// Перечисление приоритетов задач
enum SchedulerTaskPriority : ubyte
{
	Low = 1,
	Normal = 2,
	High = 4,

	NormalAndHigh = Normal | High,
	All = 255
}

/// Задача планировщика
struct SchedulerTask
{
	string description;
	Duration period;
	DateTime lastRun;
	SchedulerTaskPriority priority = SchedulerTaskPriority.Normal;
	bool logErrorOnly;
	void delegate() work;

	this(void delegate() work, Duration period, string description = null)
	{
		this.work = work;
		this.period = period;
		this.description = description;
	}
}
