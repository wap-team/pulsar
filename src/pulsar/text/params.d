module pulsar.text.params;

import std.conv : to;

class StringParams
{
private:
	string[string] _dic;

public:
	this() {}

public:
	string opIndex(string key) { return _dic[key]; }
	void opIndexAssign(string value, string key) { _dic[key] = value; }
	string get(string key, lazy string def = null) const { return _dic.get(key,def); }
	bool has(string key) const { return (key in _dic) !is null; }

	void clear()
	{
		foreach(k; _dic.keys)
			_dic.remove(k);
	}

	override string toString() { return to!string(_dic); }
	string toString() const { return to!string(_dic);  }
}
