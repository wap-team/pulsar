module pulsar.text.search;

import std.string;
import std.algorithm.searching;

alias SearchMaskFunc = bool function(string, string);

/** Структура поиска маски в строке.
	* Поддерживает символы подстановки:
	*  * - любое количество символов
	*  = точное соответствие
**/
struct SearchMask
{
private:
	string _maskOrig;
	string _mask;
	SearchMaskFunc _func;
	SearchMaskFunc _defFunc;

public:
	/// Маска поиска
	@property // mask
	{
		string mask() const { return _maskOrig; }
		void mask(string value)
		{
			_func = null;
			_mask = null;
			_maskOrig = value;
			if(_maskOrig.length == 0)
				return;
			_mask = value.toLower;

			bool starFirst, starEnd, sharp;
			if((starFirst = _mask[0] == '*') == true)
			{
				if(_mask.length == 1)
				{
					_func = &alwaysTrue;
					return;
				}
				_mask = _mask[1..$];
			}
			else if((sharp = _mask[0] == '=') == true)
			{
				_mask = _mask[1..$];
				if(_mask.length == 0)
					return;
			}
			if((starEnd = _mask[$-1] == '*') == true)
				_mask = _mask[0..$-1];

			if(sharp)
				_func = &equals;
			else if(starFirst && starEnd == false)
				_func = &endsWith;
			else if(starFirst == false && starEnd)
				_func = &startsWith;
			else if(starFirst && starEnd)
				_func = &canFind;
			else
				_func = _defFunc ? _defFunc : &canFind;
		}
	}
	/// Проверяет строку на соотвествие маске. true если маска не задана
	bool check(string str) const { return _func ? _func(str, _mask) : true; }
	bool opCall(string str) const { return _func ? _func(str, _mask) : true; }
	/// Возвращает текущий делегат проверки
	@property SearchMaskFunc func() const { return _func; }
	/// Определяет делегат проверки, который будет использоваться для маски без звездочек
	@property // defFunc
	{
		SearchMaskFunc defFunc() const { return _defFunc; }
		void defFunc(string predName) { _defFunc = byPred(predName); }
	}

	@property bool isSet() const { return _func !is null; }

public:
	this(string mask, string predName = null)
	{
		this.mask = mask;

		if(predName.length)
			_func = byPred(predName);
	}

private:
	SearchMaskFunc byPred(string predName)
	{
		if(predName.length == 0 || predName == "canFind")
			return &canFind;
		else if(predName == "startsWith")
			return &startsWith;
		else if(predName == "endsWith")
			return &endsWith;
		else if(predName == "equals")
			return &equals;
		else
			throw new Exception("Предикат " ~ predName ~ " не реализован!");
	}

public: // Предикаты проверок
	/// Начинается с
	static bool startsWith(string str, string mask) { return str.toLower().startsWith(mask); }
	/// Содержит
	static bool canFind(string str, string mask) { return str.toLower().canFind(mask); }
	/// Заканчивается на
	static bool endsWith(string str, string mask) { return str.toLower().endsWith(mask); }
	/// Точное соответствие
	static bool equals(string str, string mask) { return str == mask; }
	/// Всегда true
	static bool alwaysTrue(string str, string mask) { return true; }
}
