module pulsar.text;

import std.string;
import std.algorithm.searching;

/**
	* Base class for text exceptions.
	*/
class TextException : Exception
{
	this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		super(msg, file, line, next);
	}
}

//----




enum dstring en_letters = "abcdefghijklmnopqrstuvwxyz";
enum dstring en_Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
enum dstring ru_letters = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
enum dstring ru_Letters = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
enum dstring digits = "0123456789";
enum dstring symbols = "~`!@\"#№$:;%^&?*()_-=+<>/\\";

enum dstring letters = en_letters ~ en_Letters ~ ru_letters ~ ru_Letters;

bool included(dchar ch, dstring[] sets ...)
{
	foreach(s; sets)
		foreach(sc; s)
			if(sc == ch)
				return true;
	return false;
}
