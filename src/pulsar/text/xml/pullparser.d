module pulsar.text.xml.pullparser;

import std.conv;

import pulsar.text;

// Base class for XML exceptions.
class XmlException : TextException
{
	this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null )
	{
		super(msg, file, line, next);
	}
}

/// Values returned by the pull-parser
enum XmlTokenType : ubyte
{
	Done,
	StartElement = 1,
	Attribute = 2,
	EndElement = 3,
	EndEmptyElement = 4,
	Data = 5,
	Comment = 6,
	CData = 7,
	Doctype = 8,
	PI = 9,
	None
};

/*******************************************************************************
	Token based xml Parser. Templated to operate with char[], wchar[]	and dchar[] content.
	*******************************************************************************/
struct PullParser(Ch = char)
{
private:
	const(char)[] errMsg;
package:
	struct XmlText(Ch)
	{
		package const(Ch)*     end;
		package size_t  len;
		package const(Ch)[]    text;
		package const(Ch)*     point;

		final void reset(const(Ch[]) newText)
		{
			this.text = newText;
			this.len = newText.length;
			this.point = text.ptr;
			this.end = point + len;
		}

		__gshared immutable ubyte[64] name =
				[
				// 0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
				0,  1,  1,  1,  1,  1,  1,  1,  1,  0,  0,  1,  1,  0,  1,  1,  // 0
				1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  // 1
				0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  // 2
				1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  1,  1,  1,  0,  0   // 3
				];

		__gshared immutable ubyte[64] attributeName =
				[
				// 0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F
				0,  1,  1,  1,  1,  1,  1,  1,  1,  0,  0,  1,  1,  0,  1,  1,  // 0
				1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  // 1
				0,  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  // 2
				1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,  1,  0,  0,  0,  0   // 3
				];
	}
	XmlText!(Ch) text;

public:
	int         depth;
	const(Ch)[] prefix;
	const(Ch)[] rawValue;
	const(Ch)[] localName;
	XmlTokenType type = XmlTokenType.None;

public:
	///	Return the raw value of the current token
	@property const(Ch[]) value() const
	{
		return rawValue;
	}
	///	Return the name of the current token
	@property const(Ch[]) name() const
	{
		if (prefix.length)
			return prefix ~ ":" ~ localName;
		return localName;
	}
	///	Returns the text of the last error
	@property const(char[]) error() const
	{
		return errMsg;
	}

public:
	/// Construct a parser on the given content (may be null)
	this(const(Ch[]) content)
	{
		reset (content);
	}
	
public:
	/// Consume the next token and return its type
	XmlTokenType next()
	{
		auto e = text.end;
		auto p = text.point;

		// at end of document?
		if (p >= e)
			return endOfInput();

		// strip leading whitespace
		while (*p <= 32)
			if (++p >= e)
				return endOfInput();

		// StartElement or Attribute?
		if (type < XmlTokenType.EndElement)
		{
//			version (retainwhite)
//			{
//				// strip leading whitespace (thanks to DRK)
//				while (*p <= 32)
//					if (++p >= e)
//						return endOfInput();
//			}
			switch (*p)
			{
			case '>':
				// termination of StartElement
				++depth;
				++p;
				break;

			case '/':
				// empty element closure
				text.point = p;
				return doEndEmptyElement();

			default:
				// must be attribute instead
				text.point = p;
				return doAttributeName();
			}
		}

		// consume data between elements?
		if (*p != '<')
		{
			auto q = p;
			while (++p < e && *p != '<') {}

			if (p < e)
			{
				// include leading whitespace
				while (*(q-1) <= 32)
					--q;
				text.point = p;
				rawValue = q [0 .. p - q];
				return type = XmlTokenType.Data;
			}
			return endOfInput();
		}

		// must be a '<' character, so peek ahead
		switch (p[1])
		{
		case '!':
			// one of the following ...
			if (p[2..4] == "--")
			{
				text.point = p + 4;
				return doComment();
			}
			else
				if (p[2..9] == "[CDATA[")
				{
					text.point = p + 9;
					return doCData();
				}
				else
					if (p[2..9] == "DOCTYPE")
					{
						text.point = p + 9;
						return doDoctype();
					}
			return doUnexpected("!", p);

		case '\?':
			// must be PI data
			text.point = p + 2;
			return doPI();

		case '/':
			// should be a closing element name
			p += 2;
			auto q = p;
			while (*q > 63 || text.name[*q])
				++q;

			if (*q is ':')
			{
				prefix = p[0 .. q - p];
				p = ++q;
				while (*q > 63 || text.attributeName[*q])
					++q;

				localName = p[0 .. q - p];
			}
			else
			{
				prefix = null;
				localName = p[0 .. q - p];
			}

			while (*q <= 32)
				if (++q >= e)
					return endOfInput();

			if (*q is '>')
			{
				--depth;
				text.point = q + 1;
				return type = XmlTokenType.EndElement;
			}
			return doExpected(">", q);

		default:
			// scan new element name
			auto q = ++p;
			while (*q > 63 || text.name[*q])
				++q;

			// check if we ran past the end
			if (q >= e)
				return endOfInput();

			if (*q != ':')
			{
				prefix = null;
				localName = p [0 .. q - p];
			}
			else
			{
				prefix = p[0 .. q - p];
				p = ++q;
				while (*q > 63 || text.attributeName[*q])
					++q;
				localName = p[0 .. q - p];
			}

			text.point = q;
			return type = XmlTokenType.StartElement;
		}
	}
	///	Reset the parser
	bool reset()
	{
		text.reset (text.text);
		reset_();
		return true;
	}
	///	Reset parser with new content
	void reset(const(Ch[]) newText)
	{
		text.reset(newText);
		reset_();
	}

protected:
	XmlTokenType setError(const(char[]) msg)
	{
		errMsg = msg;
		throw new XmlException (msg.idup);
	}

private:
	XmlTokenType doAttributeName()
	{
		auto p = text.point;
		auto q = p;
		auto e = text.end;

		while (*q > 63 || text.attributeName[*q])
			++q;
		if (q >= e)
			return endOfInput();

		if (*q is ':')
		{
			prefix = p[0 .. q - p];
			p = ++q;

			while (*q > 63 || text.attributeName[*q])
				++q;

			localName = p[0 .. q - p];
		}
		else
		{
			prefix = null;
			localName = p[0 .. q - p];
		}
		
		if (*q <= 32)
		{
			while (*++q <= 32) {}
			if (q >= e)
				return endOfInput();
		}

		if (*q is '=')
		{
			while (*++q <= 32) {}
			if (q >= e)
				return endOfInput();

			auto quote = *q;
			switch (quote)
			{
			case '"':
			case '\'':
				p = q + 1;
				while (*++q != quote) {}
				if (q < e)
				{
					rawValue = p[0 .. q - p];
					text.point = q + 1;   // skip end quote
					return type = XmlTokenType.Attribute;
				}
				return endOfInput();

			default:
				return doExpected("\' or \"", q);
			}
		}
		
		return doExpected ("=", q);
	}
	XmlTokenType doEndEmptyElement()
	{
		if (text.point[0] is '/' && text.point[1] is '>')
		{
			localName = prefix = null;
			text.point += 2;
			return type = XmlTokenType.EndEmptyElement;
		}
		return doExpected("/>", text.point);
	}
	XmlTokenType doComment()
	{
		auto e = text.end;
		auto p = text.point;
		auto q = p;
		
		while (p < e)
		{
			while (*p != '-')
				if (++p >= e)
					return endOfInput();

			if (p[0..3] == "-->")
			{
				text.point = p + 3;
				rawValue = q [0 .. p - q];
				return type = XmlTokenType.Comment;
			}
			++p;
		}

		return endOfInput();
	}
	XmlTokenType doCData()
	{
		auto e = text.end;
		auto p = text.point;
		
		while (p < e)
		{
			auto q = p;
			while (*p != ']')
				if (++p >= e)
					return endOfInput();
			
			if (p[0..3] == "]]>")
			{
				text.point = p + 3;
				rawValue = q [0 .. p - q];
				return type = XmlTokenType.CData;
			}
			++p;
		}

		return endOfInput();
	}
	XmlTokenType doPI()
	{
		auto e = text.end;
		auto p = text.point;
		auto q = p;

		while (p < e)
		{
			while (*p != '\?')
				if (++p >= e)
					return endOfInput();

			if (p[1] == '>')
			{
				rawValue = q [0 .. p - q];
				text.point = p + 2;
				return type = XmlTokenType.PI;
			}
			++p;
		}
		return endOfInput();
	}
	XmlTokenType doDoctype()
	{
		auto e = text.end;
		auto p = text.point;

		// strip leading whitespace
		while (*p <= 32)
			if (++p >= e)
				return endOfInput();
		
		auto q = p;
		while (p < e)
		{
			if (*p is '>')
			{
				rawValue = q [0 .. p - q];
				prefix = null;
				text.point = p + 1;
				return type = XmlTokenType.Doctype;
			}
			else
			{
				if (*p == '[')
					do {
					if (++p >= e)
						return endOfInput();
				} while (*p != ']');
				++p;
			}
		}

		if (p >= e)
			return endOfInput();
		return type = XmlTokenType.Doctype;
	}
	XmlTokenType endOfInput ()
	{
		if (depth)
			setError("Unexpected EOF");
		return type = XmlTokenType.Done;
	}
	XmlTokenType doUnexpected (const(char[]) msg, const(Ch)* p)
	{
		return position ("parse error :: unexpected  " ~ msg, p);
	}
	XmlTokenType doExpected (const(char[]) msg, const(Ch)* p)
	{
		return position ("parse error :: expected  " ~ msg ~ " instead of " ~ to!string(p[0..1]), p);

	}
	XmlTokenType position (const(char[]) msg, const(Ch)* p)
	{
		return setError(msg ~ " at position " ~ to!string(p-text.text.ptr));
	}
	void reset_()
	{
		depth = 0;
		errMsg = null;
		type = XmlTokenType.None;

		auto p = text.point;
		if (p)
		{
			static if (Ch.sizeof == 1)
			{
				// consume UTF8 BOM
				if (p[0] is 0xef && p[1] is 0xbb && p[2] is 0xbf)
					p += 3;
			}
			
			//TODO enable optional declaration parsing
			auto e = text.end;
			while (p < e && *p <= 32)
				++p;
			
			if (p < e)
				if (p[0] is '<' && p[1] is '\?' && p[2..5] == "xml")
				{
					p += 5;
					while (p < e && *p != '\?')
						++p;
					p += 2;
				}
			text.point = p;
		}
	}
}


unittest
{
	void testParser(Ch)(PullParser!(Ch) itr)
	{
//  assert(itr.next);
//		assert(itr.value == "");
//		assert(itr.type == XmlTokenType.Declaration, to!string(itr.type));
//		assert(itr.next);
//		assert(itr.value == "version");
//		assert(itr.next);
//		assert(itr.value == "1.0");
		assert(itr.next);
		assert(itr.value == "element [ <!ELEMENT element (#PCDATA)>]");
		assert(itr.type == XmlTokenType.Doctype);
		assert(itr.next);
		assert(itr.localName == "element");
		assert(itr.type == XmlTokenType.StartElement);
		assert(itr.depth == 0);
		assert(itr.next);
		assert(itr.localName == "attr");
		assert(itr.value == "1");
		assert(itr.next);
		assert(itr.type == XmlTokenType.Attribute);
		assert(itr.localName == "attr2");
		assert(itr.value == "two");
		assert(itr.next);
		assert(itr.value == "comment");
		assert(itr.next);
		assert(itr.rawValue == "test&amp;&#x5a;");
		assert(itr.next);
		assert(itr.prefix == "qual");
		assert(itr.localName == "elem");
		assert(itr.next);
		assert(itr.type == XmlTokenType.EndEmptyElement);
		assert(itr.next);
		assert(itr.localName == "el2");
		assert(itr.depth == 1);
		assert(itr.next);
		assert(itr.localName == "attr3");
		assert(itr.value == "3three", itr.value);
		assert(itr.next);
		assert(itr.rawValue == "sdlgjsh");
		assert(itr.next);
		assert(itr.localName == "el3");
		assert(itr.depth == 2);
		assert(itr.next);
		assert(itr.type == XmlTokenType.EndEmptyElement);
		assert(itr.next);
		assert(itr.value == "data");
		assert(itr.next);
		//  assert(itr.qvalue == "pi", itr.qvalue);
		//  assert(itr.value == "test");
		assert(itr.rawValue == "pi test", itr.rawValue);
		assert(itr.next);
		assert(itr.localName == "el2");
		assert(itr.next);
		assert(itr.localName == "element");
		assert(!itr.next);
	}
}


