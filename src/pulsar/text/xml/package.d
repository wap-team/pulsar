module pulsar.text.xml;

public import pulsar.text.xml.document;
//public import pulsar.text.xml.pullparser;
//public import pulsar.text.xml.saxparser;



/*
		Это переработка с https://github.com/SiegeLord/Tango-D2/tree/d2port/tango/text/xml

		*******************************************************************************
		Copyright: Copyright (C) 2007-2008 Scott Sanders, Kris Bell. All rights reserved.
		License:   BSD style: $(LICENSE)
		version:   Initial release: February 2008
		Authors:   stonecobra, Kris
		*******************************************************************************

		defs.d       - основные определений
		pullparser.d - разбирает xml по токенам. По сути является итератором по токенам,
																	в каждый конкретный момент указывает на определенный токен.
		saxparser.d  - враппер над pullparser, вызывающий для каждого нового тега
																	соответствующий метод в классе-обработчике.
		document.d   - создает DOM модель. Относительно тяжелый, ноды, хоть и ссылаются
																	всеми значениями на исходную строку, струтуры довольно большие,
																	хранятся в массивах.
		doctester.d  - выполняет различные тесты на правильность для document
		docprinter.d - формирует текст xml по DOM модели document
		iterator.d   - итератор по записям xml. Легкая структура, использующая только PullParser
		composer.d   - простой составитель xml. Очень легкий, просто воздушный.


*/
