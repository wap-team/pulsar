module pulsar.text.xml.iterator;

import pulsar.text.xml.pullparser;

struct TagAttribute(Ch = char)
{
	const Ch[] name;
	const Ch[] value;

	this(const(Ch)[] name) { this.name = name; this.value = null; }
	this(const(Ch)[] name, const(Ch)[] value)
	{
		this.name = name;
		static if(is(Ch == char))
		{
			import undead.xml : decode, DecodeMode;
			this.value =  cast(Ch[])decode(cast(string)value);
		}
		else
			static assert(false, "ToDo");
	}

	string toString()
	{
		import std.conv : text;
		return text(name,"=\"", value,"\"");
	}
}

import std.traits : isSomeChar;
struct XmlIterator(Ch = char) if(isSomeChar!Ch)
{
private:
	PullParser!(Ch) _parser;

	const(Ch)[] _tag;
	TagAttribute!(Ch)[] _attribs;
	const(Ch)[] _text;
	bool _hasChilds;

public:
	this(const(Ch)[] text)
	{
		opAssign(text);
	}
	this(string text) { opAssign(text); }
	/// Для повтороного использования с другими данными
	void opAssign(const(Ch)[] text)
	{
		reserve(_attribs,10);
//		if (!_parser)
//			_parser = new PullParser!(Ch)(text);
//		else
		_parser.reset(text);
		popFront();
	}

public:
	@property auto tag() { return cast(immutable(Ch)[])_tag; }
	@property auto text()
	{
		static if(is(Ch == char))
		{
			import undead.xml : decode, DecodeMode;
			return cast(immutable(Ch)[])decode(cast(string)_text);
		}
		else
			static assert(false, "ToDo");
	}
	@property bool hasChilds() { return _hasChilds; }
	/// Глубина вложенности
	@property int depth()
	{
		return _parser.depth + (_parser.type == XmlTokenType.StartElement ? 0 : 1);
	}

	@property TagAttribute!(Ch)[] attributes() { return _attribs; }
	@property auto getAttribute(immutable(Ch)[] name, immutable(Ch)[] def = null)
	{
		foreach(a; _attribs)
			if(a.name == name)
				return cast(immutable(Ch)[])a.value;
		return def;
	}
	auto opIndex(immutable(Ch)[] name)
	{
		foreach(a; _attribs)
			if(a.name == name)
				return cast(immutable(Ch)[])a.value;
		throw new Exception("Атрибут " ~ name ~ " не указан!");
	}

	@property bool empty() { return _tag.length == 0; }
	@property ref XmlIterator!Ch front() { return this; }
	ref XmlIterator!Ch moveFront()
	{
		popFront();
		return this;
	}
	void popFront()
	{
		if(_parser.type == XmlTokenType.StartElement)
			_tag = _parser.localName;
		else
			_tag.length = 0;
		_attribs.length = 0;
		_text.length = 0;
		_hasChilds = false;

		while (true)
		{
			_parser.next();
			final switch (_parser.type)
			{
				case XmlTokenType.StartElement :
				{
					if (_tag.length)
					{
						_hasChilds = true;
						return;
					}
					_tag = _parser.localName;
				}	break;
				case XmlTokenType.Attribute :
				{
					_attribs ~= TagAttribute!Ch(_parser.localName, _parser.rawValue);
				}	break;
				case XmlTokenType.EndElement :
				case XmlTokenType.EndEmptyElement :
				{
					if(_tag.length)
						return;
					if(depth < 1)
						throw new XmlException("Ошибка в структуре документа!");
				}	break;
				case XmlTokenType.CData :
				case XmlTokenType.Data :
				{
					if(depth < 1)
						throw new XmlException("Ошибка в структуре документа!");
					_text ~= _parser.rawValue;
				}	break;
				case XmlTokenType.PI :
				case XmlTokenType.Doctype : break;
				case XmlTokenType.Comment :
				case XmlTokenType.None :
				{
				}	break;

				case XmlTokenType.Done:
				{
					_tag.length = 0;
					_attribs.length = 0;
					_text.length = 0;
					_hasChilds = false;
						return;
				} break;
			}
		}
	}
	void reset()
	{
		_parser.reset();
		popFront();
	}

	string toString()
	{
			import std.array;
			static import std.conv;
			return std.conv.text(replicate("  ", depth-1), "tag: ", tag, " depth: ", depth,
									" attribs: " , attributes, " hasChilds: ", hasChilds,
									" text: ", text.replace("\n","\\n").replace("\t","\\t"));
	}
}
