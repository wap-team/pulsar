module pulsar.text.xml.composer;

import std.array : appender, Appender, replicate;
public import undead.xml : decode, DecodeMode, encode;
import pulsar.atoms;

struct XmlComposer
{
private:
	Appender!string _head;
	//string _tail;
	int _deep = 0;

public:
	ref XmlComposer start(string tag, string[] attrs...) return
	{
		return start(tag, attrs);
	}
	ref XmlComposer start(string tag, string[] attrs = null) return
	{
		if(_head.data.length == 0)
			_head.add("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		assert(tag.length);
		_deep++;
		_head.add(indent(_deep),"<",tag);
		packAttrs(attrs,_head);
		_head.add(">\n");
		return this;
	}
	ref XmlComposer end(string tag) return
	{
		assert(tag.length);
		_head.add(indent(_deep),"</",tag,">\n");
		_deep--;
		return this;
	}
	ref XmlComposer elem(string tag, string content, string[] attrs...) return
	{
		return elem(tag, content, attrs);
	}
	ref XmlComposer elem(string tag, string content = null, string[] attrs = null) return
	{
		assert(tag.length);
		_head.add(indent(_deep+1),"<",tag);
		packAttrs(attrs,_head);
		if(content.length)
			_head.add(">", encode(content),"</",tag,">\n");
		else
			_head.add("/>\n");
		return this;
	}

	@property string text() { return _head.data/* ~ _tail*/; }
	string toString() { return text; }

private:
	string indent(int deep)
	{
		enum string sp = replicate(" ", 255);
		assert(deep > 0 && deep < 255);
		return sp[0..deep-1];
	}
	void packAttrs(string[] attrs, ref Appender!string app)
	{
		assert(attrs.length%2 == 0);
		for(size_t i = 0; i < attrs.length; i+=2)
			if(attrs[i].length > 0)
				app.add(" ").add(attrs[i]).add("=\"").add(encode(attrs[i+1])).add("\"");
	}
}
