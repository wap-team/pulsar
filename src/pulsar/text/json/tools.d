module pulsar.text.json.tools;

public string escapeString(string str)
{
	import std.array : appender;
	auto res = appender!string;
	void put(string s) { res.put(s); }
	escapeString(cast(void delegate(string))(&put), str);
	return res.data;
}

// https://github.com/dlang-community/std_data_json/blob/master/source/stdx/data/json/generator.d
package void escapeString(bool use_surrogates = false)(void delegate(string) put, string s)
{
	import std.format;
	import std.utf : decode;

	for (size_t pos = 0; pos < s.length; pos++)
	{
		immutable ch = s[pos];

		switch (ch)
		{
		case '\\': put(`\\`); break;
		case '\b': put(`\b`); break;
		case '\f': put(`\f`); break;
		case '\r': put(`\r`); break;
		case '\n': put(`\n`); break;
		case '\t': put(`\t`); break;
		case '\"': put(`\"`); break;
		default:
			static if (use_surrogates)
			{
				// output non-control char ASCII characters directly
				// note that 0x7F is the DEL control charactor
				if (ch >= 0x20 && ch < 0x7F)
				{
					char[1] x;
					x[0] = ch;
					put(cast(string)x);
					break;
				}

				dchar cp = decode(s, pos);
				pos--; // account for the next loop increment

				// encode as one or two UTF-16 code points
				if (cp < 0x10000)
				{ // in BMP -> 1 CP
						put(format("\\u%04X", cp));
				}
				else
				{ // not in BMP -> surrogate pair
						int first, last;
						cp -= 0x10000;
						first = 0xD800 | ((cp & 0xffc00) >> 10);
						last = 0xDC00 | (cp & 0x003ff);
						put(format("\\u%04X\\u%04X", first, last));
				}
			}
			else
			{
				if (ch < 0x20 && ch != 0x7F)
					put(format("\\u%04X", ch));
				else
				{
					char[1] x;
					x[0] = ch;
					put(cast(string)x);
				}
			}
			break;
		}
	}
}

void escapeStringTest()
{
	void test(bool surrog)(string str, string expected)
	{
		import std.array : appender;
		auto res = appender!string;
		void put(string s) { res.put(s); }
		escapeString!surrog(cast(void delegate(string))(&put), str);
		assert(res.data == expected, res.data);
	}

	test!false("hello", "hello");
	test!false("hällo", "hällo");
	test!false("a\U00010000b", "a\U00010000b");
	test!false("a\u1234b", "a\u1234b");
	test!false("\r\n\b\f\t\\\"", `\r\n\b\f\t\\\"`);
												test!true("hello", "hello");
			test!true("hällo", `h\u00E4llo`);
	test!true("a\U00010000b", `a\uD800\uDC00b`);
	test!true("a\u1234b", `a\u1234b`);
	test!true("\r\n\b\f\t\\\"", `\r\n\b\f\t\\\"`);
}

@safe unittest
{
	escapeStringTest();
}
