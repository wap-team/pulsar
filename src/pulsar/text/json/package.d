module pulsar.text.json;

public import pulsar.text.json.composer;
version(Server)
{
	public import vibe.data.json;

	T prop(T = string)(Json json, string key, T def = T.init)
	{
		auto r = json[key];
		return r.type() == Json.Type.undefined ||
		       r.type() == Json.Type.Null ? def : r.to!T();
	}
}
