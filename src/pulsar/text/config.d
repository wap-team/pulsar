module pulsar.text.config;

/// Модуль работы с конфигурационными файлами

/// Интерфейс объектов, поддерживающих инициализацию через
/// конфигурационный файл
interface IConfInit
{
	void confInit(string[string]);
}
