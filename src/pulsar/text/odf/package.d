module pulsar.text.odf;

import pulsar.atoms;

import std.string;
import std.datetime;

import pulsar.text.odf.constants;

/// Базовый класс документов OpenDocument Format
class ODFDocument
{
protected:
	string _content;
	bool _isContentClosed;
	bool _hasContent;
	private this() {};

public:
	/// MIME тип документа
	abstract @property string mimetype() const;
	/// Возвращает строку с контентом документа.
	@property string content() const { return _content; }

	/// Возвращает документ в виде массива байт.
	/// Документ уже полностью заполнен и упакован в ZIP.
	@property void[] asArray()
	{
		import std.zip;

		closeContent();

		ZipArchive ar = new ZipArchive();

		ArchiveMember af = new ArchiveMember();
		af.name = "meta.xml";
		af.compressionMethod = CompressionMethod.deflate;
		af.expandedData =
			cast(ubyte[])(format(meta_xml, Clock.currTime.toISOExtString()));
		ar.addMember(af);

		af = new ArchiveMember();
		af.name = "mimetype";
		af.compressionMethod = CompressionMethod.deflate;
		af.expandedData = cast(ubyte[])(mimetype);
		ar.addMember(af);

		af = new ArchiveMember();
		af.name = "styles.xml";
		af.compressionMethod = CompressionMethod.deflate;
		af.expandedData = cast(ubyte[])(styles_xml);
		ar.addMember(af);

		af = new ArchiveMember();
		af.name = "META-INF/manifest.xml";
		af.compressionMethod = CompressionMethod.deflate;
		af.expandedData = cast(ubyte[])(format(manifest_xml, mimetype));
		ar.addMember(af);

		af = new ArchiveMember();
		af.name = "content.xml";
		af.compressionMethod = CompressionMethod.deflate;
		af.expandedData = cast(ubyte[])(_content);
		ar.addMember(af);

		return ar.build();
	}

public:
	/// Сохраняет документ на диск
	void save(string fileName)
	{
		import std.file;
		std.file.write(fileName, this.asArray());
	}

protected:
	abstract void openContent();
	abstract void closeContent();
}

//-----
string toCellValueString(DataKind val)
{
	switch(val)
	{
		case DataKind.number   : return "float";
		case DataKind.perc     : return "percentage";
		case DataKind.currency : return "currency";
		case DataKind.date     : return "date";
		case DataKind.time     : return "time";
		case DataKind.boolean  : return "boolean";
		case DataKind.text     : return "string";
		case DataKind.image    : return "string";
		default: throw new Exception(to!string(val) ~ " не поддерживается!");
	}
}

/// Документ электронной таблицы
class ODFSpreadsheet : ODFDocument
{
private:
	size_t _colCount;

public:
	/// MIME тип документа
	override @property string mimetype() const { return mimetype_spreadsheet; }
	/// Возвращает число столбцов
	@property size_t columnCount() const { return _colCount; }

public:
	this(size_t columnCount, string templateFile = null)
	{
		assert(!templateFile, "ToDo");
		_colCount = columnCount;
		openContent();
	}

public:
	/// Добавляет ячейку указанного типа.
	/// НЕ нормализует значение.
	ODFSpreadsheet addCell(DataKind type, string value, bool startRow = false)
	{
		assert(type != DataKind.unknown);
		if(value.length == 0)
			return addEmptyCell(startRow);
		if(startRow)
			if(_hasContent)
				_content ~= "				</table:table-row>\n				<table:table-row>\n";
		_content ~= text("					<table:table-cell office:value-type=\"",
																			type.toCellValueString() ,"\" office:");

		if(type != DataKind.number && type != DataKind.currency &&
					type != DataKind.perc)
			_content ~= text(type.toCellValueString(), "-");
		_content ~= text("value=\"", value, "\" />\n");
		_hasContent = true;
		return this;
	}
	/// Добавляет ячейку указанного типа.
	/// Нормализует значение по возможности, вызывая спец. методы добавления.
	ODFSpreadsheet addCellNorm(DataKind type, string value, bool startRow = false)
	{
		if(type == DataKind.number)
			return addNumCell(value, startRow);
		if(type == DataKind.text || type == DataKind.unknown)
			return addTextCell(value, startRow);
		if(type == DataKind.date)
			return addDateCell(value, startRow);
		if(type == DataKind.time)
			return addTimeCell(value, startRow);
		if(type == DataKind.currency)
			return addMoneyCell(value, startRow);
		return addCell(type, value, startRow);
	}
	/// Добавляет пустую ячейку.
	ODFSpreadsheet addEmptyCell(bool startRow = false)
	{
		if(startRow)
			if(_hasContent)
				_content ~= "				</table:table-row>\n				<table:table-row>\n";
		_content ~= text("					<table:table-cell />\n");
		_hasContent = true;
		return this;
	}

	/// Добавляет числовую ячейку.
	/// Нормализует значение.
	ODFSpreadsheet addNumCell(string value, bool startRow = false)
	{
		string s = value.replace(" ", "").replace("\u202F", "").replace(",", ".");
		return addCell(DataKind.number, s, startRow);
	}
	ODFSpreadsheet addNumCell(double value, bool startRow = false)
	{
		return addCell(DataKind.number, to!string(value), startRow);
	}
	/// Добавляет текстовую ячейку.
	/// Нормализует значение.
	ODFSpreadsheet addTextCell(string value, bool startRow = false)
	{
		string s = value.replace("&", "&amp;").replace("\"", "&quot;").
																			replace("<", "&lt;").replace(">", "&gt;");
		return addCell(DataKind.text, s, startRow);
	}
	/// Добавляет ячейку c датой.
	/// Пытается нормализовать значение.
	ODFSpreadsheet addDateCell(string value, bool startRow = false)
	{
		import pulsar.atoms.strings;
		if(value.length == 0)
			return addCell(DataKind.date, null, startRow);
		DateTime dt = fromStringF_DateTime(value);
		if(dt.timeOfDay != TimeOfDay.init)
			return addCell(DataKind.date, dt.toISOExtString(), startRow);
		else
			return addDateCell(dt.date, startRow);
	}
	/// Нормализует значение.
	ODFSpreadsheet addDateCell(Date value, bool startRow = false)
	{
		return addCell(DataKind.date, value.toISOExtString(), startRow);
	}
	/// Добавляет ячейку cо временем.
	/// Не нормализует значение.
	ODFSpreadsheet addTimeCell(string value, bool startRow = false)
	{
		if(value.canFind(":"))
			return addTimeCell(fromStringF_Time(value), startRow);
		return addCell(DataKind.time,  value, startRow);
	}
	/// Нормализует значение.
	ODFSpreadsheet addTimeCell(TimeOfDay value, bool startRow = false)
	{
		string s = format("PT%sH%sM%sS", value.hour, value.minute, value.second);
		return addCell(DataKind.time, s, startRow);
	}
	/// Добавляет ячейку c датой.
	/// Пытается нормализовать значение.
	ODFSpreadsheet addMoneyCell(string value, bool startRow = false)
	{
		string s = value.replace(" ", "").replace("\u202F", "").replace(",", ".").replace("₽", "");
		return addCell(DataKind.currency, s, startRow);
	}

protected:
	override void openContent()
	{
		_content = text(`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<office:document-content xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:msoxl="http://schemas.microsoft.com/office/excel/formula">
	<office:body>
		<office:spreadsheet>
			<table:table table:name="Экспорт из WAP">
				<table:table-column  table:number-columns-repeated="`, columnCount, `"/>
				<table:table-row>
`);
	}
	override void closeContent()
	{
		if(_isContentClosed == false)
		{
			_content ~= `				</table:table-row>
			</table:table>
		</office:spreadsheet>
	</office:body>
</office:document-content>
`;
			_isContentClosed =true;
		}
	}
}

unittest
{
	version(trace)trace("--- ODFSpreadsheet ... ---");
	ODFSpreadsheet sp = new ODFSpreadsheet(3);
	sp.addTextCell("<a\"a'a>").addTextCell("bbb").addTextCell("ccc");
	sp.addNumCell("0.1",true).addNumCell("1.1").addNumCell("1.2");
	sp.addNumCell("100",true).addNumCell("110").addNumCell("120");
	sp.addNumCell(10000000,true).addNumCell(20000000).addNumCell(30000000);
	sp.addDateCell(Date(2001,12,31),true).addDateCell("").addDateCell(Date(2011,2,23));
	sp.addTimeCell(TimeOfDay(7,15,31),true).addEmptyCell().
				addTimeCell(TimeOfDay(18,45,14));

	sp.save("/tmp/arc2.ods.zip"); //
	version(trace)trace("--- ODFSpreadsheet done ---");
}
