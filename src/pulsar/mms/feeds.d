module pulsar.mms.feeds;

import std.uuid;
import std.datetime;

import pulsar.atoms;
import pulsar.containers.index;
//import pulsar.gol.attributes;

import pulsar.mms.messages;
import pulsar.mms.channels;

//version(SimMeta)
//	mixin pulsar.gol.support.RegModuleMetaTypes;

/// Базовый класс лент сообщений
class MessageFeed : IOIDObject
{
	//@SerializationModes(SerializationMode.Always ^ SerializationMode.Clone)
	private UUID _oid;
protected:
	string _name;
	string _desc;
	MessageChannel[] _channels;

public:
	@property // oid
	{
		override @noproxy	ref OID oid() const { return _oid; }
		void oid(OID value) { _oid = value.unqual; }
	}
	/// Имя ленты
	@property // name
	{
		string name() const { return _name; }
		void name(string value) { CheckNullOrEmpty!value; _name = value; }
	}
	/// Описание ленты
	@property // description
	{
		string description() const { return _desc; }
		void description(string value) { _desc = value; }
	}
	@property const(MessageChannel[]) channels() const { return _channels; }
	/// Дата последней модификации.
	/// Определяется как наибольшее из времен модификации каналов ленты
	@property SysTime lastModify() const
	{
		if(_channels.length == 1)
			return _channels[0].lastModify;
		SysTime t;
		foreach(ch; _channels)
			if(ch.lastModify > t)
				t = ch.lastModify;
		return t;
	}

public:
	this()
	{
		if(_oid.empty)
			_oid = randomUUID;
	}
	this(OID oid)
	{
		_oid = oid.empty ? randomUUID : oid;
	}

public:
	void addChannel(const(MessageChannel) ch)
	{
		assert(ch);
		foreach(c;_channels)
			if(ch is c)
				return;
		_channels ~= unqual(ch);
	}
	void removeChannel(const(MessageChannel) ch)
	{
		assert(ch);
		import std.algorithm : remove, SwapStrategy;
		auto pos = _channels.countUntil(ch);
		if(pos > -1)
			_channels = _channels.remove!(SwapStrategy.stable)(pos);
	}

public:
	/// Возвращает сообщение по OID.
	const(Message) get(OID id, lazy const(Message) def = null) const
	{
		foreach(ch; _channels)
		{
			auto m = ch.get(id,def);
			if(m !is def)
				return m;
		}
		return def;
	}
	/// Метод добавления сообщения.
	/// Добавляет сообщение в первый канал из списка.
	void addMessage(Message msg)
	{
		CheckNull!msg;
		if(_channels.length == 0)
			throw new Exception("Для ленты не указаны каналы сообщений!");
		_channels[0].add(msg);
		SignalBus.signal!(MessageFeed.addMessage)(this,msg);
	}
	/// Метод удаления сообщения.
	bool removeMessage(Message msg)
	{
		foreach(ch; _channels)
			if(ch.remove(msg))
				return true;
		return false;
	}

	/// Принцип определения новых сообщений
	bool isNewMessage(const(Message) msg, SysTime checkTime, AccessLevel al) const
	{
		return al > AccessLevel.None && msg.lastModifyDate > checkTime;
	}
	/// Принцип обработки новых сообщений
	bool needDownloadNewMessages() const { return false; }

	auto iter(T : Message)(bool delegate(const(T)) filter = null,
																								bool breakOnFalse = false) const
	{
		if(filter == null)
			filter = x => true;
		return FeedIter!T(this, filter, breakOnFalse);
	}

	// Проверка по умолчанию для итератора ленты.
	// Проверяет на архивный признак.
	bool iterCheck(const(Message) m) const
	{
		if(m is null || m.isArchived)
			return false;
		return true;
	}

	int opApply(int delegate(const(Message)) dg) const
	{
		if(_channels.length == 0)
			return 0;
		if(_channels.length == 1)
		{
			auto ch = _channels[0];
			int result = 0;
			for(long i = ch._list.length-1; i >= 0; i--)
			{
				if(iterCheck(ch._list[i]))
				{
					result = dg(ch._list[i]);
					if(result)
						break;
				}
			}
			return result;
		}

		import std.container;
		Message[][] chs;
		chs.length = _channels.length;
		foreach(i,ch;_channels)
			chs[i] = unqual(ch._list);

		int result = 0;
		while(true)
		{
			long x = -1;
			foreach(i,ref ch; chs)
			{
				while(ch.length > 0 && ch[$-1].isArchived)
					ch.length--;
				if(ch.length && (x < 0 || chs[x][$-1].createDate < ch[$-1].createDate))
					x = i;
			}
			if(x < 0)
				break;

			if(iterCheck(chs[x][$-1]))
			{
				result = dg(chs[x][$-1]);
				if(result)
					break;
			}
			chs[x].length--;
		}
		return result;
	}
	int opApply(int delegate(size_t, const(Message)) dg) const
	{
		int result = 0;
		size_t i = 0;
		foreach(const(Message) item; this)
		{
			result = dg(i,item);
			if(result)
				break;
			i++;
		}
		return result;
	}

	static struct FeedIter(T : Message)
	{
private:
		long pos = -1;
		Message[][] chs;

public:
		const(MessageFeed) feed;
		bool delegate(const(T)) filter;
		bool breakOnFalse;

		this(const(MessageFeed) feed, bool delegate(const(T)) filter = null,
							bool breakOnFalse = false)
		{
			assert(feed);
			this.feed = feed;
			this.filter = filter;
			this.breakOnFalse = breakOnFalse;

			if(feed._channels.length > 0)
			{
				chs.length = feed._channels.length;
				foreach(i,ch;feed._channels)
					chs[i] = unqual(ch._list);
				popFront();
			}
		}

		@property bool empty() { return pos == -1;	}
		@property T front() { return pos == -1 ? T.init : cast(T)(chs[pos][$-1]); }
		T moveFront() { assert(false, "ToDo"); }
		void popFront()
		{
			if(pos > -1)
				chs[pos].length--;

			while(true)
			{
				pos = -1;
				foreach(i,ref ch; chs)
				{
					while(ch.length > 0 && ch[$-1].isArchived)
						ch.length--;
					if(ch.length && (pos < 0 || chs[pos][$-1].createDate < ch[$-1].createDate))
						pos = i;
				}
				if(pos < 0)
					return;

				auto m = cast(const(T))chs[pos][$-1];
				bool checkFilter = true;
				if(filter && m)
				{
					checkFilter = filter(m);
					if(checkFilter == false && breakOnFalse)
					{
						pos = -1;
						return;
					}
				}

				if(checkFilter && feed.iterCheck(chs[pos][$-1]))
					return;
				chs[pos].length--;
			}
		}
		@property FeedIter!T save() { return this; }


		int opApply(int delegate(const(T)) dg) const
		{
			int result = 0;
			foreach(const(Message) mes; feed)
			{
				auto m = cast(const(T))mes;
				if(m)
					if(filter(m))
						result = dg(m);
					else if(breakOnFalse)
						break;
				if(result)
					break;
			}

			return result;
		}
	}


}


