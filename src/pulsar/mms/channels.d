module pulsar.mms.channels;

import std.datetime;

import pulsar.atoms;
import pulsar.gol.attributes;
import pulsar.containers.index;

import pulsar.mms.messages;

//version(SimMeta)
//	mixin pulsar.gol.support.RegModuleMetaTypes;

/// Класс канала сообщений
@essence class MessageChannel : OIDObject
{
private:
	string _name;
	string _desc;
	uint _ident;
	@noser SysTime _lastModify;
	string _mutatorType;
	@noproxy @noser IMessageChannelMutator _mutator;

package:
	Message[] _list;

public:
	/// Имя канала
	@property // name
	{
		string name() const { return _name; }
		void name(string value) { _name = value; }
	}
	/// Описание канала
	@property // description
	{
		string description() const { return _desc; }
		void description(string value) { _desc = value; }
	}
	/// Количество сообщений в канале
	@property size_t count() const { return _list.count; }
	/// Дата последней модификации канала и его сообщений
	@property SysTime lastModify() const { return _lastModify; }

	@noproxy @property ref Message[] array() const
	{
		return unqual(this)._list;
	}

	/// Строка с типом мутатора
	@property // mutatorType
	{
		string mutatorType() const { return _mutatorType; }
		void mutatorType(string value)
		{
			_mutatorType = value;
			makeMutator();
			if(_mutator)
				_mutator.reset(_list);
		}
	}

public:
	this() { super(); _lastModify = Clock.currTime; }
	this(OID oid) { super(oid); _lastModify = Clock.currTime; }

public:
	/// Добавляет сообщение в канал.
	void add(Message message)
	{
		assert(message);
		assert(!message.registered, "Сообщение уже было добавлено в канал!");

		if(message.createDate == SysTime.init)
			message.createDate = Clock.currTime;
		message.onModify(&this.onMessageModify);
		_list ~= message;
		save(message);
		//SignalBus.signal!(MessageChannel.add)(this,message);
	}
	/// Удаляет сообщение из канала.
	bool remove(Message message)
	{
		assert(message);
		assert(message.registered, "Сообщение не добавлено в канал!");

		import std.algorithm : remove, SwapStrategy;
		auto pos = _list.countUntil(message);
		if(pos == -1)
			return false;

		_list = _list.remove!(SwapStrategy.stable)(pos);
		message.onModify(cast(OnModifyMessage)null);
		save();
		return true;
	}
	/// Возвращает сообщение по OID.
	const(Message) get(OID id, lazy const(Message) def = null) const
	{
		foreach_reverse(m; _list)
		if(m.oid == id)
			return m;
		return def;
	}


	void onMessageModify(Message m, string methName) const
	{
		if(_mutator && _mutator.needSave(m,methName) == false)
		{
			unqual(this)._lastModify = Clock.currTime;
			return;
		}
		unqual(this).save(m);
	}
	/// Сохраняет сообщение (и канал) в хранилище.
	void save(Message m = null)
	{
		this._lastModify = Clock.currTime;
		if(_mutator)
			_mutator.onSave(_list, m);

// двигает сообщение в конец
//		if(moveUp)
//		{
//			bool find;
//			for(auto i = 0; i < _list.length; i++)
//			{
//				if(find == false)
//					find = _list[i] is m;
//				if(find && i+1 < _list.length)
//					_list[i] = _list[i+1];
//			}
//			if(find)
//				_list[$-1] = m;
//		}
	}

	int opApply(int delegate(const(Message)) dg) const
	{
		int result = 0;
		foreach(item; _list)
		{
			result = dg(item);
			if(result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, const(Message)) dg) const
	{
		int result = 0;
		foreach(size_t i, const(Message) item; _list)
		{
			result = dg(i,item);
			if(result)
				break;
		}
		return result;
	}

	override string toString() { return _name.length ? _name : oid.toString(); }
	string toString() const { return _name.length ? _name : oid.toString(); }
	void onDeserialized()
	{
		_lastModify = Clock.currTime;
		makeMutator();
		foreach(m; _list)
			m.onModify(&this.onMessageModify);
	}

private:
	@nosignal void makeMutator()
	{
		_mutator = null;
		if(_mutatorType.length == 0)
			return;
		_mutator = cast(IMessageChannelMutator)Object.factory(_mutatorType);
		if(_mutator is null)
			trace("Warning! Cannot create mutator! Feed: ", name, "(", oid,
									") mutatorType: ", _mutatorType);
	}
}

/// Интерфейс объектов, изменяющих логику работы канала сообщений.
@noproxy interface IMessageChannelMutator
{
	/// Перестраивает сообщения канала.
	void reset(ref Message[] list);
	/// Определяет необходимость сохранения канала при изменении сообщения
	bool needSave(const(Message) m, string methName) const;
	/// Определяет дополнительную обработку при сохранении канала
	void onSave(ref Message[] list, Message m);
}
