module pulsar.mms;

import pulsar.atoms;
import pulsar.gol;
import pulsar.containers.index;

public import pulsar.mms.messages;
public import pulsar.mms.channels;
public import pulsar.mms.feeds;
public import pulsar.mms.notices;

/// Message Management System
class MMS
{
static:

private:
	__gshared IndexList!(MessageChannel, "oid") _chans;
	__gshared IndexList!(MessageFeed, "oid") _feeds;
	__gshared NoticesManager _notifier;

public:
	@property auto channels()
	{
		static struct Iter
		{
			const MessageChannel[] x;
			size_t i = 0;

			this(const MessageChannel[] x) { this.x = x; }
			@property bool empty() { return i >= x.length; }
			@property const(MessageChannel) front() { return x[i]; }
			void popFront() { i++; }
			@property Iter save() { return this; }
		}

		return Iter(_chans.array);
	}
	@property auto feeds()
	{
		static struct Iter
		{
			const MessageFeed[] x;
			size_t i = 0;

			this(const MessageFeed[] x) { this.x = x; }
			@property bool empty() { return i >= x.length; }
			@property const(MessageFeed) front() { return x[i]; }
			void popFront() { i++; }
			@property Iter save() { return this; }
		}

		return Iter(_feeds.array);
	}
	@property NoticesManager notifier()
	{
		enforce(_notifier, "Менеджер уведомлений не инициализирован!");
		return _notifier;
	}

public:
	shared static this()
	{
		_chans = new IndexList!(MessageChannel, "oid");
		_feeds = new IndexList!(MessageFeed, "oid");
	}

public:
	MessageChannel regChannel(OID oid, string name)
	{
		assert(oid.empty == false);
		MessageChannel ch = _chans.oid.get(oid, null);
		if(ch is null)
		{
			ch = GOL.create!MessageChannel(oid);
			GOL.add(ch);
			ch.name = name;
			_chans.add(ch);
		}
		return ch;
	}
	const(MessageChannel) getChannel(OID oid)
	{
		return _chans.oid.get(oid, null);
	}
	const(MessageChannel) getChannel(string name)
	{
		foreach(const(MessageChannel) ch; _chans.consted)
			if(ch.name == name)
				return ch;
		return null;
	}
	bool hasChannel(OID oid)
	{
		return _chans.oid.has(oid);
	}

	MessageFeed regFeed(MessageFeed feed, bool overwrite = false)
	{
		MessageFeed f = _feeds.oid.get(feed.oid, null);
		if(f)
			if(overwrite)
				_feeds.remove(f);
			else
				return f;
		_feeds.add(feed);
		return feed;
	}
	const(MessageFeed) getFeed(OID oid, bool throwIfNotFound = true)
	{
		auto r = _feeds.oid.get(oid, null);
		if(r is null && throwIfNotFound)
			throw new Exception("Лента с OID[" ~ oid.toString() ~ "] не зарегистрирована!");
		return r;
	}
	bool hasFeed(OID oid)
	{
		return _feeds.oid.has(oid);
	}

public:
	void load()
	{
		foreach(ch;_chans)
			GOL.reload(ch, false);

		import pulsar.reflection.library;
		if(MetaTypeLibrary[typeid(NoticesManager)])
		{
			_notifier = GOL.create!NoticesManager();
			GOL.add(_notifier, "NoticesManager");
			if(GOL.reload("NoticesManager", false) == false)
				GOL.save("NoticesManager");
		}
	}
	void run()
	{
		Logger.log(1,"Кол-во каналов сообщений: %s", _chans.count);
//		foreach(ch;_chans)
//		{
//			trace(" ch.name = ", ch.name);
//			trace(" ch.count = ", ch.count);
//		}
		Logger.log(1,"Кол-во лент сообщений: %s", _feeds.count);
//		foreach(ml;_feeds)
//		{
//			trace(" ml.name = ", ml.name);
//		}
	}
}


