module pulsar.mms.notices;

import std.range;

import pulsar.atoms;
import pulsar.containers.index;
import pulsar.mms.messages;
import pulsar.mms.channels;
import pulsar.gol.attributes;

// Канал для уведомлений
enum NoticeMessagesChannel = OID("fccbcfd1-04ca-41af-9f4f-9c16c6162c08");

/// Базовый класс уведомлений
@meta class NoticeMessage : DataMessage
{
	/// Причина уведомления
	@property // reason
	{
		string reason() const { return _data.get("reason", "unknown"); }
		void reason(string value) { this["reason"] = value; }
	}
	/// Тип источника уведомления
	@property // sourceType
	{
		string sourceType() const { return _data.get("sourceType", null); }
		void sourceType(string value) { this["sourceType"] = value; }
	}

public:
	this() {}
	/// использует data как внутреннее хранилище
	this(string[string] data) { super(data); }
	/// фабрика
	static NoticeMessage opCall(string reason, string text = null, string[string] data = null)
	{
		NoticeMessage m;
		if(data is null)
			m = new NoticeMessage();
		else
			m = new NoticeMessage(data);
		m.reason = reason;
		m.text = text;
		return m;
	}
	/// фабрика
	static NoticeMessage opCall(string[string] data)
	{
		return new NoticeMessage(data);
	}
	// Важно переопределять в потомках
	override NoticeMessage clone(DataMessage m = null) const
	{
		if(m is null)
			m = new NoticeMessage();
		else
			enforce(cast(NoticeMessage)m);

		return cast(NoticeMessage)super.clone(m);
	}
}

//----

/// Структура правила поиска процессора уведомления
@meta struct NoticeRule
{
	/// Тип исходного объекта
	string sourceType;
	/// Причина уведомления
	string reason;

	/// Категория процессора уведомления (email, icq и т.д.)
	/// используется для поиска процессора
	string category;

	/// Объект получателя уведомления. Может не иметь значения.
	Object recipient; // TODO : Не хорошо, что Object
	alias owner = recipient;
	/// Дополнительная информация правила.
	string tag;

	string toString() const
	{
		return format("{ category: %s, sourceType:%s, reason:%s, recipient: %s %s}",
																category, sourceType, reason, recipient ? unqual(recipient).toString() : "",
																tag.length ? ", tag:" ~ tag : null );
	}
}

/// Интерфейс классов - обработчиков уведомлений
interface INoticeProcessor
{
	/// Определяет необходимость блокировки обработки уведомлнений данным процессором,
	/// если предыдущая обработка сообщения завершилась с ошибкой.
	// @property bool lockOnError() const;

	///
	bool canProcessNotice(const(Object) source, NoticeMessage notice, const(NoticeRule) rule);
	/// Подготавливает данные уведомления
	void prepareNoticeData(const(Object) source, NoticeMessage notice, const(NoticeRule) rule);
	/// Выполняет действия по обработке уведомлений
	void processNotice(NoticeMessage notice);
}

/// Класс управления сообщениями уведомлений
@singleton class NoticesManager
{
	struct NoticeProcessorsEntry
	{
		string category;
		string sourceType;
		INoticeProcessor processor;

		this(string entryKey)
		{
			CheckNullOrEmpty!entryKey;
			auto ss = entryKey.split(":");
			assert(ss.length == 2);
			category = ss[0];
			sourceType = ss[1];
		}
		this(string category, string sourceType)
		{
			this.category = category;
			this.sourceType = sourceType;
		}

		@property string entryKey() const { return text(category,":", sourceType); }
		@property bool isEmpty() const { return processor is null; }
	}

private:
	static __gshared  MessageChannel _ch;
	static __gshared IndexList!(NoticeProcessorsEntry, "entryKey") _processors;

	NoticeRule[] _rules;

public:
	shared static this() { _processors = new IndexList!(NoticeProcessorsEntry, "entryKey")(); }
	this() { onDeserialized(); }

public:
	static void regProcessor(string category, ClassInfo sourceType, INoticeProcessor f)
	{
		CheckNullOrEmpty!(category, sourceType, f);
		auto e = NoticeProcessorsEntry(category, sourceType.toString());
		e.processor = f;
		_processors.add(e);
	}
	/**	Ищет процессор по указанному имени и типу(и его базовым типам)
					Params:
					category   - категория процессора
					sourceType - тип объекта источника уведомления
					sharp      - определяет, должны ли использоваться процессоры для базовых типов
	**/
	static NoticeProcessorsEntry getProcessor(string category, ClassInfo sourceType, bool sharp = false)
	{
		alias ti = sourceType;
		if(sharp)
			return getProcessor(category, ti.toString());
		for(; ti !is null; ti = ti.base)
		{
			NoticeProcessorsEntry f = getProcessor(category, ti.toString());
			if(f.isEmpty == false)
				return f;
		}
		return NoticeProcessorsEntry.init;
	}
	static NoticeProcessorsEntry getProcessor(string category, const(Object) source, bool sharp = false)
	{
		CheckNull!source;
		return getProcessor(category, IProxy.typeId(source), sharp);
	}
	/// Ищет процессор по указанному имени и типу
	static NoticeProcessorsEntry getProcessor(string category, string sourceType)
	{
		NoticeProcessorsEntry e = NoticeProcessorsEntry(category, sourceType);
		return _processors.entryKey.get(e.entryKey);
	}

public:
	@property const(NoticeRule[]) rules() const { return _rules; }
	@property bool hasRule(const(NoticeRule) rule) const
	{
		return _rules.canFind(rule);
	}
	void addRule(NoticeRule rule)
	{
		if(hasRule(rule) == false)
			_rules ~= rule;
	}
	bool removeRule(NoticeRule rule)
	{
		long pos = _rules.countUntil(rule);
		if(pos == -1)
			return false;
		import std.algorithm : remove;
		_rules = _rules.remove(pos);
		return true;
	}
	void cleanRules() { _rules.length = 0; }

public:
	/// Регистрирует КОПИИ уведомления для отправки получателю.
	@nosignal void regNotice(const(Object) source, NoticeMessage notice)
	{
		CheckNull!(source,notice);

		string srcType = IProxy.typeId(source).toString();

		// TODO : оптимизация поиска правила
		foreach(rule; _rules)
			if(rule.sourceType == srcType &&
						(rule.reason.length == 0 || rule.reason == notice.reason))
			{
				NoticeProcessorsEntry pe	= getProcessor(rule.category, source);
				if(pe.isEmpty)
					continue;
				try
				{
					if(pe.processor.canProcessNotice(source, notice, rule) == false)
						continue;
					NoticeMessage n = notice.clone();
					n.sourceType = srcType;
					pe.processor.prepareNoticeData(source, n, rule);
					n["processor"] = pe.entryKey;

					_ch.add(n);
				}
				catch(LogicError err)
				{
					Logger.log("[ERROR][%s] Уведомление %s : %s",	IProxy.typeId(this), notice, err.msg);
				}
			}
	}
	@nosignal void regNotice(const(Object) source, string[string] data)
	{
		CheckNull!(source,data);
		regNotice(source, NoticeMessage(data));
	}

	/// Обрабатывает сообщения в канале
	@nosignal void process()
	{
		static __gshared bool isRunning;
		if(isRunning || _ch.array.length == 0)
		{
			//trace("skip");
			return;
		}

		isRunning = true;
		try
		{
			foreach(m; _ch.array.dup)
			{
				NoticeMessage notice = cast(NoticeMessage)m;
				try
				{
					checkLogic(notice !is null, "Сообщение не является уведомлением!");
					string k = notice["processor"];
					checkLogic(k.length > 0, "В уведомлении не указан processor!");
					NoticeProcessorsEntry e = NoticeProcessorsEntry(k);
					e = getProcessor(e.category, e.sourceType);
					checkLogic(e.isEmpty == false, "Не найден обработчик уведомления (" ~ k ~ ")!");
					assert(e.processor);
					StopWatch sw = StopWatch(AutoStart.yes);
					e.processor.processNotice(notice);
					sw.stop();
					_ch.remove(unqual(m));
					Logger.log("[Info][%s] Уведомление [source:%s reason:%s] было обработано процессором %s (%s)",
																IProxy.typeId(this), notice.get("sourceType","???"), notice.reason, e.entryKey,
																sw.peek().total!"msecs");
					pulsar.server.PulsarServer.yield();
				}
				catch(LogicError err)
				{
					Logger.log("[ERROR][%s] Уведомление %s : %s",	IProxy.typeId(this), notice, err.msg);
				}
			}
		}
		finally
		{
			isRunning = false;
		}
	}

	void onDeserialized()
	{
		if(_ch is null)
			_ch = pulsar.mms.MMS.regChannel(NoticeMessagesChannel,	"NoticesManagerChannel");
	}
}

//--------------------
/// Базовый класс процессора уведомлений для отправки по email
class EmailNoticeProcessor : INoticeProcessor
{
	import pulsar.web.email;

	bool canProcessNotice(const(Object) source, NoticeMessage notice, const(NoticeRule) rule)
	{
		return rule.recipient !is null || notice.has("To");
	}

	/// Возвращает true, если небыло ошибок
	void prepareNoticeData(const(Object) source, NoticeMessage notice, const(NoticeRule) rule)
	{
		if(notice.has("To") == false)
		{
			notice["To"] = getToAddress(source, notice, rule);
			checkLogic(notice.get("To").length > 0, "Не удалось определить email получателя!");
		}
		if(notice.has("From") == false)
			notice["From"] = getFromAddress(source, notice, rule);
		if(notice.has("Subject") == false)
			notice["Subject"] = getSubject(source, notice, rule);
		notice.text = getText(source, notice, rule);
		setExtraData(source, notice, rule);
	}

	// Методы ниже вызываются из prepareNoticeData
	string getToAddress(const(Object) source, NoticeMessage notice, const(NoticeRule) rule)
	{
		if(cast(const(IEmailAddress))rule.recipient)
			return (cast(const(IEmailAddress))rule.recipient).namedAddress;
		return null;
	}
	string getFromAddress(const(Object) source, NoticeMessage notice, const(NoticeRule) rule)
	{
		return null;
	}
	string getSubject(const(Object) source, NoticeMessage notice, const(NoticeRule) rule)
	{
		return "WAP: Уведомление";
	}
	string getText(const(Object) source, NoticeMessage notice, const(NoticeRule) rule)
	{
		return notice.text;
	}
	/// Устанавливает дополнительные заголовки в notice.data
	void setExtraData(const(Object) source, NoticeMessage notice, const(NoticeRule) rule ) { }

	/// Посылает уведомление по email
	void processNotice(NoticeMessage notice)
	{
		import pulsar.web.email;
		string s = Email.compose(notice.data, plainText(notice)/*, html(notice)*/);
		//trace(s);
		Email.sendMail(s);
	}

	// Методы ниже вызываются из processNotice
	/// Возвращает текст сообщения
	string plainText(NoticeMessage notice)
	{
		return notice.text;
	}
	// TODO
	// string html(NoticeMessage notice) {}
}
