module pulsar.mms.messages;

import std.datetime;

import pulsar.atoms;
import pulsar.gol.attributes;
import pulsar.server.security;
import pulsar.mms.channels;

///	Флаги сообщения
enum MessageFlags: ubyte
{
	None       = 0,
	Initx       = 1,
	/// Признак регистрации сообщения (помещения в канал)
	Registered = 2,
	/// Признак архивации сообщения (перемещения в архив канала)
	Archived   = 4
}

alias OnModifyMessage = void delegate(Message, string);

/// Базовый класс сообщения
class Message : OIDObject, IArchivable
{
private:
	SysTime _crDate;
	SecurityContext _sc;
	string _text;
package:
	MessageFlags _flags;
	@noser OnModifyMessage _onmodify;

public:
	@property // securityContext
	{
		const(SecurityContext) securityContext() const { return _sc; }
		void securityContext(SecurityContext sc)
		{
			assert(!registered, "Попытка изменить контекст безопасности зарегистрированного сообщения!");
			_sc = sc;
		}
	}
	/// Дата и время создания сообщения
	@property // createDate
	{
		SysTime createDate() const { return _crDate; }
		void createDate(SysTime value)
		{
			assert(!registered, "Попытка изменить дату зарегистрированного сообщения!");
			_crDate = value;
		}
	}
	/// Дата и время последней модификации
	@property SysTime lastModifyDate() const { return _crDate; }
	/// Текст сообщения
	@property // text
	{
		string text() const { return _text; }
		void text(string value)
		{
			_text = value;
			save();
		}
	}
	/// Признак регистрации (помещения в канал) сообщения
	@property bool registered() const
	{
		//return _flags.isSet(MessageFlags.Registered);
		return _onmodify !is null;
	}

	@property bool isArchived() const
	{
		return _flags.isSet(MessageFlags.Archived);
	}
	@property bool canBeArchived() const { return true; }

public:
	this()
	{
		_sc = Security.context;
		_crDate = Clock.currTime;
	}

public:
	@nosignal void onModify(string methName = __FUNCTION__)
	{
		if(_onmodify)
			_onmodify(this, methName);
	}
	@nosignal void onModify(OnModifyMessage meth)
	{
		if(meth !is null && _onmodify !is null)
			throw new Exception("Сообщение уже зарегистрировано!");
		_onmodify = meth;
		//_flags = _flags.calcFlag(MessageFlags.Registered, meth !is null);
	}

	void toArchive()
	{
		if(this.canBeArchived == false)
			throw new LogicError("Нельзя сдать в архив!");
		_flags = _flags.setFlag(MessageFlags.Archived);
		save();
	}
	void save(string methName = __FUNCTION__)
	{
		onModify(methName);
	}
}

//----
enum SystemMessageReason
{
	Info = 0,
	ObjectAdded,
	ObjectChanged,
	ObjectRemoved,
	DataError
}
@meta class SystemMessage : Message
{
private:
	SystemMessageReason _reason;
	Object _source;

public:
	/// Причина возникновения сообщения
	@property // reason
	{
		SystemMessageReason reason() const { return _reason; }
		void reason(SystemMessageReason value)
		{
			assert(!registered, "Попытка изменить сообщение после добавления в канал!");
			_reason = value;
		}
	}
	/// Объект, для которого создано сообщение
	@property // source
	{
		const(Object) source() const { return _source; }
		void source(Object value)
		{
			assert(!registered, "Попытка изменить сообщение после добавления в канал!");
			_source = value;
		}
	}
}

//----

/// Класс сообщений, содержащих данные ключ/значение
class DataMessage : Message
{
protected:
	string[string] _data;

public:
	// alias _data this; - нельзя, так как нужны метаданные
	@property const(string[string]) data() const { return _data; }

public:
	this() {}
	/// использует data как внутреннее хранилище
	this(string[string] data)
	{
		_data = data;
		if("text" in _data)
		{
			this.text = _data["text"];
			_data.remove("text");
		}
	}

public:
	@property // []
	{
		string opIndex(string key) const
		{
			if(key == "text")
				return super.text;
			return _data[key];
		}
		void opIndexAssign(string value, string key)
		{
			if(key == "text")
				super.text = value;
			else
			{
				if(value.length)
					_data[key] = value;
				else
					_data.remove(key);
				save();
			}
		}
		void opIndexAssign(T)(T value, string key)
		{
			string s = to!string(value);
			if(key == "text")
				super.text = s;
			else
			{
				if(s.length)
					_data[key] = s;
				else
					_data.remove(key);
				save();
			}
		}
	}
	bool has(string key) const	{ return (key in _data) !is null; }
	string get(string key, /*lazy*/ string ifNotFound = null) const
	{
		return _data.get(key, ifNotFound);
	}
	bool remove(string key)
	{
		bool res = _data.remove(key);
		save();
		return res;
	}

	// Важно переопределять в потомках
	DataMessage clone(DataMessage m = null) const
	{
		if(m is null)
			m = new DataMessage();
		m._data = unqual(this)._data.dup;
		m._sc = unqual(this)._sc;
		m._text = _text;
		return m;
	}

	override string toString() const
	{
		return to!string(_data);
	}
}
