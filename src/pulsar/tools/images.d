module pulsar.tools.images;

import pulsar.atoms;

enum ImageFormat : ubyte
{
	Unknown = 0,
	PNG,
	JPEG,
}
enum ubyte[2] jpgSig = [0xFF, 0xD8];
enum ubyte[8] pngSig = [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A];

struct ImageInfo
{
	ImageFormat format;
	uint width;
	uint height;
	ulong size;
}

/// Определяет, является ли файл файлом с изображением
ImageFormat isImageFile(string filePath)
{
	import std.stdio;
	try
	{
		auto f = File(filePath, "r");
		ubyte[] buf;
		buf.length = 8;
		f.rawRead(buf[0..2]);
		if(buf[0..2] == jpgSig)
			return ImageFormat.JPEG;
		f.rawRead(buf[2..8]);
		if(buf == pngSig) // PNG
			return ImageFormat.PNG;
	}
	catch(Throwable err)
	{
		Logger.logError(err);
	}
	return ImageFormat.Unknown;
}

/// Возвращает информацию об изображении. Размер берется из файла с изображением.
ImageInfo getImageInfo(string filePath)
{
	import std.path;
	import std.stdio;
	import std.string;
	ImageInfo s;
	try
	{
		auto f = File(filePath,"r");
		s.size = f.size;
		ubyte[] buf;
		buf.length = 8;
		f.rawRead(buf[0..2]);
		if(buf[0..2] == jpgSig)
		{
			s.format = ImageFormat.JPEG;
			f.rawRead(buf[0..2]); // маркер
			while(!(buf[0] == 0xFF && buf[1] >= 0xC0 && buf[1] < 0xC4))
			{
				ushort len = *(cast(ushort*)(f.rawRead(buf[2..4]).reverseInPlace));
				f.seek(len-2,SEEK_CUR);
				if(f.rawRead(buf[0..2]).length != 2)
				{
					Logger.log("Not find SOF marker in " ~ filePath);
					return s;
				}
			}
			f.rawRead(buf[0..8]);
			s.height = *(cast(ushort*)(buf[3..5].reverseInPlace));
			s.width = *(cast(ushort*)(buf[5..7].reverseInPlace));
			return s;
		}
		f.rawRead(buf[2..8]);
		if(buf == pngSig) // PNG
		{
			s.format = ImageFormat.PNG;
			f.seek(8,SEEK_CUR);
			buf = f.rawRead(buf[0..8]);
			s.width = *(cast(uint*)(buf[0..4].reverseInPlace));
			s.height = *(cast(uint*)(buf[4..8].reverseInPlace));
			return s;
		}
	}
	catch(Throwable err)
	{
		Logger.logError(err);
	}
	return s;
}
