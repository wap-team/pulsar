module pulsar.web.email;

import pulsar.atoms;

/// Интерфейс объекта,
interface IEmailAddress
{
	/// Имя
	@property string name() const;
	/// Только адрес (xxx@xx.x)
	@property string email() const;
	/// Имя и адрес (Name <xxx@xx.x>)
	final @property string namedAddress() const
	{
		return name.length ? (Email.utf8Value(name) ~ " <" ~ email ~ ">") : email;
	}

}

/// Разделитель строк
enum crlf = "\r\n";

struct Recipient
{
private:
	string _address;
	string _name;

public:
	@property // address
	{
		string address() const { return _address; }
		void address(string val)
		{
			CheckNullOrEmpty!val;
			_address = val;
		}
	}
	@property // name
	{
		string name() const { return _name; }
		void name(string val)
		{
			CheckNullOrEmpty!val;
			_name = val;
		}
	}

public:
	this(string address, string name = null)
	{
		this.address = address;
		if(name.length)
			this.name = name;
	}

public:
	bool isEmpty() const { return _address.length == 0; }

	string toString() const
	{
		if(_name.length == 0)
			return _address;
		else
			return text(_name, " <", _address, ">");
	}
}

class Email : OIDObject
{
private:

	Recipient _from;
	Recipient[] _to;
	Recipient[] _cc;
	Recipient[] _bcc;
	Recipient _replyTo;
	string _subject;
	string _plainText;

public:
	@property // from
	{
		Recipient from() const { return _from; }
		void from(Recipient r)
		{
			ensureCanEdit();
			checkLogic(!r.isEmpty, "Отправитель не может быть пустым!");
			_from = r;
		}
		void from(string address) { from(Recipient(address)); }
	}
	@property // to, recipients
	{
		const(Recipient[]) to() const { return _to; }
		const(Recipient[]) recipients() const { return _to; }
		void to(const(Recipient[]) rs)
		{
			CheckNullOrEmpty!rs;
			ensureCanEdit();
			_to.length = 0;
			foreach(r; rs)
			{
				if(r.isEmpty == false && _to.canFind(r) == false)
					_to ~= r;
			}
		}
		alias recipients = to;
	}
	@property // cc
	{
		const(Recipient[]) cc() const { return _cc; }
		void cc(const(Recipient[]) rs)
		{
			CheckNullOrEmpty!rs;
			ensureCanEdit();
			_cc.length = 0;
			foreach(r; rs)
			{
				if(r.isEmpty == false && _cc.canFind(r) == false)
					_cc ~= r;
			}
		}
	}
	@property // bcc
	{
		const(Recipient[]) bcc() const { return _bcc; }
		void bcc(const(Recipient[]) rs)
		{
			CheckNullOrEmpty!rs;
			ensureCanEdit();
			_bcc.length = 0;
			foreach(r; rs)
			{
				if(r.isEmpty == false && _bcc.canFind(r) == false)
					_bcc ~= r;
			}
		}
	}
	@property // replyTo
	{
		Recipient replyTo() const { return _replyTo; }
		void replyTo(Recipient r)
		{
			ensureCanEdit();
			if(!r.isEmpty)
				_replyTo = r;
		}
		void replyTo(string address) { replyTo(Recipient(address)); }
	}
	@property // subject
	{
		string subject() const { return _subject; }
		void subject(string val) { ensureCanEdit(); _subject = val; }
	}
	@property // plainText
	{
		string plainText() const { return _plainText; }
		void plainText(string val) { ensureCanEdit(); _plainText = val; }
	}

public:
	static string compose(const(string[string]) headers, string text)
	{
		import pulsar.atoms.arrays;
		import pulsar.atoms.strings;

		auto x = appender!string();

		x.add("MIME-Version: 1.0", crlf);
		x.add("Content-Type: text/plain;charset=utf-8", crlf);

		x.add("To: ", headers["To"], crlf);
		if("From" in headers)
			x.add("From: ", headers["From"], crlf);
		x.add("Reply-To: ", headers.get("Reply-To", headers.get("From","")), crlf);

		if("Cc" in headers)
			x.add("Cc: ", headers["Cc"], crlf);
		if("Bcc" in headers)
			x.add("Bcc: ", headers["Bcc"], crlf);

		if("Return-Path" in headers)
			x.add("Return-Path: ", headers["Return-Path"], crlf);

		foreach(k,v; headers)
			if(k.startsWith("X-"))
				x.add(k, ": ", v, crlf);

		if("Subject" in headers)
			x.add("Subject: ", Email.utf8Value(headers["Subject"]), crlf);

		if(true)
		{
			x.add(crlf);
			x.add(text);
		}

		//		const string tMultipart = format("Content-Type: multipart/mixed; boundary=\"%s\"\r\n", SmtpMessage.boundary);

		//		if (!attachments.length) {
		//			return format(tFrom, sender.name, sender.address)
		//   			 ~ format(tTo, recipients[0].name, recipients[0].address)
		//				 ~ cc()
		//				 ~ format(tSubject, subject)
		//				 ~ crlf
		//				 ~ message ~ "." ~ crlf;
		//		} else {
		//			return format(tFrom, sender.name, sender.address)
		//				 ~ format(tTo, recipients[0].name, recipients[0].address)
		//				 ~ cc()
		//				 ~ format(tSubject, subject)
		//				 ~ mime
		//				 ~ tMultipart
		//				 ~ "--" ~ boundary ~ crlf
		//				 ~ messageWithAttachments()
		//				 ~ attachmentsToString();


		return x.data;
	}
	static void sendMail(string mail)
	{
		import std.process;
		import std.stdio;


		auto pipes = pipeProcess(["sendmail","-t","-i"]);
		//scope(exit) wait(pipes.pid);
		auto pout = pipes.stdin.lockingTextWriter();

		pout.put(mail);

		pipes.stdin.close();
		wait(pipes.pid);

	}
	static string utf8Value(string s)
	{
		import std.base64;
		return "=?utf-8?b?" ~ cast(string)Base64.encode(cast(ubyte[])s) ~ "?=";
	}


	string pack() const
	{
		import pulsar.atoms.arrays;
		import pulsar.atoms.strings;

		auto x = appender!string();

		enum crlf = "\r\n";

		x.add("Message-ID: ", oid.toString(), crlf);

		x.add("From: ", from.toString(), crlf);

		x.add("To: ");
		foreach(i,r; to)
			x.add(i > 0 ? ", " : "", r.toString());
		x.add(crlf);

		if(cc.length > 0)
		{
			x.add("Cc: ");
			foreach(i,r; cc)
				x.add(i > 0 ? ", " : "", r.toString());
			x.add(crlf);
		}
		if(bcc.length > 0)
		{
			x.add("Bcc: ");
			foreach(i,r; bcc)
				x.add(i > 0 ? ", " : "", r.toString());
			x.add(crlf);
		}

		if(replyTo.isEmpty == false)
			x.add("Reply-To: ", replyTo, crlf);

		x.add("Subject: ", subject, crlf);

		if(true)
		{
			x.add(crlf);
			x.add(plainText, crlf, ".", crlf);


		}

//		const string mime       = "MIME-Version: 1.0\r\n";
//		const string tMultipart = format("Content-Type: multipart/mixed; boundary=\"%s\"\r\n", SmtpMessage.boundary);
//		const string tReplyTo   = "Reply-To:%s\r\n";

//		if (!attachments.length) {
//			return format(tFrom, sender.name, sender.address)
//   			 ~ format(tTo, recipients[0].name, recipients[0].address)
//				 ~ cc()
//				 ~ format(tSubject, subject)
//				 ~ format(tReplyTo, replyTo)
//				 ~ crlf
//				 ~ message ~ "." ~ crlf;
//		} else {
//			return format(tFrom, sender.name, sender.address)
//				 ~ format(tTo, recipients[0].name, recipients[0].address)
//				 ~ cc()
//				 ~ format(tSubject, subject)
//				 ~ mime
//				 ~ tMultipart
//				 ~ format(tReplyTo, replyTo) ~ crlf
//				 ~ "--" ~ boundary ~ crlf
//				 ~ messageWithAttachments()
//				 ~ attachmentsToString();


		return x.data;
	}

public:
	bool canEdit() { return true; }
	void ensureCanEdit() { checkLogic(canEdit, "Email не может быть изменен!"); }
}
