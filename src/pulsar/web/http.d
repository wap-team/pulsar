module pulsar.web.http;

import pulsar.atoms;

/// HTTP методы по RFC 2616
enum HttpMethod : ubyte
{
	TRACE   = 0,
	GET     = 1,
	POST    = 2,
	PUT     = 4,
	PATCH   = 8,
	DELETE  = 16,
	OPTIONS = 32,
	HEAD    = 64,
	CONNECT = 128,

	ANY = 255
}

/// HTTP status codes.
enum HttpStatus : ushort
{
	Continue                     = 100,
	SwitchingProtocols           = 101,
	Processing                   = 102,

	Ok                           = 200,
	Created                      = 201,
	Accepted                     = 202,
	NonAuthoritativeInformation  = 203,
	NoContent                    = 204,
	ResetContent                 = 205,
	PartialContent               = 206,
	MultiStatus                  = 207,
	IMUsed                       = 226,

	MultipleChoices              = 300,
	MovedPermanently             = 301,
	Found                        = 302,
	SeeOther                     = 303,
	NotModified                  = 304,
	UseProxy                     = 305,
	Reserved																					= 306,
	TemporaryRedirect            = 307,

	BadRequest                   = 400,
	Unauthorized                 = 401,
	PaymentRequired              = 402,
	Forbidden                    = 403,
	NotFound                     = 404,
	MethodNotAllowed             = 405,
	NotAcceptable                = 406,
	ProxyAuthenticationRequired  = 407,
	RequestTimeout               = 408,
	Conflict                     = 409,
	Gone                         = 410,
	LengthRequired               = 411,
	PreconditionFailed           = 412,
	RequestEntityTooLarge        = 413,
	RequestURITooLarge           = 414,
	UnsupportedMediaType         = 415,
	Requestedrangenotsatisfiable = 416,
	ExpectationFailed            = 417,
	UnprocessableEntity          = 422,
	Locked                       = 423,
	FailedDependency             = 424,
	UnorderedCollection          = 425,
	UpgradeRequired              = 426,
	PreconditionRequired         = 428,
	TooManyRequests              = 429,
	RequestHeaderFieldsTooLarge  = 431,
	RequestedHostUnavailable     = 434,
	Empty                        = 444,
	RetryWith                    = 449,
	UnavailableForLegalReasons   = 451,
	LogicError                   = 481, // не стандартный

	InternalServerError          = 500,
	NotImplemented               = 501,
	BadGateway                   = 502,
	ServiceUnavailable           = 503,
	GatewayTimeout               = 504,
	HttpVersionNotSupported      = 505,
	VariantAlsoNegotiates        = 506,
	InsufficientStorage          = 507,
	LoopDetected                 = 508,
	BandwidthLimitExceeded       = 509,
	NotExtended                  = 510,
	NetworkAuthenticationRequired = 511
}

/// Некоторые типы контента
enum ContentType : ushort
{
	PlainText = 0,
	TextXml   = 1,
	AppXml    = 2,
	WwwForm   = 3,
	Json      = 4,
	Html      = 5
}
string mimeString(ContentType type, bool withCharSet = true)
{
	string s;
	final switch(type)
	{
		case ContentType.PlainText : s = "text/plain"; break;
		case ContentType.TextXml   : s = "text/xml"; break;
		case ContentType.AppXml    : s = "application/xml"; break;
		case ContentType.WwwForm   : s = "application/x-www-form-urlencoded"; break;
		case ContentType.Json      : s = "application/json"; break;
		case ContentType.Html      : s = "text/html"; break;
	}
	if(withCharSet)
		s ~= "; charset=utf-8";
	return s;
}

/// Базовый класс ошибок HTTP протокола
class HttpStatusException : Exception
{
	const HttpStatus status;
	string request;
	string context;
	string responseBody;

	@property string message() { return cast(string)super.message; }

	this(HttpStatus status, string message = null, string request = null, string context = null,
						string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		import std.conv;
		super(message.length ? message : text(cast(ushort)status, " - ", to!string(status)),	file, line, next);
		this.status = status;
		this.responseBody = responseBody;
		this.request = request;
		this.context = context;
	}
	this(int status, string message = null, string request = null, string context = null,
						string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		this(cast(HttpStatus)status, message, request, context, file, line, next);
	}

	static T enforce(T)(T condition, HttpStatus statusCode, lazy string message = null,
																					string request = null, string context = null,
																					string file = __FILE__,	typeof(__LINE__) line = __LINE__)
	{
		static import std.exception;
		return std.exception.enforce(condition,
				new HttpStatusException(statusCode, message, request, context, file, line));
	}
	static T enforce(T)(T condition, int status, lazy string message = null,
																					string request = null, string context = null,
																					string file = __FILE__,	typeof(__LINE__) line = __LINE__)
	{
		static import std.exception;
		return std.exception.enforce(condition,
																															new HttpStatusException(status, message, request,
																																																							context, file, line));
	}

	HttpStatusException setRequest(string req) { this.request = req; return this; }
	HttpStatusException setContext(string str) { this.context = str; return this; }
	HttpStatusException setResponseBody(string str) { this.responseBody = str; return this; }
}

version(Server)
{
	alias HttpResponseCallback = void delegate(HttpRequest, HttpResponse);

	/// Структура HTTP запроса
	struct HttpRequest
	{
	private:
		HttpMethod _method;
		string[string] _headers;
		string _url;
		ushort _baseUrlLen;
		ContentType  _contentType;
		string _content;
	//	Object _data;
	//	HttpResponseCallback _responseCallback;

	public:
		this(HttpMethod method, string baseUrl = null)
		{
			this.method = method;
			this.baseUrl = baseUrl;
		}

	public:
		@property // method
		{
			HttpMethod method() const { return _method; }
			void method(HttpMethod value) { _method = value; }
		}
		@property string[string] headers()	{	return _headers;	}
		//void headers(string[string] value) { _headers = value; }
		void setHeader(string key, string value) { _headers[key] = value; }

		@property // url
		{
			string url() const { return _url; }
			void url(string value)
			{
				if(value.countUntil('?') == -1 && value.endsWith("/") == false)
					value ~= "/";
				_url = value;
			}
		}
		// http://www.site.ru:8080/root/
		@property // baseUrl
		{
			string baseUrl() const { return _url[0.._baseUrlLen]; }
			void baseUrl(string value)
			{
				if(value.length > 0 && value.endsWith("/"))
					value.length--;
				if(_url.startsWith(value) == false)
					_url = value ~ _url[_baseUrlLen..$];
				_baseUrlLen = cast(ushort)value.length;
			}
		}
		// /persons/list/
		@property // path
		{
			string path() const
			{
				long end = _url.countUntil('?');
				if(end < 0)
					end = _url.length;
				return _url[_baseUrlLen..end];
			}
			void path(string value)
			{
				if(value.startsWith("/") == false)
				value = "/" ~ value;
				long end = _url.countUntil('?');
				if(end < 0)
					end = _url.length;
				_url = _url[0.._baseUrlLen] ~ value ~ url[end..$];
			}
		}

		@property // query
		{
			string query() const
			{
				long start = _url.countUntil('?');
				if(start < 0)
					return null;
				return _url[start+1..$];
			}
			void query(string value)
			{
				if(value.length > 0)
					value = "?" ~ value;
				long start = _url.countUntil('?');
				if(start < 0)
						start = _url.length;
				_url = _url[0..start] ~ value;
			}
		}
		void addQuery(string key, string value)
		{
			CheckNullOrEmpty!key;
			long start = _url.countUntil('?');
			if(start < 0)
				_url ~= "?";
			else
				_url ~= "&";
			_url ~= key;
			_url ~= "=";
			_url ~= value;
		}

		@property // contentType
		{
			ContentType contentType() const { return _contentType; }
			void contentType(ContentType value) { _contentType = value; }
		}
		@property // content
		{
			string content() const { return _content; }
			void content(string value) { _content = value; }
		}

	//	/// Прикрепленные данные к запросу
	//	@property // data
	//	{
	//		Object data() const { return _data; }
	//		void data(Object value) { _data = value; }
	//	}

	//	/// Метод, выполнямый при ответе.
	//	@property // responseCallback
	//	{
	//		HttpResponseCallback responseCallback() const { return _responseCallback; }
	//		void responseCallback(HttpResponseCallback value) { _responseCallback = value; }
	//	}

		string toString() { return to!string(_method) ~ " " ~ _url; }
	}

	struct HttpResponse
	{
		HttpStatus status;
		string content;
	}
}
