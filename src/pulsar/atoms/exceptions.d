module pulsar.atoms.exceptions;

/*public*/ import std.exception;


/// Класс исключения проверки значения на null
class ValueNullException : Exception
{
	@safe pure nothrow this(string argName, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		super("Значение идентификатора [" ~ (argName is null ? "??" : argName) ~ "] не может быть null!", file, line, next);
	}
	@safe pure nothrow this(string argName, Throwable next, string file = __FILE__, size_t line = __LINE__)
	{
		super("Значение идентификатора [" ~ (argName is null ? "??" : argName) ~ "] не может быть null!", next, file, line);
	}
}
/// Класс исключения проверки значения на null или пустое значение
class ValueNullOrEmptyException : Exception
{
	@safe pure nothrow this(string argName, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		super("Значение идентификатора [" ~ (argName is null ? "??" : argName) ~ "] не может быть null или пустым значением!", file, line, next);
	}
	@safe pure nothrow this(string argName, Throwable next, string file = __FILE__, size_t line = __LINE__)
	{
		super("Значение идентификатора [" ~ (argName is null ? "??" : argName) ~ "] не может быть null или пустым значением!", next, file, line);
	}
}
/// Класс исключения проверки значения на вхождение в диапазон допустимых значений
class OutOfRangeException : Exception
{
	@safe pure nothrow this(string argName, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		super("Значение идентификатора [" ~ (argName is null ? "??" : argName) ~ "] выходит за пределы допустимого диапазона!", file, line, next);
	}
	@safe pure nothrow this(string argName, Throwable next, string file = __FILE__, size_t line = __LINE__)
	{
		super("Значение идентификатора [" ~ (argName is null ? "??" : argName) ~ "] выходит за пределы допустимого диапазона!", next, file, line);
	}
}
/// Класс исключения для ошибок логики использования объектов
class LogicError : Exception
{
	@safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		super(msg ? msg : "????", file, line, next);
	}
	@safe pure nothrow this(string msg, Throwable next, string file = __FILE__, size_t line = __LINE__)
	{
		super(msg ? msg : "????", next, file, line);
	}

	alias enforce = check;
	static bool check(bool check, lazy string errorMessage, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		if(check == false)
			throw new LogicError(errorMessage, file, line, null);
		return check;
	}
	static T check(T)(T exp, lazy string errorMessage, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		import pulsar.atoms.traits;

		static if(isValueType!T)
		{
			if(exp == T.init)
				throw new LogicError(errorMessage, file, line, null);
		}
		else
		{
			if(exp is null)
				throw new LogicError(errorMessage, file, line, null);
		}
		return exp;
	}
}
T checkLogic(T)(T exp, lazy string errorMessage, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
{
	return LogicError.check!T(exp, errorMessage, file, line, null);
}
/// Класс исключения для ошибок рассинхронизации клиента и сервера
class SyncError : Exception
{
	@safe pure nothrow this(string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		super("Произошла рассинхронизация клиента и сервера! Пожалуйста обновите страницу.", file, line, next);
	}
	@safe pure nothrow this(Throwable next, string file = __FILE__, size_t line = __LINE__)
	{
		super("Произошла рассинхронизация клиента и сервера! Пожалуйста обновите страницу.", next, file, line);
	}

	static T enforce(T)(T exp, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
	{
		import pulsar.atoms.traits;

		static if(isValueType!T)
		{
			if(exp == T.init)
				throw new SyncError(file, line, null);
		}
		else
		{
			if(exp is null)
				throw new SyncError(file, line, null);
		}
		return exp;
	}

}
