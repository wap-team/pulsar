module pulsar.atoms.decimal;

import std.math;
import std.conv : to, ConvOverflowException, text;
import std.format : format;
import std.traits: isInstanceOf;

/** Структура вещественного числа с фиксированной точкой */
struct Decimal
{
	private long _d;

	enum max = long.max/10;
	enum min = long.min/10;

	/// Число (мантисса)
	@property long number() const { return _d / 10; }
	/// Порядок(степень) делителя 10^-e (экспонента).
	@property ubyte exp() const { return cast(ubyte)(abs(_d) % 10); }

	this(long number, ubyte exp = 2)
	{
		if(number < Decimal.min || number > Decimal.max)
			throw new ConvOverflowException("Значение вне диапазона возможных значений!");
		if(exp > 9)
			throw new ConvOverflowException("Экспонента должна быть от 0 до 9!");
		_d = number * 10 + exp;
	}

	string toString() const
	{
		int n = 10^^exp;
		return to!string(number/n) ~ "." ~ to!string(number%n);
	}
}

/**	Шаблонная cтруктура вещественного числа с фиксированной точкой */
struct Fixed(int e)
{
	enum EXP = 10^^e;
	private long _d;

	/// Число (мантисса)
	@property long number() const { return _d; }
	/// Порядок(степень) делителя 10^-e (экспонента).
	@property ubyte exp() const { return e; }

	this(long number)
	{
		_d = number;
	}

	string toString() const
	{
		string s;
		long r = number%EXP;
		if(r > 0)
		{
			enum mask= text(".%0",e,"s");
			s = format(mask, r);
		}
		s = to!string(number/EXP) ~ s;
		return s;
	}
}

enum IsFixed(T) = isInstanceOf!(Fixed, T);


unittest
{
	trace("IsFixed");
	enforce(IsFixed!(Fixed!2));
	enforce(IsFixed!(Decimal) == false);
	enforce(IsFixed!(Fixed!3));
	enforce(IsFixed!(int) == false);


	trace("Fixed!2");
	enforce(Fixed!2(0).toString() == "0");
	enforce(Fixed!2(2).toString() == "0.02");
	enforce(Fixed!2(20).toString() == "0.20");
	enforce(Fixed!2(200).toString() == "2");
	enforce(Fixed!2(2000).toString() == "20");
	enforce(Fixed!2(20001).toString() == "200.01");


	trace("Fixed!3");
	enforce(Fixed!3(0).toString() == "0");
	enforce(Fixed!3(2).toString() == "0.002");
	enforce(Fixed!3(20).toString() == "0.020");
	enforce(Fixed!3(200).toString() == "0.200");
	enforce(Fixed!3(2000).toString() == "2");
	enforce(Fixed!3(20001).toString() == "20.001");
}
