module pulsar.atoms.iters;

import std.traits;
import pulsar.atoms.traits;

//-------------------------------------------------------------------
/** Итератор +
		Принцип использования шаблона:

	struct Iter!int
	{
		int delegate(int delegate(int) dg) const opApplyMeth;

		int opApply(int delegate(int) dg) const
		{
			return opApplyMeth ? opApplyMeth(dg) : -1;
		}
	}

	Примеры создания и использования:

		int[] arr = [1,2,3,4,5,6];

		trace("--- iter.opApplyMeth ---");
		Iter!int iter;
		iter.opApplyMeth = (int delegate(int) dg) const
		{
			int result = 0;
			foreach(i; arr)
				if((result = dg(i)) != 0)
					return result;
			return result;
		};
		foreach(i; iter)
			traceChars(i, ", ");
		trace();

		trace("--- iter = Iter!int(opApplyMeth ---");
		iter = Iter!int((int delegate(int) dg) const
		{
			int result = 0;
			foreach(i; arr)
				if((result = dg(i)) != 0)
					return result;
			return result;
		});
		foreach(i; iter)
			traceChars(i, ", ");
		trace();

		trace("--- iter = Iter!int(arr) ---");
		iter = Iter!int(arr);
		foreach(i; iter)
			traceChars(i, ", ");
		trace();

		trace("empty: ", iter.empty);
		trace("count: ", iter.count);
		trace("can find 4: ", iter.canFind(4));


		trace("--- iter = Iter!int(arr, (i)=> i % 2 == 0) ---");
		iter = Iter!int(arr, (i)=> i % 2 == 0);
		foreach(i; iter)
			traceChars(i, ", ");
		trace();

		trace("empty: ", iter.empty);
		trace("count: ", iter.count);
		trace("can find 5: ", iter.canFind(5));

		trace("--- iter = Iter!int(arr) -> foreach(index, i; iter) ---");
		iter = Iter!int(arr);
		foreach(index, i; iter)
			traceChars(index, " - ", i, ", ");
		trace();

*/
struct Iter(T)
{
	/// Метод итерирования
	int delegate(int delegate(T) dg) const opApplyMeth;

	// По идее быть не должно, нужно использовать std.algorithm и т.п.
	@property bool empty()
	{
		if(opApplyMeth is null)
			return true;
		foreach(i; this)
			return false;
		return true;
	}
	@property size_t count()
	{
		size_t res = 0;
		foreach(t; this)
			res++;
		return res;
	}
	size_t countUntil(const(T) item)
	{
		static if(isValueType!T == false)
			if(item is null)
				return -1;
		import pulsar.atoms.traits;
		foreach(i, t; this)
			if(isEqual(t, item))
				return i;
		return -1;
	}
	size_t countUntil(X)(X item) { return countUntil(cast(T)item); }
	bool canFind(const(T) item)
	{
		long x = countUntil(item);
		return x >= 0;
	}
	bool canFind(X)(X item) { return canFind(cast(T)item); }
	const(T) at(size_t pos)
	{
		foreach(i, t; this)
			if(i == pos)
				return t;
		return T.init;
	}

	this(int delegate(int delegate(T) dg) const opApplyMeth)
	{
		this.opApplyMeth = opApplyMeth;
	}
	this(TI)(TI iterable, bool delegate(T) filter = null) if(isIterable!TI)
	{
		alias TT = std.traits.ForeachType!TI;
		static assert(is(TT == T),
																"Тип перечисления " ~ fullyQualifiedName!TT ~
																" перечисляемого типа " ~ fullyQualifiedName!TI ~
																" не соответствует типу итератора " ~ 	fullyQualifiedName!T);
		this.opApplyMeth = (int delegate(T) dg) const
		{
				int result = 0;
				foreach(TT t; iterable)
					if(filter is null || filter(t) == true)
						if((result = dg(t)) != 0)
							return result;
				return 0;
		};
	}

	int opApply(scope int delegate(T) dg) const
	{
		return opApplyMeth ? opApplyMeth(dg) : -1;
	}
	int opApply(scope int delegate(size_t, T) dg) const
	{
		if(opApplyMeth is null)
			return  -1;

		size_t index = 0;
		return opApplyMeth((T t) => dg(index++, t));
	}
}
struct IterRef(T)
{
	int delegate(int delegate(ref T) dg) const opApplyMeth;

	@property bool empty() const
	{
		IterRef!T thiz = this;
		foreach(t; thiz)
			return false;
		return true;
	}

	int opApply(int delegate(ref T) dg) const
	{
		return opApplyMeth ? opApplyMeth(dg) : -1;
	}
}
struct Iter(TK,TV)
{
	int delegate(int delegate(TK,TV) dg) const opApplyMeth;

	int opApply(int delegate(TK,TV) dg) const
	{
		return opApplyMeth ? opApplyMeth(dg) : -1;
	}
}

