module pulsar.atoms.refs;

private import pulsar.atoms;
public import std.typecons : Rebindable;
import std.traits;

/// Структура ссылки, заданной указателем
struct Ref(T)
{
	T* ptr;

public:
	@disable this();
	@disable this(typeof(null));
	//@disable void opAssign(ref Ref!T);

	this(T* ptr)  { this.ptr = ptr; }
	void opAssign(T* ptr) { this.ptr = ptr; }

	@property ref inout(T) get() inout
	{
		enforce(isNull == false, "Ref!" ~ T.stringof ~ " is null");
		return *this.ptr;
	}

	@property bool isNull() const { return ptr is null; }
	@property bool notNull() const { return !isNull; }

	alias get this;

}
/// Структура типизированного указателя
struct Ptr
{
	const void* ptr;
	const TypeInfo type;

	this(T)(T* ptr)
	{
		this.ptr = ptr;
		this.type = typeid(T);
	}

	@property bool isEmpty() const { return ptr is null; }

	E to(E)()
	{
		if(typeid(E) != type)
			throw new Exception("Указатель [" ~ type.toString() ~
																							"] не может быть приведен к типу [" ~
																							E.stringof	~ "]!");
		if(ptr is null)
			return E.init;
		return *(cast(E*)ptr);
	}
}
/// Структура указателя на функцию
struct FuncPtr
{
private:
	void* ptr;
	TypeInfo ti;

public:
	@property bool isNull() const { return ptr is null; }
	static FuncPtr opCall(T)(T* func) //if(isFunctionPointer!T)
	{
		FuncPtr fp;
		fp.ptr = func;
		fp.ti = typeid(typeof(func));
		return fp;
	}

	E to(E)() //if(is(E == function))
	{
		if(ptr is null)
			return null;
		//trace(typeid(E), ti);
		if(typeid(E) != ti)
			throw new Exception("Указатель функции [" ~ ti.toString() ~
																							"] не может быть приведен к типу [" ~
																							E.stringof	~ "]!");
			return cast(E)ptr;
	}
}

// Структура делегата.
struct Delegate
{
	void* ctxPtr;
	void* funcPtr;

	bool isNull() const { return funcPtr is null; } // ctxPtr is null
	static Delegate opCall(T)(T func) if(is(T == delegate))
	{
		assert(func);
		Delegate s;
		s.ctxPtr = func.ptr;
		s.funcPtr = func.funcptr;
		return s;
	}
}
/// Структура указателя на делегат
struct DelegatePtr
{
private:
	void* objPtr;
	void* funcPtr;
	TypeInfo ti;

public:
	@property bool isNull() const { return objPtr is null; }
	static DelegatePtr opCall(T)(T func) if(is(T == delegate))
	{
		assert(func);
		DelegatePtr s;
		s.objPtr = func.ptr;
		s.funcPtr = func.funcptr;
		s.ti = typeid(T);
		return s;
	}

	E to(E)() if(is(E == delegate))
	{
		if(funcPtr is null)
			return null;
		if(typeid(E) != ti)
			throw new Exception("Делегат [" ~ ti.toString() ~
																							"] не может быть приведен к типу [" ~
																							E.stringof	~ "]!");
			E d;
			d.ptr = objPtr;
			auto x = cast(void*)(&(d.funcptr));
			x = funcPtr;
			return d;
	}
	auto call(E, Args...)(Args args) if(is(E == delegate))
	{
		if(funcPtr is null)
			return null;
		if(typeid(typeof(E.funcptr)) != ti)
			throw new Exception("Делегат [" ~ ti.toString() ~
																							"] не может быть приведен к типу [" ~
																							E.stringof	~ "]!");
		E d;
		d.ptr = objPtr;
		d.funcptr = cast(typeof(E.funcptr))funcPtr;
		static if(is(ReturnType!E == void))
		{
			d(args);
			return null;
		}
		else
			return d(args);
	}
}

/// Структура неизменяемой ссылки
/// Саму ссылку менять нельзя, но можно менять ее элементы
struct HeadConst(T)
{
	private T _t;
	@property T _get() { return _t; }
	@property bool isNull() const { return _t is null; }

	this(T value) { CheckNull!value; _t = value; }
	alias _get this;
}
unittest
{
	int[] arr = [1,20,30];
	auto r = HeadConst!(int[])(arr);
	static assert(__traits(compiles,mixin("r[0] = 10")));
	static assert(__traits(compiles,mixin("r.length > 1")));
	static assert(__traits(compiles,mixin("r[2]")));
	static assert(!__traits(compiles,mixin("r ~= 40")));
	static assert(!__traits(compiles,mixin("r = arr;")));

	string[int] arrr = [10:"111",20:"222",30:"300"];
	auto rr = HeadConst!(string[int])(arrr);
	static assert(__traits(compiles,mixin("rr[10] = \"100\"")));
	static assert(__traits(compiles,mixin("rr[20]")));
	rr._get().remove(10);
	static assert(!__traits(compiles,mixin("rr = arrr;")));
	static assert(__traits(compiles,mixin("rr.get(20,null)")));

	class A { int a = 10; }
	auto rrr = HeadConst!A(new A());
	static assert(__traits(compiles, mixin("rrr.a")));
	static assert(__traits(compiles, mixin("rrr.a = 100")));
	A a = new A();
	static assert(!__traits(compiles, mixin("rrr = a")));
}

/// Ссылку изменить можно, но объект остается константным.
struct TailConst(TT, bool checkNull = false) if (is(TT == class) || is(TT == interface))
{
	alias T = Unqual!TT;
	alias CT = const(T);
	private union
	{
		T _original;
		CT _consted;
	}
	void opAssign(CT another) @trusted pure nothrow
	{
		_original = another is CT.init ? CT.init : *(cast(T*)&another);
	}
	void opAssign(TailConst!TT another) @trusted pure nothrow
	{
		_original = another._original;
	}

	this(T initializer) @safe pure nothrow
	{
		opAssign(initializer);
	}
	this(CT initializer) @safe pure nothrow
	{
		opAssign(initializer);
	}

	@property CT consted() const @trusted pure
	{
		static if(checkNull)
			return enforce(_consted);
		else
			return _consted;
	}
	@property T unqual() @trusted pure
	{
		static if(checkNull)
			enforce(_original);
		return _original;
	}
	CT opCall() const @trusted pure	{ return consted; }
	alias opCall this;

	@property bool isNull() const @trusted pure nothrow { return _original is null; }
	@property bool notNull() const @trusted pure nothrow { return _original !is null; }
}
TailConst!T tailConst(T)(T t) { return TailConst!T(t); }
TailConst!(T, true) tailConstCheck(T)(T t) { return TailConst!(T,true)(t); }

/// Структура ссылки на массив.
/// Массив не может быть перезаписан или изменен, но его элементы не константные.
/// T - тип элемента массива
struct ConstArray(T) //if(isArray!T)
{
	private T[] _t;
	@property T[] _get() { return _t; }
	this(inout T[] value) {_t = cast(T[])value; }
	// BUG: не проходит проверку IsInputRange
	alias _get this;
	//T opIndex(size_t index) {	return _t[index]; }
	// Блокировка записи элемента массива
	@disable void opIndexAssign(size_t index, T val);

	int opApply(int delegate(ref T) dg)
	{
		int result;
		for(size_t i = 0; i < _t.length; i++)
		{
			result = dg(_t[i]);
			if(result)
				break;
		}

		return result;
	}
	int opApply(int delegate(size_t index, ref T) dg)
	{
		int result;
		for(size_t i = 0; i < _t.length; i++)
		{
			result = dg(i, _t[i]);
			if(result)
				break;
		}

		return result;
	}
}
unittest
{
	int[] arr = [1,20,30];
	auto r = ConstArray!int(arr);
	//r[0] = 10;
	static assert(!__traits(compiles,mixin("r[0] = 10")));
	static assert(__traits(compiles,mixin("r.length > 1")));
	static assert(__traits(compiles,mixin("r[2]")));
	static assert(!__traits(compiles,mixin("r ~= 40")));
	static assert(!__traits(compiles,mixin("r = arr;")));

//	string[int] arrr = [10:"111",20:"222",30:"300"];
//	auto rr = ConstArray!(string[int])(arrr);
//	static assert(!__traits(compiles,mixin("rr[10] = \"100\"")));
//	static assert(__traits(compiles,mixin("rr[20]")));
//	static assert(!__traits(compiles,mixin("rr.remove(10);")));
//	static assert(!__traits(compiles,mixin("rr = arrr;")));
//	static assert(__traits(compiles,mixin("rr.get(20,null)")));
}

/// Объектная обёртка для структур
class Objected(T) if (is(T == struct))
{
	T data;
	alias data this;

	this(){}
	this(T data) { this.data = data; }
}
