module pulsar.atoms;

version(PulsarAtoms)
{

}
version(Server)
{
	public import std.traits : Unqual;

	public import pulsar.atoms.oid;
	public import pulsar.atoms.signals;
	public import pulsar.atoms.traits : isEqual;
	public import pulsar.server.log;
	public import pulsar.server.security;
	public import pulsar.server.archiver : IArchivable;
	public import pulsar.reflection.attributes;
	public import pulsar.serialization.attributes;
	public import pulsar.atoms.proxies : IProxy, proxy, noproxy;
	public import pulsar.atoms.iters;

}
import std.traits : Unqual, isArray;

public import std.array : empty;
public import std.conv : to, text;
public import std.datetime : Clock, SysTime, Date, DateTime, TimeOfDay, Duration, dur;
public import std.datetime.stopwatch;
public import std.exception;
public import std.math : isNaN;

public import pulsar.atoms.arrays;
public import pulsar.atoms.datetime;
public import pulsar.atoms.enums;
public import pulsar.atoms.strings;
public import pulsar.atoms.exceptions;
public import pulsar.atoms.refs;

/**** trace ******************************************/
version(PulsarAtoms)
{
	void trace(T...)(T args)
	{
		import std.stdio;
		writeln(args);
		stdout.flush();
	}
	void traceChars(T...)(T args)
	{
		import std.stdio;
		write(args);
		stdout.flush();
	}
}
version(Server)
{
	alias trace = Logger.trace;
	alias traceChars = Logger.traceChars;
	alias print = Logger.print;
	alias println = Logger.println;


	/// Обобщенные виды данных
	enum DataKind : byte
	{
		unknown = 0, // неизвестный
		number   , // число
		perc     , // процент
		currency , // валюта (деньги)
		date     , // data
		time     , // время
		boolean  , // булево значение
		text     , // текстовое значение
		image      // картинка
	}
}

	//-------------------------------------------------------------------
/// Метод проверки значений на null или пустое значение (для типов значений)
void CheckNull(T...)(string file = __FILE__, size_t line = __LINE__) const
{
	import pulsar.atoms.traits: isValueType, TypeOf;
	foreach(i,val; T)
		static if(isValueType!(TypeOf!(T[i])))
			enforce(val != T[i].init, new ValueNullException(T[i].stringof, file, line));
		else
			enforce(val !is null, new ValueNullException(T[i].stringof, file, line));
}
/// Метод проверки значений на null или пустое значение (для массивов и строк)
void CheckNullOrEmpty(T...)(string file = __FILE__, size_t line = __LINE__) const
{
	import std.traits;
	foreach(i,val; T)
	{
		enforce(val !is null, new ValueNullException(T[i].stringof, file, line));
		static if (isArray!(typeof(val)) || isAssociativeArray!(typeof(val)))
			if(val.length == 0)
				throw new ValueNullOrEmptyException(T[i].stringof, file, line);
	}
}
/// Метод проверки значений на null или пустое значение (для массивов и строк)
/// Выбрасывает исключение LogicError
void LogicCheckEmpty(T...)(string file = __FILE__, size_t line = __LINE__)
{
	import std.traits;
	enum msg = "Значение не может быть пустым!";
	foreach(i,val; T)
	{
		enforce(val !is null, new LogicError(msg, file, line)); //T[i].stringof)
		static if (isArray!(typeof(val)) || isAssociativeArray!(typeof(val)))
			if(val.length == 0)
				throw new LogicError(msg, file, line);
	}
}
//-------------------------------------------------------------------
Unqual!T unqual(T)(auto ref T t) if(!isArray!T)
{
	alias UT = Unqual!T;
	static if(__traits(compiles, t is null))
		return t is null ? null : (*(cast(UT*)(&t))); //cast(Unqual!T)t;
	else
		return t == T.init ? UT.init : (*(cast(UT*)(&t))); //cast(Unqual!T)t;
}
auto unqual(T)(T[] t)
{
	return cast(Unqual!T[])t;
}
auto consted(T)(auto ref T t)
{
	return cast(const(T))t;
}
//-------------------------------------------------------------------
bool inherit(const(ClassInfo) cl, const(ClassInfo) parent)
{
	CheckNull!(cl, parent);
	for(ClassInfo c = cast(ClassInfo)cl; c; c = cast(ClassInfo)c.base)
		if(c is parent)
			return true;
	return false;
}
bool implement(const(ClassInfo) cl, const(TypeInfo_Interface) interFace)
{
	CheckNull!(cl, interFace);
	for(ClassInfo c = cast(ClassInfo)cl; c; c = cast(ClassInfo)c.base)
		foreach(Interface i; c.interfaces)
			if(i.classinfo.name == interFace.info.name)
				return true;
	return false;
}
bool implement(T)(const(ClassInfo) cl) if(is(T == interface))
{
	return implement(cl, typeid(T));
}
//--------------------------------------------------------------------
mixin template AliasThis(alias field, string prop)
{
	alias T = typeof(this);
	alias TA = Unqual!(typeof(field));

	mixin("public @property const(TA) " ~ prop ~"() const " ~
							"{ if(field is null) throw new Exception(\"alias this null!\");" ~
							" return field; }");
	mixin("alias " ~ prop ~ " this;");

//	public X opCast(X:Unqual!T)() const nothrow if(is(X == Unqual!T))
//	{
//		auto b = this;
//		return *(cast(T*)(&b));
//	}

	// opCast нужны для обхода Issue 6777 - alias this disables casting for classes
	public X opCast(X=TA)() const nothrow if(is(Unqual!X == TA))
	{
		return *(cast(X*)(&field));
	}
	public X opCast(X)() const nothrow if(!is(Unqual!X == TA)) //&& !is(X == inout(X))
	{
		const(Object) b = this;
		return cast(X)b;
	}

}
unittest
{
	class A
	{
		@property int val() { return 1; }
	}
	interface I {}
	interface II {}
	class B : I
	{
		private A _a;
		//@property inout(A) aaa() inout { return _a; }
		mixin AliasThis!(_a,"aaa");
		//alias aaa this;
		this(A a) { _a = a; }
	}

	B b = new B(new A());
	assert(cast(const(B))b);
	assert(cast(A)b);
	assert(cast(const(A))b);

	const(B) cb = new B(new A());
	assert(cast(B)cb);
	assert(cast(A)cb);
	assert(cast(const(A))b);
	assert(cast(I)b);
	auto ii = cast(II)b;
	assert(ii is null);

	assert(cast(Object)b);
	assert(cast(const(Object))b);
	assert(cast(Object)cb);
	assert(cast(const(Object))cb);
	assert(cast(I)cb);
	assert(cast(II)cb is null);
}
mixin template OpApply(T, alias iter)
{
	int opApply(int delegate(const(T)) dg) const
	{
		int result = 0;
		foreach(item; iter)
		{
			result = dg(item);
			if(result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, const(T)) dg) const
	{
		int result = 0;
		size_t i = 0;
		foreach(item; iter)
		{
			result = dg(i,item);
			if(result)
				break;
			i++;
		}
		return result;
	}

}
//--------------------------------------------------------------------
Object asObj(T)(T exp) { return cast(Object)exp; }

			pragma(inline, true):
/// Шаблонная функция, определяющая является ли значение значением по умолчанию (init)
bool isNull(T)(auto ref T val)
{
	import pulsar.atoms.traits;
	static if(isValueType!T)
		return val == T.init;
	else static if(isArray!T)
	 return val.length == 0;
	else
		return val is null;
}
bool notNull(T)(auto ref T val)
{
	import pulsar.atoms.traits;
	static if(isValueType!T)
		return val != T.init;
	else static if(isArray!T)
	 return val.length > 0;
	else
		return val !is null;
}
T ifNull(T)(T exp, lazy T ifnull)
{
	static if(isArray!T)
		return exp.length > 0 ? exp : ifnull;
	else
		return exp ? exp : ifnull;
}
T iif(T)(bool exp, lazy T True, lazy T False = T.init)
{
	return exp ? True : False;
}

