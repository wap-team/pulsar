module pulsar.atoms.enums;

/// Метод проверки значения перечисления (enum) на установленный флаг
bool isSet(T, TT)(T target, TT flag) pure
{
	return (target & flag) == flag;
}
/// Метод проверки значения перечисления (enum) на установленный флаг
bool notSet(T, TT)(T target, TT flag) pure
{
	return (target & flag) != flag;
}
/// Метод расчета установки или снятия флага значения перечисления (enum)
T calcFlag(T, TT)(T target, TT flag, bool set = true) pure
{
	if(set)
		return target | flag;
	return cast(T)(target & (~cast(int)(flag)));
}
/// Метод установки флага значения перечисления (enum)
T setFlag(T, TT)(T target, TT flag)
{
	return cast(T)(target | flag);
}
/// Метод снятия флага значения перечисления (enum)
T clearFlag(T, TT)(T target, TT flag) pure
{
	return cast(T)(target & (~cast(int)(flag)));
}
/// Метод парсинга строки значения перечисления.
/// Поддерживает комбинацию флагов (через |)
E parseFlag(E)(string str)
{
 E e = E.init;
	import std.string : split;
	import std.conv: to;
	foreach(string s; str.split('|'))
	 e |= to!E(s);
	return e;
}

import std.traits;
T setBit(T)(T number, ubyte bitIndex) if(isIntegral!T)
{
	return cast(T)(number | (1 << bitIndex));
}
T clearBit(T)(T number, ubyte bitIndex) if(isIntegral!T)
{
	return cast(T)(number & ( ~(1 << bitIndex)));
}
bool isBit(T)(T number, ubyte bitIndex) if(isIntegral!T)
{
	return (number & (1 << bitIndex)) != 0;
}

//------------

@Named("Структура")
struct Named
{
	string name;
}
template named(alias e)
{
	//pragma(msg, e);
	enum IsNamed(alias x) = is(typeof(x) == Named);

	import std.meta;
	enum Attrs = Filter!(IsNamed, __traits(getAttributes, e));
	//pragma(msg, Attrs);

	static if(Attrs.length > 0)
		enum named = Attrs[0].name;
	else
		enum named = e.stringof;
}
string named(E)(E e) if(is(E == enum))
{
	import std.traits;
	foreach(en; __traits(allMembers, E))
	{
		if(__traits(getMember, E, en) == e)
			return named!(__traits(getMember, E, en));
	}
	import std.conv;
	return to!string(e);
}

@EnumTag(0)
struct EnumTag
{
	long tag;
}
template enumTag(alias e)
{
	//pragma(msg, e);
	enum IsTagged(alias x) = is(typeof(x) == EnumTag);

	import std.meta;
	enum Attrs = Filter!(IsTagged, __traits(getAttributes, e));
	//pragma(msg, Attrs);

	static if(Attrs.length > 0)
		enum enumTag = Attrs[0].tag;
	else
		enum enumTag = -1; //e.stringof;
}
string enumTag(E)(E e) if(is(E == enum))
{
	import std.traits;
	foreach(en; __traits(allMembers, E))
	{
		if(__traits(getMember, E, en) == e)
			return enumTag!(__traits(getMember, E, en));
	}
	import std.conv;
	return -1;
}


unittest
{
	enum Option : ubyte
	{
		None = 0,
		Imperative = 1,
		IsMultiValue = 2,
		PropsValuesGroup = 32,
		NonStandart = 64,
		Special = 128
	}
	Option op;
	assert(op == Option.None);
	op = op.setFlag(Option.IsMultiValue);
	assert(op == Option.IsMultiValue);
	assert(op.isSet(Option.IsMultiValue));
	op = op.setFlag(Option.NonStandart | Option.Special);
	assert(op == (Option.IsMultiValue |
																						Option.NonStandart |
																						Option.Special));
	assert(op.isSet(Option.IsMultiValue |
																									Option.NonStandart |
																									Option.Special));
	op = op.clearFlag(Option.IsMultiValue | Option.Special);
	assert(op == Option.NonStandart);

}


// Манипуляция битами
// https://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit
