module pulsar.atoms.oid;

import std.traits : InterfacesTuple;
import std.typetuple : anySatisfy;
public import std.uuid;

//import pulsar.atoms;
import pulsar.serialization.attributes;
import pulsar.atoms.proxies;

alias OID = const(UUID);

UUID fromData(const(ubyte[]) data)
{
	assert(data.length == 16);
	ubyte[16] oid = data;
	return UUID(oid);
}

/// Длина строки с OID
enum OIDStringLength = OID.init.toString().length;

/// OID, который используется в качестве NULL значения
enum OID_Null = OID("0000000f-06d4-479b-83a4-203ba76bf020");
bool isNullOrEmpty(OID oid) { return oid.empty || oid == OID_Null; }

bool isOIDString(string s)
{
	// TODO : Проверка на строку c UUID
	return s.length == OIDStringLength;
}

//---------
/// Интерфейс объекта, содержащего идентификатор объекта.
interface IOIDObject
{
	/// Свойство, возвращающее идентификатор объекта.
	@noproxy @property ref OID oid() const;
}

/// Класс основной реализации IOIDObject
class OIDObject : IOIDObject
{
	private OID _oid;

public:
	@noproxy @property ref OID oid() const { return _oid; }

	this(OID oid = OID.init)
	{
		if(_oid.empty)
			_oid = oid.empty ? randomUUID : oid;
	}
}

/** Использование этой функции - преступление!*/
void setOid_TODEL(IOIDObject obj, OID oid)
{
	import std.exception;
	enforce(obj, "obj is null");
	// Stub нужен чтобы обойти баг - нельзя создать указатель на метод с ref
	class Stub
	{
		UUID a;
		@property ref UUID oid(){  return a;}
	}

	alias FT = typeof(&Stub.init.oid);
	auto cft = cast(FT)(&obj.oid);
	cft() = cast(OID)oid;
}


/// Определяет, наследует ли тип интерфейс IOIDObject.
/// Возвращает true для alias this типов
template IsIOIDObject(T)
{
import pulsar.atoms.traits : IsInherits;
	enum IsIOIDObject = IsInherits!(T, IOIDObject);
}

/// Определяет, наследует ли тип интерфейс IOIDObject явно.
/// Возвращает false для alias this типов
template IsImplIOIDObject(T)
{
	import pulsar.atoms.traits : IsImplInherits;
	enum IsImplIOIDObject = IsImplInherits!(T,IOIDObject);
}

