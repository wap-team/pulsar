module pulsar.atoms.math;

import std.traits;
import std.math;

static const uint[] primes = [5,11,23,47,97,127,197,251,
																														397,797,1597,3203,6421,12853,25717,32749,51437,65521,
																														102877,205759,411527,823117,1646237,3292489,6584983,13169977,
																														26339969,52679969,105359939,210719881,421439783,842879579,
																														1685759167,2147483629,3371518343,4294967291];


template MaxPrime(T)
{
	static if(is(T == byte))
		enum MaxPrime = cast(T)127;
	else static if(is(T == ubyte))
		enum MaxPrime = cast(T)251;
	else static if(is(T == short))
		enum MaxPrime = cast(T)32749;
	else static if(is(T == ushort))
		enum MaxPrime = cast(T)65521;
	else static if(is(T == int))
		enum MaxPrime = 2147483629;
	else static if(is(T == uint))
		enum MaxPrime = cast(T)4294967291;
	else static if(is(T == long))
		enum MaxPrime = 9223372036854775783L;
	else static if(is(T == ulong))
		enum MaxPrime = 18446744073709551557UL;
	else
		static assert(false, "Максимальное простое число для типа " ~ T.stringof ~ " не определено!");
}

bool isPrime(T)(T n) pure if(isIntegral!T)
{
	if((n & 1) == 0)
		return n == 2;

	T max = cast(T)std.math.sqrt(cast(float)n);

	// i*i <= n
	for (T i = 3; i <= max; i += 2)
		if ((n % i) == 0)
			return false;
	return true;
}

T getNextPrime(T)(T n) if(isIntegral!T)
{
	T i = (cast(T)(n+1)) | 1;
	for (; i < T.max; i += 2)
		if(isPrime (i))
			return i;
	if(i == T.max)
		throw new Exception("Следующее простое число больше максимального значения типа!");
	return n;
}

T getHashPrime(T)(T n) if(isIntegral!T)
{
	static if(T.sizeof <= 4)
	{
		foreach (i; primes)
			if(i > T.max)
				throw new Exception("Следующее простое число больше максимального значения типа!");
			else if (n <= i)
				return cast(T)i;
		throw new Exception("Следующее простое число больше максимального значения типа!");
	}
	else
		return getNextPrime(x*2);
}

/// Округдяет вещественное число до указанной точности (знаков после запятой)
T roundx(T)(T value, ubyte precision = 2) if(isFloatingPoint!T)
{
	ulong multi = pow(10,precision);
	auto x = round(value*multi);
	return (cast(T)x)/multi;
}

