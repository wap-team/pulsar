module pulsar.atoms.arrays;

public import std.array;
import std.range;
import std.traits;

bool has(K, V)(inout(V[K]) aa, K key)
{
	return (key in aa) !is null;
}
bool has(K, V)(inout(V[K])* aa, K key)
{
	return (*aa).get(key, defaultValue);
}
//-------------------------------------------------------------------

/// Преобразует строку данных АА массива в АА массив
auto parseAA(TKey, TValue = TKey)(string aaData)
{
	import std.conv : to;
	import std.algorithm : splitter, map;
	import std.array : assocArray;
	import std.typecons : tuple;
	import std.string : strip;
	static if(is(TKey == string) && is(TValue == string))
		return to!(string[string])(aaData);
	else
		return aaData.strip("[]")
				.splitter(',')
				.map!(a => a.splitter(':'))
				.map!(a => tuple(to!TKey(a.front.strip("\"")),
																					to!TValue(a.back.strip("\""))))
				.assocArray;
}

/// Перемещает указанный элемент в начало массива (на месте)
bool toFront(T : Object)(T[] arr, T item)
{
	size_t pos;
	for(; pos < arr.length; pos++)
		if(arr[pos] is item)
			break;
	if(pos == arr.length)
		return false;
	for(;pos > 0; pos--)
	{
		T t = arr[pos-1];
		arr[pos-1] = arr[pos];
		arr[pos] = t;
	}
	return true;
}

/// Удаляет элемент массива в указанной позиции
void removeAt(A)(auto ref A arr, size_t pos) if(isArray!A)
{
	import pulsar.atoms : unqual;
	auto ua = arr.unqual;
	import std.algorithm : remove;
	ua = ua.remove(pos);
	arr = ua;
}

//-------------------------------------------------------------------
/// Добавляет метод добавляения к Appender и RefAppender, с поддержкой цепочечных вызовов.
ref T add(T, U)(ref T appender, U item) if(is(T == Appender!U) || is(T == RefAppender!U))
{
	appender.put(item);
	return appender;
}
/// Добавляет метод форматированного добавляения к Appender и RefAppender, с поддержкой цепочечных вызовов.
ref T addF(T, Args...)(ref T appender, Args items) if(is(T == Appender!string) || is(T == RefAppender!string))
{
	appender.put(format(items));
	return appender;
}
/// Добавляет метод форматированного добавляения к Appender и RefAppender, с поддержкой цепочечных вызовов.
ref T add(T, Args...)(ref T appender, Args items) if(is(T == Appender!string) || is(T == RefAppender!string))
{
	static import std.conv;
	foreach(s; items)
		appender.put(std.conv.to!string(s));
	return appender;
}
//--------------------------------------------------------------------

/// Возвращает первый элемент в range
auto first(Range)(Range range) if(isIterable!Range)
{
	foreach(x; range)
		return x;
	return ForeachType!Range.init;
}
/// Возвращает первый элемент в range, соответствующий предикату
auto first(alias eqFun, Range)(Range range) if(isIterable!Range)
{
	foreach(x; range)
		if(eqFun(x))
			return x;
	return ForeachType!Range.init;
}


Range reverseInPlace(Range)(Range r)
if (isBidirectionalRange!Range && !isRandomAccessRange!Range && hasSwappableElements!Range)
{
	import std.algorithm : reverse;
	reverse(r);
	return r;
}

Range reverseInPlace(Range)(Range r)
if (isRandomAccessRange!Range && hasLength!Range)
{
	import std.algorithm : reverse;
	reverse(r);
	return r;
}

