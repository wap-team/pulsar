module pulsar.atoms.traits;

import std.traits;

public import std.typetuple : Alias;
//alias Alias(alias Symbol) = Symbol;

/// Раскрывает параметры как кортеж
template AsTuple(T...)
{
	alias T AsTuple;
}

/// Возвращает тип переданного символа
template TypeOf(alias symbol)
{
	static if(is(symbol))
		alias TypeOf = symbol;
	else
		alias TypeOf = typeof(symbol);
}

/// Возвращает базовый класс для указанного типа
template BaseClass(A)
{
	/*static if (is(A == Object))
		alias BaseClass = A;
	else*/ static if (is(A P == super))
		alias P[0] BaseClass;
	else
		static assert(0, A.stringof ~ " is not a class or interface");
}

/// Определяет, наследует ли тип интерфейс или класс.
/// Возвращает true для alias this типов
template IsInherits(T, I)
{
	//pragma(msg, T);
	static if(is(T: I))
		enum IsInherits = true;
	else
	{
		enum AT = __traits(getAliasThis, T);
		static if(AT.length > 0)
		{
			enum AliasThisName = AT[0];
			alias meth = Alias!(__traits(getMember, T, AliasThisName));
			static if(isSomeFunction!meth)
			{
				//pragma(msg, "xxx", ReturnType!(meth));
				enum IsInherits = IsInherits!(Unqual!(ReturnType!(meth)), I);
			}
			else
				enum IsInherits = IsInherits!(Unqual!(typeof(meth)), I);
		}
		else
			enum IsInherits = false;
	}
}

/// Определяет, наследует ли тип интерфейс IOIDObject явно.
/// Возвращает false для alias this типов
template IsImplInherits(T, I)
{
import std.meta;
	enum isImplInherits(T) = is(T == I);
	enum IsImplInherits = anySatisfy!(isImplInherits, InterfacesTuple!T);
}

/// Возвращает тип переданного символа
template IsConst(alias symbol)
{
	static if(isCallable!symbol)
		enum IsConst = is(FunctionTypeOf!(symbol) == const);
	else	static if(is(symbol))
		enum IsConst = is(symbol == const);
	else
		enum IsConst = is(typeof(symbol) == const);
}

/// Определяет, является ли символ свойством
template IsProperty(alias f)
{
	static if(isSomeFunction!f &&
											(functionAttributes!(f) & FunctionAttribute.property))
		enum IsProperty = true;
	else
		enum IsProperty = false;
}

/// Определяет, имеет ли символ уровень защиты public
template IsPublic(alias f)
{
	enum IsPublic = __traits(getProtection, f) == "public";
}
/// Определяет, имеет ли символ уровень защиты public
template IsPublicOrProtected(alias f)
{
	enum IsPublicOrProtected = __traits(getProtection, f) == "public" ||
																												__traits(getProtection, f) == "protected";
	//pragma(msg, fullyQualifiedName!f, "- ", IsPublicOrProtected);
}

/// Определяет, является ли тип классом
template isClass(T)
{
	enum isClass = is(T == class);
}

/// Шаблон, определяющий является ли тип примитивным (базовые типы, перечисления)
template isPrimitiveType(T)
{
	enum bool isPrimitiveType = isBasicType!T || is(T == enum); // || isSomeString!T;
//enum bool isPrimitive = is(T == bool) ||
//																				is(T == byte) ||
//																				is(T == cdouble) ||
//																				//is(T == cent) ||
//																				is(T == cfloat) ||
//																				is(T == char) ||
//																				is(T == creal) ||
//																				is(T == dchar) ||
//																				is(T == double) ||
//																				is(T == float) ||
//																				is(T == idouble) ||
//																				is(T == ifloat) ||
//																				is(T == int) ||
//																				is(T == ireal) ||
//																				is(T == long) ||
//																				is(T == real) ||
//																				is(T == short) ||
//																				is(T == ubyte) ||
//																				//is(T == ucent) ||
//																				is(T == uint) ||
//																				is(T == ulong) ||
//																				is(T == ushort) ||
//																				is(T == wchar);
}

/// Шаблон, определяющий является ли тип типом значения
template isValueType(T)
{
	enum isValueType = isScalarType!T || is(T == enum) || is(T == struct);
}

/// Шаблон, определяющий тип элемента массива
template ArrayElementType(T : T[])
{
	alias T ArrayElementType;
}

/// В отличии от Unqual, возвращает чистые типы и для типов параметров в массивах и словарях
template DeepUnqual(T)
{
	static if(isSomeString!T)
		alias DeepUnqual = T;
	else static if(is(T U == U[]))
	{
		alias QU = DeepUnqual!U;
		alias DeepUnqual = QU[];
	}
	else static if(is(T == V[K], V, K))
	{
		alias QV = DeepUnqual!V;
		alias QK = DeepUnqual!K;
		alias DeepUnqual = QV[QK];
	}
	else
		alias DeepUnqual = Unqual!T;
}
unittest
{
	static assert(is(QClear!(const int[]) == int[]));
	static assert(is(QClear!(const(Object)[const(int)]) == Object[int]));
	static assert(is(QClear!(const(Object)[][const(int)]) == Object[][int]));
	static assert(is(QClear!(Object[const(int)[]]) == Object[int[]]));
}

/// Проверяет на значение атрибута UDA
template hasAttributeValue(alias T,alias name)
{
	alias hasAttributeValueImpl!(name, __traits(getAttributes, T)) hasAttributeValue;
}
template hasAttributeValueImpl(alias name,T...)
{
	static if(T.length > 0)
	{
		static if(is(typeof(T[0]) == typeof(name)) && T[0] == name)
			enum hasAttributeValueImpl = true;
		else
			alias hasAttributeValueImpl!(T[1..$], name) hasAttributeValueImpl;
	}
	else
		enum hasAttributeValueImpl = false;

}
/// Проверяет на тип атрибута UDA
template hasAttributeType(alias T,AT)
{
	static if(__traits(compiles, __traits(getAttributes, T)))
		alias hasAttributeTypeImpl!(AT, __traits(getAttributes, T)) hasAttributeType;
	else
		enum hasAttributeType = false;
}
template hasAttributeTypeImpl(AT ,T...)
{
	static if(T.length > 0)
	{
		static if(is(typeof(T[0]) : AT))
			enum hasAttributeTypeImpl = true;
		else
			alias hasAttributeTypeImpl!(AT,T[1..$]) hasAttributeTypeImpl;
	}
	else
		enum hasAttributeTypeImpl = false;

}
/// Проверяет на значение атрибута UDA (и наследованные классы тоже)
template hasInheritAttributeValue(T,alias name) if (is(T == class))
{
	static if(is(T == Object))
		enum hasInheritAttributeValue = false;
	else static if(hasAttributeValue!(T,name))
		enum hasInheritAttributeValue = true;
	else
		enum hasInheritAttributeValue = hasInheritAttributeValueImpl!(name, BaseClassesTuple!T);
}
template hasInheritAttributeValueImpl(alias name,T...)
{
	//pragma(msg, T);
	static if(T.length == 0 || is(T[0] == Object))
		enum hasInheritAttributeValueImpl = false;
	else static if(hasAttributeValue!(T[0],name))
		enum hasInheritAttributeValueImpl = true;
	else
		alias hasInheritAttributeValueImpl!(name, T[1..$]) hasInheritAttributeValueImpl;
}

/// Проверяет на значение атрибута UDA (и наследованные классы тоже)
template getInheritUDA(T, name) if (is(T == class))
{
	static if(is(T == Object))
		enum getInheritUDA = TypeOf!name.init;
	else
	{
		static if(getUDAs!(T, name).length > 0)
			enum getInheritUDA = getUDAs!(T, name)[0];
		else
			enum getInheritUDA = getInheritUDA!(BaseClass!T, name);
	}
}

/// Возвращает кортеж атрибутов указанного типа
template getAttributes(T,alias target)
{
	import std.typetuple;
	enum isT(alias X) = is(typeof(X) : T);
	enum getAttributes = Filter!(isT, __traits(getAttributes,target));
}

/// Возвращает символ для вызывающей шаблон функции
/// Вызывается: ThisFunc!(this)
template ThisFunc(alias x)
{
	alias ThisFunc = Alias!(__traits(parent,x));
}

/// Возвращает строку через запятую
template Names(X...)
{
	static if(X.length == 0)
		enum Names = "";
	else static if(X.length == 1)
		enum Names = X[0];
	else
		enum Names = X[0] ~ "," ~ Names!(X[1..$]);
}

///
template ModuleName(T)
{
//	static if(T.stringof == "PostRole")
//	{
		//pragma(msg, "> ", T, " > ", moduleName!T, " > ", fullyQualifiedName!(__traits(parent, T)));
	//pragma(msg,packageName!T, " + ", T.stringof, " = ", fullyQualifiedName!T);
//		static if(isSomeFunction!(__traits(parent, T)))
//			pragma(msg,"FFF", __traits(parent, (__traits(parent, T))));

//alias Identity(alias A) = A;
//alias parentOf(alias sym) = Identity!(__traits(parent, sym));
//template parent(alias T)
//{
//				import std.algorithm.searching : startsWith;

//				static if (__traits(compiles, parentOf!T))
//{
//								enum parent = packageName!(parentOf!T);
//			pragma(msg,"X:",parent.stringof);
//}
//				else
//								enum string parent = null;
//}
//enum p = parent!T;
//		pragma(msg,T.stringof);
//		alias parentOf(alias sym) = Identity!();
//		alias p = Alias!(__traits(parent, T));
//		pragma(msg, is(p == function) );
//		pragma(msg, is(p == delegate) );
//		pragma(msg, (FunctionTypeOf!p).stringof);
//		pragma(msg, p.stringof);
//		pragma(msg,__traits(parent, T));
//		pragma(msg,packageName!T);
//		pragma(msg,"===",parentOf!T.stringof, " ", parentOf!(parentOf!T).stringof,
//		" ", parentOf!(parentOf!(parentOf!T)).stringof, " ", parentOf!(parentOf!(parentOf!(parentOf!T))).stringof);
////pragma(msg, FunctionTypeOf!(__traits(parent, T)));
//	}


//alias parent = Alias!(__traits(parent, T));
//	static if(isSomeFunction!parent)
//	{
//		pragma(msg, ">>>>>",fullyQualifiedName!(T));
//		pragma(msg, ">>>>>", fullyQualifiedName!(parent), " > ", fullyQualifiedName!(__traits(parent, parent)));
//		enum ModuleName = ModuleName!(__traits(parent, parent));
//	}
//	else
	static if(is(T == Object) || is(T : TypeInfo))
		enum ModuleName = "object";
	else static if(packageName!T ~ "." ~ T.stringof == fullyQualifiedName!T)
		enum ModuleName = packageName!T;
	else static if(__traits(compiles, __traits(parent, T)))
	{
		alias parent = Alias!(__traits(parent, T));

		import std.algorithm : startsWith;
		static if(parent.stringof.startsWith("module ") &&
				fullyQualifiedName!(parent) ~ "." ~ T.stringof == fullyQualifiedName!T)
			enum ModuleName = fullyQualifiedName!(parent);
		else
			enum ModuleName = moduleName!T;
	}
	else
		enum ModuleName = moduleName!T;

	//pragma(msg, "> ", T, " > ", moduleName!T, " > ", fullyQualifiedName!(__traits(parent, T)));

}

/// Определяет, присутствует ли Member в типе или родительских типах.
/// Игнорирует alias this.
template HasOwnMethod(T,string name)
{
	static if(__traits(getAliasThis, T).length > 0)
	{
		static if(__traits(hasMember, T, name) == false)
			enum HasOwnMethod = false;
		else
		{
			import std.typetuple : staticIndexOf, TypeTuple;
			alias MT = Alias!(__traits(parent,__traits(getMember, T, name)));
			enum HasOwnMethod = staticIndexOf!(MT, TypeTuple!(T,BaseClassesTuple!T)) != -1;
		}
	}
	else
		enum HasOwnMethod = __traits(hasMember, T, name);
}

pragma(inline, true):

bool isEqual(T)(T t1, T t2)
{
	static if(isValueType!T)
		return t1 == t2;
	else
		return t1 is t2;
}
