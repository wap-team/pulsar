module pulsar.atoms.datetime;

public import std.datetime.date;
public import std.datetime.systime;
public import core.time;
public import std.datetime.stopwatch;
import std.format;
public import std.conv : to;

import pulsar.atoms : isNull, notNull;
import pulsar.atoms.enums;
import pulsar.atoms.strings;

/// Структура интервала времени
struct DateTimeInterval
{
private:
	DateTime _start;
	DateTime _end;

public:
	/// Дата/время начала интервала
	@property // start
	{
		DateTime start() const { return _start; }
		void start(DateTime value) { _start = value; }
		void start(Date value) { _start = DateTime(value); }
	}
	/// Дата начала интервала
	@property // startDate
	{
		Date startDate() const { return _start.date; }
		void startDate(Date value) { start(DateTime(value, _start.timeOfDay)); }
	}
	/// Время начала интервала
	@property // startTime
	{
		TimeOfDay startTime() const { return _start.timeOfDay; }
		void startTime(TimeOfDay value) { start(DateTime(_start.date, value)); }
	}

	/// Дата/время завершения интервала
	@property // end
	{
		DateTime end() const { return _end; }
		void end(DateTime value) { _end = value; }
		void end(Date value) { _end = DateTime(value); }
	}
	/// Дата завершения интервала
	@property // endDate
	{
		Date endDate() const { return _end.date; }
		void endDate(Date value) { end(DateTime(value, _end.timeOfDay)); }
	}
	/// Время завершения интервала
	@property // endTime
	{
		TimeOfDay endTime() const { return _end.timeOfDay; }
		void endTime(TimeOfDay value) { end(DateTime(_end.date, value)); }
	}
	/// Дата/время завершения интервала.
	/// Если интервал открыт справа (end == init), возвращает DateTime.max
	@property DateTime endMax() const { return _end == DateTime.init ? DateTime.max : _end; }

	@property bool isNull() const { return _start == DateTime.init && _end == DateTime.init; }
	@property bool isOpen() const
	{
		return start == DateTime.init || _end == DateTime.init || _end == DateTime.max;
	}
	@property bool isClosed() const { return isNull == false && isOpen == false; }
	@property bool isValid() const { return _start < (_end.isNull ? DateTime.max : _end); }
	@property bool isOneDay() const { return startDate == endDate; }
	@property bool isOneTime() const { return startTime == endTime; }
	@property bool isTimeOnly() const { return startDate == Date.init && endDate == Date.init; }
	@property bool isDateOnly() const
	{
		return startTime == TimeOfDay.init && endTime == TimeOfDay.init;
	}
	@property bool isNormal() const { return isClosed && isValid; }

	/// Длительность интервала
	@property Duration duration() const
	{
		return isOpen || isValid == false ? dur!"hours"(0) : _end-_start;
	}
	/// Интервал в минутах
	/// Для открытых интервалов возвращает -1
	@property long minutes() const { return isOpen ? -1 : (_end - _start).total!"minutes"(); }
	/// Возвращает количество часов в интервале.
	/// если needCeil == true, неполный час трактуется как полный час
	/// Для открытых интервалов возвращает -1
	@property long hours(bool needCeil = false) const
	{
		if(isOpen)
			return -1;
		auto d = _end - _start;
		auto h = d.total!"hours"();
		if(needCeil && d > dur!"hours"(h))
			h++;
		return h;
	}

public:
	this(DateTime start)
	{
			_start = start;
	}
	this(DateTime start, DateTime end)
	{
		_start = start;
		_end = end;
	}
	this(SysTime start, SysTime end)
	{
		_start = cast(DateTime)start;
		_end = cast(DateTime)end;
	}
	this(Date start, Date end)
	{
		startDate = start;
		endDate = end;
	}
	this(TimeOfDay start, TimeOfDay end)
	{
		startTime = start;
		endTime = end;
	}

public:
	DateTimeInterval rebase(DateTimeInterval base) const
	{
		DateTimeInterval d = this;
		if(d.start.isNull || base.start.isNull || d.start < base.start || d.start > base.end)
			d.start = DateTime.init;
		if(d.end.isNull || base.end.isNull || d.end > base.end || d.end < base.start)
			d.end = DateTime.init;
		return d;
	}
	/// Определяет, содержит ли интервал указанную дату
	bool contains(DateTime d)
	{
		if(d == DateTime.max)
			return end.isNull;
		return d >= start && d < endMax;
	}
	bool contains(Date d) { return contains(cast(DateTime)d); }
	/// Определяет, пересекаются ли интервалы
	bool intersects(DateTimeInterval x)
	{
		return this.contains(x.start) || x.contains(this.start);
	}
	/// Вычисляет пересечение с другим интервалом
	DateTimeInterval intersection(DateTimeInterval x)
	{
		if(intersects(x) == false)
			return DateTimeInterval.init;

		DateTimeInterval res;
		if(this.start.isNull)
			res.start = x.start;
		else if(x.start.isNull)
			res.start = this.start;
		else
			res.start = this.start > x.start ? this.start : x.start;

		if(this.end.isNull)
			res.end = x.end;
		else if(x.end.isNull)
			res.end = this.end;
		else
			res.end = this.endMax < x.endMax ? this.end : x.end;

		return res.isOpen || res.isValid == false ? DateTimeInterval.init : res;
	}
	/// Объединяет интервалы
	DateTimeInterval merge(DateTimeInterval x)
	{
		DateTimeInterval d = this;
		if(x.start < d.start)
			d.start = x.start;
		if(x.endMax > d.endMax)
			d.end = x.end;
		return d;
	}

public:
	/// Упаковывает значение в строку
	string packValue() const
	{
		string s = _start.isNull ? "" : _start.toStringF();
		s ~= "|";
		return s ~ (_end.isNull ? "" : _end.toStringF());
	}
	/// Устанавлиевает значение из запакованной строки
	void unpackValue(string packedValue)
	{
		string[] ss = packedValue.split('|');
		this._start = ss.length > 0 ? fromStringF_DateTime(ss[0]) : DateTime.init;
		this._end = ss.length > 0 ? fromStringF_DateTime(ss[1]) : DateTime.init;
	}
	/// Возвращает интервал со значением из запакованной строки
	static DateTimeInterval parsePackedValue(string packedValue)
	{
		DateTimeInterval d;
		d.unpackValue(packedValue);
		return d;
	}

	T opCast(T = DateInterval)() const
	{
		return DateInterval(startDate, endDate);
	}

	enum ToStringOptions
	{
		Default = 0,
		/// Не отображать дату, если она одинакова для концов интервала
		NoDateIfOneDay = 1,
		/// Отображать "-" вместо "c .. по" для времени
		ShortTimeInterval = 2,
		/// Не отображать лидирующие 0 и нулевые минуты
		CompactTime = 4
	}

	string toString() const
	{
		return toString(ToStringOptions.Default);
	}
	string toString(ToStringOptions opt) const
	{
		if(isNull)
			return null;
		bool sti = opt.isSet(ToStringOptions.ShortTimeInterval) && endTime.notNull;
		bool ct = opt.isSet(ToStringOptions.CompactTime);
		bool oneDay = isOneDay;
		string res;
		if(oneDay)
		{
			if(startDate.notNull && opt.isSet(ToStringOptions.NoDateIfOneDay) == false)
				res = toStringF(startDate);
			if(isDateOnly == false)
			{
				res = res.concat(sti ? null : "c").concat(startTime.notNull ?
																																															startTime.toStringF(ct) : "00:00");
				if(endTime.notNull && end != DateTime.max)
					if(sti)
						res ~= "-" ~ endTime.toStringF(ct);
					else
						res = res.concat("до").concat(endTime.toStringF(ct));
			}
		}
		else
		{
			if(start.notNull)
			{
				res = sti ? "" : res.concat("c");
				if(startDate.notNull)
					res = res.concat(startDate.toStringF);
				if(startTime.notNull)
					res = res.concat(startTime.toStringF(ct));
			}
			if(end.notNull && end != DateTime.max)
			{
				res = res.concat(sti ? "-" : "до");
				if(endDate.notNull)
					res = res.concat(endDate.toStringF());
				if(endTime.notNull)
					res = res.concat(endTime.toStringF(ct));
			}
		}
		return res;
	}
}
unittest
{
	import pulsar.atoms;
	void tracex(string s)()
	{
		trace(s, " -> ",  mixin(s));
	}
	tracex!"DateTimeInterval(DateTime(2017,1,10, 10,25,58), DateTime(2017,2,25, 16,1,8))";
	tracex!"DateTimeInterval(DateTime(2017,1,10, 10,25,58))";
	tracex!"DateTimeInterval(DateTime(Date.init, TimeOfDay(10,25,58)))";
	tracex!"DateTimeInterval(DateTime.init, DateTime(2017,1,10))";
	tracex!"DateTimeInterval(DateTime.init, DateTime(2017,1,10, 10,25,58))";
	tracex!"DateTimeInterval(DateTime.init, DateTime(Date.init, TimeOfDay(10,25,58)))";
	tracex!"DateTimeInterval(DateTime.init, DateTime(2017,1,10))";
	tracex!"DateTimeInterval(DateTime(2017,1,10, 10,25,58), DateTime(2017,1,10, 16,1,8))";
	tracex!"DateTimeInterval(DateTime(2017,1,10), DateTime(2017,1,10, 16,1,8))";
	tracex!"DateTimeInterval(DateTime(2017,1,10, 10,25,58), DateTime(2017,1,10))";
	tracex!"DateTimeInterval(DateTime(2017,1,10), DateTime(2017,1,10))";
	tracex!"DateTimeInterval()";

}

/// Структура интервала дат
/// Конечная дата не включительно (до)
struct DateInterval
{
private:
	Date _start;
	Date _end;

public:
	/// Дата начала интервала
	@property // start
	{
		Date start() const { return _start; }
		void start(Date value) { _start = value; }
	}
	/// Дата завершения интервала
	@property // end
	{
		Date end() const { return _end; }
		void end(Date value) { _end = value; }
	}
	/// Дата завершения интервала.
	/// Если интервал открыт справа (end == init), возвращает Date.max
	@property Date endMax() const { return _end == Date.init ? Date.max : _end; }

	@property bool isNull() const { return _start == Date.init && _end == Date.init; }
	@property bool isOpen() const
	{
		return start == Date.init || _end == Date.init || _end == Date.max;
	}
	@property bool isClosed() const { return isNull == false && isOpen == false; }
	@property bool isValid() const { return _start < (_end.isNull ? Date.max : _end); }
	@property bool isOneDay() const { return start == end; }
	@property bool isNormal() const { return isClosed && isValid; }

	/// Длительность интервала
	@property Duration duration() const
	{
		return isOpen || isValid == false ? dur!"days"(0) : (_end-_start);
	}

public:
	this(Date start)
	{
		_start = start;
	}
	this(Date start, Date end)
	{
		_start = start;
		_end = end;
	}

public:
	DateInterval rebase(DateInterval base) const
	{
		DateInterval d = this;
		if(d.start.isNull || base.start.isNull || d.start < base.start || d.start > base.end)
			d.start = Date.init;
		if(d.end.isNull || base.end.isNull || d.end > base.end || d.end < base.start)
			d.end = Date.init;
		return d;
	}
	/// Определяет, содержит ли интервал указанную дату
	bool contains(Date d) const
	{
		if(d == Date.max)
			return end.isNull;
		return d >= start && d < endMax;
	}
	/// Определяет, пересекаются ли интервалы
	bool intersects(DateInterval x)
	{
		return this.contains(x.start) || x.contains(this.start);
	}
	/// Объединяет интервалы
	DateInterval merge(DateInterval x)
	{
		DateInterval d = this;
		if(x.start < d.start)
			d.start = x.start;
		if(x.endMax > d.endMax)
			d.end = x.end;
		return d;
	}

public:
	/// Упаковывает значение в строку
	string packValue() const
	{
		string s = _start.isNull ? "" : _start.toStringF();
		s ~= "|";
		return s ~ (_end.isNull ? "" : _end.toStringF());
	}
	/// Упаковывает значение в ISO строку (20200101|20221231)
	string packIsoValue() const
	{
		string s = _start.isNull ? "" : _start.toISOString();
		s ~= "|";
		return s ~ (_end.isNull ? "" : _end.toISOString());
	}
	/// Упаковывает значение в ISO ext строку (2020-01-01|2022-12-31)
	string packIsoExtValue() const
	{
		string s = _start.isNull ? "" : _start.toISOExtString();
		s ~= "|";
		return s ~ (_end.isNull ? "" : _end.toISOExtString());
	}
	/// Устанавлиевает значение из запакованной строки
	void unpackValue(string packedValue)
	{
		string[] ss = packedValue.split('|');
		this._start = ss.length > 0 ? fromStringF_Date(ss[0]) : Date.init;
		this._end = ss.length > 1 ? fromStringF_Date(ss[1]) : Date.init;
	}
	/// Возвращает интервал со значением из запакованной строки
	static DateInterval parsePackedValue(string packedValue)
	{
		DateInterval d;
		d.unpackValue(packedValue);
		return d;
	}
	/// Возвращает интервал со значением из строки ISO дат (20200101|20221231)
	static DateInterval parseIsoValue(string value)
	{
		DateInterval d;
		string[] ss = value.split('|');
		d._start = ss.length > 0 ? Date.fromISOString(ss[0]) : Date.init;
		d._end   = ss.length > 1 ? Date.fromISOString(ss[1]) : Date.init;
		return d;
	}
	/// Возвращает интервал со значением из строки ISO дат (2020-01-01|2022-12-31)
	static DateInterval parseIsoExtValue(string value)
	{
		DateInterval d;
		string[] ss = value.split('|');
		d._start = ss.length > 0 ? Date.fromISOExtString(ss[0]) : Date.init;
		d._end   = ss.length > 1 ? Date.fromISOExtString(ss[1]) : Date.init;
		return d;
	}


	T opCast(T = DateTimeInterval)() const
	{
		return DateTimeInterval(start, end);
	}

	enum ToStringOptions
	{
		Default = 0,
		/// Не отображать дату, если она одинакова для концов интервала
		NoDateIfOneDay = 1,
		/// с ... по (а не с...до)
		IncludeEndDate = 2,
		/// Даты через дефиз
		UseHyphen = 3
	}
	string toString() const
	{
		return toString(ToStringOptions.Default);
	}
	string toString(ToStringOptions opt) const
	{
		if(isNull)
			return null;
		bool hyphen = opt.isSet(ToStringOptions.UseHyphen);
		bool oneDay = isOneDay;
		string res;
		if(oneDay)
		{
			if(start.notNull && opt.isSet(ToStringOptions.NoDateIfOneDay) == false)
				res = toStringF(start);
		}
		else
		{
			if(start.notNull)
			{
				res = hyphen ? "" : res.concat("c");
				res = res.concat(start.toStringF);
			}
			if(end.notNull && end != Date.max)
			{
				res = res.concat(hyphen ? "-" : (opt.isSet(ToStringOptions.IncludeEndDate) ? "по" : "до"));
				if(opt.isSet(ToStringOptions.IncludeEndDate))
					res = res.concat(end.shiftDay(-1).toStringF());
				else
					res = res.concat(end.toStringF());
			}
		}
		return res;
	}
}

/// Структура интервала времени
struct TimeInterval
{
private:
	TimeOfDay _start;
	TimeOfDay _end;

public:
	/// Время начала интервала
	@property // start
	{
		TimeOfDay start() const { return _start; }
		void start(TimeOfDay value) { _start = value; }
	}
	/// Время завершения интервала
	@property // end
	{
		TimeOfDay end() const { return _end; }
		void end(TimeOfDay value) { _end = value; }
	}
	/// Длительность
	@property Duration duration() const
	{
		if(_start <= _end)
			return end - start;
		uint ss = _start.hour * 3600 + _start.minute * 60 + _start.second;
		uint es = _end.hour * 3600 + _end.minute * 60 + _end.second;
		return seconds(86400 - ss + es);
	}

	@property bool isNull() const { return _start == TimeOfDay.init && _end == TimeOfDay.init; }
	@property bool notNull() const { return _start != TimeOfDay.init || _end != TimeOfDay.init; }
	@property bool isOpen() const
	{
		return start == TimeOfDay.init || _end == TimeOfDay.init/* || _end == TimeOfDay.max*/;
	}
	@property bool isClosed() const { return isNull == false && isOpen == false; }
	@property bool isValid() const { return _start < _end; }
	@property bool isNormal() const { return isClosed && isValid; }

public:
	this(TimeOfDay start)
	{
			_start = start;
	}
	this(TimeOfDay start, TimeOfDay end)
	{
		_start = start;
		_end = end;
	}

public:
	/// Определяет, содержит ли интервал указанное время
	bool contains(TimeOfDay е) const
	{
		if(_start <= _end)
			return е >= _start && е < _end;
		return е >= _start || е < _end;
	}

public:
	/// Упаковывает значение в строку
	string packValue(bool packEmptyTimes = false) const
	{
		string s;
		if(_start.isNull)
			s = packEmptyTimes ? "00:00" : "";
		else
			s	= _start.toStringF();
		s ~= "|";
		if(_end.isNull)
			s ~= packEmptyTimes ? "00:00" : "";
		else
			s	~= _end.toStringF();
		return s;
	}
	/// Устанавлиевает значение из запакованной строки
	void unpackValue(string packedValue)
	{
		string[] ss = packedValue.split('|');
		this._start = ss.length > 0 ? fromStringF_Time(ss[0]) : TimeOfDay.init;
		this._end = ss.length > 0 ? fromStringF_Time(ss[1]) : TimeOfDay.init;
	}
	/// Возвращает интервал со значением из запакованной строки
	static TimeInterval parsePackedValue(string packedValue)
	{
		TimeInterval d;
		d.unpackValue(packedValue);
		return d;
	}


public:
	enum ToStringOptions
	{
		Default = 0,
		/// Не отображать лидирующие 0 и нулевые минуты
		CompactTime = 1
	}

	string toString() const
	{
		if(isNull)
			return null;
		return (start.isNull ? "00:00" : start.toStringF()) ~ " - " ~
									(end.isNull ? "00:00" : end.toStringF());
	}
	string toString(ToStringOptions opts) const
	{
		if(isNull)
			return null;
		if(opts.isSet(ToStringOptions.CompactTime))
			return (start.isNull ? "0" : start.toStringF(true)) ~ " - " ~
										(end.isNull ? "0" : end.toStringF(true));
		return toString();
	}
}

//----
enum RuDayOfWeek : ubyte
{
	пн = 0,
	вт = 1,
	ср = 2,
	чт = 3,
	пт = 4,
	сб = 5,
	вс = 6
}
string toString(RuDayOfWeek day)
{
	final switch(day)
	{
		case RuDayOfWeek.пн : return "понедельник";
		case RuDayOfWeek.вт : return "вторник";
		case RuDayOfWeek.ср : return "среда";
		case RuDayOfWeek.чт : return "четверг";
		case RuDayOfWeek.пт : return "пятница";
		case RuDayOfWeek.сб : return "суббота";
		case RuDayOfWeek.вс : return "воскресенье";
	}
}
string toStringVP(RuDayOfWeek day)
{
	final switch(day)
	{
		case RuDayOfWeek.пн : return "понедельник";
		case RuDayOfWeek.вт : return "вторник";
		case RuDayOfWeek.ср : return "среду";
		case RuDayOfWeek.чт : return "четверг";
		case RuDayOfWeek.пт : return "пятницу";
		case RuDayOfWeek.сб : return "субботу";
		case RuDayOfWeek.вс : return "воскресенье";
	}
}
RuDayOfWeek ruDayOfWeek(DayOfWeek day)
{
	return cast(RuDayOfWeek)(day == 0 ? 6 : cast(ubyte)(day-1));
}
RuDayOfWeek ruDayOfWeek(Date date)
{
	return ruDayOfWeek(date.dayOfWeek);
}

/// Структура русской недели
struct RuWeek
{
private:
	Date _mon;

public:
	@property Date monday() const { return _mon; }
	@property Date sunday() const { return _mon + 6.days; }
	Date date(RuDayOfWeek day) { return _mon + (cast(ubyte)day).days; }
	@property DateTimeInterval interval() const
	{
		return DateTimeInterval(monday, sunday);
	}

public:
	this(Date dateOfWeek) { _mon = this.dateOfWeek(dateOfWeek, RuDayOfWeek.пн); }

public:
	/// Неделя содержит дату
	bool contains(Date date) { return date >= monday && date <= sunday; }

	/// Следующая неделя
	@property RuWeek nextWeek() const { return RuWeek(_mon + 7.days); }
	/// Предыдущая неделя
	@property RuWeek prevWeek() const { return RuWeek(_mon - 7.days); }

public:
	static RuDayOfWeek dayOfWeek(Date date)
	{
		ubyte d = cast(ubyte)date.dayOfWeek;
		return cast(RuDayOfWeek)(d == 0 ? 6 : d-1);
	}
	/// Расчитывает дату дня недели относительно указанной даты
	static Date dateOfWeek(Date date, RuDayOfWeek day)
	{
		RuDayOfWeek d = dayOfWeek(date);
		if(d == day)
			return date;
		auto dif = day - d;
		return date + dur!"days"(dif);
	}

public:
	int opApply(int delegate(Date) dg) const
	{
		int result = 0;
		foreach(size_t i; 0..7)
		{
			result = dg(_mon + i.days);
			if(result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, Date) dg) const
	{
		int result = 0;
		foreach(size_t i; 0..7)
		{
			result = dg(i,_mon + i.days);
			if(result)
				break;
			i++;
		}
		return result;
	}


	alias ToStringOptions = DateTimeInterval.ToStringOptions;
	string toString() { return interval.toString(); }
	string toString(ToStringOptions opts) { return interval.toString(opts); }
}

/// Начало следующего дня
Date today() { return cast(Date)Clock.currTime; }
/// Начало следующего дня
Date tomorrow() { return cast(Date)Clock.currTime + 1.days; }

/// Возвращает дату со сдигом в daysCount дней. Параметр не изменяет.
/// Если d = init, берется сегодняшняя дата
/// daysCount может быть отрицательным
Date shiftDay(Date d = Date.init, long daysCount = 1)
{
	if(d == Date.init)
		d = cast(Date)Clock.currTime;
	return d + days(daysCount);
}
DateTime shiftDay(DateTime d, long daysCount = 1)
{
	if(d == DateTime.init)
		d = cast(DateTime)Clock.currTime;
	return d + days(daysCount);
}
SysTime shiftDay(SysTime d, long daysCount = 1)
{
	if(d == SysTime.init)
		d = Clock.currTime;
	return d + days(daysCount);
}
// Возвращает дату со сдигом в months месяцев. Параметр не изменяет.
/// Если d = Date.init, берется сегодняшняя дата
/// months может быть отрицательным
Date shiftMonth(Date d = Date.init, long months = 1)
{
	if(d == Date.init)
		d = cast(Date)Clock.currTime;
	return d.add!"months"(months);
}
DateTime shiftMonth(DateTime d , long months = 1)
{
	if(d == DateTime.init)
		d = cast(DateTime)Clock.currTime;
	return d.add!"months"(months);
}
/// Возвращает дату начала месяца
Date beginOfMonth(Date d)
{
	return Date(d.year, d.month, 1);
}
