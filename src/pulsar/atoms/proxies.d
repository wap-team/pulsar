module pulsar.atoms.proxies;

import std.conv;
import std.string;
import std.traits;
import pulsar.atoms;
import pulsar.atoms.traits;

//-------------------------------------------------------------------------------------------

/// Структура атрибута проксируемых объектов и методов
struct ProxyAttribute { }
enum proxy = ProxyAttribute.init;

/// Структура атрибута не
struct NoProxyAttribute { }
enum noproxy = NoProxyAttribute.init;

//----------

/// Базовый интерфейс проксей
interface IProxy
{
	/// Проксируемый объект
	@property const(Object) targetObject() const;
	/// ClassInfo проксируемого типа
	@property ClassInfo targetTypeId();

	/// Метод определения типа объекта.
	/// Если объект проксирован, возвращает ClassInfo проксируемого типа.
	final static ClassInfo typeId(const(Object) o)
	{
		IProxy proxy = cast(IProxy)o;
		if(proxy)
			return proxy.targetTypeId();
		else
			return typeid(unqual(o));
	}
}
/// Базовый интерфейс всех прозрачных прокси
interface ITransparentProxy : IProxy
{
}
