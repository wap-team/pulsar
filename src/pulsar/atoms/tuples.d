module pulsar.atoms.tuples;

import std.conv;

struct Pair(TKey, TVal = TKey)
{
	TKey val1;
	TVal val2;

	this(TKey val1, TVal val2)
	{
		this.val1 = val1;
		this.val2 = val2;
	}
}

struct Trio(TVal1, TVal2 = TVal1, TVal3 = TVal1)
{
	TVal1 val1;
	TVal2 val2;
	TVal3 val3;


	//@disable this();
	//this(this) {}
	this(TVal1 val1, TVal2 val2, TVal3 val3)
	{
		this.val1 = val1;
		this.val2 = val2;
		this.val3 = val3;
	}
}

struct KeyValue(TKey, TVal = TKey)
{
	TKey key;
	TVal value;

	this(TKey key, TVal value)
	{
		this.key = key;
		this.value = value;
	}
	string toString() const { return to!string(value); }
}

struct KeyValueBinder
{
	alias Getter = string delegate();

public:
	Getter keyGetter;
	Getter valueGetter;

public:
	@property string key() const { return keyGetter ? keyGetter() : null; }
	@property string value() const { return valueGetter ? valueGetter() : null; }

public:
	static KeyValueBinder bind(T, string Key, string Value)(ref T struc)
	{
		import std.traits;
		KeyValueBinder r;
		static if(isSomeFunction!(mixin("T." ~ Key)) &&
												isSomeString!(ReturnType!(mixin("T." ~ Key))))
			r.keyGetter = &mixin("struc." ~ Key);
		else
			r.keyGetter = (){ return to!string( mixin("struc." ~ Key)); };

		static if(isSomeFunction!(mixin("T." ~ Value)) &&
												isSomeString!(ReturnType!(mixin("T." ~ Value))))
			r.valueGetter = &mixin("struc." ~ Value);
		else
			r.valueGetter = (){ return to!string( mixin("struc." ~ Value)); };
		return r;
	}

	string toString() const { return text("{ ", key, " : ", value, " }"); }
}

struct KeyValueBinderIter
{
private:
	void[] _arr;
	size_t _Tsize;
	KeyValueBinder delegate(size_t) _getFront;

public:

	static KeyValueBinderIter make(T, string Key, string Value)(T[] arr)
	if(is(T == struct))
	{
		KeyValueBinderIter it;
		it._arr = arr;
		it._Tsize = T.sizeof;
		it._getFront = (t)
		{
			return KeyValueBinder.bind!(T,Key,Value)((cast(T[])it._arr)[t]);
		};
		return it;
	}

	@property bool empty() { return _arr.length == 0; }
	@property KeyValueBinder front()
	{
		return _getFront(0);
	}
	KeyValueBinder moveFront() { assert(false, "ToDo"); }
	void popFront()
	{
		static struct FatPtr
		{
			size_t length;
			void* ptr;
		}
		assert(false, "Need check!");
		if(_arr.length)
		{
			FatPtr* p = cast(FatPtr*)&_arr;
			(*p).length--;
			(*p).ptr += _Tsize;
		}
	}
	@property KeyValueBinderIter save() { return this; }


	int opApply(int delegate(KeyValueBinder) dg)
	{
		int result = 0;
		for (size_t i = 0; i < _arr.length/_Tsize; i++)
		{
			result = dg(_getFront(i));
			if(result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, KeyValueBinder) dg)
	{
		int result = 0;
		for (size_t i = 0; i < _arr.length/_Tsize; i++)
		{
			result = dg(i,_getFront(i));
			if(result)
				break;
		}
		return result;
	}
	int opApply(int delegate(string, string) dg)
	{
		int result = 0;
		for (size_t i = 0; i < _arr.length/_Tsize; i++)
		{
			result = dg(_getFront(i).key,_getFront(i).value);
			if(result)
				break;
		}
		return result;
	}
}
unittest
{
	version(trace)trace("--- KeyValueBinder ... ---");
	struct Vic
	{
		int a;
		@property string aa() { return text("a=",a); }
		double b;
		@property string bb() { return text("b=",b); }
	}

	Vic[] v = [ Vic(1,10), Vic(2,20), Vic(3,30), Vic(4,40)];
	KeyValueBinder k = KeyValueBinder.bind!(Vic,"a","b")(v[0]);
	trace("key: ", k.key, " value: ", k.value);

	foreach(x; KeyValueBinderIter.make!(Vic,"aa","bb")(v))
	{
		version(trace)trace("key: ", x.key, " value: ", x.value);
	}

	foreach(size_t i, x; KeyValueBinderIter.make!(Vic,"a","b")(v))
	{
		version(trace)trace("i: ", i," key: ", x.key, " value: ", x.value);
	}
	version(trace)trace("--- KeyValueBinder done ---");

}
