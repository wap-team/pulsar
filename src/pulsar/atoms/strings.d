module pulsar.atoms.strings;

/**
		* В этом модуле собраны публичные импорты
		* и добавочные функции для работы со строками
		*/

import std.traits;
import std.exception;

import pulsar.atoms.datetime;


public import std.string;
public import std.utf;
public import std.array : replace, Appender, RefAppender, appender, split;
public import std.algorithm : countUntil, any, canFind, stripLeft, stripRight;
public import std.uni : sicmp;

/// Проверяет строку null на и на нулевую длину.
bool isNullOrEmpty(Ch)(Ch[] value)
{
	return value is null || value.length == 0;
}

/// Проверяет, может ли строка быть преобразовано в число. Только 123 и 123.456
bool isNumberString(Ch)(Ch[] value, bool onlyInteger = false)
{
	if(value.length == 0)
		return false;

	import std.uni;
	bool isFloat = false;
	if(value[0] == '-')
		value = value[1..$];

	foreach(ch;value)
		if(ch == '.')
		{
			if(isFloat || onlyInteger)
				return false;
			isFloat = true;
		}
		else if(isNumber(ch) == false)
		return false;
	return true;
}

string concat(string base, string addon, string sep = " ")
{
	if(base.length && base.endsWith(sep) == false)
		return base ~ sep ~ addon;
	return base ~ addon;
}
ref string add(return ref string base, string addon, string sep = " ")
{
	if(base.length && base.endsWith(sep) == false)
		base ~= sep ~ addon;
	else
		base ~= addon;
	return base;
}
/// Вовзвращает остаток строки с позиции последнего after.
/// Если не найдено, возвращает строку целиком
string tail(string s, string after)
{
	import std.string;
	auto pos = lastIndexOf(s,after);
	if(pos == -1)
		return s;
	return s[pos+after.length..$];
}
/// Вовзвращает начало строки до позиции первого before
/// Если не найдено, возвращает строку целиком
string head(string s, string before)
{
	import std.string;
	auto pos = indexOf(s,before);
	if(pos == -1)
		return s;
	return s[0..pos];
}

//*********************************************************************************************
/// Флаги конвертирования в строку для функции toStringF
enum DoubleConvFlags : ubyte
{
	/// По умочланию не вставлять пробелы , отображать с заданной точностью (или =6)
	None = 0,
	/// Отображать с пробелами
	UseGroupSeparator = 1,
	///Отображать с заданной точностью(или =6)
	SharpPrec = 2,
	/// Способ округления - банковский
	BankingRounding = 4
}

/// Вставка пробелов в число и округление
string toStringF(double num, DoubleConvFlags flags) { return toStringF(num,6,flags); }

/// Вставка пробелов в число и округление
string toStringF(double num, ubyte prec = 6, DoubleConvFlags flags = DoubleConvFlags.None)
{
	import std.algorithm : reverse;
	import std.array : appender;

	auto pow = 10^^prec;
	long x = cast(long) (((num < 0) ? -num : num) * (pow*10));

	/* Округление */
	if(x % 10 > 5)
		x += 10;
	else if(x % 10 == 5)
	{
		if((flags & DoubleConvFlags.BankingRounding) != DoubleConvFlags.BankingRounding)
			x += 10;
		else if ( (x/10) % 2 != 0 )
			x += 10;
	}
	x /= 10;

	long ip = x / pow;
	long fp = x % pow;

	if((flags & DoubleConvFlags.SharpPrec) == false)
	{
		while(pow > 0 && fp%10 == 0)
		{
			fp /= 10;
			pow /= 10;
		}
	}

	auto res = appender!(char[]);

	if(num < 0 && (ip > 0 || fp > 0))
		res ~= '-';

	// целая часть
	auto shift = res.data.length;
	if(ip == 0)
		res ~= '0';
	else
	{
		for(ubyte i = 1; ip > 0; ip /= 10, i++)
		{
			if((flags & DoubleConvFlags.UseGroupSeparator) && i == 4)
			{
				res ~= "\u202F";
				i = 1;
			}
			res ~= cast(char)(ip%10 + 48);
		}
		reverse(res.data[shift..$]);
	}

	// дробная часть
	if( prec == 0 || ((flags & DoubleConvFlags.SharpPrec) == false && fp == 0))
		return cast(string)res.data;
	res ~= '.';
	shift = res.data.length;

	for(; pow > 1; fp /= 10, pow /= 10)
		res ~= cast(char)((fp%pow)%10 + 48);
	reverse(res.data[shift..$]);

	return cast(string)res.data;
}

string toMoney(double num, bool useGroupSeparator = false)
{
	DoubleConvFlags f = DoubleConvFlags.SharpPrec;
	import pulsar.atoms.enums;
	if(useGroupSeparator)
		f = f.setFlag(DoubleConvFlags.UseGroupSeparator);
	return toStringF(num, 2, f);
}

string numstr(ulong num, lazy string s1, lazy string s2_4, lazy string s)
{
	if(num%100 >= 5 && num%100 < 20)
		return s;
	else if(num%10 == 1)
		return s1;
	else if(num%10 > 1 && num%10 < 5)
		return s2_4;
	else
		return s;
}

unittest
{
	assert(toStringF(0)=="0") ;
	assert(toStringF(10)=="10") ;
	assert(toStringF(-10)=="-10") ;
	assert(toStringF(1245)=="1245") ;
	assert(toStringF(-1245)=="-1245") ;
	assert(toStringF(10, DoubleConvFlags.UseGroupSeparator)=="10") ;
	assert(toStringF(-10, DoubleConvFlags.UseGroupSeparator)=="-10") ;
	assert(toStringF(1245, DoubleConvFlags.UseGroupSeparator)=="1 245") ;
	assert(toStringF(-1245, DoubleConvFlags.UseGroupSeparator)=="-1 245") ;
	assert(toStringF(1234567890, DoubleConvFlags.UseGroupSeparator)=="1 234 567 890") ;
	assert(toStringF(-777888999, DoubleConvFlags.UseGroupSeparator)=="-777 888 999") ;


	assert(toStringF(0.0) == "0");
	assert(toStringF(0.123) == "0.123");
	assert(toStringF(0.0050) == "0.005");
	assert(toStringF(12345678900.1234) == "12345678900.1234");// только 11 знаков в целой части
	assert(toStringF(12345678900.1234, DoubleConvFlags.UseGroupSeparator) == "12 345 678 900.1234");// только 11 знаков в целой части
	assert(toStringF(1125.0125) == "1125.0125");
	assert(toStringF(1125.0125, DoubleConvFlags.UseGroupSeparator) == "1 125.0125");
	assert(toStringF(1125.0125, DoubleConvFlags.SharpPrec) == "1125.012500");
	assert(toStringF(1125.0125, cast(DoubleConvFlags)(DoubleConvFlags.UseGroupSeparator |
																																																			DoubleConvFlags.SharpPrec)) == "1 125.012500");

	assert(toStringF(0,3,DoubleConvFlags.SharpPrec) == "0.000");
	assert(toStringF(10,3,DoubleConvFlags.SharpPrec) == "10.000");
	assert(toStringF(10.05,3) == "10.05");
	assert(toStringF(10.05,3,DoubleConvFlags.SharpPrec) == "10.050");

	assert(toStringF(0.5,0) == "1");
	assert(toStringF(125.125,0) == "125");
	assert(toStringF(125.125,0,DoubleConvFlags.SharpPrec) == "125");
	assert(toStringF(125.125,2) == "125.13");
	assert(toStringF(125.124,2) == "125.12");
	assert(toStringF(11125.124,2, DoubleConvFlags.UseGroupSeparator) == "11 125.12");
	assert(toStringF(0.12050,2) == "0.12");
	assert(toStringF(0.12499,2) == "0.12");
	assert(toStringF(0.12999,2) == "0.13");

	assert(toStringF(4.5,0) == "5");
	assert(toStringF(5.5,0) == "6");
	assert(toStringF(0.4, 0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "0");
	assert(toStringF(0.5, 0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "0");
	assert(toStringF(1.5, 0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "2");
	assert(toStringF(2.5, 0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "2");
	assert(toStringF(2000.5, 0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding | DoubleConvFlags.UseGroupSeparator)) == "2 000");
	assert(toStringF(2.5, 0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding | DoubleConvFlags.SharpPrec)) == "2");
	assert(toStringF(0.12050,3, cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding | DoubleConvFlags.UseGroupSeparator | DoubleConvFlags.SharpPrec)) == "0.120");
	assert(toStringF(1000.12345,3, cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding | DoubleConvFlags.UseGroupSeparator | DoubleConvFlags.SharpPrec)) == "1 000.123");

	assert(toStringF(67.2450, 2 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "67.24");
	assert(toStringF(67.1263, 2 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "67.13");
	assert(toStringF(67.1236, 2 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "67.12");

	assert(toStringF(-0.125,0) == "0");
	assert(toStringF(-0.125,1) == "-0.1");
	assert(toStringF(-0.125,2) == "-0.13");
	assert(toStringF(-0.125,3) == "-0.125");
	assert(toStringF(-0.125,2,cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "-0.12");

	assert(toStringF(-4.4,0) == "-4");
	assert(toStringF(-4.5,0) == "-5");
	assert(toStringF(-5.5,0) == "-6");
	assert(toStringF(-4.5,0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "-4");
	assert(toStringF(-5.5,0 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "-6");
	assert(toStringF(-67.1263, 2 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "-67.13");
	assert(toStringF(-67.1213, 2 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "-67.12");
	assert(toStringF(-67.1113, 2 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "-67.11");
	assert(toStringF(-67.1153, 2 , cast(DoubleConvFlags)(DoubleConvFlags.BankingRounding)) == "-67.12");

	assert(toStringF(0.5999, 2) == "0.6");
}

string toStringF(short x, char sepChar = ' ') { return toStringF(cast(long)x,sepChar); }
string toStringF(ushort x, char sepChar = ' ') { return toStringF(cast(ulong)x,sepChar); }
string toStringF(int x, char sepChar = ' ') { return toStringF(cast(long)x,sepChar); }
string toStringF(uint x, char sepChar = ' ') { return toStringF(cast(ulong)x,sepChar); }
string toStringF(long x, char sepChar = ' ')
{
	bool neg = x < 0;
	x = neg ? -1*x : x;
	string s = toStringF(cast(ulong)x, sepChar);
	return neg ? "-"~s : s;
}
string toStringF(ulong x, char sepChar = ' ')
{
	import std.array;
	import std.conv;
	auto app = appender!(char[]);
//	bool neg = x < 0;
//	x = neg ? -1*x : x;
	for(size_t i = 1; x > 0; i++,x /= 10)
	{
		app.put(to!string(x%10));
		if(i % 3 == 0)
			app.put(sepChar);
	}
//	if(neg)
//		app.put('-');
	import std.algorithm: reverse;
	reverse(app.data);
	return cast(string)(app.data);
}

enum ToStringOpts
{
	None = 0,
	/// Не отображать нулевое время
	NoZeroTime = 1,
}
string toStringF(DateTime d, ToStringOpts opts = ToStringOpts.None)
{
	if(d == DateTime.init)
		return null;
	import pulsar.atoms.enums;
	if(opts.isSet(ToStringOpts.NoZeroTime) && d.timeOfDay == TimeOfDay.min)
		return format("%02d.%02d.%04d",
																d.day, d.month, d.year);

	return format("%02d.%02d.%04d %02d:%02d",
															d.day, d.month, d.year, d.hour, d.minute);
}
string toStringF(Date d, bool shortYear = false)
{
	if(d == Date.init)
		return null;
	if(shortYear)
		return format("%02d.%02d.%02d",	d.day, d.month, d.year%100);
	else
		return format("%02d.%02d.%04d",	d.day, d.month, d.year);
}
string toStringF(SysTime d)
{
	if(d == SysTime.init)
		return null;
	return format("%02d.%02d.%04d %02d:%02d",
															d.day, d.month, d.year, d.hour, d.minute);
}
string toStringF(SysTime d, bool withSeconds)
{
	if(withSeconds == false)
		return toStringF(d);
	if(d == SysTime.init)
		return null;
	return format("%02d.%02d.%04d %02d:%02d:%02d",
															d.day, d.month, d.year, d.hour, d.minute, d.second);
}
/// compact == true => Не отображать лидирующие 0 и нулевые минуты
string toStringF(TimeOfDay d, bool compact = false)
{
	if(/*nullInit && */d == TimeOfDay.init)
		return null;
	if(compact == false)
		return format("%02d:%02d", d.hour, d.minute);
	import std.conv: text;
	return text(d.hour,	d.minute > 0 ? format(":%02d", d.minute) : null);
//	return text(d.hour,
//										d.minute > 0 || d.second > 0 ? format(":%02d", d.minute) : null,
//										d.second > 0 ? format(":%02d", d.second) : null);

}
string toStringF(Duration d)
{
	if(d == Duration.zero)
		return null;

	long days;
	byte hours;
	byte minutes;
	byte seconds;
	d.split!("days","hours", "minutes", "seconds")(days, hours, minutes, seconds);

	string ds;
	if(days > 0)
	{
		import std.conv;
		ds = to!string(days) ~ " ";
		auto n = days%100;
		if(n >= 5 && n <= 20)
			ds ~= "дней";
		else
		{
			n = n%10;
			if(n == 1)
				ds ~= "день";
			else if(n > 1 && n < 5)
				ds ~= "дня";
			else
				ds ~= "дней";
		}
	}


	if(seconds > 0)
		return format("%s%02d:%02d:%02d",
																ds.length ? ds ~ " " : null,
																hours, minutes, seconds);
	else if(hours == 0 && minutes == 0)
		return ds;
	return format("%s%02d:%02d",
															ds.length ? ds ~ " " : null,
															hours, minutes);
}
string toStringF(inout Object o, string ifNull = null)
{
	return o ? (cast(Object)o).toString() : ifNull;
}

T fromStringF(T)(string val) if(isNumeric!T)
{
	import std.conv;
	import pulsar.atoms.exceptions : LogicError;
	if(val.length == 0)
		return T.init;

	T t;
	try
		t = to!T(val.replace(",", "."));
	catch(ConvException)
		throw new LogicError("Значение [" ~ val ~ "] не является допустимым числом!");
	return t;
}

/// Пустая строка = ошибка
void fromStringF(string str, out Date d)
{
	import pulsar.atoms.exceptions;
	import std.conv;

	string[] dp = split(str, ".");
	if(dp.length != 3)
		throw new LogicError("Неверный формат даты (должен быть ДД.ММ.ГГГГ)!");
	d = Date(to!int(dp[2]),to!int(dp[1]),to!int(dp[0]));
}
/// Пустая строка = Date.init
Date fromStringF_Date(string str)
{
	if(str.length == 0)
		return Date.init;

	import pulsar.atoms.exceptions;
	import std.conv;


	string[] dp = split(str, ".");
	if(dp.length != 3)
		throw new LogicError("Неверный формат даты (должен быть ДД.ММ.ГГГГ)!");
	try
		return Date(to!int(dp[2]),to!int(dp[1]),to!int(dp[0]));
	catch(Throwable)
		throw new LogicError("Неверный формат даты (должен быть ДД.ММ.ГГГГ)!");
}
/// Пустая строка = Date.init
// TODO : секунды
TimeOfDay fromStringF_Time(string str)
{
	if(str.length == 0)
		return TimeOfDay.init;

	import pulsar.atoms.exceptions;
	import std.conv;

	str = str.replace(",",".");
	string[] dp = split(str, ".");
	if(dp.length != 2)
		dp = split(str, ":");
	if(dp.length != 2 && str.length >= 3)
		dp = [str[0..$-2], str[$-2..$]];
	if(dp.length == 2)
		return TimeOfDay(to!int(dp[0]),to!int(dp[1])); //,to!int(dp[0]));
	else if(dp.length == 1)
		return TimeOfDay(to!int(dp[0]),0);
	else
		throw new LogicError("Неверный формат времени!");
}
DateTime fromStringF_DateTime(string str)
{
	if(str.length == 0)
		return DateTime.init;

	import pulsar.atoms.exceptions;
	import std.conv;

	string[] ss = str.split(' ');
	DateTime d;
	d.date = ss.length > 0 ? fromStringF_Date(ss[0]) : Date.init;
	d.timeOfDay = ss.length > 1 ? fromStringF_Time(ss[1]) : TimeOfDay.init;
	return d;
}

/// Пустая строка = Duration.zero
/// принимает часы,минуты,секунды
/// Например: 1 = 1 час, 2-35 = 2 часа 35 минут, 8:48-25 = 8 часов 48 минут 25 секунд
Duration fromStringF_Duration(string str)
{
	if(str.length == 0)
		return Duration.zero;

	import pulsar.atoms.exceptions;
	import std.regex;
	import std.conv;

	str = str.replaceAll(regex("[,:-]"),".");
	string[] dp = split(str, ".");
	LogicError.enforce(dp.length > 0 && dp.length <= 3,
																				"Неверный формат длительности времени!");

	long secs;
	try
	{
		if(dp.length > 0)
		{
			long v = to!uint(dp[0]);
			enforce(v < 24, "Количество часов должно быть 0..23!");
			secs = v * 3600;
		}
		if(dp.length > 1)
		{
			long v = to!uint(dp[1]);
			enforce(v < 60, "Количество минут должно быть 0..59!");
			secs += v * 60;
		}
		if(dp.length > 2)
		{
			long v = to!uint(dp[2]);
			enforce(v < 60, "Количество секунд должно быть 0..59!");
			secs += v;
		}
	}
	catch(Throwable)
		throw new LogicError("Неверный формат длительности времени!");

	return dur!"seconds"(secs);
}

/// Приводит указанное время к UTC и возвращает значение в виде строки ISO Ext.
/// secondFractionLength определяет количество знаков в фракции секунд.
string toUtcIsoExtString(SysTime time, int secondFractionLength = 0)
{
	import std.datetime;
	string r = time.toUTC.toISOExtString();
	import std.exception;
	enforce(r[$-1]=='Z');
	import std.string;

	if(secondFractionLength >= 7)
		return r;

	long p = r.length == 28 ? 19 : r.lastIndexOf('.');
	if(p < 0 || (r.length - p - 2) <= secondFractionLength)
		return r;
	if(secondFractionLength > 0)
		p++;
	(cast(char[])r)[p + secondFractionLength] = 'Z';
	r.length = p + secondFractionLength + 1;

	return r;
}
SysTime fromUtcIsoExtString(string time)
{
	import std.datetime;
	SysTime r = SysTime.fromISOExtString(time);

	return r;
}
