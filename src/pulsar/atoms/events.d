module pulsar.atoms.events;

import std.signals;
import std.variant;

import pulsar.atoms;

/***
* Структура события. Расширяет mixin std.signals.Signal(T...)
* Пропускает повторное подписывание на событие. Потокобезопасен.
* Примеры:
* Определение   Event!(Object,string) OnEvent;
* Подписывание  obj.OnEvent += &rcpt.method;
* Отписывание   obj.OnEvent -= &rcpt.method;
* Вызов         OnEvent(obj,"hello");
*/
struct Event(T...)
{
	private mixin Signal!(T);
public:
	//-------------------------------------------------------------------------------------------
	~this() { }
	//-------------------------------------------------------------------------------------------
	ref Event opOpAssign(string op)(slot_t del)if(op == "+")
	{
		synchronized
		{
			if(del == null)
				return this;
			foreach(slot; slots)
				if(slot == del)
					return this;
			connect(del);
			return this;
		}
	}
	ref Event opOpAssign(string op)(slot_t del)if(op == "-")
	{
		synchronized disconnect(del);
		return this;
	}
	void opCall(T t) const//ref Event
	{
		synchronized
		{
			if(slots_idx > 0)
				emit(t);
		}
		//return this;
	}
	void opCall(T t)//ref Event
	{
		synchronized
		{
			if(slots_idx > 0)
				emit(t);
		}
		//return this;
	}
	/// Возвращает число подписчиков
	@property size_t count() const { synchronized return slots_idx; }
	/// Переопределение метода вызова Signal!(T) для константных методов
	final void emit( T i ) const
	{
		foreach (slot; slots[0 .. slots_idx])
		{   if (slot)
			slot(i);
		}
	}
}

//-------------------------------------------------------------------------------------------
//
///// Перечисление действий, вызывающих изменение объекта
//enum ChangeNotifyAction : byte
//{
//	/// Действие неизвестно
//	Unknown = 0,
//	/// Объект изменен частично
//	ObjectChange,
//	/// Объект изменен сильно (тотально)
//	ObjectReset,
//	/// В коллекцию добавлен элемент
//	ItemAdd,
//	/// В коллекции изменен элемент
//	ItemChange,
//	/// В коллекции удален элемент
//	ItemDelete,
//	/// В коллекции перемещен элемент
//	ItemMove
//}
//
///// Базовый класс аргументов событий изменений объектов
//class ChangeNotifyEventArgs
//{
//	/// Объект-источник события изменения.
//	const Object sender;
//	/// Действие, вызвавшее изменение объекта
//	const ChangeNotifyAction action;
//	/// Аргументы исходного события
//	const ChangeNotifyEventArgs sourceArgs;
//
//	/// Инициализирующий конструктор.
//	this(Object sender, ChangeNotifyAction action, ChangeNotifyEventArgs sourceArgs)
//	{
//		this.sender	= sender;
//		this.action	= action;
//		this.sourceArgs	= sourceArgs;
//	}
//}
//
//
///// Класс аргументов событий изменений объекта
//class ObjectChangeNotifyEventArgs : ChangeNotifyEventArgs
//{
//private:
//	Variant _nv;
//	Variant _ov;
//
//public:
//	/// Имя измененного члена класса объекта.
//	const string memberName;
//	/// Новое значение изменяемого члена класса.
//	@property Variant newValue() { return _nv; }
//	/// Старое значение изменяемого члена класса.
//	@property Variant oldValue() { return _ov; }
//
//	/// Инициализирующий конструктор.
//	this(Object sender, ChangeNotifyAction action, string memberName,	Variant newValue, Variant oldValue)
//	{
//		super(sender, action, null);
//		this.memberName	=	memberName;
//		_nv	=	newValue;
//		_ov	=	oldValue;
//	}
//	/// Инициализирующий конструктор.
//	this(Object sender, ChangeNotifyAction action, ChangeNotifyEventArgs sourceArgs,
//	     string memberName,	Variant newValue, Variant oldValue)
//	{
//		super(sender, action, sourceArgs);
//		this.memberName	=	memberName;
//		_nv	=	newValue;
//		_ov	=	oldValue;
//	}
//	/// Инициализирующий конструктор.
//	this(Object sender, ChangeNotifyAction action)
//	{
//		memberName = null;
//		super(sender, action, null);
//	}
//	/// Инициализирующий конструктор.
//	this(Object sender, ChangeNotifyAction action, ChangeNotifyEventArgs sourceArgs)
//	{
//		memberName = null;
//		super(sender, action, sourceArgs);
//	}
//}
//
////-------------------------------------------------------------------------------------------
//
///// Интерфейс объектов, посылающих уведомления об изменении объекта.
//interface IObjectChangeNotify
//{
//	/// Определяет, отключена ли генерация сообщений об изменении объекта.
//	@property bool isEventsOff();
//	/// Отключает генерацию сообщений об изменении объекта.
//	/// Params:
//	/// raiseResetting = Определяет необходимость генерации события тотального изменения.
//	void eventsOff(bool raiseResetting = true);
//	/// Включает генерацию сообщений об изменении объекта.
//	/// Params:
//	/// raiseResetting = Определяет необходимость генерации события тотального изменения.
//	void eventsOn(bool raiseResetted = true);
//
//	/// Событие, возникающее при изменении объекта.
//	@property ref Event!(ObjectChangeNotifyEventArgs) objectChangingEvent();
//	/// Событие, возникающее после изменении объекта.
//	@property ref Event!(ObjectChangeNotifyEventArgs) objectChangedEvent();
//}

////**************************************************************************************
///// Класс реализации интерфейса IObjectChangeNotify
//class ObjectChangeNotify : IObjectChangeNotify
//{
//protected:
//	@("NonSerialized")
//	bool _isEventOff = false;
//	@("NonSerialized")
//	Event!(ObjectChangeNotifyEventArgs) _objectChanging;
//	@("NonSerialized")
//	Event!(ObjectChangeNotifyEventArgs) _objectChanged;
//
//public:
//	/// Событие, возникающее при изменении объекта.
//	@property ref Event!(ObjectChangeNotifyEventArgs) objectChangingEvent() { return _objectChanging; }
//	/// Событие, возникающее после изменении объекта.
//	@property ref Event!(ObjectChangeNotifyEventArgs) objectChangedEvent()  { return _objectChanged; }
//
//	/// Определяет, отключена ли генерация сообщений об изменении объекта.
//	@("NonBrowsable")
//	@property bool isEventsOff() { return _isEventOff; }
//
//	/// Отключает генерацию сообщений об изменении объекта.
//	/// Params:
//	///  raiseResetting = Определяет необходимость генерации события тотального изменения.
//	void eventsOff(bool raiseResetting = true)
//	{
//		if(raiseResetting)
//			onObjectResetting();
//		_isEventOff = true;
//	}
//	/// Включает генерацию сообщений об изменении объекта.
//	/// Params:
//	///  raiseResetted = Определяет необходимость генерации события тотального изменения.
//	void eventsOn(bool raiseResetted = true)
//	{
//		_isEventOff = false;
//		if(raiseResetted)
//			onObjectResetted();
//	}
//
//protected:
//	/** Вызывает событие ObjectChanging, используя OnObjectChanging(ObjectChangeNotifyEventArgs args)
//	 * Params:
//	 *  memberName = Имя изменяемого члена класса объекта.
//	 *  oldValue = Старое значение члена класса.
//	 *  newValue = Новое значение изменяемого члена класса
//	 */
//	ObjectChangeNotifyEventArgs onObjectChanging(T)(string memberName, T newValue, T oldValue)
//	{
//		auto args = new ObjectChangeNotifyEventArgs(this, ChangeNotifyAction.ObjectChange, memberName, newValue, oldValue);
//		onObjectChanging(args);
//		return args;
//	}
//	/// Вызывает событие ObjectChanging.
//	/// Params:
//	///  args = Аргументы события
//	void onObjectChanging(ObjectChangeNotifyEventArgs args)
//	{
//		CheckNull!args;
//		if(isEventsOff == false)
//				_objectChanging(args);
//	}
//
//	/// Вызывает событие ObjectChanged, используя OnObjectChanged(ObjectChangeNotifyEventArgs args).
//	/// Params:
//	///  memberName = Имя измененного члена класса объекта.
//	///  oldValue = Старое значение члена класса.
//	///  newValue = Новое значение изменяемого члена класса.
//	void onObjectChanged(string memberName, Variant newValue, Variant oldValue)
//	{
//		auto args = new ObjectChangeNotifyEventArgs(this, ChangeNotifyAction.ObjectChange, memberName, newValue, oldValue);
//		onObjectChanged(args);
//	}
//	/// Вызывает событие OnObjectChanged.
//	/// Params:
//	///  args = Аргументы события
//	void onObjectChanged(ObjectChangeNotifyEventArgs args)
//	{
//		CheckNull!args;
//		if(isEventsOff == false)
//			_objectChanged(args);
//	}
//
//	/// Вызывает событие ObjectResetting.
//	void onObjectResetting()
//	{
//		onObjectChanging(new ObjectChangeNotifyEventArgs(this, ChangeNotifyAction.ObjectReset));
//	}
//	/// Вызывает событие ObjectResetting.
//	void onObjectResetting(ObjectChangeNotifyEventArgs args)
//	{
//		CheckNull!args;
//		onObjectChanging(args);
//	}
//
//	/// Вызывает событие ObjectResetted.
//	void onObjectResetted()
//	{
//		onObjectChanged(new ObjectChangeNotifyEventArgs(this, ChangeNotifyAction.ObjectReset, null));
//	}
//	/// Вызывает событие ObjectResetted.
//	void onObjectResetted(ObjectChangeNotifyEventArgs args)
//	{
//		CheckNull!args;
//		onObjectChanged(args);
//	}
//}
