module pulsar.atoms.weakref;

import std.exception;
import core.atomic;
import core.memory;
import pulsar.atoms;

private alias void delegate(Object) DEvent;
private extern (C) void rt_attachDisposeEvent(Object h, DEvent e);
private extern (C) void rt_detachDisposeEvent(Object h, DEvent e);

struct InvisibleAddress
{
	version(D_LP64)
	{
		static InvisibleAddress construct(void* o)
		{
			return InvisibleAddress(~cast(ptrdiff_t)o);
		}
//		this(void* o)
//		{
//			_addr = ~cast(ptrdiff_t)(o);
//		}
		void* address() @property const
		{
			return cast(void*) ~ _addr;
		}
	private:
		ptrdiff_t _addr = ~ cast(ptrdiff_t) 0;
	}
	else
	{
		static InvisibleAddress construct(void* o)
		{
			auto tmp = cast(ptrdiff_t) cast(void*) o;
			auto addrHigh = (tmp>>16)&0x0000ffff | 0xffff0000; // Address relies in kernel space
			auto addrLow = tmp&0x0000ffff | 0xffff0000;
			return InvisibleAddress(addrHigh, addrLow);
		}
//		this(void* o)
//		{
//			auto tmp = cast(ptrdiff_t) cast(void*) o;
//			_addrHigh = (tmp>>16)&0x0000ffff | 0xffff0000; // Address relies in kernel space
//			_addrLow = tmp&0x0000ffff | 0xffff0000;
//		}
		void* address() @property const
		{
			return cast(void*) (_addrHigh<<16 | (_addrLow & 0x0000ffff));
		}
	private:
		ptrdiff_t _addrHigh = 0xffff0000;
		ptrdiff_t _addrLow = 0xffff0000;
	}
}

/// Класс мягкой ссылки на объект
struct WeakRef
{
private:
	shared(InvisibleAddress) _ptr;

	@disable this(this);
	@disable void opAssign(WeakRef other);
public:
//	/// Создает и cвязывает ссылку с объектом
//	this(Object obj) { connect(obj); }
	~this() { reset(); }
	/// Связывает ссылку с объектом
	void connect(Object obj)
	{
		CheckNull!obj;
		enforce(this is WeakRef.init, "Weak reference already have target!");
		_ptr = InvisibleAddress.construct(cast(void*)obj);
		rt_attachDisposeEvent(obj, &unhook);
	}
	/// Возвращает объект, если он живой, или null.
	Object target() @property
	{
		void* o = null;
		// Two iterations are necessary, because after the atomic load
		// we still have an invisible address, thus the GC can reset
		// _ptr after we already retrieved the data. Also the call to
		// GC.addrOf needs to be done twice, otherwise still segfaults
		// still happen. With two iterations I wasn't able to trigger
		// any segfault with test/testheisenbug.d. The assertion in
		// Observer.watch never triggered anyways with this
		// implementation.
		foreach ( i ; 0..2)
		{
			auto tmp =  atomicLoad(_ptr); // Does not work with constructor
			o = tmp.address;
			if ( o is null)
				return null; // Nothing to do then.
			o = GC.addrOf(tmp.address);
		}
		if (o)
		{
			assert(GC.addrOf(o), "Not possible!");
			return cast(Object)o;
		}
		return null;
	}
	/// Отвязывает ссылку от объекта
	void reset()
	{
		auto o = target;
		if (o)
			unhook(o);
	}

private:
	void unhook(Object o)
	{
		rt_detachDisposeEvent(o, &unhook);
		atomicStore(_ptr, InvisibleAddress.construct(null));
	}
}
unittest
{
//	import std.conv;
//	import std.stdio;
//
//	class XXX
//	{
//		private int _x;
//		@property
//		{
//			int X() const { return _x; }
//			void X(int value) { _x = value; }
//		}
//		override string toString() { return "X = " ~ to!string(_x); }
//		this(int value) { _x = value; }
//		~this()
//		{
//			writeln("dtor", _x);
//		}
//	}
//
//	WeakRef wr;
//
//	foreach(i;1..20)
//	{
//		XXX x = new XXX(i);
//		WeakRef wr2;
//		if(i == 5)
//			//wr = GoRef(x, 10);
//			wr.connect(x);
//		else
//			//wr2 = GoRef(x, 20);
//			wr2.connect(x);
//	}
//	writeln(wr.target);
//
//	writeln("collect");
//	GC.collect();
//	writeln("collect done");
//
//	if(wr.target is null)
//		writeln("null");
//	else
//		writeln(wr.target);
//	writeln("unittest - weakref");
}

//---


/** Структура ссылки на объект
		* Позволяет хранить сильную или слабую ссылки на объект.
		* Также позволяет хранить сильную ссылку указанное количество времени,
		* после чего она делается слабой.
		*/
struct ObjectRef
{
private:
	/// = DateTime.init - ссылка постоянно сильная
	/// = не DateTime.init - ссылка станет слабой после этой даты
	/// = DateTime.max  - ссылка уже слабая
	DateTime _ttl;
	union
	{
		shared(InvisibleAddress) _ptr;
		Object _obj;
	}


public:
	/// obj - связываемый объект
	/// ttl - количество секунд, через которое ссылка станет слабой. 0 - навсегда сильная ссылка.
	this(Object obj, uint ttl = 0) { connect(obj, ttl); }
	~this() { reset(); }


public:
	/// Возвращает объект, если он живой, или null.
	Object opCall() { return target(); }
	/// Возвращает объект, если он живой, или null.
	@property Object target()
	{
		synchronized
		{
			if(_ttl != DateTime.max)
				return _obj;
			else
			{
				void* o = null;
				// Two iterations are necessary, because after the atomic load
				// we still have an invisible address, thus the GC can reset
				// _ptr after we already retrieved the data. Also the call to
				// GC.addrOf needs to be done twice, otherwise still segfaults
				// still happen. With two iterations I wasn't able to trigger
				// any segfault with test/testheisenbug.d. The assertion in
				// Observer.watch never triggered anyways with this
				// implementation.
				foreach ( i ; 0..2)
				{
					auto tmp =  atomicLoad(_ptr); // Does not work with constructor
					o = tmp.address;
					if ( o is null)
						return null; // Nothing to do then.
					o = GC.addrOf(tmp.address);
				}
				if (o)
				{
					assert(GC.addrOf(o), "Not possible!");
					return cast(Object)o;
				}
				return null;
			}
		}
	}
	/// Определяет, жив ли объект
	@property bool isAlive() { return target !is null; }


	/// Связывает ссылку с объектом
	/// Params:
	/// obj - связываемый объект
	/// ttl - количество секунд, через которое ссылка станет слабой. 0 - навсегда сильная ссылка.
	void connect(Object obj, uint ttl = 0)
	{
		CheckNull!obj;
		if(this._ptr != shared(InvisibleAddress).init)
			reset();
		_obj = obj;
		if(ttl > 0)
		{
			DateTime now = cast(DateTime)(Clock.currTime());
			now += seconds(ttl);
			_ttl = now;
		}
	}

	/// Делает ссылку слабой.
	/// Params:
	///  ttl - значение в секундах, через которое ссылка станет слабой. Если 0, ссылка становится мягкой немедленно.
	void makeWeak(uint ttl)
	{
		synchronized
		{
			if(_ttl == DateTime.max)
				return;
			if(ttl == 0)
				makeWeak();
			else
			{
				DateTime now = cast(DateTime)(Clock.currTime());
				now += seconds(ttl);
				_ttl = now;
			}
		}
	}
	void makeWeak()
	{
		synchronized
		{
			//writeln("makeWeak ", Clock.currTime());
			if(_ttl == DateTime.max)
				return;
			Object obj = _obj;
			_ptr = InvisibleAddress.construct(cast(void*)obj);
			rt_attachDisposeEvent(obj, &unhook);
			_ttl = DateTime.max;
		}
	}

	/// Отвязывает ссылку от объекта
	void reset()
	{
		synchronized
		{
			if(_ttl != DateTime.max)
				_obj = null;
			else
			{
				auto o = target;
				if (o)
					unhook(o);
			}
			_ttl = DateTime.init;
		}
	}

	bool checkAndWeak()
	{
		synchronized
		{
			if(_ttl == DateTime.init || _ttl == DateTime.max)
				return false;
			if(_ttl < cast(DateTime)(Clock.currTime()))
			{
				makeWeak();
				return true;
			}
			return false;
		}
	}
private:
	void unhook(Object o)
	{
		rt_detachDisposeEvent(o, &unhook);
		//atomicStore(_ptr, InvisibleAddress.construct(null));
		_obj = null;
	}
}

version(unittest)
{
	import std.conv;
	import std.stdio;
	class XXX
	{
		private int _x;
		@property
		{
			int X() const { return _x; }
			void X(int value) { _x = value; }
		}
		override string toString() { return "X = " ~ to!string(_x); }
		protected this() {}
		this(int value) { _x = value; }
		~this()
		{
			writeln("dtor", X);
		}
		void dest(int i)
		{
			GolRef wr2;
			wr2.connect(this,10);
		}
	}
}
unittest
{
//	import std.conv;
//	import std.stdio;
//	import core.memory;
//
//	class XXX : GlobalObject
//	{
//		private int _x;
//		@property
//		{
//			int X() const { return _x; }
//			void X(int value) { _x = value; }
//		}
//		override string toString() { return "X = " ~ to!string(_x); }
//		this(int value) { _x = value; }
//		~this()
//		{
//			writeln("dtor", _x);
//		}
//		void dest(int i)
//		{
//			GoRef wr2;
//			wr2.connect(this,10);
//		}
//	}
//
//	GoRef wr;
//
//	foreach(i;1..20)
//	{
//		XXX x = new XXX(i);
//		GoRef wr2;
//		if(i == 5)
//			//wr = GoRef(x, 10);
//			wr.connect(x,0);
//		if(i == 10)
//			x.dest(i);
//		else
//			//wr2 = GoRef(x, 20);
//			wr2.connect(x,0);
//	}
//	writeln(wr.target);
//
//	writeln("collect");
//	GC.collect();
//	writeln("collect done");
//
//	if(wr.target is null)
//		writeln("null");
//	else
//		writeln(wr.target);
//	writeln("unittest - weakref");
}
