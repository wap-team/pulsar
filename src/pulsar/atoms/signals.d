module pulsar.atoms.signals;

import std.traits;
import std.typetuple : Filter, TypeTuple;
import std.meta;

import pulsar.atoms;
import pulsar.atoms.traits;

/// Структура атрибута класса или метода, не использующего сигналы.
struct NoSignalAttribute { }
enum nosignal = NoSignalAttribute.init;
template IsNoSignalAttribute(alias T)
{
	enum IsNoSignalAttribute = is(typeof(T) : NoSignalAttribute);
}
alias HasNoSignalAttribute(alias T) = std.meta.anySatisfy!(IsNoSignalAttribute,
																																																													__traits(getAttributes, T));


class SignalThrowable : Throwable
{
	string signal;
	string slot;
	this(Throwable tr)
	{
		super(tr.msg, tr.file, tr.line,  tr);
		this.next = tr;
	}
}

/// Статический класс шины сигналов
class SignalBus
{
private:
	static __gshared bool _enabled;
	static __gshared Slot[] _slots;
	static __gshared size_t[ushort.max] _ixSlots;
	static __gshared Slot[] _pollSlots;
	static __gshared size_t[ushort.max] _ixPollSlots;

	// Структура информации о подписчике
	struct Slot
	{
		TypeInfo senderType;
		// Указатель на метод, посылающий сигналы
		void* senderPtr;
		// Указатель на метод подписчика, принимающий сигналы
		void* funcPtr;
		// Объект, принимающий сигналы (подписчик)
		void* objPtr;
		ushort next;
		string handler;

		// Вызов делегата
		auto call(E, Args...)(Args args) if(is(E == delegate))
		{
			if(funcPtr is null)
				return null;
			E d;
			d.ptr = objPtr;
			d.funcptr = cast(typeof(E.funcptr))funcPtr;
			d(args);
		}
	}

public:
	@property // enabled
	{
		static bool enabled() nothrow { return _enabled; }
		static void enabled(bool val) { _enabled = val; }
	}

	/** Регистрация метода подписчика на метод
		Params:
			sender = Полное имя метода, на который осуществляется подписка
			mx = Делегат метода подписчика.

			Делегат метода подписчика должен быть вида:
				void name(SenderType, a, b) nothrow
				1. Первый параметр должен быть типом, объявляющим метод,
							на который осуществляется подписка.
				2. Типы остальных параметров должны в точности совпадать с
							параметрами метода, на который осуществляется подписка
				3. nothrow не обязятелен

		Example:
		--------------------
		class Sender { int meth(int a, ref string b) {  } }
		class Listner
		{
			void meth2(Sender s, int a, ref string b) nothrow {  }
			this()
			{
				SignalBus.regSlot!(Sender.meth)(&meth2);
			}
		}
		--------------------
	**/
	static void regSlot(alias srcMeth,DT, string catcher = __FUNCTION__)(DT mx)
		if(isCallable!srcMeth)
	{
		alias Sender = Alias!(__traits(parent, srcMeth));
		static if(is(DT == delegate))
			alias Dp = ParameterTypeTuple!(typeof(mx.funcptr));
		else static if(isFunctionPointer!mx)
			alias Dp = ParameterTypeTuple!(typeof(mx));
		else
			static assert(false, "To Do - " ~ typeof(mx).stringof);

		// Ищем метод класса, подходящий по параметрам
		enum IsHasParams(alias func) = is(ParameterTypeTuple!func == Dp[1..$]);
		alias sender = Filter!(IsHasParams,__traits(getVirtualMethods, Sender,
																																														__traits(identifier,srcMeth)))[0];


		alias Tp = ParameterTypeTuple!sender;

		static assert(Dp.length >= 1 && is(Dp[0] : Sender) ,
																"У метода обработчика первым параметром должен быть " ~
																Sender.stringof ~  "!");
		static assert(Tp.stringof == Dp[1..$].stringof, // Строки чтобы ref не крался
																"Отличаются типы или количество параметров!");

		Slot slot;
		slot.senderType = typeid(Sender);
		slot.senderPtr = &sender;
		static if(is(DT == delegate))
		{
			slot.objPtr = mx.ptr;
			slot.funcPtr = mx.funcptr;
		}
		else static if(isFunctionPointer!mx)
		{
			import std.functional : toDelegate;
			auto d = toDelegate(mx);

			slot.objPtr  = d.ptr;
			slot.funcPtr = d.funcptr;
		}
		else
			static assert(false, "To Do");
		slot.handler = catcher;
		add(slot, _slots, _ixSlots);
	}
	/** Регистрация метода подписчика на тип
		Params:
			Sender = Полное имя типа, на который осуществляется подписка
			mx = Делегат метода подписчика.

			Делегат метода подписчика должен быть вида:
				void name(SenderType, string) nothrow
				1. Первый параметр должен быть типом,
							на который осуществляется подписка.
				2. Второй параметр имя метода, вызвавшего сигнал.
				3. nothrow не обязятелен

		Example:
		--------------------
		class Sender { int meth(int a, ref string b) {  } }
		class Listner
		{
			void meth2(Sender s, string methName) nothrow {  }
			this()
			{
				SignalBus.regSlot!(Sender)(&meth2);
			}
		}
		--------------------
	**/
	static void regSlot(Sender,DT, string catcher = __FUNCTION__)(DT mx)
		if(is(Sender == class))
	{
		static if(is(DT == delegate))
			alias Dp = ParameterTypeTuple!(typeof(mx.funcptr));
		else static if(isFunctionPointer!mx)
			alias Dp = ParameterTypeTuple!(typeof(mx));
		else
			static assert(false, "To Do - " ~ typeof(mx).stringof);

		static assert(Dp.length == 2 && is(Dp[0] : Sender) && is(Dp[1] : string),
																"У метода обработчика первым параметром должен быть " ~
																Sender.stringof ~  ", вторым string!");

		Slot slot;
		slot.senderType = typeid(Sender);
		static if(is(DT == delegate))
		{
			slot.objPtr = mx.ptr;
			slot.funcPtr = mx.funcptr;
		}
		else static if(isFunctionPointer!mx)
		{
			slot.objPtr  = null; //mx.ptr;
			slot.funcPtr = mx;
		}
		else
			static assert(false, "To Do - " ~ typeof(mx).stringof);
		slot.handler = catcher;
		add(slot, _slots, _ixSlots);
	}

	/** Метод для посылки сигнала подписчикам.
			Params:
			sender = Полное имя метода, посылающего сигнал
			args   = Аргументы вызова.

		Example:
		--------------------
		class Sender
		{
			int meth(int a, string b)
			{
				SignalBus.signal!(Sender.meth)(this,a,b);
			}
		}
		--------------------
	**/
	static void signal(alias srcMeth, Args...)(Args args) nothrow
	{
		try
		{
			alias Sender = Alias!(__traits(parent, srcMeth));
			static assert(is(Args[0] : Sender));
			//pragma(msg, "Sender=",Sender);

			template XX(alias meth, size_t index = 0)
			{
				alias M = void delegate(Args[0],ParameterTypeTuple!meth);
				//pragma(msg, ParameterTypeTuple!M,"-", Args);
				static if(is(ParameterTypeTuple!M == Args))
					alias XX = meth;
				else
					alias XX = XX!(__traits(getOverloads, Sender,
																													__traits(identifier,srcMeth))[index], index+1);
			}
			alias sender = XX!srcMeth;
			alias DT = void delegate(Sender, ParameterTypeTuple!sender);
			alias ODT = void delegate(Sender, string);
			alias FT = void function(Sender, string);
			//pragma(msg, Sender.stringof, ".",__traits(identifier,srcMeth)," => ",DT);

			size_t i = getFirst(typeid(Sender), _slots, _ixSlots);

			if(i)
			{
				do
				{
					Slot* sl = _slots.ptr + i ;
					try
					{
						if((*sl).senderPtr is null)
						{
							if((*sl).objPtr is null)
							{
								FT f = cast(FT)((*sl).funcPtr);
								f(args[0], __traits(identifier,srcMeth));
							}
							else
							{
								ODT d;
								d.ptr = (*sl).objPtr;
								d.funcptr = cast(typeof(ODT.funcptr))((*sl).funcPtr);
								d(args[0], __traits(identifier,srcMeth));
							}
						}
						else if((*sl).senderPtr is &sender)
						{
							DT d;
							d.ptr = (*sl).objPtr;
							d.funcptr = cast(typeof(DT.funcptr))((*sl).funcPtr);
							d(args);
						}
					}
					catch(Throwable tr)
					{
						SignalThrowable str = new SignalThrowable(tr);
						str.signal = fullyQualifiedName!srcMeth;
						str.slot = (*sl).handler;
						import pulsar.server.log;
						Logger.logError(str);
					}
					i = (*sl).next;
				} while(i > 0);
			}
		}
		catch(Throwable tr)
		{
			import pulsar.server.log;
			Logger.logError(tr);
		}
	}
	static void signal(Sender)(Sender s, string msg) nothrow
	{
		try
		{
			alias ODT = void delegate(Sender, string);
			alias FT = void function(Sender, string);
			//pragma(msg, Sender.stringof, ".",__traits(identifier,srcMeth)," => ",DT);

			size_t i = getFirst(typeid(Sender), _slots, _ixSlots);

			if(i)
			{
				do
				{
					Slot* sl = _slots.ptr + i ;
					try
					{
						if((*sl).senderPtr is null)
						{
							if((*sl).objPtr is null)
							{
								FT f = cast(FT)((*sl).funcPtr);
								f(s, msg);
							}
							else
							{
								ODT d;
								d.ptr = (*sl).objPtr;
								d.funcptr = cast(typeof(ODT.funcptr))((*sl).funcPtr);
								d(s, msg);
							}
						}
					}
					catch(Throwable tr)
					{
						SignalThrowable str = new SignalThrowable(tr);
						str.signal = fullyQualifiedName!Sender ~ " => " ~ msg;
						str.slot = (*sl).handler;
						import pulsar.server.log;
						Logger.logError(str);
					}
					i = (*sl).next;
				} while(i > 0);
			}
		}
		catch(Throwable tr)
		{
			import pulsar.server.log;
			Logger.logError(tr);
		}
	}

	/* ** Методы голосования ***/
	static void regPollSlot(alias srcMeth,DT, string catcher = __FUNCTION__)(DT mx)
		if(isCallable!srcMeth)
	{
		alias Sender = Alias!(__traits(parent, srcMeth));
		static if(is(DT == delegate))
			alias Dp = ParameterTypeTuple!(typeof(mx.funcptr));
		else static if(isFunctionPointer!mx)
			alias Dp = ParameterTypeTuple!(typeof(mx));
		else
			static assert(false, "To Do - " ~ typeof(mx).stringof);

		// Ищем метод класса, подходящий по параметрам
		enum IsHasParams(alias func) = is(ParameterTypeTuple!func == Dp[1..$]);
		alias sender = Filter!(IsHasParams,__traits(getVirtualMethods, Sender,
																																														__traits(identifier,srcMeth)))[0];


		alias Tp = ParameterTypeTuple!sender;

		static assert(Dp.length >= 1 && is(Dp[0] : Sender) ,
																"У метода обработчика первым параметром должен быть " ~
																Sender.stringof ~  "!");
		static assert(Tp.stringof == Dp[1..$].stringof, // Строки чтобы ref не крался
																"Отличаются типы или количество параметров!");

		Slot slot;
		slot.senderType = typeid(Sender);
		slot.senderPtr = &sender;
		static if(is(DT == delegate))
		{
			slot.objPtr = mx.ptr;
			slot.funcPtr = mx.funcptr;
		}
		else static if(isFunctionPointer!mx)
		{
			import std.functional : toDelegate;
			auto d = toDelegate(mx);

			slot.objPtr  = d.ptr;
			slot.funcPtr = d.funcptr;
		}
		else
			static assert(false, "To Do");
		slot.handler = catcher;
		add(slot, _pollSlots, _ixPollSlots);
	}
	static void poll(alias srcMeth, Args...)(Args args)
	{
		try
		{
			alias Sender = Alias!(__traits(parent, srcMeth));
			static assert(is(Args[0] : Sender));
			//pragma(msg, "Sender=",Sender);

			template XX(alias meth, size_t index = 0)
			{
				alias M = void delegate(Args[0],ParameterTypeTuple!meth);
				//pragma(msg, ParameterTypeTuple!M,"-", Args);
				static if(is(ParameterTypeTuple!M == Args))
					alias XX = meth;
				else
					alias XX = XX!(__traits(getOverloads, Sender,
																													__traits(identifier,srcMeth))[index], index+1);
			}
			alias sender = XX!srcMeth;
			alias DT = void delegate(Sender, ParameterTypeTuple!sender);
			alias ODT = void delegate(Sender, string);
			alias FT = void function(Sender, string);
			//pragma(msg, Sender.stringof, ".",__traits(identifier,srcMeth)," => ",DT);

			size_t i = getFirst(typeid(Sender), _pollSlots, _ixPollSlots);

			if(i)
			{
				do
				{
					Slot* sl = _pollSlots.ptr + i ;
					try
					{
						if((*sl).senderPtr is null)
						{
							if((*sl).objPtr is null)
							{
								FT f = cast(FT)((*sl).funcPtr);
								f(args[0], __traits(identifier,srcMeth));
							}
							else
							{
								ODT d;
								d.ptr = (*sl).objPtr;
								d.funcptr = cast(typeof(ODT.funcptr))((*sl).funcPtr);
								d(args[0], __traits(identifier,srcMeth));
							}
						}
						else if((*sl).senderPtr is &sender)
						{
							DT d;
							d.ptr = (*sl).objPtr;
							d.funcptr = cast(typeof(DT.funcptr))((*sl).funcPtr);
							d(args);
						}
					}
					catch(Throwable tr)
					{
						SignalThrowable str = new SignalThrowable(tr);
						str.signal = fullyQualifiedName!srcMeth;
						str.slot = (*sl).handler;
						import pulsar.server.log;
						throw str;
					}
					i = (*sl).next;
				} while(i > 0);
			}
		}
		catch(Throwable tr)
		{
			if(typeid(tr) is typeid(SignalThrowable))
				throw tr;
			import pulsar.server.log;
			Logger.logError(tr);
		}
	}


private:
	static void add(ref Slot slot, ref Slot[] arr, ref size_t[ushort.max] ix)
	{
		if(arr.length == 0)
			arr.length++;
		arr ~= slot;
		ushort h = cast(ushort)((cast(void*)(slot.senderType)));
		auto end = h-1;
		for( ; h != end; h++)
			if(h > 0 && ( ix[h] == 0 || arr[ix[h]].senderType is slot.senderType))
				break;
		if(h == end)
			throw new Exception("Шина переполнена!");

		//trace(slot.senderType, " ", h, " ", ix[h]);

		if(ix[h] == 0)
			ix[h] = cast(ushort)(arr.length-1);
		else
		{
			auto i = ix[h];
			while(arr[i].next > 0)
				i = arr[i].next;
			arr[i].next = cast(ushort)(arr.length-1);
		}
	}
	static size_t getFirst(TypeInfo val, ref Slot[] arr, ref size_t[ushort.max] ix)
	{
		ushort h = cast(ushort)((cast(void*)val));
		//trace(" > ", val, " ", h);
		if(h == 0)
			h = 1;
		if(ix[h] == 0)
			return 0;
		if(arr[ix[h]].senderType is val)
			return ix[h];

		auto end = h++;
		for( ; h != end; h++)
			if(h > 0 && ( ix[h] == 0 || arr[ix[h]].senderType is val))
				break;
		if(h == end || ix[h] == 0)
			return 0;
		return ix[h];
	}
}

unittest
{
	import pulsar.atoms;

	version(trace)trace("--- SignalBus ... ---");

	class Sender
	{
		int _a;
		bool _b;

		bool work() const { return true; }
		void work(int a, ref bool b)
		{
			version(trace)trace("Sender -> work(a=",a,", b=",b, ")");
			_a = a;
			_b = b;
			SignalBus.signal!(Sender.work)(this,a,b);
		}
		void work_c(int a, ref bool b)
		{
			version(trace)trace("Sender -> work_c(a=",a,", b=",b, ")");
			_a = a; _b = b;
			// SignalBus.signal!(Sender.work_c)(this, a, b);
		}
		void print()
		{
			version(trace)trace("Sender -> print()");

			SignalBus.signal!(Sender.print)(this);
		}
	}

	class Rec
	{
		this()
		{
			SignalBus.regSlot!(Sender.work)(&this.x_work);
			//SignalBus.regSlot!(Sender.print)(&this.x_print);
			//SignalBus.regSlot!(Sender)(&this.x_all);
		}

		void x_work(Sender sender, int a, ref bool b) nothrow
		{
			try
			{
				version(trace)trace("x_work -> a=",a, " b=", b, " sender.a=", sender._a, " sender.b=", sender._b);
			}
		catch {}
		}
		void x_print(Sender sender) nothrow
		{
			try
			{
				version(trace)trace("x_print -> sender.a=", sender._a, " sender.b=", sender._b);
				}
			catch {}
		}
		void x_all(Sender sender, string methName) nothrow
		{
			try {
			version(trace)trace("-- all: ",methName, " --");
			} catch {}
		}
	}

	Sender s = new Sender();
	Rec r = new Rec();
	//Rec r2 = new Rec();
	bool b = true;
	s.work(7,b);
	b = false;
	s.work_c(9,b);
	s.print();

	version(benchmark)
	{
		trace("*benchmark*");
		StopWatch sw;
		sw.reset();
		sw.start();
		foreach(i;0..10000000)
			s.work_c(7,b);
		sw.stop();
		trace("clear:\t",sw.peek().msecs);

		sw.reset();
		sw.start();
		foreach(i;0..10000000)
			s.work(7,b);
		sw.stop();
		trace("signal&slot:\t",sw.peek().msecs);

		sw.reset();
		sw.start();
		foreach(i;0..10000000)
			s.print();
		sw.stop();
		trace("signal only:\t",sw.peek().msecs);
	}

	version(trace)trace("--- SignalBus done ---");
}

