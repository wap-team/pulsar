module pulsar.gol.support;

public import std.traits;
public import std.typetuple;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;
import pulsar.gol;
import pulsar.gol.servant;

// Определение суррогатов для создания проксей объектов для GOL.
shared static this()
{
	SerializationTuning.hasSerializationSurrogate = & hasSerializationSurrogate;
	SerializationTuning.toStringSurrogate = & toStringSurrogate;
	SerializationTuning.fromStringSurrogate = & fromStringSurrogate;
}
version(Server)
{
	bool hasSerializationSurrogate(IMetaType mt, bool isRoot)
	{
		return isRoot == false && mt.kind == MetaTypeKind.Class &&
				(cast(IMetaTypeAggregate)mt).hasAttribute(typeid(GolEssenceAttribute));
	}
	string toStringSurrogate(IMetaType mt, MetaPtr ptr)
	{
		if(mt.kind == MetaTypeKind.Class)
		{
			Object o;
			mt.setVarValuePtr(&o, ptr);
			IServant oi = cast(IServant)o;
			if(oi is null)
				throw new Exception("Объект типа " ~ mt.fullName ~	" не упакован в прокси Servant!");
			return oi.oid.toString();
		}
		throw new Exception("Сурогат сериализации для типа " ~ mt.fullName ~	" не определен!");
	}
	MetaPtr fromStringSurrogate(IMetaType mt, string oid)
	{
		if(mt.kind == MetaTypeKind.Class)
		{
			// Это хак для исключения полей из классов
			if(mt.typeInfo is typeid(Object))
				return MetaPtr.init;

			Object obj = GOL.get(UUID(oid), cast(const(ClassInfo))mt.typeInfo);
			if(obj is null)
				throw new Exception("Не удалось раскрыть заглушку глобального объекта [" ~
																								oid ~ "] типа " ~ mt.fullName);
			TypeInfo ti = typeid(obj);
			if(cast(IProxy)obj)
				ti = (cast(IProxy)obj).targetTypeId;
			if(ti != mt.typeInfo)
				throw new Exception("Глобальный объект [" ~ oid ~
																								"] имеет тип " ~ ti.toString() ~
																								" вместо ожидаемого " ~ mt.fullName);
			return MetaPtr.of(obj);
		}
		throw new Exception("Сурогат десериализации для типа " ~ mt.fullName ~ " не определен!");
	}
}

//-----------------------------------------------------------------------------------
