module pulsar.gol.servant;

import std.traits;

import pulsar.atoms;
public import pulsar.atoms.proxies;
import pulsar.atoms.traits;
import pulsar.reflection;

//-----------------------------------------------------------------------------
/// Интерфейс слуг объектов сервера Пульсара.
interface IServant : ITransparentProxy, IOIDObject
{
	/// Тип проксируемого объекта
	@property ClassInfo targetTypeId();

	/// Определяет, заблокированы ли сигналы изменения объекта
	@property bool signalsBlocked() const nothrow;
	// Блокирует сигналы изменения объекта.
	// Возвращает true, если вызов метода установил блокировку (то есть, блокировки
	// до этого момента не было)
	bool blockSignals();
	// Разблокирует сигналы изменения объекта
	// Если sendSignal = true, посылаается сообщение об изменении
	void unblockSignals(bool sendSignal = true);

	final static bool blockSignals(const(Object) o)
	{
		IServant i = cast(IServant)o;
		if(i)
			return i.blockSignals();
		return false;
	}
	final static void unblockSignals(const(Object) o,bool sendSignal = true)
	{
		IServant i = cast(IServant)o;
		if(i)
			i.unblockSignals(sendSignal);
	}


	@property // oid
	{
		ref OID oid() const;
		/// Установка OID для серванта
		void oid(OID);
	}

//	/// Фабрика создания слуг.
//	static auto create(T,Args...)(Args ctorArgs)
//	{
//		static if(Args.length == 1 && is(Args[0] == T))
//			return new Servant!(T)(ctorArgs[0]);
//		else
//			return new Servant!(T)(randomUUID,ctorArgs);
//	}
}

//-----------------------------------------------------------------------------
	/** Класс серверных слуг объектов GOL.
	* Важно! Шаблонные методы не проксируются.
	* Params:
	* T - тип проксируемого объекта.
	*/
final class Servant(T : Object) : T, IServant
{
	static assert(T.stringof != "Client", "Client !!!");
	static assert(!is(T : ITransparentProxy),"Cannot create Servant of [" ~ T.stringof ~
																																										"] - it is implement ITransparentProxy!");

	enum IsOidObj = IsIOIDObject!T;

private:
	bool _signalsBlocked = false;
	int _callDeep;

public:
	// IProxy
	@property const(T) targetObject() const { return super; }
	@property ClassInfo targetTypeId() { return typeid(T); }

	// IOIDObject
	static if(IsOidObj)
	{
		override @property ref OID oid() const
		{
//			static if(__traits(isAbstractFunction, super.oid))
//				assert(false,"oid is abstract!");
//			else
				return super.oid;
		}
		@property void oid(OID val)
		{
			static if(__traits(isAbstractFunction, super.oid) == false)
				enforce(super.oid.empty == false, "Пустой OID у " ~ T.stringof);
		}
	}
	else
	{
		private UUID _golOID;
		@property ref OID oid() const { return _golOID; }
		@property void oid(OID val) { _golOID = val; }
	}

	@property bool signalsBlocked() const nothrow { return _signalsBlocked; }
	bool blockSignals()
	{
		if(_signalsBlocked)
			return false;
		_signalsBlocked = true;
		return true;
	}
	void unblockSignals(bool sendSignal = true)
	{
		_signalsBlocked = false;
		if(sendSignal)
		{
			SignalBus.signal!T(super, "unblockSignals");
			onChanged("unblockSignals");
		}
	}

package(pulsar): // Constructors
//	this(T t)
//	{
//		if(t !is null)
//			assert(false, "TODO: Packing " ~ T.stringof ~ "...");
//	}
	this(CtorArgs...)(CtorArgs args) //if(CtorArgs.length != 1)
	{
		static if(IsOidObj == false)
			if(_golOID == OID.init)
				_golOID = randomUUID;
		_signalsBlocked = true;
		scope(exit)
			_signalsBlocked = false;
		static if(args.length > 0)
			super(args);
		else static if(is(typeof(T.__ctor())))
			super();
	}

	bool onChanged(string name) nothrow
	{
		//_callDeep--;
		if(SignalBus.enabled && _signalsBlocked == false)
		{
			if(_callDeep <= 1)
			{
				import pulsar.gol;
				try
					trace("**Autosave(",T.stringof,".",name,") => ", super.toString());
				catch(Throwable) { }
				// TODO : не GOL.save, а StorageProvider.save
				GOL.save(this, name);
				try
					trace("");
				catch(Throwable) { }
			}
			return true;
		}
		return false;
	}

	//-- Override Methods ---
public:
		pragma(msg, "--- ", T.stringof, " (IsOidObj:", IsOidObj, ") ----");

		static if(HasNoSignalAttribute!T)
			enum X = getProxyMethodsDefinitions!(T,"false",null,"false",null,"super."~ T.stringof ~ ".")();
		else
			enum X = getProxyMethodsDefinitions!(T,
				// beforeCallCheck
				"!(IsPublic!meth == false || IsConst!meth || pulsar.atoms.signals.HasNoSignalAttribute!meth)",
				// beforeCall
				"_callDeep = _callDeep < 0 ? 1 : (_callDeep + 1);\n scope(exit)_callDeep--;" ,
				// afterCallCheck
				"!(IsPublic!meth == false || IsConst!meth || pulsar.atoms.signals.HasNoSignalAttribute!meth)",
				// afterCall
				//"import " ~ ModuleName!T ~ ";\n" ~
				"if(onChanged(\"__funcName__\"))\n" ~
				"   SignalBus.signal!(__funcFullName__)(super,__paramsNames__);",
				// callPrefix
				"super.")();

//		static if(T.stringof == "ProcessSale")
//			pragma(msg,X);
		mixin(X);

public:
	override bool opEquals(Object o)
	{
		auto x = cast(Servant!(T))(o);
		if(x !is null)
			o = cast(Object)x.targetObject;
		return super.opEquals(o);
	}
	override int opCmp(Object o)
	{
		auto x = cast(Servant!(T))(o);
		if(x !is null)
			o = cast(Object)x.targetObject;
		return super.opCmp(o);
	}
	override size_t toHash() nothrow @trusted
	{
		return super.toHash();
	}
	IServant opCast(IServant)()  nothrow
	{
		return this;
	}
	IServant opCast(IServant)() const nothrow
	{
		return this;
	}

	//--- Alias this ---
	static if(__traits(getAliasThis, T).length > 0)
	{
		enum AliasThisName = __traits(getAliasThis, T)[0];
		pragma(msg,"AliasThis = ", AliasThisName);

// закомментированно, так как непонятно зачем нужно
// вроде как для вызова сервантных методов алиас объекта,
// но 02.05.2018 v2.078.3 и так вызываются вроде

//		alias meth = Alias!(__traits(getMember, T, AliasThisName));
//		static assert(isSomeFunction!meth,"ToDo");
//		alias AT = Unqual!(ReturnType!(meth));
//		static if(is(AT == class))
//		{
//			Servant!AT _aliasThis;
//			enum ATs = "override @property const(AT) " ~ AliasThisName ~ `() const
//			{
//				auto x =	super.`~ T.stringof ~ "." ~ AliasThisName ~ `();
//				if(cast(Servant!AT)x !is null)
//					return cast(const(AT))x;
//				if(_aliasThis is null || _aliasThis.targetObject !is x)
//					*(cast(AT*)&_aliasThis) = new Servant!AT(cast(AT)x);
//				return _aliasThis;
//			}
//			alias ` ~ AliasThisName ~ " this;";
//			pragma(msg, ATs);
//			mixin(ATs);
//		}
	}
	//-- Serialization actions ---
	static if(HasOwnMethod!(T,"onSerializing"))
		override void onSerializing()
		{
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			super.onSerializing();
		}
	static if(HasOwnMethod!(T,"onSerialized"))
		override void onSerialized()
		{
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			super.onSerialized();
		}
	static if(HasOwnMethod!(T,"onDeserializing"))
		override void onDeserializing()
		{
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			super.onDeserializing();
		}
	static if(HasOwnMethod!(T,"onDeserialized"))
		override void onDeserialized()
		{
			//pragma(msg, T.stringof ~"."~"onDeserialized");
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			super.onDeserialized();
		}
}

/* Сервант с ссылкой на объект.
			Ссылка на объект вызывает проблемы:
			1. Не правильно сериализуется. Надо добелать сериализацию,
						чтобы значения в _t сериализовались как this.
			2. Методы, вызываемые из методов _t не вызывают методов серванта (нарушение полиморфизма).
			В общем, скорее всего проще сериализовать объект и десериализовать в сервант.


final class Servant(T : Object) : T, IServant
{
	//static assert(__traits(getAliasThis, T).length == 0, "Servant для alias this не реализован!");
	static assert(!is(T : ITransparentProxy),"Cannot create Servant of [" ~ T.stringof ~
																																										"] - it is implement ITransparentProxy!");

	enum IsOidObj = IsIOIDObject!T;

private:
	T _t;
	bool _signalsBlocked = false;
	int _callDeep;

public:
	/// Проксируемый объект
	@property inout(T) target() inout { return _t ? _t : super; }
	/// Тип проксируемого объекта
	@property ClassInfo targetTypeId() { return typeid(T); }
	// OID
	static if(IsOidObj)
	{
		override @noproxy @property ref OID oid() const { return _t ? _t.oid : super.oid; }
		ref OID golOID() const { return _t ? _t.oid : super.oid; }
		void golOID(UUID val)
		{
			if(_t)
				setOid(_t, oid);
			else
				setOid(super, val);

//			if(oid.empty)
//			{
//				trace(targetTypeId);
//				if(_t)
//					setOid(_t, val);
//				else
//					setOid(this, val);
			}
		}
	}
	else
	{
		private UUID _golOID;

		ref OID golOID() const { return _golOID; }
		void golOID(UUID val) { _golOID = val; }
	}

	@property bool signalsBlocked() const nothrow { return _signalsBlocked; }
	void blockSignals()
	{
		_signalsBlocked = true;
	}
	void unblockSignals(bool sendSignal = true)
	{
		_signalsBlocked = false;
		if(sendSignal)
		{
			SignalBus.signal!T(_t?_t:super, "unblockSignals");
			onChanged("unblockSignals");
		}
	}

package(pulsar): // Constructors
	this(T t)
	{
		_t = t;
		static if(IsOidObj == false)
			if(_golOID == OID.init)
				_golOID = randomUUID;
	}
	this(CtorArgs...)(CtorArgs args) if(CtorArgs.length != 1)
	{
		static if(IsOidObj == false)
			if(_golOID == OID.init)
				_golOID = randomUUID;
		_signalsBlocked = true;
		scope(exit)
			_signalsBlocked = false;
		static if(args.length > 0)
			super(args);
		else static if(is(typeof(T.__ctor())))
			super();
	}

	bool onChanged(string name) nothrow
	{
		_callDeep--;
		if(SignalBus.enabled && _signalsBlocked == false)
		{
			if(_callDeep <= 0)
			{
				import pulsar.gol;
				try
				{
					trace("");
					trace("**Autosave(",T.stringof,".",name,") => ", super.toString());
				} catch { }
				GOL.save(this);
			};
			return true;
		}
		return false;
	}
	//-- Override Methods ---
	version(LogProxyCall)
	{
private:
	void beforeCall(string funName) const { Logger.log(0, "↳ %s", funName); }
	void afterCall (string funName) const { Logger.log(0, "↵ %s", funName); }
public:
	pragma(msg,getProxyMethodsDefinitions!(T)("beforeCall(__funcName__);",
																																											"afterCall(__funcName__);",
																																											"(_t?_t:super)."~ T.stringof ~ "."));
	mixin(getProxyMethodsDefinitions!(T)("beforeCall(__funcName__);",
																																						"afterCall(__funcName__);",
																																						"(_t?_t:super)."~ T.stringof ~ "."));
	}
	else
	{
public:
		pragma(msg, "--- ", T.stringof, " ----");

		static if(HasNoSignalAttribute!T)
			enum X = getProxyMethodsDefinitions!(T,"false",null,"false",null,
				"(_t?_t:super)."~ T.stringof ~ ".")();
		else
			enum X = getProxyMethodsDefinitions!(T,
				// beforeCallCheck
				"!(IsPublic!meth == false || IsConst!meth || pulsar.atoms.signals.HasNoSignalAttribute!meth)",
				// beforeCall
				"_callDeep = _callDeep < 0 ? 1 : (_callDeep + 1);",
				// afterCallCheck
				"!(IsPublic!meth == false || IsConst!meth || pulsar.atoms.signals.HasNoSignalAttribute!meth)",
				// afterCall
				"import " ~ ModuleName!T ~ ";\n" ~
				"  if(onChanged(\"__funcName__\"))\n" ~
				"   SignalBus.signal!(" ~ fullyQualifiedName!T ~ ".__funcName__)" ~
																																								"((_t?_t:super),__paramsNames__);",
				// callPrefix
				"(_t?_t:super)."~ T.stringof ~ ".")();

//		static if(T.stringof == "FeedTree")
//			pragma(msg,X);
		mixin(X);
	}

public:
	override bool opEquals(Object o)
	{
		auto x = cast(Servant!(T))(o);
		if(x !is null)
			o = x.target;
		return _t ? _t.opEquals(o) : super.opEquals(o);
	}
	override int opCmp(Object o)
	{
		auto x = cast(Servant!(T))(o);
		if(x !is null)
			o = x.target;
		return _t ? _t.opCmp(o) : super.opCmp(o);
	}
	override size_t toHash() nothrow @trusted
	{
		return _t ? _t.toHash() : super.toHash();
	}
//	static if(IsConst!(__traits(getMember, T, "toString")))
//  override string toString() const { return _t ? _t.toString() : super.toString(); }
//	else
//		override string toString() { return _t ? _t.toString() : super.toString(); }
	IServant opCast(IServant)()  nothrow
	{
		return this;
	}
	IServant opCast(IServant)() const nothrow
	{
		return this;
	}

	//--- Alias this ---
	static if(__traits(getAliasThis, T).length > 0)
	{
		enum AliasThisName = __traits(getAliasThis, T)[0];
		pragma(msg,"AliasThis = ", AliasThisName);
		alias meth = Alias!(__traits(getMember, T, AliasThisName));

		static assert(isSomeFunction!meth,"ToDo");

		alias AT = Unqual!(ReturnType!(meth));
		Servant!AT _aliasThis;
		mixin("override @property const(AT) " ~ AliasThisName ~ `() const
		{
			auto x = _t ? _t.`~ T.stringof ~ "." ~ AliasThisName ~ `() :
																	super.`~ T.stringof ~ "." ~ AliasThisName ~ `();
			if(cast(Servant!AT)x !is null)
				return cast(inout(AT))x;
			if(_aliasThis is null || _aliasThis.target !is x)
				*(cast(AT*)&_aliasThis) = new Servant!AT(cast(AT)x);
			return _aliasThis;
		}
		alias ` ~ AliasThisName ~ " this;");
	}
	//-- Serialization actions ---
	static if(HasOwnMethod!(T,"onSerializing"))
		override void onSerializing()
		{
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			if(_t)
				_t.onSerializing();
			else
				super.onSerializing();
		}
	static if(HasOwnMethod!(T,"onSerialized"))
		override void onSerialized()
		{
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			if(_t)
				_t.onSerialized();
			else
				super.onSerialized();
		}
	static if(HasOwnMethod!(T,"onDeserializing"))
		override void onDeserializing()
		{
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			if(_t)
				_t.onDeserializing();
			else
				super.onDeserializing();
		}
	static if(HasOwnMethod!(T,"onDeserialized"))
		override void onDeserialized()
		{
			_signalsBlocked = true;
			scope(exit)
				_signalsBlocked = false;
			if(_t)
				_t.onDeserialized();
			else
				super.onDeserialized();
		}
}


*/
