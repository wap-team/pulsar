module pulsar.gol.attributes;

import std.typetuple;

public import pulsar.reflection.attributes;
public import pulsar.atoms.proxies;
public import pulsar.serialization.defs;


/// Структура атрибута сущности
/// (собственное хранение и отдельная сериализация)
struct GolEssenceAttribute {}
enum essence = GolEssenceAttribute.init;
alias singleton = essence;

enum IsEssenceAttribute(alias T) = is(typeof(T) : GolEssenceAttribute);
alias HasEssenceAttribute(T) =
		anySatisfy!(IsEssenceAttribute,	__traits(getAttributes, T));

template IsEssenceOrProxyAttribute(alias T)
{
	enum IsEssenceOrProxyAttribute =
			is(typeof(T) : GolEssenceAttribute) ||
			is(typeof(T) : ProxyAttribute);
}
template IsAnyMetaTypeAttribute(alias T)
{
	enum IsAnyMetaTypeAttribute =
			is(typeof(T) : MetaTypeAttribute) ||
			is(typeof(T) : GolEssenceAttribute) ||
			is(typeof(T) : ProxyAttribute);
}

alias HasAnyMetaTypeAttribute(T) =
	anySatisfy!(IsAnyMetaTypeAttribute, __traits(getAttributes, T));
