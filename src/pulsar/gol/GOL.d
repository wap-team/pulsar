module pulsar.gol.GOL;

import pulsar.atoms;
//import pulsar.atoms.weakref;
public import pulsar.gol.servant : IServant;
import pulsar.gol.servant;
import pulsar.reflection.defs;
import pulsar.reflection.library;
//----------------------------------------------------------------------------------------


/// Класс глобального списка объектов.
class GOL
{
	static:
	private:
		struct GolRef
			{
			private:
				UUID oid;
				ClassInfo type;

			public:
				this(OID oid, const(ClassInfo) type)
				{
					assert(oid.empty == false);
					assert(type);
					this.oid = oid;
					this.type = cast(ClassInfo)type;
				}
			}

	// Временно заменяет pulsar.atoms.weakref.ObjectRef, пока
	// слабые ссылки не используются из-за бага при очиске
	struct ObjectRef
	{
		Object target;

		@property bool isAlive() { return target !is null; }

		this(Object obj, uint ttl = 0) { target = obj; }
	}
	alias OidDic = ObjectRef[OID];
	__gshared OidDic[const(ClassInfo)] _gol ;
	__gshared GolRef[string] _nicknames;

public:
	/** Признак режима восстановления.
					При его установке, get будет создавать те объекты,
					которые не могут быть загружены.
	**/
	bool recoveryMode = false;

	/// Возвращает число ссылок на объекты (включая мертвые) в GOL.
	synchronized @property size_t count()
	{
		synchronized
		{
			size_t sum = 0;
			foreach(tl; _gol)
				sum += tl.length;
			return sum;
		}
	}

	/// Проверяет глобальный объект на соответствие требованиям GOL.
	GolRef check(inout Object obj)
	{
		// Когда будем убирать IServant как требование,
		// тип obj нужно получать через IProxy.typeId
		IServant serv = cast(IServant)obj;
		if(serv is null)
			throw new Exception("Объект [" ~ typeid(obj).toString() ~ //unqual(obj).toString() ~
																							"] не упакован в Servant!");
		if(serv.oid.empty)
			throw new Exception("У глобального объекта типа ["~ typeid(obj).name ~"] пустой OID!");
		return GolRef(serv.oid, serv.targetTypeId);
	}

	/// Определяет, присутствует ли глобальный объект с заданным OID в GOL и жив ли он.
	/// Не производит поиск и загрузку объекта из хранилищ.
	bool has(OID oid, const(ClassInfo) type)
	{
		if(oid.empty)
			return false;
		assert(type);

		synchronized
		{
			auto p = type in _gol;
			if(p is null)
				return false;
			ObjectRef* or = oid in *p;
			return or !is null && or.isAlive;
		}
	}
	bool has(T)(OID oid) { return has(oid, typeid(Unqual!T)); }
	bool has(GolRef gr) { return has(gr.oid, gr.type); }
	bool has(const(Object) obj)
	{
		assert(obj);
		auto gr = check(obj);
		return has(gr.oid, gr.type);
	}
	/// Определяет, присутствует ли глобальный объект с заданным nickname в GOL.
	/// Не производит поиск и загрузку объекта из хранилищ.
	bool has(string nickname)
	{
		CheckNullOrEmpty!nickname;
		synchronized
		{
			return (nickname in _nicknames) !is null;
		}
	}

	/// Получает объект из GOL.
	/// Не производит поиск и загрузку объекта из хранилищ.
	/// Возвращает null, если объект не найден.
	Object take(OID oid, const(ClassInfo) type)
	{
		if(oid.empty)
			return null;
		assert(type);
		synchronized
		{
			auto p = type in _gol;
			ObjectRef* tp = p ? oid in *p : null;
			return tp ? (*tp).target : null;
		}
	}
	T take(T)(OID oid) { return cast(T)take(oid, typeid(Unqual!T)); }
	/// Получает объект из GOL.
	/// Не производит поиск и загрузку объекта из хранилищ.
	Object take(string nickname)
	{
		CheckNullOrEmpty!nickname;
		synchronized
		{
			GolRef* gr = nickname in _nicknames;
			return gr ? take((*gr).oid, (*gr).type) : null;
		}
	}
	T take(T)(string nickname) { return cast(T)take(nickname); }

	/// Возвращает первый попавшийся объект с указанным OID
	/// Не производит загрузку объекта из хранилищ.
	Object findByOID(OID oid)
	{
		if(oid.empty)
			return null;
		synchronized
		{
			foreach(k, p; _gol)
			{
				ObjectRef* tp = p ? oid in p : null;
				if(tp)
					return (*tp).target;
			}
		}
		return null;
	}

	/** Получает объект из GOL. Если объекта в GOL нет, производит его загрузку из хранилищ.
			* Params:
			*  oid   = OID объекта
			*  type  = Тип объекта
			*  throwIfCannotLoad = Определяет выброс исключения, если объект не был загружен.
			*  ttl = в случае загрузки, количество секунд, на которое объект добавляется в GOL.
			*/
	Object get(OID oid, const(ClassInfo) type, bool throwIfCannotLoad = true, uint ttl = 0)
	{
		CheckNull!(oid, type);
		Object obj = take(oid, type);
		if(obj !is null)
			return obj;

		pulsar.server.PulsarServer.storageManager.load(obj, oid, type);

		if(obj is null)
		{
			if(throwIfCannotLoad)
			{
				string msg = "Не удалось загрузить объект [" ~ oid.toString() ~
						"] типа " ~ type.toString() ~ "!";

				if(recoveryMode)
				{
					trace("\n!!!!!! ",  msg);
					return type.create();
				}
				else
					throw new Exception(msg);
			}
			else
				return null;
		}

		IServant io = cast(IServant)obj;
		if(io) // && io.oid != oid)
			io.oid = oid;

		add(obj, ttl);

		return obj;
	}
	T get(T)(OID oid, bool throwIfCannotLoad = true, uint ttl = 0)
	{
		return cast(T)get(oid, typeid(Unqual!T), throwIfCannotLoad, ttl);
	}
	/** Получает объект из GOL. Если объекта в GOL нет, производит его загрузку из хранилищ.
			* Params:
			*  nickname = Псевдоним объекта.
			*  type     = Тип объекта
			*  throwIfCannotLoad = Определяет выброс исключения, если объект не был загружен.
			*  ttl = в случае загрузки, количество секунд, на которое объект добавляется в GOL.
			*/
	Object get(string nickname, const(ClassInfo) type = null, bool throwIfCannotLoad = true,
												uint ttl = 0)
	{
		CheckNullOrEmpty!nickname;
		synchronized
		{
			GolRef* gr = nickname in _nicknames;
			if(gr)
				return get((*gr).oid, (*gr).type);
		}

		Object obj = null;

		pulsar.server.PulsarServer.storageManager.load(obj, nickname, nickname);

		if(obj is null)
		{
			if(throwIfCannotLoad)
				throw new Exception("Не удалось загрузить объект [" ~ nickname ~ "]" ~
																								(type ? " типа " ~ type.toString() : "") ~ "!");
		}
		else
		{
			IServant io = cast(IServant)obj;
			if(io && io.oid == OID.init)
				io.oid = randomUUID;

			add(obj, nickname, ttl);
		}
		return obj;
	}
	T get(T)(string nickname, bool throwIfCannotLoad = true, uint ttl = 0)
	{
		return cast(T)get(nickname, typeid(Unqual!T), throwIfCannotLoad, ttl);
	}

	/** Метод добавления объекта в GOL.
		* Params: obj = Добавляемый объект
		*         ttl = Количество секунд, на которое объект добавляется в GOL.
		*/
	GolRef add(Object obj, uint ttl = 0)
	{
		assert(obj);
		auto gr = check(obj);
		synchronized
		{
			ClassInfo type = gr.type;
			// Нельзя все подряд добавлять - проблемы (например, с PropsValuesContainer)
			do
			{
				OidDic d = _gol.get(type, null);
				if(d is null)
				{
					d[gr.oid] = ObjectRef(obj, ttl);
					_gol[type] = d;
				}
				else
				{
					if(gr.oid in d)
					{
						// Строгость такая возможно и не нужна.
						// Если нужна - снять комментарии и написать зачем
						// throw new Exception("Объект [" ~ gr.oid.toString() ~"] типа " ~
						// 										type.toString() ~ " уже находится в GOL!");
						enforce(d[gr.oid].target is obj, "Объект [" ~ gr.oid.toString() ~"] типа " ~
																																							type.toString() ~ " отличается от находящегося в GOL!");
					}
					else
						d[gr.oid] = ObjectRef(obj, ttl);
				}

				for(type = type.base; type != Object.classinfo; type = type.base)
				{
					IMetaTypeAggregate imta = cast(IMetaTypeAggregate)MetaTypeLibrary[type];
					if(imta && imta.hasServant)
					{
						//trace("add ", type, " as base of ", IProxy.typeId(obj));
						break;
					}
				}
			} while(type != Object.classinfo);

			return gr;
		}
	}
	/** Метод добавления объекта в GOL.
		* Params: obj = Добавляемый объект
		*         nickname = псевдоним добавляемого объекта.
		*                    Может использоваться для его получения.
		*         ttl = Количество секунд, на которое объект добавляется в GOL.
		*/
	GolRef add(Object obj, string nickname, uint ttl = 0)
	{
		CheckNullOrEmpty!nickname;
		GolRef gr;
		synchronized
		{
			gr = add(obj, ttl);
			_nicknames[nickname] = gr;
		}
		return gr;
	}

	/** Перезагружает данные объекта, находящегося в GOL.
			* Params:
			*  oid   = OID объекта
			*  type  = Тип объекта
			*  throwIfCannotLoad = Определяет выброс исключения, если объект не был загружен.
			*/
	bool reload(OID oid, const(ClassInfo) type, bool throwIfCannotLoad = true)
	{
		assert(oid.empty == false);
		assert(type);

		Object obj = take(oid, type);
		if(obj is null)
			throw new Exception("Объект [" ~ oid.toString() ~"] типа " ~
																							type.toString() ~ " не находится в GOL!");

		bool loaded = pulsar.server.PulsarServer.storageManager.load(obj, oid, type);
		if(loaded == false && throwIfCannotLoad)
			throw new Exception("Не удалось загрузить объект [" ~ oid.toString() ~
																							"] типа " ~ type.toString() ~ "!");
		return loaded;
	}
	bool reload(const(Object) obj, bool throwIfCannotLoad = true)
	{
		GolRef gr = check(obj);
		return reload(gr.oid, gr.type, throwIfCannotLoad);
	}
	bool reload(string nickname, bool throwIfCannotLoad = true)
	{
		CheckNullOrEmpty!nickname;

		Object obj = take(nickname);
		if(obj is null)
			throw new Exception("Объект [" ~ nickname ~"] не находится в GOL!");

		bool loaded = pulsar.server.PulsarServer.storageManager.load(obj, nickname, nickname);
		if(loaded == false && throwIfCannotLoad)
			throw new Exception("Не удалось загрузить объект [" ~ nickname ~ "]!");
		return loaded;
	}

	/// Сохраняет объект в хранилище.
	/// Объект должен находиться в GOL.
	void save(const(Object) obj, string reason = null, bool logSave = true) nothrow
	{
		assert(obj);
		try
		{
			if(logSave == false)
				Logger.logOff();

			traceChars(format("└ save %s(%s)", unqual(obj), IProxy.typeId(obj)));
			GolRef gr = check(obj);
			trace(" [", gr.oid.toString(), "] ... ");
			if(has(gr) == false)
			{
				/*
						Непонятно зачем исключение.
						Оно мешает создавать временные (до первого сохранения) объекты.
					*/
				//throw new Exception("Объект [" ~ gr.oid.toString() ~"] не находится в GOL!");
				/*
						Тоже вопрос, надо ли.
						На всякий случай, добавляем временно
					*/
				GOL.add(cast(Object)obj, 600);
			}

			string nickname;
			foreach(k,v; _nicknames)
				if(v == gr)
				{
					nickname = k;
					break;
				}

			import pulsar.server.storages;
			bool res;
			if(nickname.length)
				res = pulsar.server.PulsarServer.storageManager.save(obj, nickname, nickname);
				else
				res = pulsar.server.PulsarServer.storageManager.save(obj, gr.oid, reason);
			if(res == false)
				throw new Exception("Не удалось сохранить объект [" ~ gr.oid.toString() ~"]!");

			return;
		}
		catch(Throwable tr)
		{
   static import core.stdc.stdlib;
			Logger.logError(tr);
			core.stdc.stdlib.abort();
		}
		finally
		{
			if(logSave == false)
				Logger.logOn();
		}
	}
	/// Сохраняет объект с указанным nickname в хранилище.
	/// Объект должен находиться в GOL.
	void save(string nickname, string reason = null)
	{
		assert(nickname);

		Object obj;
		synchronized
		{
			GolRef gr = _nicknames.get(nickname, GolRef.init);
			if(gr.oid == UUID.init)
				throw new Exception("Объект [" ~ nickname ~"] не находится в GOL!");
			obj = take(gr.oid, gr.type);
			if(!obj)
				throw new Exception("Объект [" ~ gr.oid.toString() ~"] не находится в GOL!");
		}
		save(obj, reason);
	}

	/// Сохраняет все объекты в GOL
	void saveAll(const(TypeInfo_Class) ti = null)
	{
		synchronized
		{
			if(ti is null)
			{
				foreach(p;_gol)
					foreach(x; p)
						if(x.target)
							save(x.target, null, false);
			}
			else
			{
				auto p = ti in _gol;
				if(p)
					foreach(x;*p)
						if(x.target)
							save(x.target);
			}
			foreach(s; pulsar.server.PulsarServer.storageManager)
				s.flush();
		}
	}
	//----------------------------------------------------------------

	/// Создает глобальный объект, подготовленный для добавления в GOL.
	static T create(T: Object, CtorArgs...)(CtorArgs args)
	{
		//return createWithOID!(T, CtorArgs)(randomUUID, args);

		auto mt = MetaTypeLibrary[typeid(T)];
		if(mt is null)
			throw new Exception("Метатип для [" ~ T.stringof ~ "] не зарегистрирован!");

		auto imt = cast(IMetaTypeAggregate)mt;
		assert(imt !is null);

		if(imt.hasServant == false)
			throw new Exception("Метатип для [" ~ T.stringof ~ "] не имеет Servant!");
		MetaPtr p = imt.allocServant();
		assert(!p.isNull);

		T obj = p.getValue!T();
		assert(obj);

		// Нужно, так конструктор серванта может не вызываться
		IServant.blockSignals(obj);
		scope(exit)
			IServant.unblockSignals(obj, false);

		static if(CtorArgs.length > 0)
		{
			obj.__ctor(args);
		}
		else static if(is(typeof(T.__ctor())))
		{
			alias Ctor = T delegate();
			Ctor c = &obj.__ctor;
			c();
		}

		IServant ist = cast(IServant)obj;
		enforce(ist);
		enforce(ist && ist.oid.empty == false, "Пустой OID у IServant[" ~ T.stringof ~ "]!");

		static if(IsIOIDObject!T)
			enforce(obj.oid.empty == false, "Пустой OID у IsIOIDObject[" ~ T.stringof ~ "]!");

		return obj;

	}
	static Object create(const(TypeInfo) type)
	{
		CheckNull!type;
		IMetaTypeAggregate mt = cast(IMetaTypeAggregate)MetaTypeLibrary[type.unqual];
		enforce(mt, "Метатип для [" ~ type.toString() ~ "] не зарегистрирован!");

		if(mt.hasServant == false)
			throw new Exception("Метатип для [" ~ type.toString() ~ "] не имеет Servant!");
		MetaPtr p = mt.createServant();
		assert(!p.isNull);

		Object obj = p.getValue!Object();
		assert(obj);

		return obj;
	}
	// не работает
//	static Object create(CtorArgs...)(const(TypeInfo) type, CtorArgs args)
//	{
//		CheckNull!type;
//		IMetaTypeAggregate mt = cast(IMetaTypeAggregate)MetaTypeLibrary[type.unqual];
//		enforce(mt, "Метатип для [" ~ type.toString() ~ "] не зарегистрирован!");

//		if(mt.hasServant == false)
//			throw new Exception("Метатип для [" ~ type.toString() ~ "] не имеет Servant!");
//		MetaPtr p = mt.createServant(OID.init, args);
//		assert(!p.isNull);

//		Object obj = p.getValue!Object();
//		assert(obj);

//		return obj;
//	}

	/// Создает глобальный объект, добавляет его в GOL и сохраняет.
	static T make(T: Object, CtorArgs...)(CtorArgs args)
	{
		T t = create!T(args);
		add(t);
		save(t);
		return t;
	}
	/// Создает глобальный объект, добавляет его в GOL и сохраняет.
	static Object make(const(TypeInfo) type)
	{
		CheckNull!type;
		Object t = create(type);
		add(t);
		save(t);
		return t;
	}
	/// Создает глобальный объект и добавляет его в GOL, но не сохраняет.
	static T createAndAdd(T: Object, CtorArgs...)(CtorArgs args)
	{
		T t = create!T(args);
		add(t);
		return t;
	}

//	/// Создает глобальный объект с указанным OID, добавляет его в GOL и сохраняет.
//	static T makeWithOID(T: Object, CtorArgs...)(OID oid, CtorArgs args)
//	{
//		T t = createWithOID!T(oid,args);
//		add(t);
//		save(t);
//		return t;
//	}
	/// Создает глобальный объект с указанным OID, добавляет его в GOL и сохраняет.
	static MetaPtr createAndAdd(IMetaTypeAggregate mt, OID oid,
																													string nickname, bool noSave = false)
	{
		assert(mt);
		assert(!oid.empty);
		MetaPtr p = mt.createServant(oid);
		if(p.isNull)
			throw new Exception("Не удалось создать объект " ~ oid.toString() ~
																							" типа " ~ mt.fullName ~ "!");
		Object obj = cast(Object)p.ptr;
		add(obj,nickname);
		if(noSave == false)
			save(obj);
		return p;
	}

/* TODO: Сохранение не работает!!! Проблема в сериализации, надо починить!
	* 	/// Метод упаковки объекта.
	static auto pack(T: Object)(T obj)
	{
		//if(cast(IServant)obj)
		if(typeid(obj) == typeid(Servant!T))
			return obj;
		return new Servant!(T)(obj);
	}
*/

package(pulsar):
	/// Метод чистки GOL.
	/// Переводит просроченные сильные ссылки в слабые, удаляет мертвые ссылки.
	static void clean()
	{
		// Вызов этого метода при использовании вызывал SIGSEGV

//		StopWatch sw = StopWatch(AutoStart.yes);
//		uint del, weak;
//		foreach(t; _gol)
//		{
//			OID[] toDel;
//			foreach(k,ref v; t)
//			{
//				if(v.checkAndWeak())
//					weak++;
//				if(v.isAlive == false)
//				{
//					toDel ~=k;
//					del++;
//				}
//			}
//			foreach(k;toDel)
//				t.remove(k);
//		}
//		sw.stop();
//		if(del > 0 || weak > 0)
//			Logger.log("GOL clean %s msecs: %s to weak, %s to del", sw.peek().total!"msecs", weak, del);
//		else if(sw.peek().total!"msecs" > 100)
//			Logger.log("!!! ALERT !!! GOL clean was %s msecs!", sw.peek().total!"msecs");
////		import core.memory;
////		GC.collect();
	}
}

unittest
{
//	import core.thread;
//	import core.memory;

//	XXX x = new XXX(1);
//	GOL.add(x);
//	XXX x2 = new XXX(2);
//	GOL.add(x2, 5);
//	x2 = new XXX(3);
//	GOL.add(x2, 15);
//	x2 = null;

//	XXX[] arr;

//	writeln("start ", Clock.currTime());
//	while(GOL.count > 0)
//	{
//		GOL.clean();
//		Thread.sleep(dur!("seconds")(1));
//		arr ~= new XXX(5);
//		GC.collect();
//		if(GOL.count == 1 && x !is null)
//		{
//			GOL._dic[x.oid].makeWeak(5);
//			x = null;
//		}
//	}

}

/// Структура ссылки на объект в GOL.
/// Расскрытие ссылки происходит при первом обращении к объекту
struct GolObjectRef
{
	alias T = typeof(this);
package:
	UUID _oid;
	string _typeStr;

	@noser Object _object;
	@noser Object /*ClassInfo - ошибки сборки*/ _type;

public:
	@property // oid
	{
		OID oid() const
		{
			IOIDObject io = cast(IOIDObject)_object;
			return io ? io.oid : _oid;
		}
		void oid(OID oid)
		{
			enforce(isSetObject == false, "Объект уже раскрыт!");
			_oid = cast(UUID)oid;
		}
	}
	@property // typeString
	{
		string typeString() const { return isSetObject ? type.toString() : _typeStr; }
		void typeString(string value)
		{
			enforce(isSetObject == false, "Объект уже раскрыт!");
			_typeStr = value;
		}
	}
	@property const(ClassInfo) type() const
	{
		if(_object)
			return IProxy.typeId(_object);
		if(_typeStr.length)
		{
			if(_type is null)
			{
				T* dr = cast(T*)&this;
				dr._type = cast(Object)ClassInfo.find(_typeStr);
			}
			return enforce(cast(ClassInfo)_type, "Тип [" ~ _typeStr ~ "] не найден!");
		}
		return IProxy.typeId(object);
	}
	@property // object
	{
		const(Object) object() const
		{
			if(_object is null)
			{
				// Нельзя, так как свойства используют object
				if(_oid == OID.init)
					throw new Exception("Получение объекта пустой ссылки!");

				//trace(">>> OIDObjectRef: ",  _oid, " ", _typeStr);
				static import pulsar.gol.GOL;
				Object p = pulsar.gol.GOL.GOL.get(_oid, type, true, 3600); // 1 час
				if(p)
				{
					T* dr = cast(T*)&this;
					(*dr).object(p);
				}
			}
			return enforce(_object, "Объект [" ~ _oid.toString() ~ "] не найден!");
		}
		private void object(const(Object) value)
		{
			_object = value.unqual;
			// Надо заполнять, так как структура может быть сериализована, а _object не сер
			IOIDObject io = _object ? cast(IOIDObject)_object : null;
			_oid = io ? io.oid : OID.init;
			_typeStr = _object ? IProxy.typeId(_object).toString() : null;
			_type = null;
		}
	}
	//alias object this;
	@property bool isSetObject() const { return _object !is null; }
	@property bool empty() const @safe nothrow { return _object is null && _oid == OID.init; }

public:
	this(OID oid, const(ClassInfo) type)
	{
		enforce(oid.empty == false);
		enforce(type, "type не может быть null!");
		this.oid = oid;
		this.typeString = type.toString();
		_type = cast(Object)type;
	}
	this(OID oid, string type)
	{
		enforce(oid.empty == false);
		enforce(type.length, "type не может быть пустым!");
		this.oid = oid;
		this.typeString = type;
	}
	this(const(IOIDObject) obj) { object = cast(Object)obj; }

	void opAssign(const(IOIDObject) obj) { object = cast(Object)obj; }
	bool opEquals()(auto ref const GolObjectRef obj) const
	{
		if(this.empty && obj.empty)
			return true;
		return oid == obj.oid;
	}
	bool opEquals()(auto ref const Object obj) const
	{
		if(this.empty && obj is null)
			return true;
		IOIDObject io = cast(IOIDObject)_object;
		if(io)
			return oid == io.oid;
		return object is obj;
	}

	size_t toHash() const @safe nothrow
	{
		return empty ? 0 : _oid.toHash();
	}
	string toString()  { return empty ? "GolObjectRef.init" : object.unqual.toString(); }
	string toString() const { return empty ? "GolObjectRef.init" : object.unqual.toString(); }
}
