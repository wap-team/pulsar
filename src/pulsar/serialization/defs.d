module pulsar.serialization.defs;

import pulsar.reflection.defs;
import pulsar.serialization.attributes;

/// Структура настроек сериализации
struct SerializationSettings
{
	/// Режим сериализации
	uint mode = SerializationMode.Always;
	/// Определяет, будут ли сериализоваться значения по умолчанию.
	bool noSerializeDefaultValue = true;
}


/// Класс глобальных параметров сериализации и десериализации.
class SerializationTuning
{
	static __gshared bool function(IMetaType, bool) hasSerializationSurrogate =
		&_hasSerializationSurrogate;
	static __gshared string function(IMetaType mt, MetaPtr ptr) toStringSurrogate =
		&_toStringSurrogate;
	static __gshared MetaPtr function(IMetaType mt, string value) fromStringSurrogate =
		&_fromStringSurrogate;

	static bool _hasSerializationSurrogate(IMetaType mt, bool isRoot)
	{
		return false;
	}
	static string _toStringSurrogate(IMetaType mt, MetaPtr ptr)
	{
		return null;
	}
	static MetaPtr _fromStringSurrogate(IMetaType mt, string value)
	{
		return MetaPtr.init;
	}
}
