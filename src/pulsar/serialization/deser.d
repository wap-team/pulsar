module pulsar.serialization.deser;

import std.array;
//import std.container : Array;

import pulsar.atoms;
import pulsar.containers.arrays;
import pulsar.reflection.defs;
import pulsar.reflection.library;
import pulsar.serialization.defs;
import pulsar.serialization.archiver;

//StopWatch swa;
//StopWatch swr;

struct Deserializer
{
public:
	static Object run(Archiver arch, Object root = null)
	{
		CheckNull!arch;
		return deserialize(arch, root);
	}
}

Object deserialize(Archiver arch, Object root = null)
{
	import core.memory;
	GC.disable();
	scope(exit)
		GC.enable();

	CheckNull!arch;

	scope _objs = ElasticArray!MetaPtr(4000);
	scope _types = ElasticArray!IMetaType(400);

	if(root !is null)
		_objs[1] = MetaPtr.of(root);

	arch.openDeserialization();
	DeserItem di;
	while(arch.getObjectRecord(di))
	{
		if(di.type & DeserItemFlags.Type)
		{
			IMetaType mt = MetaTypeLibrary[di.name];
			if(mt is null)
				throw new Exception("Метатип для типа [" ~ di.name ~ "] не зарегистрирован!");
			if(_types.length <= di.tid)
				_types.length(di.tid);
			_types[di.tid] = mt;
		}
		else if(di.type & DeserItemFlags.Object)
		{
			IMetaTypeAggregate mt = cast(IMetaTypeAggregate)_types[di.tid];
			if(mt is null)
				throw new Exception("Метатип для типа [" ~ di.name ~ "] не зарегистрирован!");
			MetaPtr obj = getObj(mt, di.id, 0, _objs);
			mt.onDeserializing(obj);

			for(di = DeserItem.init;arch.getFieldRecord(di);di = DeserItem.init)
			{
				mt = cast(IMetaTypeAggregate)_types[di.tid];
				assert(mt);
				MetaTypeField f = mt.getField(di.name);
				if(f == MetaTypeField.Null)
					continue;
				if(di.type & DeserItemFlags.ValueIsLinkedObjectId)
					f.setValuePtr(obj, getObj(f.type, di.linkId, 0, _objs));
				else if(di.type & DeserItemFlags.ValueIsString)
					f.setValueString(obj,di.stringValue);
//					else if(di.type & DeserItemFlags.ValueIsBinary)
//						f.setValueBinary(obj,di.binValue);
			}
			mt.onDeserialized(obj);
		}
		else if(di.type & DeserItemFlags.Array)
		{
			IMetaTypeArray mt = cast(IMetaTypeArray)_types[di.tid];
			if(mt is null)
				throw new Exception("Метатип для типа [" ~ di.name ~ "] не зарегистрирован!");
			uint arrLen = di.length;
			MetaPtr obj = getObj(mt, di.id, arrLen, _objs);

			for(di = DeserItem.init;arch.getArrayItemRecord(di);di = DeserItem.init)
			{
				if(di.index >= arrLen)
					throw new Exception("Индекс элемента больше длины массива!");

				if(di.type & DeserItemFlags.ValueIsLinkedObjectId)
					mt.setValuePtrAt(obj, di.index, getObj(mt.elemType, di.linkId, 0, _objs));
				else if(di.type & DeserItemFlags.ValueIsString)
					mt.setValueStringAt(obj, di.index, di.stringValue);
//					else if(di.type & DeserItemFlags.ValueIsBinary)
//						f.setValueBinary(obj,di.binValue);
			}
		}
		else if(di.type & DeserItemFlags.Dictionary)
		{
			IMetaTypeDictionary mt = cast(IMetaTypeDictionary)_types[di.tid];
			if(mt is null)
				throw new Exception("Метатип для типа [" ~ di.name ~ "] не зарегистрирован!");
			MetaPtr obj = getObj(mt, di.id, 0, _objs);

			for(di = DeserItem.init;arch.getKeyValuePairRecord(di);di = DeserItem.init)
			{
				MetaPtr key, val;
				bool keyLinked = cast(bool)(di.type & DeserItemFlags.KeyIsLinkedObjectId);
				bool valLinked = cast(bool)(di.type & DeserItemFlags.ValueIsLinkedObjectId);

				if(di.type & DeserItemFlags.KeyIsLinkedObjectId)
					key = getObj(mt.keyType,di.keyLinkId, 0, _objs);
				if(di.type & DeserItemFlags.ValueIsLinkedObjectId)
					val = getObj(mt.valueType,di.valueLinkId, 0, _objs);

				if(keyLinked && valLinked)
					mt.setValuePtrByKey(obj, key, val);
				else if(keyLinked == false && valLinked)
					mt.setValuePtrByKey(obj, di.key, val);
				else if(keyLinked && valLinked == false)
					mt.setValueStringByKey(obj, key, di.value);
				else if(keyLinked == false && valLinked == false)
					mt.setValueStringByKey(obj, di.key, di.value);
			}
		}
		else if(di.type & DeserItemFlags.Surrogate)
		{
			IMetaTypeAggregate mt = cast(IMetaTypeAggregate)_types[di.tid];
			if(mt is null)
				throw new Exception("Метатип для типа [" ~ di.name ~ "] не зарегистрирован!");
//   if(di.id >= _objs.length)
//    _objs.length(_objs.length  * (di.id/_objs.length + 1));
			_objs[di.id] = SerializationTuning.fromStringSurrogate(mt, di.stringValue);
		}
		di = DeserItem.init;
	}

	MetaPtr p = _objs[1];
	root = (cast(Object)(p.ptr));
	return root;
}
private MetaPtr getObj(IMetaType mt, uint objId, uint arrLen, ref ElasticArray!MetaPtr _objs)
{
	//trace(mt.name);
// if(objId >= _objs.length)
//  _objs.length(_objs.length  * (objId/_objs.length + 1));
	MetaPtr obj = _objs[objId];
	if(obj.isNull && objId > 0)
	{
		//trace(mt.name);
		if(mt.kind & MetaTypeKind.Aggregate)
			obj = (cast(IMetaTypeAggregate)mt).allocServant();
		else if	(mt.kind & MetaTypeKind.Array)
			obj = (cast(IMetaTypeArray)mt).allocInstance(arrLen);
		else if	(mt.kind & MetaTypeKind.Dictionary)
			obj = (cast(IMetaTypeDictionary)mt).allocInstance();
		else
			assert(0);
		// Может быть null при сериализации значений по умолчанию.
		//assert(!obj.isNull);
		_objs[objId] = obj;
	}
	return obj;
}


enum DeserItemFlags : ushort
{
	None = 0,
	Type   = 1,
	Object = 2,
	Field  = 4,
	Array = 8,
	ArrayItem = 16,
	Dictionary = 32,
	KeyValuePair = 64,
	Surrogate = 128,
	KeyIsLinkedObjectId = 2048,
	ValueIsLinkedObjectId = 4096,
	ValueIsString = 8192,
	ValueIsBinary = 16384
}

struct DeserItem
{
	DeserItemFlags type;
	// Объединено, так как запись поля не имеет id.
	union
	{
		uint id;
		uint linkId;
		uint keyLinkId;
	}
	ushort tid;
	union
	{
		string name;
		uint index;
		string key;
	}

	// Информация, какие данные использовать должна прийти с DeserItemFlags
	union
	{
		string stringValue;
		ubyte[] binValue;
		uint length;
		string value;
		string oid;
		Object surrogate;
		uint valueLinkId;
	}
}
