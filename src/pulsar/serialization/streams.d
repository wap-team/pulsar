module pulsar.serialization.streams;

import undead.stream;
import core.vararg;

/// Interface for writable streams.
class NullOutStream : OutputStream
{

		/***
			* Write exactly size bytes from buffer, or throw a WriteException if that
			* could not be done.
			*/
		void writeExact(const void* buffer, size_t size){}

		/***
			* Write as much of the buffer as possible,
			* returning the number of bytes written.
			*/
		size_t write(const(ubyte)[] buffer){ return 0; }

		/***
			* Write a basic type.
			*
			* Outside of byte, ubyte, and char, the format is implementation-specific
			* and should only be used in conjunction with read.
			* Throw WriteException on error.
			*/
		void write(byte x){}
		void write(ubyte x){}          /// ditto
		void write(short x){}          /// ditto
		void write(ushort x){}         /// ditto
		void write(int x){}            /// ditto
		void write(uint x){}           /// ditto
		void write(long x){}           /// ditto
		void write(ulong x){}          /// ditto
		void write(float x){}          /// ditto
		void write(double x){}         /// ditto
		void write(real x){}           /// ditto
//		void write(ifloat x){}         /// ditto
//		void write(idouble x){}        /// ditto
//		void write(ireal x){}          /// ditto
//		void write(cfloat x){}         /// ditto
//		void write(cdouble x){}        /// ditto
//		void write(creal x){}          /// ditto
		void write(char x){}           /// ditto
		void write(wchar x){}          /// ditto
		void write(dchar x){}          /// ditto

		/***
			* Writes a string, together with its length.
			*
			* The format is implementation-specific
			* and should only be used in conjunction with read.
			* Throw WriteException on error.
			*/
				void write(const(char)[] s){}
				void write(const(wchar)[] s){} /// ditto

		/***
			* Write a line of text,
			* appending the line with an operating-system-specific line ending.
			*
			* Throws WriteException on error.
			*/
		void writeLine(const(char)[] s){}

		/***
			* Write a line of text,
			* appending the line with an operating-system-specific line ending.
			*
			* The format is implementation-specific.
			* Throws WriteException on error.
			*/
				void writeLineW(const(wchar)[] s){}

		/***
			* Write a string of text.
			*
			* Throws WriteException if it could not be fully written.
			*/
				void writeString(const(char)[] s){}

		/***
			* Write a string of text.
			*
			* The format is implementation-specific.
			* Throws WriteException if it could not be fully written.
			*/
		void writeStringW(const(wchar)[] s){}

		/***
			* Print a formatted string into the stream using printf-style syntax,
			* returning the number of bytes written.
			*/
		size_t vprintf(const(char)[] format, va_list args){ return 0; }
		size_t printf(const(char)[] format, ...){ return 0; }    /// ditto

		/***
			* Print a formatted string into the stream using writef-style syntax.
			* References: <a href="std_format.html">std.format</a>.
			* Returns: self to chain with other stream commands like flush.
			*/
		OutputStream writef(...){ return null; }
		OutputStream writefln(...)
		{
			return null;
		} /// ditto
		OutputStream writefx(TypeInfo[] arguments, va_list argptr, int newline = false){ return null; }  /// ditto

		void flush(){} /// Flush pending output if appropriate.
		void close(){} /// Close the stream, flushing output if appropriate.
		@property bool isOpen(){ return false; } /// Return true if the stream is currently open.
}
