module pulsar.serialization.attributes;

/// Перечисление режимов сериализации
enum SerializationMode : uint
{
	/// Никогда не сериализуется
	Never   = 0,
	/// Сериализация для сохранения объекта в хранилище
	Save      = 1,
	/// Сериализация для передачи объекта клиенту
	ForClient = 2,
	/// Сериализация для клонирования объекта
	Clone = 4,
	/// Сериализация по умолчанию (всегда)
	Always = uint.max
}

/// Структура атрибута режима сериализации
struct SerializationModes
{
	/// Значение режима сериализации.
	/// Трактуется сериализатором как флаг !!!
	SerializationMode mode;
}
enum NonSerialized = SerializationModes(SerializationMode.Never);
enum noser = SerializationModes(SerializationMode.Never);
