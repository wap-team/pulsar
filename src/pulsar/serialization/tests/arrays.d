module pulsar.serialization.tests.arrays;

version(SerTests) :
version(unittest)
{

import std.stdio;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;


class A
{
	int _a;
	this(int a) { _a = a; }
	override string toString() const
	{
		return "A{_a=" ~ to!string(_a)~ "}";
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(A)obj)
		{
			return _a == o._a;
		}
		return false;
	}
}
mixin RegMetaType!B;
class B : A
{
	int _b;
	this(int b) { super(b/10); _b = b; }
	override string toString() const
	{
		return "B{_a=" ~ to!string(_b)~ "}";
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(B)obj)
		{
			return _b == o._b;
		}
		return false;
	}
}

mixin RegMetaType!XXX;
class XXX
{
	string _str;
	int[] arr;
	A[] arrA;
	string[] arrS;
	A[][] arrArr;
	this()
	{
		_str = "Йогущш";
		arr = [0,1,2,3,4,5];
		arrA = [ new A(10), new A(20), new B(30) ];
		arrS = ["x","yy","zzz", "яяяя"];
		arrArr = [ [new A(11), new A(12)], [new A(21)], [] ];
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(XXX)obj)
		{
			return arrA == o.arrA && _str == o._str && arr == o.arr && arrS == o.arrS &&
										arrArr == o.arrArr;
		}
		return false;
	}

}


void serTestArrays()
{
	version(trace)Logger.log("--------------------------------------------");
	version(trace)Logger.log("--- Arrays ---------------------------------");
	XXX h = new XXX();
	version(trace)Logger.log("Orig(%s):%s", cast(void*)h, printFieldsValues(h));
	Serializer.run(h, XmlArchiver.fileWriter("arrays.ser"));
	Object dh = Deserializer.run(XmlArchiver.fileReader("arrays.ser"));
	version(trace)Logger.log("Desr(%s):%s", cast(void*)dh, printFieldsValues(dh));

	assert(h == dh, "Arrays");
}
}
