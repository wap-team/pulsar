module pulsar.serialization.tests.test;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;

version(SerTests) :

version(unittest)
{
class A
{
	int a;
	this(int v) { a = v; }
	override string toString() const
	{
		return "A: a = " ~ to!string(a);
	}
}
}
unittest
{
	import std.stdio;
	import std.variant;
	import std.traits;

	try
	{
		//StopWatch sw; //Logger.log(2, "fff = %s", 15);"test.xml" ia._addr
		//-------------------------------------------------------------------------------------------

//		foreach(v; MetaTypeLibrary.metaTypes)
//		{
//			Logger.log("--- %s --------------------------------------------", v.name);
//			Logger.log(1,"ID: %s, Kind:%s, Module:%s, FullName:%s",v.typeId, v.kind, v.moduleName, v.fullName);
//			if(v.kind & MetaTypeKind.Aggregate)
//			{
//				Logger.log(1,"Fields:%s", (cast(IMetaTypeAggregate)v).fields.length);
//				foreach(fi; (cast(IMetaTypeAggregate)v).fields)
//					Logger.log(3,"name:%s,\ttype: %s", fi.name, fi.type);
//			}
//		}
//		Logger.log("--------------------------------------------");



//	Logger.log(pi.ptr);


		import pulsar.serialization.tests.primitives;
		serTestPrimitives();
		import pulsar.serialization.tests.subclasses;
		serTestSubclasses();
		import pulsar.serialization.tests.enums;
		serTestEnums();
		import pulsar.serialization.tests.structs;
		serTestStructs();
		import pulsar.serialization.tests.arrays;
		serTestArrays();
		import pulsar.serialization.tests.dictionaries;
		serTestDictionaries();
		import pulsar.serialization.tests.sermodes;
		serTestSerModes();
		import pulsar.serialization.tests.serevents;
		serTestSerEvents();
		import pulsar.serialization.tests.customser;
		serTestCustomSer();
		import pulsar.serialization.tests.circularref;
		serTestCircularRef();
		import pulsar.serialization.tests.polymorph;
		serTestPolymorph();
		import pulsar.serialization.tests.indexlist;
		serTestIndexList();

		//-------------------------------------------------------------------------------------------
	}
	catch(Throwable err) { writeln(err); }
}
