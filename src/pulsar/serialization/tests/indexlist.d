module pulsar.serialization.tests.indexlist;

version(SerTests) :
version(unittest)
{

import std.stdio;
import std.typecons;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;
import pulsar.containers.index;

struct S
{
	int a;
}

mixin RegMetaType!IxList;
//mixin Servant!(IxList,false);
class IxList
{
	int a;
	IndexList!int ii;
	IndexList!(S, "ia", int) ix; // = new IndexList!(S, "ia", int)(x => x.a);

	this()
	{
		a = 10;
		ix = new IndexList!(S,"ia", int)([S(1),S(2), S(3)], (x) => (x.a * 10));
		ii = new IndexList!int([10,20,30,40]);
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(IxList)obj)
		{
			foreach(i;0..ix.count)
				if(ix[i] != o.ix[i])
					return false;
			foreach(i;0..ii.count)
				if(ii[i] != o.ii[i])
					return false;
			return a == o.a;
		}
		return false;
	}

	//@property const(IndexList!(S, "a", getA)) getI() const { return l4; }
	//@property XXX!(S,int,byte) getI() const { return xxx; }

	void onDeserialized()
	{
		ix.ia.getIndexedValue = (x) => (x.a * 10);
		ix.rebuild();
	}
}
void serTestIndexList()
{
	version(trace)Logger.trace("--------------------------------------------");
	version(trace)Logger.trace("--- IndexList ------------------------------");

	IxList c = new IxList();

	version(trace)Logger.trace("Orig(%s):%s", cast(void*)c, printFieldsValues(c));
	Serializer.run(c, XmlArchiver.fileWriter("indexlist.ser"));

	IxList dc = cast(IxList)Deserializer.run(XmlArchiver.fileReader("indexlist.ser"));
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)dc, printFieldsValues(dc));
	assert(c == dc, "IndexList");
	assert(dc.ix.ia.has(20));
	assert(dc.ii.has(20));
}
}

