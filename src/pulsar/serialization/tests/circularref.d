module pulsar.serialization.tests.circularref;

version(SerTests) :
version(unittest)
{

import std.stdio;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;

mixin RegMetaType!A;
class A
{
	string s;
	B b;

	this() { s = "A"; }
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(A)obj)
		{
			return s == o.s;
		}
		return false;
	}
}
mixin RegMetaType!B;
class B
{
	string s;
	C c;

	this() { s = "B";	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(B)obj)
		{
			return s == o.s;
		}
		return false;
	}
}
mixin RegMetaType!C;
class C
{
	string s;
	A a;

	this() { s = "C";	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(C)obj)
		{
			return s == o.s;
		}
		return false;
	}
}
void serTestCircularRef()
{
	version(trace)Logger.trace("--------------------------------------------");
	version(trace)Logger.trace("--- CircularRef ----------------------------");

	IMetaTypeAggregate mtA = cast(IMetaTypeAggregate)(MetaTypeLibrary[typeid(A)]);
	IMetaTypeAggregate mtB = cast(IMetaTypeAggregate)(MetaTypeLibrary[typeid(B)]);
	IMetaTypeAggregate mtC = cast(IMetaTypeAggregate)(MetaTypeLibrary[typeid(C)]);

	assert(mtA.getField("b").type == mtB);
	assert(mtB.getField("c").type == mtC);
	assert(mtC.getField("a").type == mtA);

	A a = new A();
	B b = new B();
	C c = new C();
	a.b = b;
	b.c = c;
	c.a = a;

	version(trace)Logger.trace("Orig(%s):%s", cast(void*)a, printFieldsValues(a));
	Serializer.run(a, XmlArchiver.fileWriter("circularref.ser"));

	A da = cast(A)Deserializer.run(XmlArchiver.fileReader("circularref.ser"));
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)da, printFieldsValues(da));
	assert(a == da, "CircularRef");
	assert(da == da.b.c.a, "CircularRef");
}
}
