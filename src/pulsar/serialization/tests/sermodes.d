module pulsar.serialization.tests.sermodes;

version(SerTests) :
version(unittest)
{

import std.stdio;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;


mixin RegMetaType!A;
class A
{
	int _a;
	@SerializationModes(SerializationMode.Save)
	string _b;
	struct Point { byte x, y; }
	@SerializationModes(SerializationMode.Save | SerializationMode.ForClient)
	Point _p;
	@SerializationModes(SerializationMode.Never)
	float _f;

	this(int a = 7)
	{
		_a = a; _b = to!string(a*10); _p = Point(10,20);
		_f = 1.25f;
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(A)obj)
		{
			return _a == o._a && _b == o._b && _p == o._p && _f == o._f;
		}
		return false;
	}
}

void serTestSerModes()
{
	version(trace)Logger.trace("--------------------------------------------");
	version(trace)Logger.trace("--- SerModes -------------------------------");
	A a = new A();
	version(trace)Logger.trace("Mode: Default (not _f)");
	version(trace)Logger.trace("Orig(%s):%s", cast(void*)a, printFieldsValues(a));
	Serializer.run(a, XmlArchiver.fileWriter("sermodes1.ser"));
	A da = cast(A)Deserializer.run(XmlArchiver.fileReader("sermodes1.ser"));
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)da, printFieldsValues(da));
	assert(a._a == da._a && a._b == da._b && a._p == da._p, "SerModes");

	version(trace)Logger.trace("Mode: Save (not _f)");
	version(trace)Logger.trace("Orig(%s):%s", cast(void*)a, printFieldsValues(a));
	SerializationSettings s;
	s.mode = SerializationMode.Save;
	Serializer.run(a, XmlArchiver.fileWriter("sermodes2.ser"), s);
	da = cast(A)Deserializer.run(XmlArchiver.fileReader("sermodes2.ser"));
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)da, printFieldsValues(da));
	assert(a._a == da._a && a._b == da._b && a._p == da._p, "SerModes");

	version(trace)Logger.trace("Mode: ForClient (not _f and _b)");
	version(trace)Logger.trace("Orig(%s):%s", cast(void*)a, printFieldsValues(a));
	s.mode = SerializationMode.ForClient;
	Serializer.run(a, XmlArchiver.fileWriter("sermodes3.ser"), s);
	da = cast(A)Deserializer.run(XmlArchiver.fileReader("sermodes3.ser"));
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)da, printFieldsValues(da));
	assert(a._a == da._a && a._p == da._p, "SerModes");


}
}
