module pulsar.serialization.tests.polymorph;

version(SerTests) :
version(unittest)
{

import std.stdio;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;

mixin RegMetaType!A;
class A
{
	string s;

	this() { s = "A"; }
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(A)obj)
		{
			return s == o.s;
		}
		return false;
	}
	override string toString() { return printFieldsValues(this); }
}
mixin RegMetaType!B;
class B : A
{
	string s;
	int b;

	this() { s = "B";	 b = 10; }
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(B)obj)
		{
			return s == o.s && b == o.b;
		}
		return false;
	}
	override string toString() { return printFieldsValues(this); }
}
mixin RegMetaType!C;
class C
{
	string s;
	A a;
	Object o;

	this() { s = "C";	a = new B(); o = new A(); }
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(C)obj)
		{
			return s == o.s;
		}
		return false;
	}
}
void serTestPolymorph()
{
	version(trace)Logger.trace("--------------------------------------------");
	version(trace)Logger.trace("--- Polymorph ------------------------------");

	C c = new C();

	version(trace)Logger.trace("Orig(%s):%s", cast(void*)c, printFieldsValues(c));
	Serializer.run(c, XmlArchiver.fileWriter("polymorph.ser"));

	C dc = cast(C)Deserializer.run(XmlArchiver.fileReader("polymorph.ser"));
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)dc, printFieldsValues(dc));
	assert(c == dc, "Polymorph");
	assert(typeid(dc.a) == typeid(B));
	assert(typeid(dc.o) == typeid(A));
}
}

