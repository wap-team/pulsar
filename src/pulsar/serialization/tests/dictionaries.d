module pulsar.serialization.tests.dictionaries;

version(SerTests) :
version(unittest)
{

import std.stdio;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;


class A
{
	int _a;
	this(int a) { _a = a; }
	override string toString() const
	{
		return "A{_a=" ~ to!string(_a)~ "}";
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(A)obj)
		{
			return _a == o._a;
		}
		return false;
	}
}

mixin RegMetaType!XXX;
class XXX
{
	char[byte] _byteChar;
	dchar[byte] _byteDchar;
	string[string] _strStr;
	int[][const(A)] _arrIA;

	this()
	{
		_byteChar = [ 1:'a', 2:'b', 3:'c'];
		_byteDchar = [ 5:'У', 6:'Й', 7:'Щ'];
		_strStr = [ "a":"x", "b":"y"];
		A b = new A(2);
		_arrIA = [ new A(1) : [1], b : [1,2], new A(3) : [1,2,3]];
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(XXX)obj)
		{
			return _byteChar !is o._byteChar && _byteChar == o._byteChar &&
										_byteDchar !is o._byteDchar && _byteDchar == o._byteDchar &&
										_strStr !is o._strStr && _strStr == o._strStr &&
										_arrIA !is o._arrIA/* && _arrIA == o._arrIA*/;
		}
		return false;
	}

}


void serTestDictionaries()
{
	version(trace)Logger.log("--------------------------------------------");
	version(trace)Logger.log("--- Dictionaries ---------------------------");
	XXX h = new XXX();
	version(trace)Logger.log("Orig(%s):%s", cast(void*)h, printFieldsValues(h));
	Serializer.run(h, XmlArchiver.fileWriter("dictionaries.ser"));
	Object dh = Deserializer.run(XmlArchiver.fileReader("dictionaries.ser"));
	version(trace)Logger.log("Desr(%s):%s", cast(void*)dh, printFieldsValues(dh));

	assert(h == dh, "Dictionaries");
}
}
