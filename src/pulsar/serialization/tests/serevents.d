module pulsar.serialization.tests.serevents;

version(SerTests) :
version(unittest)
{

import std.stdio;
import std.datetime;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;

mixin RegMetaType!A;
class A
{
	@SerializationModes(SerializationMode.Never)
	bool sering, sered, desering, desered;

	string _b;
	DateTime _dt;

	this(int a = 7)
	{
		_b = "xxx";
		_dt = cast(DateTime)Clock.currTime;
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(A)obj)
		{
			return _b == o._b && _dt == o._dt;
		}
		return false;
	}

	void onSerializing() { sering = true; }
	void onSerialized() { sered = true; }
	void onDeserializing() { desering = true; }
	void onDeserialized() { desered = true; }
}

void serTestSerEvents()
{
	version(trace)Logger.trace("--------------------------------------------");
	version(trace)Logger.trace("--- SerEvents -------------------------------");
	A a = new A();
	version(trace)Logger.trace("Orig(%s):%s", cast(void*)a, printFieldsValues(a));
	Serializer.run(a, XmlArchiver.fileWriter("serevents.ser"));
	version(trace)Logger.trace("Orig(%s):%s", cast(void*)a, printFieldsValues(a));
	assert(a.sering && a.sered);
	A da = cast(A)Deserializer.run(XmlArchiver.fileReader("serevents.ser"));
	assert(da.desering && da.desered);
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)da, printFieldsValues(da));
	assert(a == da, "SerEvents");
}
}
