module pulsar.serialization.tests.subclasses;

version(SerTests) :
version(unittest)
{

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;

mixin RegMetaType!AAA;
class AAA
{
private:
	int _int;

public:
	double _double;
	BBB _bbb;

	this()
	{
		_int = 1;
		_double = 12.43;
		_bbb = new BBB();
	}
	override string toString()
	{
		return format("%s(_int:%s,_double:%s,_bbb:%s)", super.toString(), _int,_double,_bbb);
	}
	override equals_t opEquals (Object obj)
	{
		if (auto o =  cast(AAA)obj)
		{
			return _int == o._int && _double == o._double && _bbb == o._bbb;
		}
		return false;
	}
}
class BBB
{
	bool _bool;
	this() { _bool = true; }
	override string toString()
	{
		return format("%s(_bool:%s)", super.toString(), _bool);
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o =  cast(BBB)obj)
		{
			return _bool == o._bool;
		}
		return false;
	}
}

interface IBase{}

mixin RegMetaType!AClass;
class AClass //: IBase
{
	bool _bool;
	byte _byte;
	this() { _bool = true; _byte = -125; }
	override equals_t opEquals (Object obj)
	{
		if(auto o =  cast(typeof(this))obj)
			return _bool == o._bool && _byte == o._byte;
		return false;
	}
}
mixin RegMetaType!BClass;
class BClass : AClass, IBase
{
	short _short;
	ushort _ushort;
	this() { _short = short.min; _ushort = ushort.max; }
	override equals_t opEquals (Object obj)
	{
		if(auto o =  cast(typeof(this))obj)
			return super.opEquals(o) && _short == o._short && _ushort == o._ushort;
	return false;
	}
}

mixin RegMetaType!CClass;
class CClass : BClass, IBase
{
	int _int;
	uint _uint;
	this() { _int = int.min; _uint = uint.max; }
	override equals_t opEquals (Object obj)
	{
		if(auto o =  cast(typeof(this))obj)
			return super.opEquals(o) && _int == o._int && _uint == o._uint;
	return false;
	}
}

void serTestSubclasses()
{
	version(trace)Logger.log("--------------------------------------------");
	version(trace)Logger.log("--- Subclasses -----------------------------");

	AAA a = new AAA();
	version(trace)Logger.log("Orig(%s):%s",cast(void*)a, printFieldsValues(a));
	Serializer.run(a, XmlArchiver.fileWriter("subclasses_a.ser"));
	Object obj = Deserializer.run(XmlArchiver.fileReader("subclasses_a.ser"));
	version(trace)Logger.log("Desr(%s):%s",cast(void*)obj, printFieldsValues(obj));
	assert(a == obj);

	CClass b = new CClass();
	version(trace)Logger.log("Orig(%s):%s", cast(void*)b, printFieldsValues(b));
	Serializer.run(b, XmlArchiver.fileWriter("subclasses_b.ser"));
	Object bc = Deserializer.run(XmlArchiver.fileReader("subclasses_b.ser"));
	version(trace)Logger.log("Desr(%s):%s", cast(void*)bc, printFieldsValues(bc));
	assert(b == bc);
}
}
