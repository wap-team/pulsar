module pulsar.serialization.tests.enums;

version(unittest):

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;

enum EnOut { A,B,C }
enum EnOutByte : byte { Ab,Bb = 10,Cb }
enum EnOutS
{
	As = "aaa",
	Bs = "bbb",
	Cs = "ccc"
}
struct Color
{
	int r,g,b;
}
mixin RegMetaType!Colors;
enum Colors : Color { black = Color(0,0,0), white = Color(255,255,255)  }
class Base
{
	int a;
	EnOut enout;
	this() { a = 10; enout = EnOut.B; }
	override equals_t opEquals (Object obj)
	{
		if (auto o = cast(Base) obj)
		{
			return a == o.a && enout == o.enout;
		}
		return false;
	}
}
mixin RegMetaType!XXX;
class XXX : Base
{
	bool b;
	EnOutByte enOutByte;
	EnOutS enOutS;
	Colors col;

	this() { b = true; enOutByte = EnOutByte.Cb; enOutS = EnOutS.Bs;
	col = Colors.white;
}
	override equals_t opEquals (Object obj)
	{
		if (auto o =  cast(XXX)obj)
		{
			return super.opEquals(o) && b == o.b && enOutByte == o.enOutByte
					&& enOutS == o.enOutS && col == o.col;
		}
		return false;
	}

}



void serTestEnums()
{
	version(trace)Logger.log("--------------------------------------------");
	version(trace)Logger.log("--- Enums ----------------------------------");
	XXX h = new XXX();
	version(trace)Logger.log("Orig(%s):%s", cast(void*)h, printFieldsValues(h));
	Serializer.run(h, XmlArchiver.fileWriter("enums.ser"));
	Object dh = Deserializer.run(XmlArchiver.fileReader("enums.ser"));
	version(trace)Logger.log("Desr(%s):%s", cast(void*)dh, printFieldsValues(dh));

	assert(h == dh, "Enums");
}

