module pulsar.serialization.tests.primitives;

version(SerTests) :
version(unittest)
{

import pulsar.atoms;
import pulsar.serialization;
import pulsar.reflection;

mixin RegMetaType!H;
class H
{
	//cdouble cdouble_; // currently not suppported by to!()
	//cent cent_; // currently not implemented but a reserved keyword
	//cfloat cfloat_; // currently not suppported by to!()
	//creal creal_; // currently not suppported by to!()
	//idouble idouble_; // currently not suppported by to!()
	//ifloat ifloat_; // currently not suppported by to!()
	//ireal ireal_;  // currently not suppported by to!()
	//ucent ucent_; // currently not implemented but a reserved keyword

	bool bool_;
	byte byte_;
	short short_;
	int int_;
	long long_;

	ubyte ubyte_;
	ushort ushort_;
	uint uint_;
	ulong ulong_;

	double double_;
	float float_;
	real real_;

	char char_;
	wchar wchar_;
	dchar dchar_;

//	string str_;
//	wstring wstr_;
//	dstring dstr_;

	this()
{
	//h.cdouble_ = 0.0 + 0.0 * 1.0i; // currently not supported by to!()
	//h.cfloat_ = 0.0f + 0.0f * 1.0i; // currently not supported by to!()
	//h.creal_ = 0.0 + 0.0 * 1.0i; // currently not supported by to!()
	//h.idouble_ = 0.0 * 1.0i; // currently not supported by to!()
	//h.ifloat_ = 0.0f * 1.0i; // currently not supported by to!()
	//h.ireal_ = 0.0 * 1.0i; // currently not supported by to!()

	bool_ = true;
	byte_ = 1;
	short_ = 1;
	int_ = 1;
	long_ = 1L;

	ubyte_ = 1U;
	ushort_ = 1U;
	uint_ = 1U;
	ulong_ = 1LU;

	float_ = 0.0f;
	double_ = 0.0;
	real_ = 0.0;

	char_  = 'c';
	wchar_ = 'Й';
	dchar_ = 'Щ';

//	str_  = "stringЙЩ";
//	wstr_ = "wstringЙЩ";
//	dstr_ = "dstringЙЩ";
}

	override equals_t opEquals (Object other)
	{
		if (auto o = cast(H) other)
		{
			return bool_ == o.bool_ &&
										byte_ == o.byte_ &&
										short_ == o.short_ &&
										int_ == o.int_ &&
										long_ == o.long_ &&

										ubyte_ == o.ubyte_ &&
										ushort_ == o.ushort_ &&
										uint_ == o.uint_ &&
										ulong_ == o.ulong_ &&

										float_ == o.float_ &&
										double_ == o.double_ &&
										real_ == o.real_ &&

										char_ == o.char_ &&
										wchar_ == o.wchar_ &&
										dchar_ == o.dchar_ /*&&

										str_ == o.str_ &&
										wstr_ == o.wstr_ &&
										dstr_ == o.dstr_*/;

			//idouble_ == o.idouble_ && // currently not suppported by to!()
			//ifloat_ == o.ifloat_ && // currently not suppported by to!()
			//ireal_ == o.ireal_ &&  // currently not suppported by to!()
			//creal_ == o.creal_ && // currently not suppported by to!()
			//ucent_ == o.ucent_ && // currently not implemented but a reserved keyword
			//cdouble_ == o.cdouble_ && // currently not suppported by to!()
			//cent_ == o.cent_ && // currently not implemented but a reserved keyword
			//cfloat_ == o.cfloat_ && // currently not suppported by to!()
		}

		return false;
	}
}

void serTestPrimitives()
{
	auto t = MetaTypeAggregate!(H)();

	version(trace)Logger.log("--------------------------------------------");
	version(trace)Logger.log("--- Primitives -----------------------------");
	H h = new H();
	version(trace)Logger.log("Orig(%s):%s", cast(void*)h, printFieldsValues(h));
	Serializer.run(h, XmlArchiver.fileWriter("primitives.ser"));
	Object dh = Deserializer.run(XmlArchiver.fileReader("primitives.ser"));
	version(trace)Logger.log("Desr(%s):%s", cast(void*)dh, printFieldsValues(dh));
	H h2 = cast(H)dh;
	assert(h == dh, "Primitives");
}

}
