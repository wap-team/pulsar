module pulsar.serialization.tests.structs;

version(SerTests) :
version(unittest)
{

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;


struct Struct
{
	int i;
	A a;
}
class A
{
	int cl;
}
mixin RegMetaType!XXX;
class XXX
{
	int x;
	Struct s;
	A _a;
	this() { x = 10; s.i = 5; _a = new A(); _a.cl = 100; s.a = _a; }
	override equals_t opEquals (Object obj)
	{
		if (auto o =  cast(XXX)obj)
		{
			return x == o.x && s.i == o.s.i && _a.cl == o._a.cl;
		}
		return false;
	}

}

import std.stdio;
void serTestStructs()
{
	version(trace)Logger.log("--------------------------------------------");
	version(trace)Logger.log("--- Structs --------------------------------");
	XXX h = new XXX();
	version(trace)Logger.log("Orig(%s):%s", cast(void*)h, printFieldsValues(h));
	Serializer.run(h, XmlArchiver.fileWriter("structs.ser"));
	Object dh = Deserializer.run(XmlArchiver.fileReader("structs.ser"));
	version(trace)Logger.log("Desr(%s):%s", cast(void*)dh, printFieldsValues(dh));
	XXX h1 = cast(XXX)dh;
	assert(h1._a is h1.s.a);
	assert(h == dh, "Structs");
}
}
