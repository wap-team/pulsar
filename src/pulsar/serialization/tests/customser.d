module pulsar.serialization.tests.customser;

version(SerTests) :
version(unittest)
{

import std.stdio;
import std.datetime;
import std.format;

import pulsar.atoms;
import pulsar.reflection;
import pulsar.serialization;

shared static this()
{
	MetaType!Point().customCanSetValueFromString = () { return true; };
	MetaType!Point().customValueToString = &toPointString;
	MetaType!Point().customValueFromString = &fromPointString;

	SerializationTuning.hasSerializationSurrogate = (IMetaType mt, bool isRoot)
	{
		return (isRoot == false && mt.typeInfo == typeid(XXX));
	};
	SerializationTuning.toStringSurrogate = (IMetaType mt, MetaPtr ptr)
	{
		if(mt.typeInfo == typeid(XXX))
			return (cast(IMetaTypeAggregate)mt).getField("_val").getValueString(ptr);
		else
			throw new Exception("Сурогат сериализации для типа " ~ mt.fullName ~
																							" не определен!");
	};
	SerializationTuning.fromStringSurrogate = (IMetaType mt, string value)
	{
		if(mt.typeInfo == typeid(XXX))
		{
			MetaPtr p = (cast(IMetaTypeAggregate)mt).createInstance();
			(cast(IMetaTypeAggregate)mt).getField("_val").setValueString(p, value);
			return p;
		}
		else
			throw new Exception("Сурогат десериализации для типа " ~ mt.fullName ~
																								" не определен!");
	};

}

struct Point
{
	int x;
	int y;
}

string toPointString(Point p) { return std.string.format("(%s,%s)", p.x,p.y); }
Point fromPointString(string s)
{
	CheckNullOrEmpty!s;
	Point p;
	formattedRead(s, "(%s,%s)", &(p.x), &(p.y));
	return p;
}

mixin RegMetaType!XXX;
class XXX
{
	string _val;
	this(string s) { _val = s; }
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(XXX)obj)
		{
			return _val == o._val;
		}
		return false;
	}
	override string toString() const
	{
		return "XXX[_val = "~ _val ~ "]";
	}
}

mixin RegMetaType!A;
class A
{
	int _a;
	Point _p;
	XXX _x;

	this()
	{
		_a = 10;
		_p = Point(11,22);
		_x = new XXX("xxx");
	}
	override equals_t opEquals (Object obj)
	{
		if(auto o = cast(A)obj)
		{
			return _p == o._p && _a == o._a && _x == o._x;
		}
		return false;
	}
}

void serTestCustomSer()
{
	version(trace)Logger.trace("--------------------------------------------");
	version(trace)Logger.trace("--- CustomSer ------------------------------");

	assert(MetaType!Point().canSetValueFromString);
	A a = new A();
	assert(MetaType!Point().getVarValueString(&(a._p)) == "(11,22)");

	version(trace)Logger.trace("Orig(%s):%s", cast(void*)a, printFieldsValues(a));
	Serializer.run(a, XmlArchiver.fileWriter("customser.ser"));

	A da = cast(A)Deserializer.run(XmlArchiver.fileReader("customser.ser"));
	version(trace)Logger.trace("Desr(%s):%s", cast(void*)da, printFieldsValues(da));
	assert(a == da, "CustomSer");
}
}
