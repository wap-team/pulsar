module pulsar.serialization.archiver;

import undead.stream;
import std.outbuffer;

import std.string : format, replace, translate;
import std.conv : to;
import pulsar.atoms;
import pulsar.text.xml.pullparser;
import pulsar.reflection.defs;
import pulsar.serialization.defs;
import pulsar.serialization.ser : SerObjectInfo;
import pulsar.serialization.deser : DeserItem, DeserItemFlags;

import pulsar.serialization.streams;

interface Archiver
{
	void beginSerialization();
	void endSerialization();
	void regType(ushort tid, IMetaType mt);
	void beginObjectSerialization(SerObjectInfo objInfo);
	void endObjectSerialization(SerObjectInfo objInfo);
	void writeObjectSurrogate(SerObjectInfo objInfo);
	void beginArraySerialization(SerObjectInfo objInfo);
	void endArraySerialization(SerObjectInfo objInfo);
	void beginDictionarySerialization(SerObjectInfo objInfo);
	void endDictionarySerialization(SerObjectInfo objInfo);

	void fieldSerialization(ushort typeId, ref MetaTypeField fi, MetaPtr obj, uint linkedObjectId = 0);
	//void arrayItemSerialization(uint index, IMetaTypeArray arrayType, MetaPtr array, uint linkedObjectId = 0);
	void arrayItemSerialization(uint index, IMetaType mt, MetaPtr obj, uint linkedObjectId = 0);
	void dictionaryItemSerialization(IMetaType keyType, MetaPtr keyPtr, uint keyLink,
																																		IMetaType valType, MetaPtr valPtr, uint valLink);

	void openDeserialization();
	bool getObjectRecord(ref DeserItem di);
	bool getFieldRecord(ref DeserItem di);
	bool getArrayItemRecord(ref DeserItem di);
	bool getKeyValuePairRecord(ref DeserItem key);
}

class XmlArchiver : Archiver
{
private:
	union
	{
		InputStream _is;
		OutputStream _os;
	}
	string x;
	PullParser!(char) _sp;

	enum byte lt = 2;
	enum byte gt = 3;
	enum byte qt = 4;

	enum dchar[dchar] unEscTable = [ '\u0002' : '<', '\u0003' : '>'];

public:
	static immutable byte ver = 1;
	static Archiver fileWriter(string fileName)
	{
		return new XmlArchiver(fileName, FileMode.OutNew);
	}
	static Archiver fileReader(string fileName)
	{
		return new XmlArchiver(fileName, FileMode.In);
	}

	static Archiver xmlWriter(OutputStream ostream)
	{
		return new XmlArchiver(ostream);
	}
	// XML PullParser не поддерживает потоки
	static Archiver xmlReader(string data /*InputStream istream*/)
	{
		return new XmlArchiver(data /*istream*/);
	}
//	static Archiver xmlBWriter(ref ubyte[] data)
//	{
//		return new XmlArchiver(data);
//	}

	this(string fileName, FileMode mode)
	{
		if(mode == FileMode.In)
		{
			x = new File(fileName, mode).toString();
			//_is = new MemoryStream(cast(ubyte[])x);
		}
		else
			_os = new File(fileName, mode); // new NullOutStream;
	}
	this(string data /*InputStream istream*/)
	{
//		assert(istream);
//		_is = istream;
		assert(data);
		x = data;
	}
	this(OutputStream ostream)
	{
		assert(ostream);
		_os = ostream;
	}
//	this(ref ubyte[] data)
//	{
//		assert(data);
//		_os = data;
//	}
	//------------------------------------------------------------------------
	void regType(ushort tid, IMetaType mt)
	{
		assert(mt);
		_os.writeString("<type tid=\"");
		_os.writeString(to!string(tid));
		_os.writeString("\" name=\"");
		_os.writeString(mt.fullName.replace("\"","`"));
		_os.writeLine("\" />");

	}
	void writeObjectSurrogate(SerObjectInfo objInfo)
	{
		_os.writeString("<surrogate id=\"");
		_os.writeString(to!string(objInfo.objId));
		_os.writeString("\" tid=\"");
		_os.writeString(to!string(objInfo.typeId));
		_os.writeString("\">");
		_os.writeString(SerializationTuning.toStringSurrogate(objInfo.mt, objInfo.objPtr));
		_os.writeLine("</surrogate>");

	}
	void beginObjectSerialization(SerObjectInfo objInfo)
	{
		_os.writeString("<object id=\"");
		_os.writeString(to!string(objInfo.objId));
		_os.writeString("\" tid=\"");
		_os.writeString(to!string(objInfo.typeId));
		_os.writeLine("\">");
	}
	void endObjectSerialization(SerObjectInfo objInfo)
	{
		_os.writeLine("</object>");
	}
	void beginArraySerialization(SerObjectInfo objInfo)
	{
		_os.writeString("<array id=\"");
		_os.writeString(to!string(objInfo.objId));
		_os.writeString("\" tid=\"");
		_os.writeString(to!string(objInfo.typeId));
		_os.writeString("\" length=\"");
		_os.writeString(to!string(objInfo.arrayLength));
		_os.writeLine("\">");
	}
	void endArraySerialization(SerObjectInfo objInfo)
	{
		_os.writeLine("</array>");
	}
	void beginDictionarySerialization(SerObjectInfo objInfo)
	{
		_os.writeString("<dict id=\"");
		_os.writeString(to!string(objInfo.objId));
		_os.writeString("\" tid=\"");
		_os.writeString(to!string(objInfo.typeId));
		_os.writeLine("\">");
	}
	void endDictionarySerialization(SerObjectInfo objInfo)
	{
		_os.writeLine("</dict>");
	}

	void fieldSerialization(ushort typeId, ref MetaTypeField fi, MetaPtr obj, uint linkedObjectId = 0)
	{
		if(linkedObjectId)
		{
			_os.writeString(" <field tid=\"");
			_os.writeString(to!string(typeId));
			_os.writeString("\" name=\"");
			_os.writeString(fi.name);
			_os.writeString("\" linkId=\"");
			_os.writeString(to!string(linkedObjectId));
			_os.writeLine("\" />");
		}
		else
		{
			_os.writeString(" <field tid=\"");
			_os.writeString(to!string(typeId));
			_os.writeString("\" name=\"");
			_os.writeString(fi.name);
			_os.writeString("\">");
			writeEscString(fi.getValueString(obj));
			_os.writeLine("</field>");
		}
	}
	void arrayItemSerialization(uint index, IMetaType mt, MetaPtr obj, uint linkedObjectId = 0)
	{
		if(linkedObjectId)
		{
			_os.writeString(" <item index=\"");
			_os.writeString(to!string(index));
			_os.writeString("\" linkId=\"");
			_os.writeString(to!string(linkedObjectId));
			_os.writeLine("\" />");
		}
		else
		{
			_os.writeString(" <item index=\"");
			_os.writeString(to!string(index));
			_os.writeString("\">");
			writeEscString(mt.valueToString(obj));
			_os.writeLine("</item>");
		}
	}
	void dictionaryItemSerialization(IMetaType keyType, MetaPtr keyPtr, uint keyLink,
																																		IMetaType valType, MetaPtr valPtr, uint valLink)
	{
		_os.writeString(" <pair ");
		if(keyLink)
		{
			_os.writeString("keyLinkId=\"");
			_os.writeString(to!string(keyLink));
			_os.writeString("\"");
		}
		else
		{
			_os.writeString("key=\"");
			writeEscString(keyType.valueToString(keyPtr), true);
			_os.writeString("\"");
		}
		_os.writeString(" ");
		if(valLink)
		{
			_os.writeString("valueLinkId=\"");
			_os.writeString(valLink == uint.max ? "0" : to!string(valLink));
			_os.writeString("\"");
		}
		else
		{
			_os.writeString("value=\"");
			writeEscString(valType.valueToString(valPtr), true);
			_os.writeString("\"");
		}
		_os.writeLine(" />");
	}

	void beginSerialization()
	{
		_os.writeLine("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
		_os.writeString("<PulsarXmlArchiver version=\"");
		_os.writeString(to!string(ver));
		_os.writeLine("\">");
	}
	void endSerialization()
	{
		_os.writeLine("</PulsarXmlArchiver>");
		_os.flush();
		_os.close();
	}
	void writeEscString(string str, bool quote = false)
	{
		if(str.ptr is null)
			return;
		else if(quote)
		{
			for(auto t = 0; t < str.length; t++)
				if(str[t] == '"')
					_os.write(qt);
				else
					_os.write(str[t]);
		}
		else
		{
			for(auto t = 0; t < str.length; t++)
				if(str[t] == '<')
					_os.write(lt);
				else if(str[t] == '>')
					_os.write(gt);
				else
					_os.write(str[t]);
		}
	}
	//------------------------------------------------------------------------

	void openDeserialization()
	{
		_sp = PullParser!(char)(x);
		checkXml(_sp.next == XmlTokenType.StartElement);
		checkXml(_sp.localName == "PulsarXmlArchiver");
		checkXml(_sp.next == XmlTokenType.Attribute);
		checkXml(_sp.localName == "version");
		checkXml(_sp.rawValue == to!string(ver), "Данные сериализации не соответствуют версии архиватора!");
	}

	bool getObjectRecord(ref DeserItem di)
	{
		while(_sp.type != XmlTokenType.StartElement)
		{
			if(_sp.type == XmlTokenType.EndElement && _sp.localName == "PulsarXmlArchiver")
				return false;
			if(!_sp.next())
				return false;
		}

		if(_sp.localName == "type")
		{
			di.type = DeserItemFlags.Type;
			readAttribs(di);
			di.name = di.name.replace("`","\"");
			if(!_sp.next())
				return false;
			return true;
		}
		else if(_sp.localName == "object")
		{
			di.type = DeserItemFlags.Object;
			readAttribs(di);
			if(!_sp.next())
				return false;
			return true;
		}
		else if(_sp.localName == "array")
		{
			di.type = DeserItemFlags.Array;
			readAttribs(di);
			if(!_sp.next())
				return false;
			return true;
		}
		else if(_sp.localName == "dict")
		{
			di.type = DeserItemFlags.Dictionary;
			readAttribs(di);
			if(!_sp.next())
				return false;
			return true;
		}
		else if(_sp.localName == "surrogate")
		{
			di.type = DeserItemFlags.Surrogate;
			readAttribs(di);
			if(_sp.type == XmlTokenType.Data)
			{
				di.type |= DeserItemFlags.ValueIsString;
				di.stringValue = (cast(string)(_sp.rawValue)).translate(unEscTable);
			}
			if(!_sp.next())
				return false;
			return true;
		}
		else
				throw new Exception("Ошибка в структуре файла данных сериализации!");
		return true;
	}
	bool getFieldRecord(ref DeserItem di)
	{
		while(_sp.type != XmlTokenType.StartElement)
		{
			if(!_sp.next())
				return false;
		}

		if(_sp.localName == "field")
		{
			di.type = DeserItemFlags.Field;
			readAttribs(di);
			if(!(di.type & DeserItemFlags.ValueIsLinkedObjectId))
			{
				if(_sp.type == XmlTokenType.Data)
				{
					di.type |= DeserItemFlags.ValueIsString;
					di.stringValue = (cast(string)(_sp.rawValue)).translate(unEscTable);
				}
			}
			if(_sp.next() == false)
				return false;
			return true;
		}
		return false;
	}
	bool getArrayItemRecord(ref DeserItem di)
	{
		while(_sp.type != XmlTokenType.StartElement)
		{
			if(!_sp.next())
				return false;
		}

		if(_sp.localName == "item")
		{
			di.type = DeserItemFlags.ArrayItem;
			readAttribs(di);
			if(!(di.type & DeserItemFlags.ValueIsLinkedObjectId))
			{
				if(_sp.type == XmlTokenType.Data)
				{
					di.type |= DeserItemFlags.ValueIsString;
					di.stringValue = (cast(string)(_sp.rawValue)).translate(unEscTable);
				}
			}
			if(_sp.next() == false)
				return false;
			return true;
		}
		return false;
	}
	bool getKeyValuePairRecord(ref DeserItem di)
	{
		while(_sp.type != XmlTokenType.StartElement)
		{
			if(!_sp.next())
				return false;
		}

		if(_sp.localName == "pair")
		{
			di.type = DeserItemFlags.KeyValuePair;
			readAttribs(di);
			if(!(di.type & DeserItemFlags.ValueIsLinkedObjectId))
			{
				if(_sp.type == XmlTokenType.Data)
				{
					di.type |= DeserItemFlags.ValueIsString;
					di.stringValue = (cast(string)(_sp.rawValue)).translate(unEscTable);
				}
			}
			if(_sp.next() == false)
				return false;
			return true;
		}
		return false;
	}
private:
	void readAttribs(ref DeserItem di)
	{
		if(_sp.type != XmlTokenType.Attribute)
			_sp.next();
		do
		{
			if(_sp.localName == "tid")
				di.tid = to!ushort(_sp.rawValue);
			else if(_sp.localName == "id")
				di.id = to!uint(_sp.rawValue);
			else if(_sp.localName == "name")
				di.name = cast(string)_sp.rawValue;
			else if(_sp.localName == "key")
				di.key = (cast(string)_sp.rawValue).replace("\u0004","\"");
			else if(_sp.localName == "value")
				di.value = (cast(string)_sp.rawValue).replace("\u0004","\"");
			else if(_sp.localName == "length")
				di.length = to!uint(_sp.rawValue);
			else if(_sp.localName == "index")
				di.index = to!uint(_sp.rawValue);
			else if(_sp.localName == "linkId")
			{
				di.type |= DeserItemFlags.ValueIsLinkedObjectId;
				di.linkId = to!uint(_sp.rawValue);
			}
			else if(_sp.localName == "keyLinkId")
			{
				di.type |= DeserItemFlags.KeyIsLinkedObjectId;
				di.keyLinkId = to!uint(_sp.rawValue);
			}
			else if(_sp.localName == "valueLinkId")
			{
				di.type |= DeserItemFlags.ValueIsLinkedObjectId;
				di.valueLinkId = to!uint(_sp.rawValue);
			}
			else if(_sp.localName == "oid")
				di.oid = cast(string)_sp.rawValue;
		} while(_sp.next == XmlTokenType.Attribute)	;
	}
	T checkXml(T)(T value, lazy string msg = "Ошибка в структуре файла данных сериализации!")
	{
		if (!value)
			throw new Exception(msg);
		return value;
	}

}


/*public enum XmlTokenType : byte
{
	Done,
	StartElement = 1,
	Attribute = 2,
	EndElement = 3,
	EndEmptyElement = 4,
	Data = 5,
	Comment = 6,
	CData = 7,
	Doctype = 8,
	PI = 9,
	None
};*/
