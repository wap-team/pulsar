module pulsar.serialization.ser;

import std.array;
import std.container;

import pulsar.atoms;
import pulsar.reflection.defs;
import pulsar.reflection.library;
import pulsar.serialization.defs;
import pulsar.serialization.archiver;

class Serializer
{
private:
	SerializationSettings _sets;
	Archiver _arch;
	ushort[IMetaType] _types;
	uint[MetaPtr] _objs;

	this() { /*_stack.reserve(1000);*/ }
public:
	static void run(inout Object obj, Archiver arch,
																	SerializationSettings sets = SerializationSettings.init)
	{
		CheckNull!arch;
		assert(obj);
		scope Serializer s = new Serializer();
		s._arch = arch;
		s._sets = sets;
		s.serialize(obj);
	}

	void serialize(inout Object obj)
	{
		Array!SerObjectInfo _stack;
		_stack.reserve(1000);


		_arch.beginSerialization();
		SerObjectInfo si = new SerObjectInfo(MetaPtr.of(obj), MetaTypeLibrary[obj]);
		si.typeId = regType(si.mt);
		si.objId = regObject(si.objPtr);
		_stack ~= si;

		while(_stack.length > 0)
		{
			si = _stack[$-1];
			//trace("*> ", si.objPtr," - ", si.mt.name);

			//-- Проверка на объекты (исследователь) ----
			while(si.typeToCheck !is null)
			{
				if(si.mt.kind & MetaTypeKind.Primitive) {}
				else if(si.mt.kind & MetaTypeKind.Aggregate)
				{
					IMetaTypeAggregate mt = cast(IMetaTypeAggregate)si.typeToCheck;
					//trace(mt.name);
					if(SerializationTuning.hasSerializationSurrogate(mt, si.objId == 1))
					{
						si.typeToCheck = null;
						break;
					}
					for(; si.memberIndex < mt.fields.length; si.memberIndex ++)
					{
						MetaTypeField fi = mt.fields[si.memberIndex];
						//trace(fi.name);
						if(isSerializableField(fi) == false ||
									_sets.noSerializeDefaultValue && fi.hasDefaultValue(si.objPtr))
							continue;

						regType(fi.type);

						if(!(fi.type.kind & MetaTypeKind.Complex) ||
									fi.type.canSetValueFromString())
							continue;

						IMetaType t = fi.type;
						MetaPtr p = fi.getValuePtr(si.objPtr);
						if(t.kind == MetaTypeKind.Class)
						{
							Object o = cast(Object)(p.ptr);
							if(t.typeInfo != typeid(o))
							{
								p = MetaPtr.of(o);
								t = unqual(p.type());
							}
						}
						if(p in _objs)
							continue;

						SerObjectInfo	fsi = new SerObjectInfo(p, t);
						fsi.typeId = regType(t);
						fsi.objId = regObject(p);
						if(t.kind == MetaTypeKind.Array)
							fsi.arrayLength = (cast(IMetaTypeArray)(t)).count(p);
						_stack ~= fsi;
						break;
					}
					if(si.memberIndex >= mt.fields.length)
					{
						mt = mt.baseType;
						si.typeToCheck = mt;
						si.memberIndex = 0;
						if(mt !is null)
							regType(mt);
					}
					else
						break;
				}
				else if(si.mt.kind & MetaTypeKind.Array)
				{
					IMetaTypeArray mt = cast(IMetaTypeArray)si.typeToCheck;
					ushort tid = regType(mt.elemType);
					if(!(mt.elemType.kind & MetaTypeKind.Complex) ||
								mt.elemType.canSetValueFromString())
					{
						si.typeToCheck = null;
						break;
					}


					for(; si.memberIndex < si.arrayLength; si.memberIndex ++)
					{
						MetaPtr p = mt.getValuePtrAt(si.objPtr, si.memberIndex);
						auto mte = mt.elemType;
						if(mte.kind == MetaTypeKind.Class)
						{
							Object o = p.getValue!Object;
							if(o is null)
							{
								throw new Exception("Object is null, mte = ", mte.name);
							}
							mte = MetaTypeLibrary[o];
							//trace(mte);
							if(mte is null)
								throw new Exception("Метатип для типа [" ~ typeid(o).toString() ~
																												"] не зарегистрирован!");

							tid = regType(mte);
							p = mte.getVarValuePtr(&o);
						}

						if(_sets.noSerializeDefaultValue && mte.isDefaultValue(p))
							continue;
						if(p in _objs)
							continue;


						SerObjectInfo	fsi = new SerObjectInfo(p, mte);
						fsi.typeId = tid;
						fsi.objId = regObject(p);
						if(mte.kind == MetaTypeKind.Array)
							fsi.arrayLength = (cast(IMetaTypeArray)(mte)).count(p);
						_stack ~= fsi;
						break;
					}
					if(si.memberIndex >= si.arrayLength)
						si.typeToCheck = null;
					break;
				}
				else if(si.mt.kind & MetaTypeKind.Dictionary)
				{
					IMetaTypeDictionary mt = cast(IMetaTypeDictionary)si.typeToCheck;
					ushort ktid = regType(mt.keyType);
					ushort vtid = regType(mt.valueType);

					bool needCheckKeys = mt.keyType.canSetValueFromString() ? false :
																											((mt.keyType.kind & MetaTypeKind.Complex) != 0);
					bool needCheckVals = mt.valueType.canSetValueFromString() ? false :
																											((mt.valueType.kind & MetaTypeKind.Complex) != 0);

					if(needCheckKeys == false && needCheckVals == false)
					{
						si.typeToCheck = null;
						break;
					}

					for(; si.dictIter.empty == false; si.dictIter.popFront())
					{
						MetaPtr pkey = si.dictIter.front;
						if(needCheckKeys && !(pkey in _objs))
						{
							auto mte = mt.keyType;
							if(mte.kind == MetaTypeKind.Class)
							{
								Object o = pkey.getValue!Object;
								assert(o);
								mte = MetaTypeLibrary[o];
								//trace(mte);
								if(mte is null)
									throw new Exception("Метатип для типа [" ~ typeid(o).toString() ~
																													"] не зарегистрирован!");

								ktid = regType(mte);
								pkey = mte.getVarValuePtr(&o);
							}
							if(_sets.noSerializeDefaultValue && mte.isDefaultValue(pkey))
								continue;
							if(pkey in _objs)
								continue;

							SerObjectInfo	fsi = new SerObjectInfo(pkey, mte);
							fsi.typeId = ktid;
							fsi.objId = regObject(pkey);
							if(mte.kind == MetaTypeKind.Array)
								fsi.arrayLength = (cast(IMetaTypeArray)(mte)).count(pkey);
							_stack ~= fsi;
							break;
						}
						if(needCheckVals)
						{
							MetaPtr pval = mt.getValuePtrByKey(si.objPtr, pkey);
							if(!(pval in _objs))
							{
								auto mte = mt.valueType;
								if(mte.kind == MetaTypeKind.Class)
								{
									Object o = pval.getValue!Object;
									if(o)
									{
										mte = MetaTypeLibrary[o];
										//trace(mte);
										if(mte is null)
											throw new Exception("Метатип для типа [" ~ typeid(o).toString() ~
																															"] не зарегистрирован!");

										vtid = regType(mte);
									}
									pval = mte.getVarValuePtr(&o);
								}
								if(_sets.noSerializeDefaultValue && mte.isDefaultValue(pval))
									continue;
								if(pval in _objs)
									continue;

								SerObjectInfo	fsi = new SerObjectInfo(pval, mte);
								fsi.typeId = vtid;
								fsi.objId = regObject(pval);
								if(mte.kind == MetaTypeKind.Array)
									fsi.arrayLength = (cast(IMetaTypeArray)(mte)).count(pval);
								_stack ~= fsi;
								break;
							}
						}
					}
					if(si.dictIter.empty)
						si.typeToCheck = null;
					break;
				}
				else
					assert(false, "ToDO");
			}
			if(si.typeToCheck !is null)
				continue;

			//-- Сериализация -----------------------------------------
			if(si.mt.kind & MetaTypeKind.Aggregate)
			{
				//trace(si.mt.name);

				IMetaTypeAggregate aggrT = cast(IMetaTypeAggregate)si.mt;
				if(SerializationTuning.hasSerializationSurrogate(aggrT, si.objId == 1))
					_arch.writeObjectSurrogate(si);
				else
				{
					_arch.beginObjectSerialization(si);
					aggrT.onSerializing(si.objPtr);
					foreach(mt; aggrT.getBaseClassesMetaTypes())
					{
						// trace(mt.name);
						ushort typeId = _types[mt];
						foreach(ref fi; mt.fields)
						{
							//trace(fi.name);
							if(isSerializableField(fi) == false)
								continue;
							if(_sets.noSerializeDefaultValue && fi.hasDefaultValue(si.objPtr))
								continue;

							uint link = 0;
							//ushort linkType = 0;
							if(fi.type.kind & MetaTypeKind.Complex &&
										fi.type.canSetValueFromString() == false)
							{
								MetaPtr p = fi.getValuePtr(si.objPtr);
								if(fi.type.kind == MetaTypeKind.Class)
								{
									Object o = cast(Object)(p.ptr);
									if(fi.type.typeInfo != typeid(o))
									{
										p = MetaPtr.of(o);
										//linkType = regType(p.getType());
									}
								}
								uint* v = p in _objs;
								if(v)
									link = *v;
								else
									throw new Exception("Объект не раскрыт!");

							}
							_arch.fieldSerialization(typeId, fi, si.objPtr, link);
						}
					}
					aggrT.onSerialized(si.objPtr);
					_arch.endObjectSerialization(si);
				}
			}
			else if(si.mt.kind == MetaTypeKind.Array)
			{
				IMetaTypeArray mt = cast(IMetaTypeArray)si.mt;
				_arch.beginArraySerialization(si);
				for(auto i = 0; i < si.arrayLength; i++)
				{
					MetaPtr p = mt.getValuePtrAt(si.objPtr, i);
					auto mte = mt.elemType;
					if(mte.kind == MetaTypeKind.Class)
					{
						Object o = p.getValue!Object;
						assert(o);
						mte = MetaTypeLibrary[o];
						assert(mte);
						p = mte.getVarValuePtr(&o);
					}

					if(_sets.noSerializeDefaultValue && mte.isDefaultValue(p))
						continue;

					uint link = 0;
					if(mte.kind & MetaTypeKind.Complex &&
								mte.canSetValueFromString() == false)
					{
						uint* v = p in _objs;
						if(v)
							link = *v;
						else
							throw new Exception("Объект не раскрыт!");
					}
					_arch.arrayItemSerialization(i, mte, p, link);
				}
				_arch.endArraySerialization(si);
			}
			else if(si.mt.kind == MetaTypeKind.Dictionary)
			{
				_arch.beginDictionarySerialization(si);

				IMetaTypeDictionary mt = cast(IMetaTypeDictionary)si.mt;
				bool needLinkKeys = mt.keyType.canSetValueFromString() ? false :
																									((mt.keyType.kind & MetaTypeKind.Complex) != 0);
				bool needLinkVals = mt.valueType.canSetValueFromString() ? false :
																									((mt.valueType.kind & MetaTypeKind.Complex) != 0);

				void keyValueIterBody(MetaPtr keyPtr, MetaPtr valPtr)
				{
					uint keyLink;
					uint valLink;
					auto mtk = mt.keyType;
					auto mtv = mt.valueType;
					if(needLinkKeys)
					{
						if(mtk.kind == MetaTypeKind.Class)
						{
							Object o = keyPtr.getValue!Object;
							assert(o, "Ключ == null ?!?!?!");
							mtk = MetaTypeLibrary[o];
							assert(mtk);
							keyPtr = mtk.getVarValuePtr(&o);
						}

						uint* v = keyPtr in _objs;
						if(v)
							keyLink = *v;
						else
							throw new Exception("Объект не раскрыт!");
					}
					if(needLinkVals)
					{
						if(mtv.kind == MetaTypeKind.Class)
						{
							Object o = valPtr.getValue!Object;
							if(o)
							{
								mtv = MetaTypeLibrary[o];
								assert(mtv);
							}
							valPtr = mtv.getVarValuePtr(&o);
						}

						if(valPtr.isNull == false)
						{
							uint* v = valPtr in _objs;
							if(v)
								valLink = *v;
							else
							{
								trace("SER ERROR: ", valPtr.type.name);
								throw new Exception("Объект не раскрыт! ");
							}
						}
						else
							valLink = uint.max;
					}
					_arch.dictionaryItemSerialization(mtk, keyPtr, keyLink,
																																							mtv, valPtr, valLink);
				}
				mt.iterate(si.objPtr, &keyValueIterBody);
				_arch.endDictionarySerialization(si);
			}
			else
				assert("ToDo");

			_stack.removeBack();
		}
		_arch.endSerialization();

	}

private:
	ushort regType(IMetaType ri)
	{
		assert(ri);

		ushort* val = ri in _types;
		if(val)
			return *val;

		//trace(to!string(ri));

		ushort id = cast(ushort)(_types.length +1);
		_types[ri] = id;
		_arch.regType(id, ri);
		return id;
	}
	uint regObject(MetaPtr o)
	{
		uint* val = o in _objs;
		if(val)
			return *val;

		//trace(o.toString());

		uint id = cast(uint)(_objs.length +1);
		_objs[o] = id;
		return id;
	}
	bool isSerializableField(MetaTypeField fi)
	{
		uint m = uint.max;
		foreach(a; fi.attributes)
			if(a.type == typeid(SerializationModes))
			{
				m = a.to!SerializationModes.mode;
				break;
			}
		return (m & _sets.mode) > 0;
	}
}

class SerObjectInfo
{
private:
	IMetaType typeToCheck;
	union
	{
		uint memberIndex = 0;
		IMetaIterator dictIter;
	}

	this(MetaPtr op, IMetaType type)
	{
		objPtr = op;
		mt = type;
		if(type.kind & MetaTypeKind.Complex)
			typeToCheck = type;
		if(type.kind == MetaTypeKind.Dictionary)
			dictIter = (cast(IMetaTypeDictionary)type).newKeysIterator(op);
	}

public:
	MetaPtr objPtr;
	IMetaType mt;

	uint objId = 0;
	ushort typeId = 0;
	union
	{
		uint arrayLength = 0;
		UUID oid;
	}
}
