module pulsar.containers.arrays;

import core.stdc.stdlib;

// Значительно быстрее, чем std.container : Array
struct ElasticArray(T)
{
private:
	size_t _size;
	T* _buf;

public:
	@property // length
	{
		size_t length() const { return _size; }
		void length(size_t val) { resize(val); }
	}
	@property // []
	{
		T opIndex(size_t pos)
		{
			if(pos >= _size)
				return T.init;
			return _buf[pos];
		}
		void opIndexAssign(T val, size_t pos)
		{
			resize(pos+1);
			_buf[pos] = val;
		}
	}

public:
	this(size_t size) { resize(size); }
	~this()
	{
		if(_buf !is null)
			free(_buf);
	}

	void resize(size_t size)
	{
		if(size <= _size)
			return;
		_buf = cast(T*) realloc(_buf, T.sizeof * size);
		_buf[_size..size-1] = T.init;
		_size = size;
	}

}

unittest
{
	import std.stdio;
	import std.uuid;

	UUID id = randomUUID();

	ElasticArray!byte arr;
	arr[2] = 2;
	assert(arr.length == 3);
	assert(arr[2] == 2);
	assert(arr[0] == 0);

	// тест на автоуничтожение
// writeln("...");
// for(int i = 0; i < 1000000; i++)
// {
//  auto a = ElasticArray!UUID(1000);
//  a[100] = id;
// }
}
