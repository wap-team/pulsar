module pulsar.containers.stack;

import std.conv;

class Stack(T)
{
private:
	T[] _arr;

public:
	@property size_t count() const { return _arr.length; }
	@property bool isEmpty() const { return _arr.length == 0; }

public:
	this() {}
	this(T[] arr) { _arr = arr; }

public:
	void push(T t) { _arr ~= t; }
	T front(bool throwIfEmpty = true)
	{
		if(_arr.length == 0)
			if(throwIfEmpty)
				throw new Exception("Stack is empty!");
			else
				return T.init;
		return _arr[$-1];
	}
	T pop(bool throwIfEmpty = true)
	{
		if(_arr.length == 0)
			if(throwIfEmpty)
				throw new Exception("Stack is empty!");
			else
				return T.init;
		T t = _arr[$-1];
		_arr.length--;
		return t;
	}
	void clear() { _arr.length = 0; }

	override string toString() const { return to!string(_arr); }
}
