module pulsar.containers.trees;

import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.typecons;

import pulsar.atoms;
import pulsar.serialization.defs;


interface ITree
{
	@property const(ITree) parent() const;
	@property const(ITree) firstChild() const;
	/// Следующий за этим элемент в родительской коллекции дочерних элементов
	@property const(ITree) next() const;

//	@property const(T) parent(this T)() const { return cast(T)parenT; }
//	@property const(T) firstChild(this T)() const  { return cast(T)firstChildT; }
//	@property const(T) next(this T)() const  { return cast(T)nexT; }

	/// Предыдущий этому элемент в родительской коллекции дочерних элементов
	final @property const(T) prev(this T)() const
	{
		if(parent is null)
			return null;
		TailConst!T prev;
		foreach(ch; parent.childs)
			if(ch is this)
				return prev();
			else
				prev = cast(T)ch;
		return null;
	}
	/// Определяет, имеет ли элемент родительский элемент
	final @property bool hasParent() const { return parent !is null; }
	/// Определяет, имеет ли элемент дочерние элементы
	final @property bool hasChilds() const { return firstChild !is null; }
	/// Уровень элемента
	final @property uint level() const { return parent is null ? 0 : parent.level+1; }
	/// Возвращает корневой элемент дерева
	final @property const(T) root(this T)() const
	{
		ITree t = cast(ITree)this;
		while(t.parent !is null)
			t = cast(ITree)t.parent;
		return cast(T)t;
	}
	/// Возвращает последний дочерний элемент дерева
	final @property const(T) lastChild(this T)() const
	{
		TailConst!T ch = cast(T)firstChild;
		while(ch.next !is null)
			ch = ch.next;
		return ch;
	}
	/// Возвращает родительский элемент с заданным уровнем
	final const(T) parentWithLevel(this T)(uint level) const
	{
		TailConst!T t = (cast(T)this).parent;
		while(t.isNull == false && t.level != level)
			t = t.parent;
		return t.consted;
	}
	/// Возвращает итератор по родительским элементам (начиная с корня)
	final auto parentsChain(this T)(bool withThis = true) const
	{
		final struct Iter
		{
			T t;

			int opApply(int delegate(const(T)) dg)
			{
				int result = 0;
				if(t is null)
					return -1;


				if(t.hasParent)
				{
					if(Iter(t.parent.unqual).opApply(dg))
						return result;
				}

				result = dg(t);
				return result;
			}
		}
		return Iter(cast(T)(withThis ? this : this.parent));
	}
	/// Возвращает true, если текущий элемент является родительским для указанного
	final bool isParentFor(this T)(const(ITree) el) const
	{
		CheckNull!el;
		if(el.hasParent == false)
			return false;
		for(ITree t = cast(ITree)el.parent; t !is null; t = cast(ITree)t.parent)
			if(t is this)
				return true;
		return false;
	}


	/// Возвращает структуру обхода всех элементов дерева (рекурсивно, сохраняя порядок).
	final @property auto allItems(this T)() const
	{
		static struct IterAllChilds
		{
			Unqual!T[] i;

		public:
			@property bool empty() const { return i.length == 0; }
			@property const(T) front( ) const { return cast(T)i[$-1]; }
			void popFront()
			{
				Unqual!T t = i[$-1];
				if(t.firstChild is null)
				{
					do
					{
						t = i[$-1];
						i.length--;
						if(i.length == 0)
							break;
						if(t.next !is null)
							i ~= cast(Unqual!T)t.next;
					}	while(t.next is null);
				}
				else
					i ~= cast(Unqual!T)t.firstChild;
			}
			auto save() { return this; }

		}
		return IterAllChilds([cast(Unqual!T)this]);
	}
	/// Дочерние элементы
	final @property auto childs(this T)() const
	{
		static struct IterChilds
		{
			Unqual!T t;

			@property bool empty() const { return t is null; }
			@property const(T) front( ) const	{ return t; }
			void popFront() { t = t ? t.next.unqual : null; }
			auto save() { return this; }

			int opApply(int delegate(const(T)) dg) const
			{
				int result = 0;
				TailConst!T x = t;
				for(; x !is null; x = x.next)
				{
					result = dg(x());
					if(result)
						break;
				}
				return result;
			}
		}
		return IterChilds(cast(Unqual!T)(this.firstChild)); // TailConst!T(cast(const(T))firstChildT));
	}

	/// Ищет первый дочерний элемент (рекурсивно) в дереве,
	/// соответствующий предикату
	final const(T) find(this T)(bool delegate(const(T)) eqFun) const
	{
		foreach(t; (cast(T)this).allItems)
			if(eqFun(t))
				return t;
		return null;
	}
	/// Ищет первый дочерний элемент (не рекурсивно) в дереве,
	/// соответствующий предикату
	final const(T) findChild(this T)(bool delegate(const(T)) eqFun) const
	{
		foreach(t; (cast(T)this).childs)
			if(eqFun(t))
				return t;
		return null;
	}

	/// Возвращает true, если текущий элемент "выше" в списке дочерних элементов, чем указанный
	final bool isHigher(ITree otherChild) const
	{
		if(parent is null || otherChild is null || parent !is otherChild.parent)
			return false;

		for(ITree ch = this.unqual; ch !is null; ch = ch.next.unqual)
			if(ch is otherChild)
				return true;
		return false;
	}
}

mixin template TreeBase(this T, bool withOnDeser = true)
{
protected:
	// Родительский элемент
	@NonSerialized
	T _parent;
	// Первый чилд этого элемента
	T _firstChild;
	// Следующий элемент чилдов родителя
	T _next;

public:
	/// Родительский элемент
	@property // parent
	{
		override const(T) parent() const { return _parent; }
		protected void parent(T t) { _parent = t; }
	}
	/// Возвращает первый дочерний элемент дерева
	@property // firstChild
	{
		override const(T) firstChild() const { return _firstChild; }
		protected void firstChild(T t) { _firstChild = t; }
	}
	/// Возвращает следующий за данным элемент в списке дочерних элементов родителя
	@property // next
	{
		override const(T) next() const { return _next; }
		protected void next(T t) { _next = t; }
	}

	static if(withOnDeser == true)
	{
		public	void onDeserialized()
		{
			for(auto ch = _firstChild; ch; ch = ch._next)
				ch._parent = cast(T)this;
		}
	}
	else
	{
		private	void _onDeserialized()
		{
			for(auto ch = _firstChild; ch; ch = ch._next)
				ch._parent = cast(T)this;
		}
	}
}

mixin template TreeMutators(this T)
{
	/** Линкует элемент дерева как дочерний к текущему элементу.
		* Params:
		* item = Линкуемый элемент
		* toEnd = Признак линкования в конец списка чилдов.
		*         Линкование в конец списка медленне, чем в начало,
		*         но дает "правильное" дерево.
		*/
	final T link(T item, bool toEnd = false)
	{
		CheckNull!(item);
		if(item.parent !is null)
			throw new Exception("Элемент не может быть прилинкован повторно!");

		if(firstChild is null)
			(cast(T)this).unqual.firstChild = item.unqual;
		else if(toEnd)
			unqual((cast(T)this).lastChild).next = item.unqual;
		else
		{
			(cast(T)item).next = unqual(firstChild);
			(cast(T)this).firstChild = item.unqual;
		}
		(cast(T)item).parent = cast(T)this;
		return item;
	}
	/** Массовое линкование дочерних элементов. Сохраняется порядок добавления.
		* Returns:
		* Всегда возвращает this для реализации цепочечных вызовов.
		*/
	final T links(T[] childs ...)
	{
		foreach_reverse(n; childs)
			this.link(n);
		return cast(T)this;
	}
	/// Перелинковывает элемент к другому родителю в том же дереве
	final void relink(T newParent)
	{
		assert(newParent);
		assert(this.root is newParent.root);
		if(this.parent is newParent)
			return;

		for(TailConst!T t = newParent; t !is null; t = t.parent)
			if(t() is this)
				throw new LogicError("Новый родительский элемент не может быть " ~
																									"дочерним у перемещаемого элемента!");
		(cast(T)this).unlink();
		newParent.link(cast(T)this);
	}
	/// Разлинковывает элемент дерева.
	/// Дочерние элементы остаются в текущем элементе.
	final void unlink()
	{
		if(parent is null)
			return;
		if(parent.firstChild == this)
			unqual(parent).firstChild = unqual(next);
		else
		{
			foreach(ch; parent.childs)
				if(ch.next == this)
				{
					unqual(ch).next = unqual(next);
					break;
				}
		}
		next = null;
		parent = null;
	}
	/// Разлинковывает все дочерние элементы дерева.
	/// deepUnlink - разлинковывает дочерние элементы дочерних элементов
	final void unlinkAllChilds(bool deepUnlink = false)
	{
		if(firstChild is null)
			return;

		if(deepUnlink)
		{
			while(firstChild)
			{
				unqual(firstChild).unlinkAllChilds(true);
				unqual(firstChild).unlink();
			}
		}
		else
		{
			unqual(firstChild).parent = null;
			firstChild = null;
		}
	}
	/// Перемещает элемент в списке дочерних элементов на одну позицию вверх или вниз.
	final bool move(bool up)
	{
		if(this.parent is null)
			return false;
		if(up && this.parent.firstChild == this)
			return false;
		if(!up && this.next is null)
			return false;
		T pprev = unqual(this.parent.firstChild);
		T prev = unqual(pprev.next);
		if(prev != this && pprev != this)
			while(prev.next != this)
			{
				pprev = prev;
				prev = unqual(prev.next);
			}
		if(up)
		{

			if(prev == this)
			{
				unqual((cast(T)this).parent).firstChild = cast(T)this;
				pprev.next = unqual(this.next);
				this.next = pprev;
			}
			else
			{
				pprev.next = cast(T)this;
				prev.next = unqual(this.next);
				this.next = prev;
			}
		}
		else
		{
			if(pprev == this)
			{
				unqual(this.parent).firstChild = prev;
				this.next = unqual(prev.next);
				prev.next = cast(T)this;
			}
			else if(prev == this)
			{
				pprev.next = unqual(this.next);
				this.next = unqual(pprev.next.next);
				unqual(pprev.next).next = cast(T)this;
			}
			else
			{
				prev.next = unqual(this.next);
				this.next = unqual(prev.next.next);
				unqual(prev.next).next = cast(T)this;
			}
		}
		return true;
	}
	/// Функция сортировки дочерних элементов дерева
	final void sortChilds(bool delegate(const(T), const(T)) cmpFun, bool recursive = false)
	{
		if(this.firstChild is null)
			return;
		T ch = cast(T)this.firstChild;
		if(ch.next is null)
		{
			if(recursive)
				ch.sortChilds(cmpFun,recursive);
			return;
		}

		T[] chs;
		while(ch !is null)
		{
			if(recursive)
				ch.sortChilds(cmpFun,recursive);
			chs ~= ch;
			ch = cast(T)ch.next.unqual;
			chs[chs.length-1].next = null;
		}
		import std.algorithm:sort;
		sort!(cmpFun)(chs);
		for(ulong a = chs.length-1; a > 0; a--)
			chs[a-1].next = chs[a];
		this.firstChild = chs[0];
	}
}

/// Печатает дерево на консоли
void printTree(T : ITree)(T tree)
{
	import std.range;
	foreach(ch; tree.allItems)
		Logger.log(cast(int)(ch.level), "%s", ch.unqual.toString());
	Logger.log();
}

//-------------
/// Класс дерева (элемента дерева)
class Tree(T) : ITree
{
	mixin TreeBase!(T);

public:
	this() { }

public:
	mixin TreeMutators!(T);
}

/// Класс дерева с OID
class TreeOID(T) : Tree!T, IOIDObject
{
protected:
	UUID _oid;

public:
	this()
	{
		if(_oid.empty)
			_oid = randomUUID;
	}

public:
	@noproxy	@property ref OID oid() const { return _oid; }
}

/// Класс дерева с текстом
class TTreeText(T) : Tree!T
{
protected:
	string _txt;

public:
	/// Текст элемента
	@property // text
	{
		string text() const { return _txt; }
		void text(string value) { _txt = value; }
	}

public:
	override string toString() const { return _txt; }
}
class TreeText : TTreeText!TreeText
{
public:
	this()	{	}
	this(string text) { _txt = text; }
}
