module pulsar.containers.index;

import std.algorithm : remove, SwapStrategy;
import std.conv;
import std.exception;
import std.string;
import std.traits;
public import std.typecons: Nullable;
import std.typetuple;

import pulsar.atoms;
import pulsar.atoms.math;
import pulsar.atoms.traits;

version(PulsarAtoms)
{
	enum NonSerialized = "";
}
version(Server)
{
	import pulsar.serialization.attributes;
}


enum NULL = Nullable!(size_t)();

/// Класс хеш-индекса для массива элементов
/// T - тип элемента массива
/// fun - функция получения индексируемого значения
/// X - тип управляющих массивов индекса
class HashIndex(T, alias  fun /* = (x) => x*/, X = uint)  if(isUnsigned!X)
{
	//
	static if(is(typeof(fun) : string))
	{
		//pragma(msg, ToString!fun, "- string");
		mixin(`
			auto getIndexedValue(const(T) x) inout
			{
				static if(__traits(compiles, x is null))
				{
					assert(x);
				}
				return unqual(x).` ~ fun ~ `;
			}
		`);
		//auto getIndexedValue(T x) inout { return x.fun(); }
		alias TKey = ReturnType!getIndexedValue;
		alias GetIndexedValue = typeof(getIndexedValue);
	}
	else
	{
		//pragma(msg, ToString!fun, "- fun");
		alias TKey = typeof(fun(T.init));
		//alias TKey = ReturnType!fun;
		//pragma(msg, typeof(this), "->", TKey);
		alias GetIndexedValue = typeof(fun);
		alias getIndexedValue = fun;
	}

public:
	this() {  }
	this(ref T[] array) { opAssign(array); }

	mixin HashIndexMethods;
}

/// Класс хеш-индекса для массива элементов
/// T - тип элемента массива
/// TI - тип индексируемого значения
/// X - тип управляющих массивов индекса
class HashIndex(T, TI = void, X = uint)  if(isUnsigned!X)
{
	static if(is(TI == void))
		alias TKey = T;
	else
		alias TKey = TI;

	enum FT = fullyQualifiedName!T;
	enum FTKey = fullyQualifiedName!TKey;
	enum FuncType = FTKey ~ " function(" ~ FT ~ ")";
	static if(is(TI == void))
		T getIndexedValue(T t) inout { return t; }
	else
	{
		mixin("alias GetIndexedValue = " ~ FuncType ~";");
		@NonSerialized @noproxy GetIndexedValue getIndexedValue;
	}

	static if(is(TI == void))
	{
		package this() { }
		this(T[] array) { opAssign(array); }
	}
	else
	{
		package this() { }
		this(GetIndexedValue f) { assert(f); getIndexedValue = f; }
		this(T[] array, GetIndexedValue f)
		{
			assert(f);
			getIndexedValue = f;
			opAssign(array);
		}
	}

	mixin HashIndexMethods;
}

mixin template HashIndexMethods()
{
	enum MAX  = X.max;

private:
	T[] _arr;
	@NonSerialized X[] _ix;
	@NonSerialized X[] _ls;

public:
	/// Возвращает несущий массив
	@property const(T[]) array() const { return _arr; }

	void opAssign(ref T[] arr)
	{
		reset(arr, 0, true);
	}
	/// Возвращает количество элементов в индексируемом массиве.
	@property size_t count() const { return _arr.length; }
	/// Перестраивает индекс
	void rebuild()
	{
		reset(_arr,0, false);
	}
	/// Определяет, присутствует ли элемент с указанным значением в индексируемом массиве
	bool has(const(TKey) value) const
	{
		return indexOf(value).isNull == false;
	}
	static if(!is(T == TKey))
	{
		/// Определяет, присутствует ли данный элемент в индексируемом массиве
		bool has(const(T) t) const
		{
			return indexOf(getIndexedValue(unqual(t))).isNull == false;
		}
	}
	/// Определяет индекс элемента с указанным значением в индексируемом массиве
	Nullable!(size_t) indexOf(const(TKey) value) const
	{
		size_t ii = typeid(TKey).getHash(&value) % _ix.length;
		ii = _ix[ii];
		while(ii != MAX)
		{
			T t = (cast(Unqual!T[])_arr)[ii];
			Unqual!TKey tk;
			tk = cast(Unqual!TKey)getIndexedValue(t);
			static if(is(TKey == class) || is(TKey == interface))
			{
				if(value is tk)
					return Nullable!(size_t)(ii);
			}
			else
			{
				if(value == tk)
					return Nullable!(size_t)(ii);
			}
			ii = _ls[ii];
		}
		return NULL;
	}
	static if(!is(T == TKey))
	{
		/// Определяет индекс элемента в индексируемом массиве
		Nullable!(size_t) indexOf(const(T) t) const
		{
			return indexOf(getIndexedValue(unqual(t)));
		}
	}
	/// Возвращает элемент массива по значению индекса.
	inout(T) get(const(TKey) value, lazy const(T) ifNotFound = T.init) inout
	{
		Nullable!(size_t) pos = indexOf(value);
		if(pos.isNull)
		{
			const(T) x = ifNotFound;
			return *(cast(inout(T)*)&x);
		}
		return _arr[pos.get];
	}
	static if(is(T == struct))
	{
		/// Возвращает ссылку на элемент массива по значению индекса.
		Ref!T getRef(const(TKey) value) const
		{
			Nullable!(size_t) pos = indexOf(value);
			if(pos.isNull)
				return Ref!T.init;
			return Ref!T(unqual(_arr).ptr + pos.get);
		}
	}

package:
	void reset(T[] arr, size_t capacity, bool check)
	{
		static if(__traits(compiles, getIndexedValue is null))
		{
			if(getIndexedValue is null)
				return;
		}

		assert(arr.length < X.max,"Индекс не может использоваться для массивов" ~
																												"с длиной более " ~ to!string(X.max) ~ "!");

		_arr = arr;
		if(_arr.length == 0 || _arr.length > _ix.length)
		{
			if(capacity)
				_ix.length = getHashPrime!X(cast(X)capacity);
			else
				_ix.length = getNextPrime!X(cast(X)(_arr.length));
			_ls.length = _ix.length;
		}
		_ix[] = MAX;
		_ls[] = MAX;

		for(X ai = 0; ai < _arr.length; ai ++)
			if(check)
			{
				if(!indexAdd(ai))
					throw new Exception("Массив содержит неуникальные значения!");
			}
		else
			indexAddNoCheck(ai);
	}
	bool indexAdd(X pos)
	{
		TKey value = getIndexedValue(_arr[pos]);
		size_t ii = typeid(TKey).getHash(&value) % _ix.length;
		if(_ix[ii] == MAX)
			_ix[ii] = pos;
		else if(getIndexedValue(_arr[_ix[ii]]) == value)
			return false;
		else
		{
			for(ii = _ix[ii]; _ls[ii] != MAX; ii = _ls[ii])
			{
				if(getIndexedValue(_arr[ii]) == value)
					return false;
			}
			_ls[ii] = pos;
		}
		return true;
	}
	void indexAddNoCheck(X pos)
	{
		TKey value = getIndexedValue(_arr[pos]);
		size_t ii = typeid(TKey).getHash(&value) % _ix.length;
		if(_ix[ii] == MAX)
			_ix[ii] = pos;
		else
		{
			for(ii = _ix[ii]; _ls[ii] != MAX; ii = _ls[ii]){}
			_ls[ii] = pos;
		}
	}
	bool indexRemove(X pos)
	{
		TKey value = getIndexedValue(_arr[pos]);
		size_t ii = typeid(TKey).getHash(&value) % _ix.length;
		if(_ix[ii] == MAX)
			return false;
		X* cur = &_ix[ii];
		ii = _ix[ii];
		do
		{
			if(*cur == pos)
			{
				*cur = _ls[ii];
				_ls[ii] = MAX;
				return true;
			}
			cur = &_ls[ii];
			ii = _ls[ii];
		} while(ii < _ls.length && *cur != MAX);
		return false;
	}
	void clear()
	{
		_ix[] = MAX;
		_ls[] = MAX;
	}

	public void onDeserialized()
	{
		rebuild();
	}
}

version(unittest)
{
	class XXX
	{
		int a;
		this(int v) { a = v; }
		static XXX opCall(int v) { return new XXX(v); }
	}

	struct S
	{
		int a;
		string b;
		XXX x;
	}
}

unittest
{
	auto orig = [S(1),S(2),S(8),S(4),S(15)];
	auto pix1 = new HashIndex!(S,(x)=> x.a, ubyte)(orig);
	auto pix2 = new HashIndex!(S,int, ubyte)(orig,(x)=> x.a);
	auto pix3 = new HashIndex!(S,"a", ubyte)(orig);


	assertThrown(new HashIndex!int([1,2,8,1,15]));
	assertThrown(new HashIndex!int([1,3,8,3,15]));

	byte[] arr = [1,2,8,4,15];
	auto index = new HashIndex!(byte)(arr);
	foreach(i; arr)
		assert(index.has(i));
	foreach(i; cast(byte[])[3, 7, 11, 22])
		assert(!index.has(i));
	for(auto i = 0; i < arr.length; i++)
		assert(index.indexOf(arr[i]) == i);
	foreach(i; cast(byte[])[3, 7, 11, 22])
		assert(index.indexOf(i).isNull);


	S[] arrS = [S(1),S(2),S(8),S(4),S(15)];
	auto indexS = new HashIndex!(S)(arrS);
	foreach(i; arrS)
		assert(indexS.has(i));
	foreach(i; [S(3), S(7), S(11), S(22)])
		assert(!indexS.has(i));
	for(auto i = 0; i < arrS.length; i++)
		assert(indexS.indexOf(arrS[i]) == i);
	foreach(i; [S(3), S(7), S(11), S(22)])
		assert(indexS.indexOf(i).isNull);

	auto indexSI = new HashIndex!(S,int,ubyte)(arrS, (s) => s.a);
	foreach(i; arr)
		assert(indexSI.has(i));
	foreach(i; [3, 7, 11, 22])
		assert(!indexSI.has(i));
	for(auto i = 0; i < arr.length; i++)
		assert(indexSI.indexOf(arr[i]) == i);
	foreach(i; [3, 7, 11, 22])
		assert(indexSI.indexOf(i).isNull);

	XXX[] arrX = [XXX(1),XXX(2),XXX(8),XXX(4),XXX(15)];
	auto indexX = new HashIndex!(XXX)(arrX);
	foreach(i; arrX)
		assert(indexX.has(i));
	foreach(i; [XXX(3), XXX(7), XXX(11), XXX(22)])
		assert(!indexX.has(i));
	for(auto i = 0; i < arrX.length; i++)
		assert(indexX.indexOf(arrX[i]) == i);
	foreach(i; [XXX(3), XXX(7), XXX(11), XXX(22)])
		assert(indexX.indexOf(i).isNull);

	foreach(i; arr)
		assert(pix1.has(i));
	foreach(i; [3, 7, 11, 22])
		assert(!pix1.has(i));
	for(auto i = 0; i < arr.length; i++)
		assert(pix1.indexOf(arr[i]) == i);
	foreach(i; [3, 7, 11, 22])
		assert(pix1.indexOf(i).isNull);
	foreach(i; arr)
		assert(pix2.has(i));
	foreach(i; [3, 7, 11, 22])
		assert(!pix2.has(i));
	for(auto i = 0; i < arr.length; i++)
		assert(pix2.indexOf(arr[i]) == i);
	foreach(i; [3, 7, 11, 22])
		assert(pix2.indexOf(i).isNull);
	foreach(i; arr)
		assert(pix3.has(i));
	foreach(i; [3, 7, 11, 22])
		assert(!pix3.has(i));
	for(auto i = 0; i < arr.length; i++)
		assert(pix3.indexOf(arr[i]) == i);
	foreach(i; [3, 7, 11, 22])
		assert(pix3.indexOf(i).isNull);

	index.indexRemove(0);
	assert(!index.has(1));
	foreach(i; cast(byte[])[2,8,4,15])
		assert(index.has(i));
	index.indexRemove(1);
	foreach(i; cast(byte[])[8,4,15])
		assert(index.has(i));

	index.rebuild();
	index.indexRemove(2);
	assert(!index.has(8));
	foreach(i; cast(byte[])[1,2,4,15])
		assert(index.has(i));
	index.rebuild();
	index.indexRemove(4);
	assert(!index.has(15));
	foreach(i; cast(byte[])[1,2,8,4])
		assert(index.has(i));

}

//----------------------------------------------------------------

template IndexList(T, Indexes...)
{
	alias IndexList = IndexList!(T,uint,Indexes);
}

/** Класс индексированного списка элементов
	* Params:
	*		T = Тип элементов индекса
	*		X = Тип управляющего массива списка
	*		Indexes = Перечисление индексов
	*
	* Examples:
	*		auto l1 = new IndexList!(S);
	*		auto l2 = new IndexList!(S, ubyte);
	*		auto l3 = new IndexList!(S, ubyte, "ix", "a")();
	*		auto l4 = new IndexList!(S, ubyte, "a")();
	*/
class IndexList(T,X, Indexes...)	if(isUnsigned!X)
{
private:
	Unqual!T[] _arr = [];
	alias TI = Unqual!T;
	alias LT = typeof(this);

public:
	static if(Indexes.length == 0)
	{
		alias IxNames = TypeTuple!("_defIndex");
//		pragma(msg, moduleName!T);
//		mixin("import " ~ moduleName!T ~ ";");
		private @NonSerialized HashIndex!(TI,void,X) _defIndex;
		private void ixInit() { _defIndex = new HashIndex!(TI,void,X); }
		enum CtorArgs = "";
		enum CtorBody = "";
//		auto opDispatch(string s, TArgs...)(TArgs args) inout
//		{
//			mixin("return _defIndex."~s~"(args);");
//		}
	}
	else
	{
		template ParseIndexes(size_t i)
		{
			static if(i >= Indexes.length)
			{
				alias IxNames = TypeTuple!();
				enum MakeVars = "";
				enum InitVars = "";
				enum CtorArgs = "";
				enum CtorBody = "";
			}
			else static if(!is(typeof(Indexes[i]) : string) )
				static assert(0, "invalid index definition: need string, not ["~	Indexes[i].stringof~"]");
			else
			{
				//pragma(msg, "T." ~ Indexes[i], " => ", __traits(compiles, mixin("T." ~ Indexes[i])));
				static if(i+1 >= Indexes.length || is(typeof(Indexes[i+1]) : string))
				{
					static if(__traits(compiles, mixin("T." ~ Indexes[i])))
						enum val = i;
					else static if(i+1 >= Indexes.length)
						static assert(0, "invalid index definition: "~Indexes[i]~	" is not member of "~T.stringof);
					else static if(!__traits(compiles, mixin("T." ~ Indexes[i+1])))
						static assert(0, "invalid index definition: "~Indexes[i+1]~	" is not member of "~T.stringof);
					else
						enum val = i+1;
				}
				else
					enum val = i + 1;

				alias Iter = ParseIndexes!(i + (val - i) + 1);
				alias IxNames = TypeTuple!(Indexes[i], Iter.IxNames);

				//pragma(msg, format("auto %s = new HashIndex!(T,Indexes[%s],X);", Indexes[i], val));
//				enum MakeVars =
//						format("@NonSerialized auto %s = new HashIndex!(T,Indexes[%s],X);", Indexes[i], val) ~
//						Iter.MakeVars;
					enum MakeVars =
							format("@NonSerialized HashIndex!(TI,Indexes[%s],X) %s; ", val, Indexes[i]) ~
							Iter.MakeVars;
					enum InitVars =
							format("if(%s is null) %s = new HashIndex!(TI,Indexes[%s],X);", Indexes[i], Indexes[i], val) ~
							Iter.InitVars;

				static if(is(Indexes[val]))
				{
					enum FT = fullyQualifiedName!T;
					enum FTKey = fullyQualifiedName!(Indexes[val]);
					enum FuncType = FTKey ~ " function(" ~ FT ~ ")";
					enum CtorArgs =	format("%s %s_,", FuncType, Indexes[i]) ~ Iter.CtorArgs;
					enum CtorBody =	format("%s.getIndexedValue = %s_;", Indexes[i], Indexes[i]) ~ Iter.CtorBody;
				}
				else
				{
					enum CtorArgs = Iter.CtorArgs;
					enum CtorBody = Iter.CtorBody;
				}
			}

		}

		//pragma(msg, "---- ", typeof(this), " ----");
		alias Ixes = ParseIndexes!0;
//		pragma(msg, "IxNames: ", Ixes.IxNames);
//		pragma(msg, "MakeVars: ", Ixes.MakeVars);
//		pragma(msg, "InitVars: ", Ixes.InitVars);
//		pragma(msg, "CtorArgs: ", Ixes.CtorArgs);
//		pragma(msg, "CtorBody: ", Ixes.CtorBody);
		mixin(Ixes.MakeVars);
		alias IxNames = Ixes.IxNames;
		alias CtorArgs = Ixes.CtorArgs;
		alias CtorBody = Ixes.CtorBody;

		private void ixInit()
		{
			mixin(Ixes.InitVars);
		}
	}


public:
	mixin("this(" ~ CtorArgs ~ `)
	{
		ixInit();
		mixin(CtorBody);
		rebuild();
	}`);
	mixin("this(size_t capacity," ~ CtorArgs ~ `)
	{
		ixInit();
		assert(capacity > 0);
		_arr.reserve(capacity);
		mixin(CtorBody);
		foreach(i; IxNames)
			mixin(i).reset(_arr,cast(X)capacity, true);
	}`);
	mixin("this(T[] arr, " ~ CtorArgs ~ `)
	{
		ixInit();
		mixin(CtorBody);
		opAssign(arr);
	}`);

public:
	/// Возвращает несущий массив
	@property const(T[]) array() const { return _arr; }
	/// Устанавливает указаный массив в качестве несущего.
	void opAssign(T[] arr)
	{
		_arr = cast(Unqual!T[])arr;
		rebuild();
	}
	/// Возвращает количество элементов в массиве.
	@property size_t count() const { return _arr.length; }
	/// Возвращает позицию элемента в списке (по первому индексу).
	Nullable!(size_t) indexOf(const(T) t) const
	{
		return mixin(IxNames[0]).indexOf(t);
	}
	bool has(const(T) t) const { return mixin(IxNames[0]).indexOf(t).isNull == false; }

	/// Метод добавления элемента в массив.
	/// checkExists определяет выполнение проверки на присутсвие элемента (по всем индексам).
	/// Возвращает позицию элемента в массиве или null,если стоит проверка и элемент присутствует.
	Nullable!(size_t) add(T t, bool checkExists = true)
	{
		assert(_arr.length < MaxPrime!X,"Индекс не может использоваться для массивов" ~
																																		"с длиной более " ~ to!string(MaxPrime!X) ~ "!");

		if(checkExists)
			foreach(I;IxNames)
				if(mixin(I).has(t))
					return NULL;

		T* p = _arr.ptr;
		_arr ~= t;

		bool needRebuild = p != _arr.ptr;

		foreach(I;IxNames)
		{
			mixin(I)._arr = _arr;
			if(needRebuild || _arr.length > mixin(I)._ix.length)
				mixin(I).reset(_arr,_arr.length, false);
			else
				mixin(I).indexAddNoCheck(cast(X)(_arr.length-1));
		}

		return Nullable!(size_t)(_arr.length-1);
	}
	/// Метод вставки элемента в массив.
	/// checkExists определяет выполнение проверки на присутсвие элемента (по всем индексам).
	void insert(T t, size_t pos, bool checkExists = true)
	{
		assert(_arr.length < MaxPrime!X,"Индекс не может использоваться для массивов" ~
																																		"с длиной более " ~ to!string(MaxPrime!X) ~ "!");

		if(checkExists)
			foreach(I;IxNames)
				if(mixin(I).has(t))
					throw new Exception("Элемент уже присутствует в списке!");

		import std.array;

		insertInPlace(_arr, pos, t);
		foreach(I;IxNames)
		{
			mixin(I)._arr = _arr;
			mixin(I).reset(_arr,_arr.length, false);
		}
	}
	/// Устанавливает элемент в массиве, если он уже там, иначе добавляет в массив.
	/// Возвращает true, если элемент был изменен в массиве, false - если был добавлен.
	bool set(T t)
	{
		assert(_arr.length < MaxPrime!X,"Индекс не может использоваться для массивов" ~
																																		"с длиной более " ~ to!string(MaxPrime!X) ~ "!");

		Nullable!(size_t) pos = indexOf(t);
		import std.array;

		if(pos.isNull)
		{
			add(t, false);
			return false;
		}
		else
		{
			_arr[pos.get] = t;
			foreach(I;IxNames)
			{
				mixin(I)._arr = _arr;
				mixin(I).reset(_arr,_arr.length, false);
			}
			return true;
		}
	}

	void swapAt(size_t pos1, size_t pos2)
	{
		if(pos1 == pos2)
			return;
		import std.algorithm : swapAt;
		_arr.swapAt(pos1, pos2);
		rebuild();
	}

	/// Удаляет элемент из списка.
	/// При quick = true производится быстрое удаление, индексы корректируются.
	/// При quick = false производится удаление с сохранением порядка, индексы перестраиваются.
	bool remove(const(T) t, bool quick = true)
	{
		static if(__traits(compiles, t is null))
		{
			if(t is null)
				return false;
		}
		Nullable!size_t pos = mixin(IxNames[0]).indexOf(t);
		if(pos.isNull)
			return false;

		if(quick)
		{
			X p = cast(X)pos.get;
			foreach(I;IxNames)
			{
				mixin(I).indexRemove(p);
				if(p < _arr.length)
					mixin(I).indexRemove(cast(X)(_arr.length-1));
			}
			//_arr = std.algorithm.remove!(SwapStrategy.unstable)(_arr, p);
			_arr[p] = _arr[$-1];
			_arr = _arr[0..$-1];
			foreach(I;IxNames)
			{
				mixin(I)._arr = _arr;
				if(p < _arr.length)
					mixin(I).indexAddNoCheck(p);
			}
		}
		else
		{
			X p = cast(X)pos.get;
			// Вызывает ошибку
			//_arr = std.algorithm.remove!(SwapStrategy.stable)(_arr, pos.get);
			if(pos.get < _arr.length-1)
				for(size_t i = pos.get; i < _arr.length-1; i++)
					_arr[i] = _arr[i+1];
			_arr.length--;

			foreach(I;IxNames)
			{
				mixin(I)._arr = _arr;
				mixin(I).rebuild();
			}
		}
		return true;
	}
	bool removeAt(size_t pos, bool quick = true)
	{
		enforce(pos < this.count);

		if(quick)
		{
			X p = cast(X)pos;
			foreach(I;IxNames)
			{
				mixin(I).indexRemove(p);
				if(p < _arr.length)
					mixin(I).indexRemove(cast(X)(_arr.length-1));
			}
			//_arr = std.algorithm.remove!(SwapStrategy.unstable)(_arr, p);
			_arr[p] = _arr[$-1];
			_arr = _arr[0..$-1];
			foreach(I;IxNames)
			{
				mixin(I)._arr = _arr;
				if(p < _arr.length)
					mixin(I).indexAddNoCheck(p);
			}
		}
		else
		{
			X p = cast(X)pos;
			// Вызывает ошибку
			//_arr = std.algorithm.remove!(SwapStrategy.stable)(_arr, pos.get);
			if(pos < _arr.length-1)
				for(size_t i = pos; i < _arr.length-1; i++)
					_arr[i] = _arr[i+1];
			_arr.length--;

			foreach(I;IxNames)
			{
				mixin(I)._arr = _arr;
				mixin(I).rebuild();
			}
		}
		return true;
	}
	/// []
	inout(T) opIndex(size_t pos) inout { return _arr[pos]; }
	void opIndexAssign(T val, size_t pos)
	{
		if(pos >= _arr.length)
			throw new pulsar.atoms.OutOfRangeException("pos");

		T old = _arr[pos];

		foreach(I;IxNames)
			if(mixin(I).getIndexedValue(old) != mixin(I).getIndexedValue(val)
											&& mixin(I).has(val))
				throw new Exception("Список уже содержит устанавливаемый элемент!");

		foreach(I;IxNames)
			mixin(I).indexRemove(cast(X)pos);

		_arr[pos] = val;
		foreach(I;IxNames)
			mixin(I).indexAddNoCheck(cast(X)pos);
	}
	/// Полностью очищает список
	void clear()
	{
		_arr = _arr[0..0];
		rebuild();
	}
	/// Перестраивает все индексы
	void rebuild()
	{
		foreach(I;IxNames)
			mixin(I).reset(_arr,cast(X)_arr.length, true);
	}

	/// Сортирует список
	void sort(bool function(T,T) cmp)
	{
		static import std.algorithm.sorting;
		std.algorithm.sort!(cmp)(_arr);
		rebuild();
	}
	void sort(bool delegate(T,T) cmp)
	{
		static import std.algorithm.sorting;
		std.algorithm.sort!(cmp)(_arr);
		rebuild();
	}


	int opApply(int delegate(ref T) dg)
	{
		int result = 0;
		for (int i = 0; i < _arr.length; i++)
		{
			result = dg(_arr[i]);
			if (result)
				break;
		}
		return result;
	}
	int opApply(int delegate(ref const(T)) dg) const
	{
		int result = 0;
		for (int i = 0; i < _arr.length; i++)
		{
			result = dg(_arr[i]);
			if (result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, ref T) dg)
	{
		int result = 0;
		for (int i = 0; i < _arr.length; i++)
		{
			result = dg(i,_arr[i]);
			if (result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, ref const(T)) dg) const
	{
		int result = 0;
		for (int i = 0; i < _arr.length; i++)
		{
			result = dg(i,_arr[i]);
			if (result)
				break;
		}
		return result;
	}

	static struct Iter
	{
		LT list;

		int opApply(int delegate(ref const(T)) dg) const
		{
			int result = 0;
			foreach (c; list)
			{
				result = dg(c);
				if (result)
					break;
			}
			return result;
		}
		int opApply(int delegate(size_t, ref const(T)) dg) const
		{
			int result = 0;
			size_t i = 0;
			foreach(c; list)
			{
				result = dg(i,c);
				if (result)
					break;
				i++;
			}
			return result;
		}

	}
	@property Iter iter() const { return Iter(this.unqual); }


	void onDeserialized()
	{
		ixInit();
		foreach(I;IxNames)
			mixin(I).reset(_arr,cast(X)_arr.length, false);
	}
}

version(unittest)
{
	auto aaa(S x) { return x.a; }
	auto bbb(S x) { return x.b; }
	auto xxx(S x) { return x.x; }
}
unittest
{
	auto l0 = new IndexList!(S,ubyte, "a");

	auto l3_1 = new IndexList!(S, ubyte, "ia", (x) => x.a, "ib", bbb, "ix", xxx);
	auto l3_2 = new IndexList!(S, ubyte, "ia", int, "ib", string, "ix", XXX)((x) => x.a, &bbb,&xxx);
	auto l3_3 = new IndexList!(S, ubyte, "a", "bf", "x","x");
	auto l3_4 = new IndexList!(S, ubyte, "ia", (x)=>x.a, "ib", string, "x")(&bbb);


	auto l1 = new IndexList!(S);
	auto l2 = new IndexList!(S, ubyte);
	auto l3 = new IndexList!(S, ubyte, "ix", "a")();
//	auto l3_2 = new IndexList!(S, ubyte, "ix", int)((x)=>x.a);
//	auto l3_3 = new IndexList!(S, ubyte, "ix", "a")();
	auto l4 = new IndexList!(S, "ix", int)((x)=>x.a);
	auto l5 = new IndexList!(S, "ix", int,"ixs", string)((x)=>x.a,
																																																						(x)=> to!string(x.a));
	assert(!l4.ix.has(1));

	auto sa1 = [S(1),S(2),S(8),S(4),S(15)];

	l1 = sa1;
	foreach(i; sa1)
		assert(l1.has(i));
	foreach(i; [S(3), S(7), S(11), S(22)])
		assert(!l1.has(i));

	l4 = sa1;
	foreach(i; [1,2,8,4,15])
		assert(l4.ix.has(i));
	foreach(i; [3, 7, 11, 22])
		assert(!l4.ix.has(i));

	assert(l4.add(S(2)).isNull);
	assert(!l4.add(S(3)).isNull);
	assert(!l4.add(S(5)).isNull);
	assert(!l4.add(S(7)).isNull);

	assert(l4.count == 8);
	foreach(i; [1,2,3,4,5,7,8,15])
		assert(l4.ix.has(i));

	l4.clear();
	foreach(i; [1,2,8,4,15])
		assert(!l4.ix.has(i));

	auto sa = [1,2,8,4,15];
	auto l = new IndexList!int(sa);
	assert(l.remove(1));
	assert(l._arr == [15,2,8,4]);
	foreach(i;[2,8,4,15])
		assert(l.has(i));

	sa = [1,2,8,4,15];
	l = new IndexList!int(sa);
	assert(l.remove(8, false));
	assert(l._arr == [1,2,4,15]);
	foreach(i;[1,2,4,15])
		assert(l.has(i));

	assert(!l.remove(20));
	assert(l[2] == 4);
	l[2] = 3;
	assert(l._arr == [1,2,3,15]);

	l5 = sa1;
	assert(l5.ixs.has("1"));
}
