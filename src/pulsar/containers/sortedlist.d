module pulsar.containers.sortedlist;

import std.conv : to;

/// reverseOrder - хранение по убыанию
mixin template SortedArrayMeths(T, bool reverseOrder)
{
	import std.algorithm: sort, remove, SwapStrategy;
	import std.array: insertInPlace;
	private T[] _arr;

	@property size_t count() const { return _arr.length; }
	@property const(T[]) data() const { return _arr; }

	void opAssign(T[] arr) { _arr = arr; sort!(less)(_arr); }

	const(T) opIndex(size_t pos) const { return _arr[pos]; }

	/// Вычисляет позицию элемента, без сравнения на равенство
	size_t position(const T value) const
	{
		size_t first = 0, count = _arr.length;
		size_t it;
		while (count > 0)
		{
			immutable step = count / 2;
			it = first + step;
			if (less(_arr[it],value)) // Less than value, bump left bound up
			{
				first = it + 1;
				count -= step + 1;
			}
			else if (less(value,_arr[it])) // Greater than value, chop count
				count = step;
			else // Found!!!
				return it;
		}
		if(it >= _arr.length)
			return _arr.length;
		else
			return less(value, _arr[it]) ? it : it + 1;
	}
	/// Вычисляет позицию элемента, проверяя на равенство
	ptrdiff_t indexOf(const T value) const
	{
		size_t it = position(value);
		if(it >= _arr.length)
			return -1;
		return value == _arr[it] ? it : -1;
	}

	bool has(const T value) const { return indexOf(value) > -1; }
	/// true - значение добавлено, false - установлено поверх
	// не используется position чтобы не сравнивать значения
	bool set(const T value)
	{
		size_t first = 0, count = _arr.length;
		size_t it;
		while (count > 0)
		{
			immutable step = count / 2;
			it = first + step;
			if (less(_arr[it],value)) // Less than value, bump left bound up
			{
				first = it + 1;
				count -= step + 1;
			}
			else if (less(value,_arr[it])) // Greater than value, chop count
				count = step;
			else // Found!!!
				break;
		}
		if(_arr.length <= it)
			_arr ~= cast(T)value;
		else if(_arr[it] == cast(T)value)
		{
			_arr[it] = cast(T)value;
			return false;
		}
		else
			_arr.insertInPlace(less(value,_arr[it]) ? it : it + 1, cast(T)value);
		return true;
	}
	/// Удаляет элемент, проверяя на равенство
	bool remove(const T value)
	{
		auto pos = indexOf(value);
		if(pos == -1)
			return false;
		_arr = _arr.remove!(SwapStrategy.stable)(pos);
		return true;
	}

	static bool less(const T t1, const T t2)
	{
		static if(reverseOrder)
			return t2 < t1;
		else
			return t1 < t2;
	}

	int opApply(int delegate(const(T)) dg) const
	{
		int result = 0;
		for(size_t i = 0; i < _arr.length; i++)
		{
			result = dg(_arr[i]);
			if(result)
				break;
		}
		return result;
	}
	int opApply(int delegate(size_t, const(T)) dg) const
	{
		int result = 0;
		for(size_t i = 0; i < _arr.length; i++)
		{
			result = dg(i,_arr[i]);
			if(result)
				break;
		}
		return result;
	}

}
struct SortedArray(T)
{
	mixin SortedArrayMeths!(T, false) SortedArray;

	string toString() { return to!string(_arr); }
	string toString() const { return to!string(_arr); }
}
struct SortedArray(T, bool reverseOrder)
{
	mixin SortedArrayMeths!(T, reverseOrder) SortedArray;

	string toString() { return to!string(_arr); }
	string toString() const { return to!string(_arr); }
}
class SortedList(T)
{
	mixin SortedArrayMeths!(T,false) SortedList;

	this() {}
	this(T[] arr) { opAssign(arr);  }

	override string toString() { return to!string(_arr); }
	string toString() const { return to!string(_arr); }
}
class SortedList(T, bool reverseOrder)
{
	mixin SortedArrayMeths!(T,reverseOrder) SortedList;

	this() {}

	override string toString() { return to!string(_arr); }
	string toString() const { return to!string(_arr); }
}
