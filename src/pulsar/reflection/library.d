module pulsar.reflection.library;

import std.typecons;

import pulsar.containers.index;
import pulsar.reflection.defs;
import pulsar.atoms;

/******************************************************
	* Глобальная библиотека метатипов.	*/
class MetaTypeLibrary
{
	alias LibType = IndexList!(IMetaType,ushort, "typeInfos", "typeInfo",
																																														"names", "fullName",
																																														"ids", "typeId");
static:
private:
	__gshared LibType _lib;
	shared static this()
	{
		init();
	}
	void init()
	{
		if(_lib is null)
			_lib = new LibType(10000); //, &getTypeInfo,&getName, &getTypeId);
	}

public:
	/// Метод поиска замены имен для типов, не присутствующих в библиотеке
	string function(string) replaceTypeName;


	/** Добавляет метатип в библиотеку.
		*	Params:
		*  type = Добавляемый метатип
		*/
	ushort add(IMetaType type)
	{
		import pulsar.server.log;
		//Logger.trace(type.fullName); //typeInfo);
		init();
		Nullable!(size_t) r = _lib.indexOf(type);

		if(r.isNull)
			r = _lib.add(type, false);
		else
			assert(type.fullName == _lib[r.get].fullName);
		return cast(ushort)(r.get+1);
	}
	/// Индексаторы
	IMetaType opIndex(TypeInfo ti)
	{
		if(ti is null)
			return null;
		return _lib.typeInfos.get(ti, null);
	}
	IMetaType opIndex(inout Object obj)
	{
		if(obj is null)
			return null;
		IProxy proxy = cast(IProxy)obj;
		if(proxy !is null)
			return _lib.typeInfos.get(proxy.targetTypeId, null);
		return _lib.typeInfos.get(typeid(obj), null);
	}
	IMetaType opIndex(string typeFullName)
	{
		assert(typeFullName, "typeFullName is null");

		auto mt = _lib.names.get(typeFullName, null);
		if(mt is null && replaceTypeName !is null)
		{
			// Подмена типа
			typeFullName = replaceTypeName(typeFullName);
			if(typeFullName.length > 0)
				mt = _lib.names.get(typeFullName, null);
		}
		return mt;
	}
	IMetaType opIndex(ushort typeId)
	{
		return _lib.ids.get(typeId, null);
	}
	/// Возвращает количество элементов в библиотеке
	@property size_t count()
	{
		if(_lib is null)
			init();
		return _lib.count;
	}
	/// Возвращает итератор по всем зарегистрированным метатипам
	@property MetaTypeLibraryIterator metaTypes()
	{
		return MetaTypeLibraryIterator();
	}
}

/// Структура итератора по библиотеке метатипов
struct MetaTypeLibraryIterator
{
private:
	size_t i = 0;

public:
	@property bool empty() { return i == MetaTypeLibrary._lib.count; }
	@property IMetaType front()
	{
		return i == MetaTypeLibrary._lib.count ? null :
																																												MetaTypeLibrary._lib[i];
	}
	void popFront() { i++; }
}
