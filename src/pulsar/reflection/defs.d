module pulsar.reflection.defs;

import std.typetuple;
import std.traits;

import pulsar.atoms;
import pulsar.atoms.traits;

// Перечисление видов классов информации о типе (метатипов).
enum MetaTypeKind : byte
{
	Unknown    = 0,

	Basic      = 1,
	Enum       = 2,
	String     = 4,
	Primitive  = Basic | Enum /*| String*/,

	Class      = 8,
	Struct     = 16,
	Aggregate  = Class | Struct,

	ValueType  = Basic | Enum | Struct,

	Array      = 32,
	Dictionary = 64,
	Complex    = Class | Struct | Array | Dictionary

	//Interface,
	//Union
}

//***********************************************************
interface IMetaIterator
{
	@property bool empty();
	@property MetaPtr front( );
	void popFront();
}

//***********************************************************

/// Основной интерфейс классов информации о типе (метатипов).
interface IMetaType
{
	/// Номер типа
	@property ushort typeId() const nothrow pure;
	/// TypeInfo типа
	@property TypeInfo typeInfo() const nothrow pure;
	/// Вид метатипа
	@property MetaTypeKind kind() nothrow pure const;

	/// Имя типа
	@property string name() const nothrow pure;
	/// Имя модуля, в котором определен тип
	@property string moduleName() const nothrow pure;
	/// Полное имя типа
	@property string fullName() const nothrow pure;

	///  Определяет, является ли значение указателя значением по умолчанию.
	@property bool isDefaultValue(MetaPtr value) const;
	/// Определяет, является ли значение переменной значением по умолчанию.
	@property bool varHasDefaultValue(const void* variable) const;


	/// Возвращает указатель на значение переменной.
	MetaPtr getVarValuePtr(const void* variable) const;
	/// Устанавливает значение переменной значением указателя.
	void setVarValuePtr(const void* variable, MetaPtr value);

	/// Определяет, может ли значение быть установленным из строки
	@property bool canSetValueFromString() const;
		/// Возвращает значение переменной в виде строки.
	string getVarValueString(const void* variable) const;
	/// Устанавливает значение переменной из строкового представления значения.
	void setVarValueString(const void* variable, string value);
	// TODO : binary
	//	/// Возвращает значение переменной в виде массива байт.
	//	ubyte[] getVarValueBinary(void* variable) const;
	//	/// Устанавливает значение переменной из массива байт.
	//	void setVarValueBinary(void* variable, ubyte[] value);

	/// Возвращает значение указателя (объекта) в виде строки.
	/// Указатель на значение, НЕ на переменную.
	string valueToString(MetaPtr value) const;

	static final IMetaType find(TypeInfo ti)
	{
		import pulsar.reflection.library;
		return MetaTypeLibrary[ti];
	}
	static final IMetaType find(Object obj)
	{
		import pulsar.reflection.library;
		return MetaTypeLibrary[obj];
	}
}

/// Интерфейс метатипов для примитивных типов.
interface IMetaTypePrimitive : IMetaType
{
}

/// Интерфейс метатипов для классов и структур.
interface IMetaTypeAggregate : IMetaType
{
	/// Отображаемое имя типа. Задается атрибутом DisplayName
	@property string displayName() const nothrow pure;
	///Базовый метатип класса
	@property IMetaTypeAggregate baseType() nothrow;
	/// Массив метаатрибутов типа
	@property Ptr[] attributes();
	/// Определяет, задан ли атрибут у типа
	@property bool hasAttribute(TypeInfo attr);
	/// Массив метаполей класса или структуры
	@property MetaTypeField[] fields() const;
	/// Возвращает метаполе по имени. Использует быстрый (бинарный) поиск.
	ref MetaTypeField getField(string name) const;

	/// Создает объект в куче, не вызывая конструктор.
	MetaPtr allocInstance();
	/// Создает объект в куче.
	MetaPtr createInstance();
	/// Определяет, должен ли метатип иметь сервант
	@property bool hasServant() const pure;
	/// Создает сервант объекта в куче, не вызывая конструктор.
	//MetaPtr allocServant(OID oid = OID.init);
	MetaPtr allocServant();
	/// Создает сервант объекта в куче.
	MetaPtr createServant(OID oid = OID.init);
	// не работает
	MetaPtr createServant(CtorArgs...)(OID oid, CtorArgs args);

	/// Враппер метода, вызываемого при сериализации объекта.
	void onSerializing(MetaPtr obj);
	/// Враппер метода, вызываемого после сериализации объекта.
	void onSerialized(MetaPtr obj);
	/// Враппер метода, вызываемого при десериализации объекта.
	void onDeserializing(MetaPtr obj);
	/// Враппер метода, вызываемого после десериализации объекта.
	void onDeserialized(MetaPtr obj);

	/** Возвращает массив метатипов базовых классов,
					начиная с самогообщего (Object не включается).
					Сам тип добавляется последним.
			*/
	final IMetaTypeAggregate[] getBaseClassesMetaTypes()
	{
		import std.algorithm : reverse;
		IMetaTypeAggregate mt = this;
		if(mt.kind == MetaTypeKind.Struct)
			return [ mt ];
		if(mt.kind != MetaTypeKind.Class)
			return null;
		IMetaTypeAggregate[] res = [ mt ];
		while(mt.baseType !is null)
		{
			res ~= mt.baseType;
			mt = mt.baseType;
		}
		reverse(res);
		return res;
	}

	static final IMetaTypeAggregate find(TypeInfo ti)
	{
		import pulsar.reflection.library;
		return cast(IMetaTypeAggregate)MetaTypeLibrary[ti];
	}
	static final IMetaTypeAggregate find(Object obj)
	{
		import pulsar.reflection.library;
		return cast(IMetaTypeAggregate)MetaTypeLibrary[obj];
	}
}

/// Интерфейс метатипов для массивов.
interface IMetaTypeArray : IMetaType
{
	/// Метатип элементов массива
	@property IMetaType elemType() nothrow;

	// Создает массив в куче.
	MetaPtr allocInstance(uint arrLen);

	/// Возвращает количество элементов массива
	uint count(MetaPtr target) const;

	/// Возвращает указатель на значение массива по указанному индексу
	MetaPtr getValuePtrAt(MetaPtr target, uint index);
	void    setValuePtrAt(MetaPtr target, uint index, MetaPtr value);
//	final void setValuePtrAt(MetaPtr target, uint index, void* value)
//	{
//		setValuePtrAt(target, index, MetaPtr(value));
//	}
//	final void setValuePtrAt(MetaPtr target, uint index, Object value)
//	{
//		setValuePtrAt(target, index, MetaPtr(value));
//	}
//	final void setValuePtrAt(MetaPtr target, uint index, void[] value)
//	{
//		setValuePtrAt(target, index, MetaPtr(value));
//	}

	string getValueStringAt(MetaPtr target, uint index);
	void   setValueStringAt(MetaPtr target, uint index, string value);
}

/// Интерфейс метатипов для словарей.
interface IMetaTypeDictionary : IMetaType
{
	/// Метатип ключа словаря
	@property IMetaType keyType() nothrow;
	/// Метатип значения словаря
	@property IMetaType valueType() nothrow;

	// Создает словарь в куче.
	MetaPtr allocInstance();

	/// Возвращает количество пар в словаре
	uint count(MetaPtr target) const;

	/// Определяет, содержит ли словарь запись с указанным ключем
	bool containsKey(MetaPtr target, MetaPtr key);
	/// Возвращает указатель на значение словаря для указанного ключа
	MetaPtr getValuePtrByKey(MetaPtr target, MetaPtr key);
	MetaPtr getValuePtrByKey(MetaPtr target, string key);
	/// Устанавливает значение словаря для указанного ключа
	void    setValuePtrByKey(MetaPtr target, MetaPtr key, MetaPtr value);
	void    setValuePtrByKey(MetaPtr target, string key, MetaPtr value);
	/// Возвращает строку значения словаря для указанного ключа
	string getValueStringByKey(MetaPtr target, MetaPtr key);
	string getValueStringByKey(MetaPtr target, string key);
	/// Устанавливает значение словаря из строки для указанного ключа
	void   setValueStringByKey(MetaPtr target, MetaPtr key, string value);
	void   setValueStringByKey(MetaPtr target, string key, string value);

	/// Удаляет запись с указанным ключем из словаря
	bool removeByKey(MetaPtr target, MetaPtr key);

	alias IterDelegate = void delegate(MetaPtr key, MetaPtr value);
	/// Метод итерации по словарю.
	/// Для каждой пары вызывается метод, указанный через делегат
	void iterate(MetaPtr target, IterDelegate iter);
	/// Возвращает итератор по ключам словаря.
	IMetaIterator newKeysIterator(MetaPtr target);
	/// Возвращает итератор по значениям словаря.
	IMetaIterator newValuesIterator(MetaPtr target);
}

//***********************************************************

/// Структура метауказателя
struct MetaPtr
{
private:
	uint _length;
	IMetaType _type;
	void* _ptr;

public:
	/// Фабрика создания метауказателей
	static MetaPtr of(const(Object) obj)
	{
		static import pulsar.reflection.library;
		IMetaType p = pulsar.reflection.library.MetaTypeLibrary[obj];
		if(p is null)
			throw new Exception("Метатип для типа [" ~ typeid(obj).toString() ~ "] не зарегистрирован!");
		return MetaPtr(p, obj);
	}

public:
	@property const(IMetaType) type()	const { return _type; }
	@property const(void*) ptr()	const { return _ptr; }
	@property uint length()	const { return _length; }

package:
	this(IMetaType type, void* p)
	{
		_type = type;
		_length = 0;
		_ptr = p;
	}
	this(IMetaType type, const(Object) p)
	{
		_type = type;
		_length = 0;
		_ptr = cast(void*)p;
	}
	this(IMetaType type, void[] p)
	{
		assert(type);
		if(p.length > uint.max)
			throw new Exception("MetaPtr не может адресовать массивы с числом элементов более " ~ uint.max.stringof);
		_type = type;
		_length = cast(uint)p.length;
		_ptr = p.ptr;
	}

public:
	@property bool isNull() const
	{
		return _type is null || _ptr is null;
	}

	@property T getValue(T)() inout
	{
		if(isNull)
			return T.init;

		static if(is(T == class))
		{
			auto t = cast(IMetaTypeAggregate)(_type);
			if(t is null)
				throw new Exception("Метатип класса " ~ T.stringof ~ " не IMetaTypeAggregate!");

			while(t !is null)
				if(t == _type)
					return cast(T)(_ptr);
				else
				{
					//trace(t, " - ", t.baseType);
					t = t.baseType;
				}
			throw new Exception("Тип значения MetaPtr " ~ _type.fullName ~
																							" не соответствует типу " ~ T.stringof ~ "!");
		}
		else
		{
			if(typeid(T) != _type.typeInfo)
				throw new Exception("Тип значения MetaPtr " ~ _type.fullName ~
																								" не соответствует типу " ~ T.stringof ~ "!");
			static if(isValueType!T)
				return *cast(T*)(_ptr);
			else static if(isDynamicArray!T)
				return cast(T)(_ptr[0.._length]);
			else static if(isStaticArray!T)
				return *cast(T*)_ptr;
			else static if(isAssociativeArray!T)
			{
				struct AssArray { void* ptr; }
				AssArray a;
				a.ptr = cast(void*)_ptr;
				return cast(T)(*cast(T*)&a);
			}
			else
				static assert(false, "ToDo");
		}

	}
}

version(unittest)
{
	class A
	{
		int a;
		this(int v) { a = v; }
		override string toString() const
		{
			return "A: a = " ~ to!string(a);
		}
	}
	alias regMetaType = pulsar.reflection.metatypes.MetaType!A;
}
unittest
{
	MetaPtr pnull;

	int i = 10;
	MetaPtr p = MetaPtr.of(i);
	*cast(int*)p.ptr = 20;
	assert(i == 20);


	A a = new A(5);
	MetaPtr po = MetaPtr.of(a);
	(cast(A)(po.ptr) ).a = 100;
	assert(a.a == 100);

	int[] iar = [1,2,3];
	MetaPtr par = MetaPtr.of(iar);
	(cast(int*)(par.ptr)[0..par.length])[1] = 22;
	assert(iar == [1,22,3]);
}

//***********************************************************
/// Структура информации о поле
struct MetaTypeField
{
package:
	IMetaTypeAggregate _owner;
	IMetaType _type;
	Ptr[] _attrs;
	string _name;
	uint _offset;

public:
	// Нужен, что бы можно было возвращать ссылку на пустой элемент
	__gshared static @property MetaTypeField Null;

	@property string name() const { return _name; }
	@property uint offset() const { return _offset; }
	@property IMetaType type() { return _type; }
	@property IMetaTypeAggregate owner() { return _owner; }
	@property Ptr[] attributes() { return _attrs; }

package:
	this(IMetaTypeAggregate owner, IMetaType type,
						string name, uint offset, Ptr[] attrs) //MetaAttribute[] attrs)
	{
		assert(owner);
		assert(type);
		assert(name);
		this._owner = owner;
		this._type = type;
		this._name = name;
		this._offset = offset;
		this._attrs = attrs;
	}

public:
	bool hasDefaultValue(MetaPtr target) const
	{
		assert(!target.isNull);
		return _type.varHasDefaultValue(target.ptr + offset);
	}

	MetaPtr getValuePtr (MetaPtr target) const
	{
		assert(!target.isNull);
		return _type.getVarValuePtr(target.ptr + offset);
	}
	void setValuePtr (MetaPtr target, MetaPtr value)
	{
		assert(!target.isNull);
		return _type.setVarValuePtr(target.ptr + offset, value);
	}


	string getValueString(MetaPtr target) const
	{
		assert(!target.isNull);
		return _type.getVarValueString(target.ptr + offset);
	}
	void setValueString(MetaPtr target, string value)
	{
		assert(!target.isNull);
		return _type.setVarValueString(target.ptr + offset, value);
	}

	/// Сравнение по имени поля.
	/// Нужен для сортировки метаполей в массиве полей метатипа.
	int opCmp(ref const MetaTypeField s) const
	{
		if(name == s.name)
			return 0;
		return name < s.name ? -1 : 1;
	}
}



