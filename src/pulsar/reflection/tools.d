module pulsar.reflection.tools;

import std.array;
import std.conv;
import std.traits;
import std.typecons;

import pulsar.atoms;
import pulsar.atoms.traits;
import pulsar.reflection.defs;
import pulsar.reflection.library;

/// Возвращает строку, содержащую имена и значения полей указанного объекта
string printFieldsValues(Object o)
{
	MetaPtr p = MetaPtr.of(o);
	string s = "{ ";
	foreach(mt; (cast(IMetaTypeAggregate)MetaTypeLibrary[o]).getBaseClassesMetaTypes())
		foreach(f; mt.fields)
		{
			//Logger.log(f.name);
			s ~= format("%s=%s ", f.name, f.getValueString(p));
		}
	s ~= "}";
	return s;
}

// TODO : возможно стоит переделать в mixin ?
T convertFromString(T)(string value)
{
	static if(is(T == wchar))
	{
		import std.utf;
		if(value.length == 0)
			return wchar.init;
		if(value.length > 2)
			throw new Exception("Невозможно конвертировать строку \"" ~ value ~ "\" в значение типа wchar!");
		return toUTF16(value)[0];
	}
	else
	return to!T(value);
}

/// Класс заглушки для метатипа.
/// Используется для разрешения циклических зависимостей метатипов
class MetaTypeStub(T, TField, string fieldName) : IMetaTypeAggregate
{
	enum msg = "MetaTypeStub for " ~ TField.stringof;
	alias ThisT = typeof(this);
	static __gshared const IMetaType _ins = new ThisT();

	shared static this()
	{
//		Logger.trace("Open MetaTypeStub!(" ~ TField.stringof ~ ") for field " ~
//															T.stringof ~ "." ~ fieldName  ~ " ...");
		// TODO : По хорошему, надо в случае возврата false передать
		// делегат в MetaTypeLibrary для последующего вызова при
		// регистрации нужного типа.
		if(!correctFieldType())
			throw new Exception("Заглушка метатипа " ~ TField.stringof ~
																							" поля " ~ T.stringof ~ "." ~ fieldName ~
																							" не раскрыта!");
	}

	static ThisT opCall() { return cast(ThisT)_ins; };

	static bool correctFieldType()
	{
		auto t = cast(IMetaTypeAggregate)MetaTypeLibrary[typeid(T)];
		auto tf = cast(IMetaType)MetaTypeLibrary[typeid(TField)];
		if(t is null || tf is null)
			return false;

		foreach(ref f; t.fields)
			if(f.name == fieldName)
			{
				f._type = tf;
				break;
			}
		return true;
	}

	@property ushort typeId() const nothrow pure { assert(false,msg); }
	@property TypeInfo typeInfo() const nothrow pure { assert(false,msg); }
	@property MetaTypeKind kind() nothrow pure const { assert(false,msg); }

	@property string name() const nothrow pure { assert(false,msg); }
	@property string moduleName() const nothrow pure { assert(false,msg); }
	@property string fullName() const nothrow pure { assert(false,msg); }
	@property string displayName() const nothrow pure { assert(false,msg); }

	@property bool isDefaultValue(MetaPtr value) const{ assert(false,msg); }
	@property bool varHasDefaultValue(const void* variable) const{ assert(false,msg); }
	MetaPtr getVarValuePtr(const void* variable) const{ assert(false,msg); }
	void setVarValuePtr(const void* variable, MetaPtr value){ assert(false,msg); }
	@property bool canSetValueFromString() const{ assert(false,msg); }
	string getVarValueString(const void* variable) const{ assert(false,msg); }
	void setVarValueString(const void* variable, string value){ assert(false,msg); }
	string valueToString(MetaPtr value) const{ assert(false,msg); }


	@property IMetaTypeAggregate baseType() nothrow { assert(false,msg); }
	@property Ptr[] attributes() { assert(false,msg); }
	@property bool hasAttribute(TypeInfo attr) { assert(false,msg); }
	@property MetaTypeField[] fields() const { assert(false,msg); }
	ref MetaTypeField getField(string name) const { assert(false,msg); }
	MetaPtr allocInstance() { assert(false,msg); }
	MetaPtr createInstance() { assert(false,msg); }
	@property bool hasServant() const pure { assert(false,msg); }
	MetaPtr allocServant(){ assert(false,msg); }
	MetaPtr createServant(OID oid = OID.init){ assert(false,msg); }

	/// Враппер метода, вызываемого при сериализации объекта.
	void onSerializing(MetaPtr obj) { assert(false,msg); }
	/// Враппер метода, вызываемого после сериализации объекта.
	void onSerialized(MetaPtr obj) { assert(false,msg); }
	/// Враппер метода, вызываемого при десериализации объекта.
	void onDeserializing(MetaPtr obj) { assert(false,msg); }
	/// Враппер метода, вызываемого после десериализации объекта.
	void onDeserialized(MetaPtr obj) { assert(false,msg); }


}

/// Шаблон получения информации о методе
/// Params:
/// T - тип или экземпляр класса
/// name - имя функции
template ReflectMethod(T, string name, ParamsTypes...)
{
	static if(__traits(compiles, __traits(getMember, T, name)) && isSomeFunction!(__traits(getMember, T, name)))
	{
		alias MemberFunctionsTuple!(T, name) meths;

		template FindMeth(size_t index)
		{
			static if(index < meths.length)
			{
				alias ParameterTypeTuple!(meths[index]) pars;
				static if(is(pars == ParamsTypes))
					alias meths[index] FindMeth;
				else
					alias FindMeth!(index+1) FindMeth;
			}
			else
				static assert(false, "Not found method " ~ name ~ ParamsTypes.stringof ~"!");
		}
		alias ReflectFunction!(FindMeth!0) ReflectMethod;
		//alias ReflectFunction!(__traits(getMember, T, name)) ReflectMethod;
	}
	else
		static assert(false, "Not found method " ~ name ~ ParamsTypes.stringof ~"!");
}

/// Шаблон получения информации о методе или функции
/// Params:
/// func - символ метода или функции
template ReflectFunction(alias func)
{
	/// Тип функции или метода
	alias funcType = FunctionTypeOf!(func) ;
	/// Имя функции
	enum name =  __traits(identifier, func);
	/// Имя функции
	enum fullname = fullyQualifiedName!func;;
	/// Определяет. является ли метод конструктором
	enum isCtor = name == "__ctor";
	/// Атрибуты функции или метода
	alias functionAttributes!(func) attributes;
	/// Строка атрибутов функции или метода
	import std.array : join;
	import std.algorithm : filter;
	enum attributesString =
		filter!(a => a != "ref" )([__traits(getFunctionAttributes, func)]).join(" ");

	/// Тип возвращаемого значения
	alias returnType = ReturnType!(func);

	// Это не работает - не может вывести тип при перегрузке
//	static if(__traits(compiles, __traits(parent, returnType)) &&
//											isSomeFunction!(__traits(parent, returnType)))
//	{
//		pragma(msg, fullyQualifiedName!returnType);
//		private enum ts = "auto";
//	}
//	else
		private enum ts = fullyQualifiedName!returnType;

	/// Строка с типом возвращаемого значения
	static if(attributes & FunctionAttribute.ref_)
		enum returnTypeString = "ref " ~ ts;
	else
		enum returnTypeString = ts;

	/// Информация о параметрах
	alias ReflectFunctionParams!func params;
}

/// Шаблон получения информации о параметрах метода или функции
/// Params:
/// func - символ метода или функции
template ReflectFunctionParams(alias func)
{
	static if(isSomeFunction!func)
	{
		/// Кортеж имен параметров
		alias ParameterIdentifierTuple!func   paramsNames;
		/// Кортеж типов параметров
		alias ParameterTypeTuple!func         paramsTypes;
		/// Кортеж классов хранения типов параметров
		alias ParameterStorageClassTuple!func paramsStorageClasses;
		/// Кортеж значений по умолчанию параметров
		alias ParameterDefaultValueTuple!func paramsDefValues;

		template BuildParams(size_t count)
		{
			//pragma(msg, count);
			static if(count == 0)
				enum string BuildParams = "";
			else static if(count == 1)
				enum string BuildParams = BuildParam!(count-1);
			else
				enum string BuildParams = BuildParams!(count-1) ~ ", " ~ BuildParam!(count-1);
		}
		template BuildParam(size_t i)
		{
			enum stor  = text((paramsStorageClasses[i] & ParameterStorageClass.scope_) ? "scope " : "",
																					(paramsStorageClasses[i] & ParameterStorageClass.out_)   ? "out " : "",
																					(paramsStorageClasses[i] & ParameterStorageClass.ref_  ) ? "ref " : "",
																					(paramsStorageClasses[i] & ParameterStorageClass.lazy_ ) ? "lazy " : "");
			alias paramsTypes[i] Ti;
	// Проблема в nested enum типах
//			static if((isBasicType!Ti  || is(Ti == Object) || isSomeString!Ti)
//				enum type = Ti.stringof;
//			else
			enum type = fullyQualifiedName!Ti; //(TypeOf!Ti);
			//pragma(msg, ">>> ", Ti.stringof, " - ", type);
//			static if(is(typeof(paramsDefValues[i]) == enum))
//			{
//				//pragma(msg, "!!!! 1 !!!", typeof(paramsDefValues[i]));
//				enum defVal = paramsDefValues[i].stringof;
//				//pragma(msg, "!!!! 2 !!!", defVal);
//				static if(defVal.startsWith("cast("))
//					enum string def = "cast(" ~ ModuleName!(typeof(paramsDefValues[i])) ~ "." ~ defVal[5..$];
//				else
//				{
//					//enum string def = fullyQualifiedName!(paramsDefValues[i]);
//					// баг в fullyQualifiedName - для DepotZoneType.Storage метода mainZone
//					// возвращает почему-то T.Get!(0LU)
//					enum string fqn = fullyQualifiedName!(paramsDefValues[i]);
//					//pragma(msg, "!!!! 3 !!!", fqn);
//					static if(fqn.startsWith("T.Get!"))
//						enum string def = fullyQualifiedName!(typeof(paramsDefValues[i])) ~ "." ~	tail(defVal, ".");
//					else
//						enum string def = fqn;
//				}
//			}
//			else
				enum string def = paramsDefValues[i].stringof;
			//pragma(msg, paramsDefValues);
			enum BuildParam = text(stor, type, " ", name!(i), def != "void" ? " = " ~ def : "");
		}
		template name(size_t i) {
			enum name = paramsNames[i] is null || paramsNames[i] == "" ? "a" ~ to!string(i) : paramsNames[i];
		}
		template BuildNames(size_t count)
		{
			//pragma(msg, count);
			static if(count == 0)
				enum string BuildNames = "";
			else static if(count == 1)
				enum string BuildNames = name!(count-1);
			else
				enum string BuildNames = BuildNames!(count-1) ~ ", " ~ name!(count-1);
		}

		/// Строка определения параметров метода или функции
		enum string paramsString = BuildParams!(paramsTypes.length);
		/// Строка имен параметров метода или функции (через запятую)
		enum string paramsNamesString = BuildNames!(paramsTypes.length);
	}
}

unittest
{
//	class C1
//	{
//		private int x = 0;
//		@property
//		{
//			int X() { return x; }
//			void X(int x) { this.x = x; }
//		}
//	}
//	alias ReflectMethod!(C1,"X", i) mi;
//	pragma(msg, "(" ~ mi.params.paramsString ~ ")");
}

//----------------

/** Возвращает строку с проксирующим определением всех публичных методов для
	*   указанного типа и его базовых типов.
	* Params:
	* beforeCall: Строка, вставляемая перед выполнением метода.
	* afterCall : Строка, вставляемая после выполнением метода.
	* callPrefix: Если оканчивается точкой, используется как суффикс для вызова,
	*             иначе заменяет сам вызов на указанное значение.
	*/
string getProxyMethodsDefinitions(T,string beforeCallCheck,
																																				string beforeCall,
																																				string afterCallCheck,
																																				string afterCall,
																																				string callPrefix = "super.")()
{
	string ret = "private import " ~ ModuleName!T ~ ";\n";
	foreach(BT; BaseClassesTuple!T)
		if(is(BT == Object) == false)
				ret ~= "private import " ~ ModuleName!BT ~ ";\n";
	static if( __traits(getAliasThis, T).length > 0)
		enum aliasThis = __traits(getAliasThis, T)[0];
	else
		enum aliasThis = "";
	foreach(name; __traits(allMembers, T))
	{
//		static if(T.stringof == "Division")
//			pragma(msg, name, __traits(compiles, __traits(getMember, T, name)));
		static if(name != "__ctor" && name != "__dtor" && name != "Monitor" &&
												name != "factory" && name != "opEquals" && name != "opCmp" &&
												name != "toHash" && name != "toString" &&
												name != "onSerializing" && name != "onSerialized" &&
												name != "onDeserializing" && name != "onDeserialized" &&
												__traits(compiles, __traits(getMember, T, name)) &&
												isSomeFunction!(__traits(getMember, T, name)))
		{
//			static if(T.stringof == "Depot")
//				pragma(msg,name);
			foreach(meth; MemberFunctionsTuple!(T, name))
			{
				//pragma(msg,fullyQualifiedName!meth ~ "  " ~ to!string(IsPublic!meth));
				static if(IsPublic!meth || hasAttributeValue!(meth, proxy))
				{
					static if(name == aliasThis && isSomeFunction!meth)
					{
	//					pragma(msg,"alias this>",name);
	//					alias ReflectFunction!meth mi;
	//					alias returnType = ReturnType!(meth);
	//					alias Unqual!(returnType) type;

	//					ret ~= text("private TransparentProxy!(",fullyQualifiedName!type, ",true) _aliasThis;\n");
	//					ret ~= buildMethodDefinition!(meth)(null, null, "return this._aliasThis;");

					}
					else static if(hasAttributeValue!(meth, noproxy) == false)
						ret ~= buildMethodDefinition!(meth, beforeCallCheck,	beforeCall,
																																										afterCallCheck , afterCall,
																																										callPrefix)();

				}
			}
		}
	}
	return ret;
}
/** Возвращает строку с проксирующим определением для указанного метода.
	* Params:
	* beforeCall: Строка, вставляемая перед выполнением метода.
	* afterCall : Строка, вставляемая после выполнением метода.
	* callPrefix: Если оканчивается точкой, используется как суффикс для вызова,
	*             иначе заменяет сам вызов на указанное значение.
	*/
string buildMethodDefinition(alias meth,string beforeCallCheck,
																																								string beforeCall,
																																								string afterCallCheck,
																																								string afterCall,
																																								string callPrefix = "super.")()
{
	byte[string] imports;
	alias mi = ReflectFunction!meth;
	//pragma(msg, mi.name);
	alias ret = Unqual!(mi.returnType);

	string def = format("override %s %s(%s) %s", mi.returnTypeString, mi.name,
																																														mi.params.paramsString,
																																														mi.attributesString);
	def ~= "\n{\n";

	static if(__traits(isAbstractFunction, meth))
	{
		if(is(ret == void))
			def ~= " return;";
		else
			def ~= text(" return ", ret.stringof, ".init;");
	}
	else
	{
		static if(mixin(beforeCallCheck) && beforeCall !is null)
		{
			auto bc = beforeCall.replace("__funcName__", mi.name).
																								replace("__funcFullName__", mi.fullname);
			def ~= text(" ", bc, "\n");
		}
		static if(mixin(afterCallCheck) && afterCall !is null)
		{
			import std.array: replace;
			auto ac = afterCall.replace("__funcName__", mi.name).
																							replace("__funcFullName__", mi.fullname).
																							replace("__paramsNames__", mi.params.paramsNamesString);
			def ~= text(" scope(success)\n {\n  ",ac, "\n }\n");
		}

		if(callPrefix !is null)
			if(callPrefix[$-1] != '.')
				def ~= text(" ", callPrefix);
			else if(is(mi.returnType == void))
				def ~= text(" ",callPrefix, mi.name, "(", mi.params.paramsNamesString, ");");
			else
				def ~= text(" return ",callPrefix, mi.name, "(", mi.params.paramsNamesString, ");");
	}
	def ~="\n}\n";

	static if(isArray!ret)
	{
		import std.range : ElementType;

		alias ElementType!ret Arr;
		static if(is( Arr == enum ) || isAggregateType!(Arr))
			imports[ModuleName!(Unqual!Arr)] = 0;
	}
	else static if(isAssociativeArray!ret)
	{
		alias KeyType!ret Key;
		static if(is( Key == enum ) || isAggregateType!Key)
			imports[ModuleName!(Unqual!Key)] = 0;
		alias ValueType!ret Value;
		static if(is( Value == enum ) || isAggregateType!Value)
			imports[ModuleName!(Unqual!Value)] = 0;
	}
	else static if(ret.stringof.canFind("!")) //__traits(isTemplate,ret))
	{
		alias templArgs = TemplateArgsOf!ret;
		//pragma(msg, ">>> ", ret, " -> ", templArgs);
		imports[ModuleName!ret] = 0;
		foreach(t; templArgs)
		{
			alias TA = TypeOf!t;
			static if(is(TA == enum) || (isAggregateType!TA && is(TA == Object) == false))
				imports[ModuleName!t] = 0;
		}
	}
	else static if(is(ret == enum) || (isAggregateType!ret && is(ret == Object) == false))
		imports[ModuleName!ret] = 0;


	foreach(pt; mi.params.paramsTypes)
	{
		alias Unqual!(pt) tt;
		static if(isAssociativeArray!tt)
		{
			alias KeyType!tt KT;
			static if(!(isBasicType!KT || is(KT == Object) || isSomeString!KT))
				imports[ModuleName!KT] = 0;
			alias PT = ValueType!tt;
		}
		else static if(isArray!tt)
		{
			import std.range : ElementType;
			alias ElementType!tt PT;
		}
		else static if(tt.stringof.canFind("!"))
		{
			alias templArgs = TemplateArgsOf!tt;
			//pragma(msg, ">>> ", ret, " -> ", templArgs);
			foreach(t; templArgs)
			{
				alias TA = TypeOf!t;
				static if(is(TA == enum) || (isAggregateType!TA && is(TA == Object) == false))
					imports[ModuleName!t] = 0;
			}
			alias tt PT;
		}
		else static if(isDelegate!tt)
		{
			//pragma(msg, ">> ", tt);
			foreach(t; ReflectFunctionParams!tt.paramsTypes)
			{
				//pragma(msg, ">>> ", t);
				alias TA = TypeOf!t;
				static if(is(TA == enum) || (isAggregateType!TA && is(TA == Object) == false))
					imports[ModuleName!t] = 0;
			}
			alias PT = Object;
		}
		else
			alias tt PT;

		static if((is(PT == enum) || (isAggregateType!PT && is(PT == Object) == false)) &&
												__traits(compiles, ModuleName!PT))
			imports[ModuleName!PT] = 0;
	}
	string imps;
	foreach(k,v;imports)
		imps ~= text("private import ", k, ";\n");
	return imps ~ def;
}
