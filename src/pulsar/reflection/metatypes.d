module pulsar.reflection.metatypes;

import std.conv;
import std.traits;
import std.algorithm;
import std.typetuple;
import std.typecons;

import pulsar.atoms;
import pulsar.reflection.defs;
import pulsar.reflection.library;
import pulsar.reflection.tools;
import pulsar.atoms.traits;
import pulsar.atoms.proxies;

import pulsar.gol.attributes;
import pulsar.gol.servant;

extern (C) Object _d_newclass(const TypeInfo_Class ci);

alias MetaObject = MetaType!Object;

/// Шаблон создания метатипа
template MetaType(T)
{
	import std.traits;
	import pulsar.atoms.traits;
	alias UT = DeepUnqual!T;

	//pragma(msg, T , " -> ", UT);

	static if(isPrimitiveType!UT)
		alias MetaType = MetaTypePrimitive!UT;
	else static if(is(UT == class) || is(UT == struct)) //isAggregateType!UT)
		alias MetaType = MetaTypeAggregate!UT;
	else static if(isArray!UT)
		alias MetaType = MetaTypeArray!UT;
	else static if(isAssociativeArray!UT)
		alias MetaType = MetaTypeDictionary!UT;
	else
	{
		//pragma(msg, "Warning! : The type [" ~ UT.stringof ~ "] is not reflectable!");
		//static assert(0,"The type " ~ UT.stringof ~ " is not reflective!");
		alias MetaType = void;
	}
}

// Общая реализация для всех типов
private mixin template MetaTypeBase(T)
{
	alias ThisT = typeof(this);
private:
	static __gshared const ThisT _ins = new ThisT();
	shared static this()
	{
		//trace(fullyQualifiedName!ThisT);
		static if(isAggregateType!T)
		{
			//_fields = GetMetaFieldsArray!(0);
			import std.algorithm;
			sort!((x,y) => x.name < y.name)(_fields);
		}
		id = cast(ushort)(MetaTypeLibrary.count+1);
		id = MetaTypeLibrary.add(cast(IMetaType)_ins);
		//Logger.trace(id, "-", T.stringof);
	}
	this(){}

public:
	static __gshared const ushort id;
	static ThisT opCall() { return cast(ThisT)_ins; };

	@property ushort typeId() const nothrow { return id; }
	@property TypeInfo typeInfo() const nothrow pure { return typeid(T); }
	@property MetaTypeKind kind() const nothrow pure
	{
		static if(isBasicType!T)
			return MetaTypeKind.Basic;
		else static if(isSomeString!T)
			return MetaTypeKind.String;
		else static if(is(T == enum))
			return MetaTypeKind.Enum;
		else static if(is(T == class))
			return MetaTypeKind.Class;
		else static if(is(T == struct))
			return MetaTypeKind.Struct;
		else static if(isArray!T)
			return MetaTypeKind.Array;
		else static if(isAssociativeArray!T)
			return MetaTypeKind.Dictionary;
		else
			static assert(0,"Type " ~ T.stringof ~ " not  reflective !");
	}

	@property string name() const nothrow pure { return T.stringof; }
	@property string fullName() const nothrow pure { return std.traits.fullyQualifiedName!(T); }
	@property string moduleName() const nothrow pure
	{
		static if(__traits(compiles, ModuleName!T))
				return ModuleName!T;
		else
			return null; /*__MODULE__;*/
	}

	@property bool isDefaultValue(MetaPtr value) const
	{
		enforce(value.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		static if(isValueType!T)
			return (*cast(T*)(value.ptr)) == T.init;
		static if(isArray!T)
			return value.ptr is null || value.length == 0;
		else
			return value.ptr is null;
	}
	@property bool varHasDefaultValue(const void* variable) const
	{
		static if(isStaticArray!T)
			return *cast(T*)variable == T.init;
		else static if(isArray!T)
			return *cast(T*)variable is T.init || (*cast(T*)variable).length == 0;
		else
			return *cast(T*)variable is T.init;
	}

	override string toString() const
	{
		return "MT{" ~ fullName ~ "}";
	}

	/// Возвращает значение переменной.
	T getVarValue(const void* variable) const
	{
		CheckNull!variable;
		return *cast(T*)variable;
	}
	/// Устанавливает значение переменной.
	void setVarValue(const void* variable, T value)
	{
		static assert(is(T == DeepUnqual!T));
		CheckNull!variable;
		static if(__traits(compiles, *(cast(T*)variable) = value))
		{
			*(cast(T*)variable) = value;
		}
		else
			throw new Exception("XXX");
	}
	/// Преобразует значение метауказателя к значению типа.
	T castFromPtr(inout MetaPtr value) const
	{
		return value.getValue!T;
	}

	/// Возвращает указатель на значение переменной.
	MetaPtr getVarValuePtr(const void* variable) const
	{
		CheckNull!variable;
		static if(isValueType!T)
			return MetaPtr(cast(IMetaType)this, cast(void*)variable);
		else static if(is(T == class))
			return MetaPtr(cast(IMetaType)this, *cast(T*)variable);
		else static if(isArray!T)
			return MetaPtr(cast(IMetaType)this, cast(void[])(*cast(T*)variable));
		else static if(isAssociativeArray!T)
			return MetaPtr(cast(IMetaType)this, cast(void*)(*cast(T*)variable));
		else
			static assert(false, "ToDo");
	}
	/// Устанавливает значение переменной значением указателя.
	void setVarValuePtr(const void* variable, MetaPtr value)
	{
		CheckNull!variable;
		static if(__traits(compiles, *cast(T*)variable = value.getValue!T()))
		{
			*cast(T*)variable = value.getValue!T();
		}
		else
		{
			// Выражение выше может не компилироваться, ошибки при этом
			// не выдается, но и метатип не создается (что-то в компиляторе)
			// Бывает, например, когда поле структуры константное.
			throw new Exception("XXX - " ~ this.toString());
		}
	}

	@property bool canSetValueFromString() const
	{
		if(_ins.customCanSetValueFromString)
			return _ins.customCanSetValueFromString();

		static if(isPrimitiveType!T)
			return __traits(compiles,to!T(to!string(T.init)));
		else static if(isAggregateType!T)
			return false;
		else static if(isArray!T)
		{
			static if(MetaType!(ElementType)().canSetValueFromString())
				return __traits(compiles,to!T("[]"));
			else
				return false;
		}
		else static if(isAssociativeArray!T)
		{
			static if(MetaType!(TKey)().canSetValueFromString() &&
													MetaType!(TValue)().canSetValueFromString())
				return __traits(compiles,to!T("[]"));
			else
			return false;
		}
		else
			static assert(0,"ToDo");
	}
	/// Возвращает значение переменной в виде строки.
	string getVarValueString(const void* variable) const
	{
		return valueToString(getVarValue(variable));
	}
	/// Устанавливает значение переменной из строкового представления значения.
	void setVarValueString(const void* variable, string value)
	{
		setVarValue(variable, valueFromString(value));
	}

	string valueToString(MetaPtr value) const
	{
		enforce(value.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		T t = value.getValue!T;
		return valueToString(t);
	}
	string valueToString(T value) const
	{
		if(_ins.customValueToString)
			return _ins.customValueToString(value);
		static if(is(T == enum))
		{
			alias OT = OriginalType!T;
			static if(__traits(compiles,to!OT(to!string(value))))
				return to!string(cast(OT)value);
			else
				return to!string(value);
		}
		else
			return to!string(value);
	}
	T valueFromString(string value)
	{
		if(_ins.customValueFromString)
			return _ins.customValueFromString(value);

		static if(is(T == wchar))
		{
			import std.utf;
			if(value.length == 0)
				return wchar.init;
			else
			{
				if(value.length > 2)
					throw new Exception("Невозможно конвертировать строку \"" ~ value ~
																									"\" в значение типа wchar!");
				return toUTF16(value)[0];
			}
		}
		else static if(is(T == enum))
		{
			alias OT = OriginalType!T;
			static if(__traits(compiles,to!OT(value)))
				return cast(T)(to!OT(value));
			else
				return cast(T)(to!T(value));
		}
		else static if(__traits(compiles,to!T(value)))
			return to!T(value);
		else
			throw new Exception("Невозможно конвертировать строку \"" ~ value ~
																								"\" в значение типа " ~ T.stringof ~ "!");
	}
	// TODO
	//	ubyte[] valueToBinary(T value) const
	//	{
	//		assert(false, "ToDo");
	//	}
	//	T valueFromBinary(string value)
	//	{
	//		assert(false, "ToDo");
	//	}

	/// Указатель на собственный метод canSetValueFromString
	bool function() customCanSetValueFromString;
	/// Указатель на собственный метод valueToString
	string function(T) customValueToString;
	/// Указатель на собственный метод valueFromString
	T function(string) customValueFromString;
}

//******************************************************
/// Класс метатипа для примитивов.
class MetaTypePrimitive(T)	: IMetaTypePrimitive
	if(isPrimitiveType!T && is(T == DeepUnqual!T))
{
	mixin MetaTypeBase!T; //(T, "MetaTypePrimitive");
}

//******************************************************

/// Класс метатипа для классов и структур.
class MetaTypeAggregate(T)	: IMetaTypeAggregate
	if(isAggregateType!T && is(T == DeepUnqual!T))
{
	template MakePtrArray(T...)
	{
		static if(T.length == 0)
			enum MakePtrArray = [];
		else
			enum MakePtrArray = [ Ptr(&T[0]) ] ~ MakePtrArray!(T[1..T.length]);
	}
	template GetMetaFieldsArray (size_t i = 0)
	{
		static if (T.tupleof.length == 0 || i >= T.tupleof.length)
			static const GetMetaFieldsArray = [];
		else  static if(hasAttributeValue!(T.tupleof[i], noproxy))
			static const GetMetaFieldsArray = GetMetaFieldsArray!(i + 1);
		else
		{
			alias TField = DeepUnqual!(typeof(T.tupleof[i]));

			// Проверка __traits(compiles нужна для определения
			// циклического замыкания типов
			static if(__traits(compiles, MetaType!TField()))
				alias MTField = MetaType!TField;
			else static if(is(MetaType!TField == void))
				alias MTField = void;
			else
				alias MTField = MetaTypeStub!(T, TField, T.tupleof[i].stringof);

			static if(is(MTField == void))
				static const GetMetaFieldsArray = GetMetaFieldsArray!(i + 1);
			else
			{
				static __gshared auto _fieldAttrs = __traits(getAttributes,T.tupleof[i]);
				static const GetMetaFieldsArray =
					[ MetaTypeField(ThisT(), MTField(),
							T.tupleof[i].stringof, T.tupleof[i].offsetof,
							MakePtrArray!_fieldAttrs) ]	~ GetMetaFieldsArray!(i + 1);
			}
		}
	}

	// Атрибуты для типа
	static __gshared auto _attrsThis = __traits(getAttributes,T);

	// Поля
	static __gshared MetaTypeField[] _fields = GetMetaFieldsArray!(0);

	mixin MetaTypeBase!T; //(T, "MetaTypeAggregate");

	@property IMetaTypeAggregate baseType() nothrow
	{
		static if(is(T == class))
		{
			static if(is(T == Object))
				return null;
			else
			{
				alias BaseClass!T BT;
				return MetaType!(BT)();
			}
		}
		return null;
	}
	@property Ptr[] attributes() { return MakePtrArray!_attrsThis; }
	@property bool hasAttribute(TypeInfo attr)
	{
		foreach(a; _attrsThis)
			if(typeid(a) == attr)
				return true;
		return false;
	}

	@property string displayName() const
	{
		enum udas = getUDAs!(T, DisplayName);
		static if(udas.length > 0)
			return udas[0].name;
		else
			return null;
	}

	@property MetaTypeField[] fields() const { return _fields; }
	ref MetaTypeField getField(string name) const
	{
		if(_fields.length > 0)
		{
			int res = cmp(name, _fields[0].name);
			if(res == 0)
				return _fields[0];
			if (res < 0 || _fields.length == 1)
				//throw new Exception("Поле " ~ name ~ " не найдено!");
				return MetaTypeField.Null;

			res = cmp(name, _fields[_fields.length-1].name);
			if(res == 0)
				return _fields[_fields.length-1];
			if (res > 0 || _fields.length == 2)
				//throw new Exception("Поле " ~ name ~ " не найдено!");
				return MetaTypeField.Null;

			size_t last = _fields.length;
			size_t first = 0;
			size_t mid = last/2; // size_t mid = first + (last - first) / 2;

			while (first < last)
			{
				res = cmp(name, _fields[mid].name);
				if(res == 0)
					return _fields[mid];
				if(res < 0)
					last = mid;
				else
					first = mid + 1;
				mid = first + (last - first)/2;
			}
		}
		return MetaTypeField.Null;
		//throw new Exception("Поле " ~ name ~ " не найдено!");
	}

	MetaPtr allocInstance()
	{
		static if(is(T == class))
			return MetaPtr(cast(IMetaType)this, _d_newclass(typeid(T)));
		else static if(is(T == struct))
			return MetaPtr(cast(IMetaType)this, new T);
		else
			static assert(false, "allocInstance() of "~T.stringof~"not implemented!");
	}
	MetaPtr createInstance()
	{
		static if(is(T == class))
		{
			T t = cast(T)_d_newclass(typeid(T));
			static if(__traits(compiles, t.__ctor()))
					t.__ctor();
			return MetaPtr(cast(IMetaType)this, t);
		}
		else static if(is(T == struct))
			return MetaPtr(cast(IMetaType)this, new T());
		else
			static assert(false, "createInstance() of "~T.stringof~"not implemented!");
	}
	MetaPtr createInstance(CtorArgs...)(CtorArgs args)
	{
		static if(is(T == class))
		{
			T t = cast(T)_d_newclass(typeid(T));
			static if(__traits(compiles, t.__ctor(args)))
					t.__ctor();
			return MetaPtr(cast(IMetaType)this, t);
		}
		else static if(is(T == struct))
			return MetaPtr(cast(IMetaType)this, new T(args));
		else
			static assert(false, "createInstance() of "~T.stringof~"not implemented!");
	}

	enum HasServant = anySatisfy!(IsEssenceOrProxyAttribute,
																															__traits(getAttributes, T));
	@property bool hasServant() const pure
	{
		static if(is(T == class))
			return HasServant;
		else
			return false;
	}
	MetaPtr allocServant()
	{
		static if(HasServant)
		{
			auto o = _d_newclass(cast(TypeInfo_Class)typeid(Servant!T));
			auto s = cast(Servant!T)(o);
			// Нужно, так как конструктор серванта не вызывается.
			if(s.IsOidObj == false)
				s.oid = randomUUID;
			return MetaPtr(cast(IMetaType)this,s);
		}
		else
			return allocInstance();
	}
	MetaPtr createServant(OID golOID = OID.init)
	{
		static if(HasServant)
		{
			auto s = new Servant!T();
			s.oid = golOID == OID.init ? randomUUID : golOID;
			return MetaPtr(cast(IMetaType)this, s);
		}
		else
			return createInstance();
	}
	MetaPtr createServant(CtorArgs...)(OID golOID, CtorArgs args)
	{
		static if(HasServant)
		{
			auto s = new Servant!T(args);
			s.golOID = golOID == OID.init ? randomUUID : golOID;
			return MetaPtr(cast(IMetaType)this, s);
		}
		else
			return createInstance(args);
	}

	void onSerializing(MetaPtr obj)
	{
		static if(HasOwnMethod!(T,"onSerializing"))
			obj.getValue!T.onSerializing();
		}
	void onSerialized(MetaPtr obj)
	{
		static if(HasOwnMethod!(T,"onSerialized"))
			obj.getValue!T.onSerialized();
	}
	void onDeserializing(MetaPtr obj)
	{
		static if(HasOwnMethod!(T,"onDeserializing"))
			obj.getValue!T.onDeserializing();
	}
	void onDeserialized(MetaPtr obj)
	{
		// TODO : добавить обработчик сигнала с выводом ошибки
		// Если на этом месте возникает сигнал SIGSEGV,
		// значит где-то есть циклические ссылки (круговое замыкание)
		static if(HasOwnMethod!(T,"onDeserialized"))
			obj.getValue!T.onDeserialized();
	}
}

//******************************************************

/// Класс метатипа для массивов.
class MetaTypeArray(T)	: IMetaTypeArray
	if(isArray!T && is(T == DeepUnqual!T))
{
	mixin MetaTypeBase!T;

	static if(is(ArrayElementType!T == interface))
		alias ElementType = Object;
	else
		alias ElementType = ArrayElementType!T;
	@property IMetaType elemType() nothrow
	{
		return MetaType!(ElementType)();
	}

	MetaPtr allocInstance(uint arrLen)
	{
		return MetaPtr(cast(IMetaType)this, cast(void[])(new ElementType[arrLen]));
	}

	uint count(MetaPtr target) const
	{
		enforce(target.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		return cast(uint)(target.getValue!T.length);
	}

	MetaPtr getValuePtrAt(MetaPtr target, uint index)
	{
		enforce(target.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		const void* p = target.ptr + index* ElementType.sizeof;
		return elemType.getVarValuePtr(p);
	}
	void  setValuePtrAt(MetaPtr target, uint index, MetaPtr value)
	{
		enforce(target.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		const void* p = target.ptr + index* ElementType.sizeof;
		elemType.setVarValuePtr(p, value);
	}
	string getValueStringAt(MetaPtr target, uint index)
	{
		enforce(target.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		const void* p = target.ptr + index* ElementType.sizeof;
		return elemType.getVarValueString(p);
	}
	void   setValueStringAt(MetaPtr target, uint index, string value)
	{
		enforce(target.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		const void* p = target.ptr + index* ElementType.sizeof;
		return elemType.setVarValueString(p, value);
	}
}

//******************************************************

/// Класс метатипа для ассоциативных массивов.
class MetaTypeDictionary(T) : IMetaTypeDictionary
	if(isAssociativeArray!T && is(T == DeepUnqual!T))
{
	mixin MetaTypeBase!T;

	alias TKey = KeyType!T;
	alias TValue = ValueType!T;

	// TODO : проблема! KeyIterator выдает копию ключей-структур, а не указатель на них.
	// Поэтому итератор iterate выдает отличную ссылку на ключ.
	// Такая же тема была со значениями-структурами, но сейчас исправлена.
	static if(is(TKey == struct) && !is(TKey == UUID))
		pragma(msg, "!!! ВНИМАНИЕ !!! Словарь типа ", T.stringof, " содержит структуры в качестве ключей! Будут проблемы с сериализацией!");

	@property MetaType!(TKey) keyType() nothrow
	{
		return MetaType!(TKey)();
	}
	@property MetaType!(TValue) valueType() nothrow
	{
		return MetaType!(TValue)();
	}

	MetaPtr allocInstance()
	{
		T t;
		try
		{
			t[TKey.init] = TValue.init;
			t.remove(TKey.init);
		}
		catch(Throwable) {}
		return MetaPtr(cast(IMetaType)this, cast(void*)t);
	}

	uint count(MetaPtr target) const
	{
		enforce(target.type.typeId == id, "Тип значения MetaPtr не соответствует типу " ~ fullName);
		if(target.isNull)
			return 0;
		return cast(uint)(castFromPtr(target).length);
	}
	bool containsKey(MetaPtr target, MetaPtr key)
	{
		return (keyType.castFromPtr(key) in castFromPtr(target)) !is null;
	}

	MetaPtr getValuePtrByKey(MetaPtr target, MetaPtr key)
	{
		TValue* v = keyType.castFromPtr(key) in castFromPtr(target);
		return valueType.getVarValuePtr(v);
	}
	MetaPtr getValuePtrByKey(MetaPtr target, string key)
	{
		TKey k;
		keyType.setVarValueString(&k, key);
		TValue* v = k in castFromPtr(target);
		return valueType.getVarValuePtr(v);
	}
	void    setValuePtrByKey(MetaPtr target, MetaPtr key, MetaPtr value)
	{
		T x =	castFromPtr(target);
		x[(keyType.castFromPtr(key))] = (valueType.castFromPtr(value));
	}
	void    setValuePtrByKey(MetaPtr target, string key, MetaPtr value)
	{
		TKey k;
		keyType.setVarValueString(&k, key);
		T x =	castFromPtr(target);
		x[k] = (valueType.castFromPtr(value));
	}

	string getValueStringByKey(MetaPtr target, MetaPtr key)
	{
		TValue* v = keyType.castFromPtr(key) in castFromPtr(target);
		return valueType.getVarValueString(v);
	}
	string getValueStringByKey(MetaPtr target, string key)
	{
		TKey k;
		keyType.setVarValueString(&k, key);
		TValue* v = k in castFromPtr(target);
		return valueType.getVarValueString(v);
	}
	void   setValueStringByKey(MetaPtr target, MetaPtr key, string value)
	{
		TValue v;
		valueType.setVarValueString(&v, value);
		T x =	castFromPtr(target);
		x[keyType.castFromPtr(key)] = v;
	}
	void   setValueStringByKey(MetaPtr target, string key, string value)
	{
		TKey k;
		keyType.setVarValueString(&k, key);
		TValue v;
		valueType.setVarValueString(&v, value);
		T x =	castFromPtr(target);
		x[k] = v;
	}

	bool removeByKey(MetaPtr target, MetaPtr key)
	{
		TKey k;
		keyType.setVarValuePtr(&k, key);
		T x =	castFromPtr(target);
		return x.remove(k);
	}

	void iterate(MetaPtr target, IterDelegate iter)
	{
		CheckNull!iter;
		/* static if(is(TValue == struct))
		{
			pragma(msg, "TValue == struct");
			foreach(TKey k, ref TValue v; castFromPtr(target))
				iter(keyType.getVarValuePtr(&k), valueType.getVarValuePtr(&v));
		}
		else*/
			// ref TValue v вроде как работает для всех (и для значений и для объектов)
			//pragma(msg, "TKey k, TValue v");
			foreach(TKey k, ref TValue v; castFromPtr(target))
				iter(keyType.getVarValuePtr(&k), valueType.getVarValuePtr(&v));
	}
/*	class KeyIterator : IMetaIterator
	{
		alias TSysIter = typeof(T.byKey());
	private:
		TSysIter i;
		TKey current;
		this(TSysIter it)
		{
			i = it;
			if(i.empty == false)
				current = i.front();
		}

	public:
		@property bool empty() { return i.empty; }
		@property MetaPtr front()
		{
			return MetaType!TKey().getVarValuePtr(&current);
		}
		void popFront()
		{
			i.popFront();
			if(i.empty)
				current = TKey.init;
			else
				current = i.front();
		}

	}
	IMetaIterator newKeysIterator(MetaPtr target)
	{
		return new KeyIterator(castFromPtr(target).byKey());
	}

	class ValueIterator : IMetaIterator
	{
		alias TSysIter = typeof(T.byValue());
	private:
		TSysIter i;
		TValue current;
		this(TSysIter it)
		{
			i = it;
			if(i.empty == false)
				current = i.front();
		}

	public:
		@property bool empty() { return i.empty; }
		@property MetaPtr front()
		{
			return MetaType!TValue().getVarValuePtr(&current);
		}
		void popFront()
		{
			i.popFront();
			if(i.empty)
				current = TValue.init;
			else
				current = i.front();
		}

	}
	IMetaIterator newValuesIterator(MetaPtr target)
	{
		return new ValueIterator(castFromPtr(target).byValue());
	}
*/
	import std.range;
	class KeyIterator : IMetaIterator
	{
	private:
		InputRange!TKey i;
		TKey current;
		this(T dic)
		{
			i = dic.byKey().inputRangeObject;
			if(i.empty == false)
				current = i.front();
		}

	public:
		@property bool empty() { return i.empty; }
		@property MetaPtr front()
		{
			return MetaType!TKey().getVarValuePtr(&current);
		}
		void popFront()
		{
			i.popFront();
			if(i.empty)
				current = TKey.init;
			else
				current = i.front();
		}

	}
	IMetaIterator newKeysIterator(MetaPtr target)
	{
		return new KeyIterator(castFromPtr(target));
	}

	class ValueIterator : IMetaIterator
	{
		InputRange!TValue i;
		TValue current;
		this(T dic)
		{
			i = dic.byValue().inputRangeObject;
			if(i.empty == false)
				current = i.front();
		}

	public:
		@property bool empty() { return i.empty; }
		@property MetaPtr front()
		{
			return MetaType!TValue().getVarValuePtr(&current);
		}
		void popFront()
		{
			i.popFront();
			if(i.empty)
				current = TValue.init;
			else
				current = i.front();
		}

	}
	IMetaIterator newValuesIterator(MetaPtr target)
	{
		return new ValueIterator(castFromPtr(target));
	}


}

//----------------------------------------------------------
version(unittest)
{
	class A
	{
		int a;
		this(int v) { a = v; }
		override string toString() const
		{
			return "A: a = " ~ to!string(a);
		}
	}
}
unittest
{
	//-- Primitives ---
	IMetaType mt = MetaType!int();
	IMetaType mto = MetaType!A();
	IMetaTypeArray mta = MetaTypeArray!(byte[])();
	IMetaTypeDictionary mtd = MetaTypeDictionary!(string[int])();

	int i;
	A x;
	byte[] arr;
	string[int] d;

//		assert(mt.isDefaultValue(MetaPtr(&i)));
//		assert(mto.isDefaultValue(MetaPtr(x)));
//		assert(mta.isDefaultValue(MetaPtr(arr)));
//		assert(mtd.isDefaultValue(MetaPtr(cast(void*)d)));

	void* p = &i;
	void* po = &x;
	void* pa = &arr;
	void* pd = &d;
	assert(mt.varHasDefaultValue(p));
	assert(mto.varHasDefaultValue(po));
	assert(mta.varHasDefaultValue(pa));
	assert(mtd.varHasDefaultValue(pd));

	i = 10;
	x = new A(100);
	arr = [1,2,3,4,5];
	d = [ 1:"aaa", 2:"bbb", 3:"ccc" ];
	assert(!mt.varHasDefaultValue(p));
	assert(!mto.varHasDefaultValue(po));
	assert(!mta.varHasDefaultValue(pa));
	assert(!mtd.varHasDefaultValue(pd));

	//----
	MetaPtr tmpi = MetaType!int().getVarValuePtr(&i);
	assertThrown(MetaType!A().setVarValuePtr(&x, tmpi));

	//----
	(cast(MetaTypePrimitive!int)mt).setVarValue(p, 20);
	assert(i == 20);
	i = (cast(MetaTypePrimitive!int)mt).getVarValue(p);
	assert(i == 20);

	A xx = (cast(MetaTypeAggregate!A)mto).getVarValue(po);
	assert(x is xx);
	xx.a = 200;
	assert(x.a == 200);
	(cast(MetaTypeAggregate!A)mto).setVarValue(po, new A(300));
	assert(x !is xx && x.a == 300);

	byte[] arr1 = (cast(MetaTypeArray!(byte[]))mta).getVarValue(pa);
	assert(arr1 is arr);
	arr1[4] = 55;
	assert(arr[4] == 55);
	(cast(MetaTypeArray!(byte[]))mta).setVarValue(pa, [1,2,3,4,5]);
	assert(arr !is arr1 && arr[4] == 5 && arr1[4] == 55);

	string[int] d2 = (cast(MetaTypeDictionary!(string[int]))mtd).getVarValue(pd);
	assert(d is d2);
	d2[1] = "aa";
	assert(d[1] == "aa");
	(cast(MetaTypeDictionary!(string[int]))mtd).setVarValue(pd, [ 1:"AAA", 2:"BBB", 3:"CCC" ]);
	assert(d !is d2 && to!string(d2) == `[3:"ccc", 2:"bbb", 1:"aa"]` &&
																				to!string(d) == `[3:"CCC", 2:"BBB", 1:"AAA"]`);


	//---
	int ii = 30;
	MetaPtr pi = mt.getVarValuePtr(p);
	assert(mt.valueToString(pi) == "20");
	mt.setVarValuePtr(p, MetaPtr.of(ii));
	assert(mt.getVarValueString(p) == "30");
	mt.setVarValueString(p, "40");
	assert(i == 40);

	MetaPtr pio = mto.getVarValuePtr(&x);
	assert(mto.valueToString(pio) == "A: a = 300");
	A x2 = new A(400);
	mto.setVarValuePtr(po, MetaPtr.of(x2));
	assert(mto.getVarValueString(po) == "A: a = 400");
	assertThrown(mto.setVarValueString(po, "A: a = 500"));

	MetaPtr pia = mta.getVarValuePtr(pa);
	assert(mta.valueToString(pia) == "[1, 2, 3, 4, 5]");
	auto arr2 = cast(byte[])[5,4,3];
	mta.setVarValuePtr(pa, MetaPtr.of(arr2));
	assert(mta.getVarValueString(pa) == "[5, 4, 3]");
	if(mta.canSetValueFromString())
	{
		mta.setVarValueString(pa, "[1, 2, 3]");
		assert(mta.getVarValueString(pa) == "[1, 2, 3]");
	}
	else
		assertThrown(mta.setVarValueString(pa, "[1, 2, 3]"));

	pia = mta.getVarValuePtr(pa);
	assert(mta.count(pia) == 3);
	MetaPtr pv = mta.getValuePtrAt(pia, 1);
	IMetaType mtb = MetaTypePrimitive!byte();
	assert(mtb.valueToString(pv) == "2");
	byte b = 22;
	mta.setValuePtrAt(pia,1,MetaPtr.of(b));
	assert(mta.getValueStringAt(pia, 1) == "22");
	mta.setValueStringAt(pia, 2, "33");
	assert(arr == cast(byte[])[1,22,33]);

	MetaPtr pid = mtd.getVarValuePtr(pd);
	assert(mtd.valueToString(pid) == `[3:"CCC", 2:"BBB", 1:"AAA"]`);
	auto dic2 = [1:"aaa", 2:"bbb", 3:"ccc"];
	mtd.setVarValuePtr(pd, MetaPtr.of(dic2));
	assert(mtd.getVarValueString(pd) == `[3:"ccc", 2:"bbb", 1:"aaa"]`);
	if(mtd.canSetValueFromString())
	{
		mtd.setVarValueString(pd, `[10:"Aaa", 20:"Bbb", 30:"Ccc"]`);
		assert(d == [10:"Aaa", 20:"Bbb", 30:"Ccc"]);
	}
	else
		assertThrown(mtd.setVarValueString(pd, `[10:"Aaa", 20:"Bbb", 30:"Ccc"]`));

	IMetaType mts = MetaType!(string)();
	pid = mtd.getVarValuePtr(pd);
	//trace(mtd.getVarValueString(pd));
	ii = 20;
	pi = mt.getVarValuePtr(&ii);
	MetaPtr pVal = mtd.getValuePtrByKey(pid,pi);
	assert(mts.valueToString(pVal) == "Bbb");
	ii = 40;
	pVal = mtd.getValuePtrByKey(pid, pi);
	assert(pVal.isNull);
	string sss = "Ddd";
	mtd.setValuePtrByKey(pid, pi, MetaPtr.of(sss));
	assert(d == [10:"Aaa", 20:"Bbb", 30:"Ccc", 40:"Ddd"]);
	mtd.setValueStringByKey(pid,pi,"dDD");
	assert(mtd.getValueStringByKey(pid,pi) == "dDD");
	mtd.setValueStringByKey(pid,"50","Eee");
	assert(mtd.getValueStringByKey(pid,"50") == "Eee");
	ii = 20;
	mtd.removeByKey(pid,pi);
	assert(d == [10:"Aaa", 30:"Ccc", 40:"dDD", 50:"Eee"]);

	dic2 = [1:"10", 2:"20", 3:"30"];
	mtd.setVarValuePtr(pd, MetaPtr.of(dic2));
	pid = mtd.getVarValuePtr(pd);
	mtd.iterate(pid, (k,v)
	{
		int kk = MetaType!int().castFromPtr(k);
		string vv = MetaType!(string)().castFromPtr(v);
		assert(to!string(kk*10) == vv);
	});

	IMetaIterator iter = mtd.newKeysIterator(pid);
	while(iter.empty == false)
	{
		//Logger.log(mtd.keyType.valueString(iter.front()));
		assert(to!int(mtd.keyType.valueToString(iter.front())) in d);
		iter.popFront();
	}
	iter = mtd.newValuesIterator(pid);
	foreach(it; iter)
	{
		//Logger.log(mtd.valueType.valueString(it));
		assert(mtd.valueType.valueToString(it) in ["10":0, "20":0, "30":0]);
	}
}

version(unittest)
{
	struct NoArg {}
	enum noargs = NoArg.init;
	struct DisplayName
	{
		const string name;
		string toString() const
		{
			return "DispName=" ~ name;
		}
	}
	struct Level
	{
		const uint level = 0;
		string toString() const
		{
			return "Level=" ~ to!string(level);
		}
	}

	@("hello")	@noargs @DisplayName("XxXx")
	class XXX
	{
		@DisplayName("varA") @Level(10)
		int a;

		@(10.11) @DisplayName("varB")
		string b;
	}
}
unittest
{
	version(trace)trace("--- MetaType attributes ... ---");
	MetaTypeAggregate!XXX mt = MetaTypeAggregate!XXX();

	assert(mt.hasAttribute(typeid(string)));
	assert(mt.attributes[0].to!string == "hello");
	assert(mt.hasAttribute(typeid(NoArg)));
	assert(mt.hasAttribute(typeid(DisplayName)));
	assert(mt.attributes[2].to!DisplayName.name == "XxXx");

	assert(mt.fields.length);
	foreach(f; mt.fields)
	{
		version(trace) trace(f);
		assert(f.attributes.length);
		if(f.name == "a")
		{
			assert(f.attributes[0].type == typeid(DisplayName));
			assert(f.attributes[0].to!DisplayName.name == "varA");
			assert(f.attributes[1].type == typeid(Level));
			assert(f.attributes[1].to!Level.level == 10);
		}
		else
		{
			assert(f.attributes[0].type == typeid(double));
			assert(f.attributes[0].to!double == 10.11);
			assert(f.attributes[1].type == typeid(DisplayName));
			assert(f.attributes[1].to!DisplayName.name == "varB");
		}
	}
	version(trace)trace("--- MetaType attributes done ---");
}

