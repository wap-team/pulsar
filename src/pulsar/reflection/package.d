module pulsar.reflection;

public import pulsar.reflection.defs;
public import pulsar.reflection.tools;
public import pulsar.reflection.library;
public import pulsar.reflection.metatypes;
public import pulsar.reflection.attributes;

/// Шаблон создания метатипа
template RegMetaType(T)
{
	static import pulsar.reflection.metatypes;
	alias pulsar.reflection.metatypes.MetaType!T regMetaType;
}
//------------------------------------------------------------------------------------
/// Шаблон регистрации метатипов для всех классов модуля,
/// у которых указаны атрибуты метатипов.
template RegModuleMetaTypes(string moduleName = __MODULE__)
{
	import std.typetuple;
	import pulsar.gol.attributes;
	import std.conv : text;
	import pulsar.reflection : RegMetaType;

	mixin("import modul = " ~ moduleName ~";");
	//pragma(msg, moduleName, " -> ", __traits(allMembers, modul));

	template IsMetaClass(string name)
	{
		static if (mixin("is(" ~ name ~ " == class)") &&
													HasAnyMetaTypeAttribute!(mixin(name)))
			enum IsMetaClass = true;
		else
			enum IsMetaClass = false;
	}
	enum RegMetaStr(string n) = n.length > 0 ? "mixin RegMetaType!" ~ n ~ ";" : "";

	enum xn = TypeTuple!("",Filter!(IsMetaClass, __traits(allMembers, modul)));
	mixin(text(staticMap!(RegMetaStr, xn)));
}





