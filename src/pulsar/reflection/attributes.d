module pulsar.reflection.attributes;

/// Структура атрибута метатипа
struct MetaTypeAttribute {}
enum meta = MetaTypeAttribute.init;

enum IsMetaTypeAttribute(alias T) =		is(typeof(T) : MetaTypeAttribute);
alias HasMetaTypeAttribute(T) =
		anySatisfy!(IsMetaTypeAttribute,	__traits(getAttributes, T));

/// Структура атрибута
struct DisplayName
{
	string name;
}
