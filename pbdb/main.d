module main;

import std.conv;
import std.stdio;
import std.string;
import std.getopt;
import std.uuid;

import pulsar.db.berkeleydb.classes;

//---
void trace(T...)(T args)
{
	writeln!T(args);
	stdout.flush();
}
//---
string cmd;
string base;
string key;
bool keysAsUUID;
bool keysAsUTF8;
bool valueAsUTF8;

int main(string[] args)
{
	try
	{
		auto opts = getopt(args,
				"cmd",  "Command", &cmd,
				"base|b",  "Base file path", &base,
				"key|k", "Key", &key,
				"keysAsUUID|u", "Show the keys as UUID.", &keysAsUUID,
				"keysAsUTF8|s", "Show the keys as UTF-8 text.", &keysAsUTF8,
				"valueAsUTF8|t", "Show the value as UTF-8 text.", &valueAsUTF8,
				);    // enum

		if (opts.helpWanted)
		{
			enum s = "Usage:\r\n" ~
					" pdbm cmd -b [options]\r\n\r\n" ~
					"Commands\r\n:" ~
					" keys - print all keys\r\n" ~
					" get  - print value with key\r\n" ~
					"Options:";
			defaultGetoptPrinter(s,	opts.options);
			return 0;
		}
		if(cmd.length == 0)
			cmd = args.length > 1 ? args[1] : null;
		switch(cmd)
		{
			case "keys" : listkeys(); break;
			case "get" : get(); break;
			default: trace("Unrecognized command ", cmd); break;
		}
	}
	catch(Throwable err)
	{
		trace(err);
	}
	return 0;
}
//-------
void listkeys()
{
	DataBase db = new DataBase();
	db.open(base, DbType.DB_UNKNOWN, DbOpenFlags.DB_RDONLY);
	scope(exit)
		db.close();

	int i = 0;

	foreach(k; db.keys)
		if(keysAsUUID && k.length == 16)
		{
			ubyte[16] oid = k;
			trace(i++, " ", UUID(oid));
		}
		else if(keysAsUTF8)
			trace(i++, " ", cast(string)k);
		else
			trace(i++, " ", to!string(k).replace(" ", ""));
	trace("----------");
	trace("Total: " ~ to!string(i));
}
void get()
{
	DataBase db = new DataBase();
	db.open(base, DbType.DB_UNKNOWN, DbOpenFlags.DB_RDONLY);
	scope(exit)
		db.close();

	if(key.length == 0)
		throw new Exception("Need key");

	ubyte[] kdata;
	if(keysAsUUID && key.length == 36)
		kdata = UUID(key).data;
	else
		kdata = to!(ubyte[])(key);

	ubyte[] val;

	db.get(kdata, val);

	if(valueAsUTF8)
		trace(cast(string)val);
	else
		trace(to!string(val).replace(" ", ""));
}
